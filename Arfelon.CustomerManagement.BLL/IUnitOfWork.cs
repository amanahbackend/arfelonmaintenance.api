﻿using Arfelon.CustomerManagement.BLL.Customers;

namespace Arfelon.CustomerManagement.BLL
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customers { get; }
    }
}
