﻿using Arfelon.CustomerManagement.BLL.Customers;
using Microsoft.EntityFrameworkCore;

namespace Arfelon.CustomerManagement.BLL
{
    public class ArfelonCustomerManagementDbContext : DbContext
    {
        public ArfelonCustomerManagementDbContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Customer> Customer { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
