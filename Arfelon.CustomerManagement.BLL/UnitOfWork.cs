﻿using Arfelon.CustomerManagement.BLL.Customers;

namespace Arfelon.CustomerManagement.BLL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ArfelonCustomerManagementDbContext _context;

        private ICustomerRepository _customersRepos;

        public UnitOfWork(ArfelonCustomerManagementDbContext context)
        {
            _context = context;
        }

        public ICustomerRepository Customers => _customersRepos ?? (_customersRepos = new CustomerRepository(_context));
    }
}
