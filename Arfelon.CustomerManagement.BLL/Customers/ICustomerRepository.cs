﻿using Amanah.Microservices.CommonLib;

namespace Arfelon.CustomerManagement.BLL.Customers
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
