﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Amanah.Microservices.CommonLib;

namespace Arfelon.CustomerManagement.BLL.Customers
{
    [Table("Customer", Schema = "custMgt")]
    public class Customer : BaseEntity
    {
        [StringLength(20)]
        public string Code { get; set; }

        [StringLength(20)]
        public string FirstName { get; set; }

        [StringLength(20)]
        public string SecondName { get; set; }

        [StringLength(20)]
        public string ThirdName { get; set; }

        [StringLength(20)]
        public string FourthName { get; set; }

        [StringLength(15)]
        public string Phone1 { get; set; }

        [StringLength(15)]
        public string Phone2 { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(200)]
        public string Address { get; set; }
    }
}
