﻿using Amanah.Microservices.CommonLib;
using Microsoft.EntityFrameworkCore;

namespace Arfelon.CustomerManagement.BLL.Customers
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DbContext context) : base(context)
        {
        }
    }
}
