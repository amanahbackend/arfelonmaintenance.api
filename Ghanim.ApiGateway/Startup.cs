﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.ApiGateway.API.Middleware;
using Dispatching.ApiGateway.API.Proxy;
using Newtonsoft.Json.Serialization;
using System.Net;
using DnsClient;
using System.Net.Sockets;
using Dispatching.ApiGateway.API.ServiceCommunications.Identity;
using Dispatching.ApiGateway.API.ServicesSettings;
using Microsoft.AspNetCore.Hosting.Internal;
using System.IO;
//using BuildingBlocks.ServiceDiscovery;

namespace Dispatching.ApiGateway.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            _env.WebRootPath = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("authignore.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddSingleton(provider => Configuration);

            services.Configure<IdentityServiceSettings>
                (Configuration.GetSection("ServiceCommunications:IdentityService"));

            services.AddScoped<ProxyService>();
            services.AddScoped<IIdentityUserService, IdentityUserService>();

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            //////////////////// commented the part of ServiceRegisteryPort  ////////////////////////////
            // Dns Query for consul - service discovery
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            /////////////////////////////////////////////////////////////////////////////////

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<AuthorizationMiddleware>();
            app.UseMiddleware<RoutingMiddleware>();
            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}
