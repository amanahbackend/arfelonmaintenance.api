﻿using Dispatching.ApiGateway.API.ServicesSettings;
using Dispatching.ApiGateway.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DnsClient;
using DispatchProduct.Api.HttpClient;
using Utilites.ProcessingResult;
using Microsoft.Extensions.Options;

namespace Dispatching.ApiGateway.API.ServiceCommunications.Identity
{
    public class IdentityUserService :
        DefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>,
        IIdentityUserService
    {
        IdentityServiceSettings _settings;

        public IdentityUserService(IOptions<IdentityServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<bool> IsTokenValid(string version, string token)
        {
            string baseUrl = _settings.Uri;

            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            bool result = false;
            if (!String.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = _settings.IsTokenValidAction;
                var url = $"{baseUrl}/{requestedAction}";
                //var url = $"{baseUrl}/{version}/{requestedAction}";
                var response = await HttpRequestFactory.Get(url, token);
                var responseResult = response.ContentAsType<ProcessResultViewModel<bool>>();
                if (responseResult.IsSucceeded)
                {
                    result = responseResult.Data;
                }
            }
            return result;
        }
        public async Task<bool> IsTokenExpired(string token)
        {
            string baseUrl = _settings.Uri;

            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            bool result = false;
            if (!String.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = _settings.IsTokenExpired;
                var url = $"{baseUrl}/{requestedAction}?token=" + token;
                //var url = $"{baseUrl}/{version}/{requestedAction}";
                var response = await HttpRequestFactory.Get(url);
                var responseResult = response.ContentAsType<ProcessResultViewModel<bool>>();
                if (responseResult.IsSucceeded)
                {
                    result = responseResult.Data;
                }
            }
            return result;
        }
        public async Task<bool> IsUserHavePrivelege(string version, string token, string privelege)
        {
            string baseUrl =_settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            bool result = false;
            if (!String.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = _settings.IsUserHasPrivilegeAction;
                //var url = $"{baseUrl}/{version}/{requestedAction}?privilege={privelege}";
                var url = $"{baseUrl}/{requestedAction}?privilege={privelege}";
                var response = await HttpRequestFactory.Get(url, token);
                var responseResult = response.ContentAsType<ProcessResultViewModel<bool>>();
                if (responseResult.IsSucceeded)
                {
                    result = responseResult.Data;
                }
            }
            return result;
        }
    }
}
