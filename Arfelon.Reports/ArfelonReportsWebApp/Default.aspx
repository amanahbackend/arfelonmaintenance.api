﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReportsWebApp.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Reports Home Page.
        </div>
        <br />
        <div>
            Report Service URL: <asp:Label runat="server" id="lblServiceUrl"></asp:Label>
        </div>
        <br />
        <div>
            <a href="report.aspx?id=0">Print test report.</a></div>
        <%--<div>
            <div>
                <a href="Report.aspx?id=1&customerName=&startDate=&endDate=&serviceTypeId=&jobCardStatusId=&vehiclePlateNo=&vehicleFleetNo=">Job Cards List
                </a>
            </div>
            <div>
                <a href="Report.aspx?id=2&jobCardId=334">Job Card Report</a>
            </div>
        </div>--%>
    </form>
</body>
</html>
