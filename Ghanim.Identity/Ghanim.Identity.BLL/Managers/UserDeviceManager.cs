﻿using Ghanim.Identity.API.Models;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Context;
using Ghanim.Identity.Models.Entities;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.NotificationHubs.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ghanim.Identity.BLL.Managers
{
    public class UserDeviceManager : IUserDeviceManager
    {
        private NotificationHubClient hub;
        private readonly ApplicationDbContext _dbContext;
        public UserDeviceManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            hub = Notifications.Instance.Hub;
        }
        public void Delete(string deviceId)
        {
            var userDeviceRes = _dbContext.UserDevices.FirstOrDefault(x => x.DeveiceId == deviceId);
            if (userDeviceRes!=null)
            {
               _dbContext.UserDevices.Remove(userDeviceRes);
                _dbContext.SaveChanges();
            }
        }
        public void DeleteAzure(string deviceId)
        {
            var userDeviceRes = _dbContext.UserDevices.FirstOrDefault(x => x.DeveiceId == deviceId);
            if (userDeviceRes != null)
            {
                _dbContext.UserDevices.Remove(userDeviceRes);
                _dbContext.SaveChanges();
            }

            DeleteFromAzure(deviceId);
        }
        public void AddIfNotExist(UserDevice userDevice)
        {
            var count = _dbContext.UserDevices.Count(x => x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id);
            if (count > 0)
            {
                var isDeviceExist = _dbContext.UserDevices.Count(x =>
                                        x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id &&
                                        x.DeveiceId == userDevice.DeveiceId) > 0;
                if (!isDeviceExist)
                {
                    _dbContext.UserDevices.Add(userDevice);
                }
            }
            else
            {
                _dbContext.UserDevices.Add(userDevice);
            }
            _dbContext.SaveChanges();
        }
        public async Task AddIfNotExistAzureAsync(UserDevice userDevice)
        {
            var count = _dbContext.UserDevices.Count(x => x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id);
            if (count > 0)
            {
                var isDeviceExist = _dbContext.UserDevices.Count(x =>
                                        x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id &&
                                        x.DeveiceId == userDevice.DeveiceId) > 0;
                if (!isDeviceExist)
                {
                    _dbContext.UserDevices.Add(userDevice);
                }
            }
            else
            {
                _dbContext.UserDevices.Add(userDevice);
            }
            _dbContext.SaveChanges();

            var RegisterationId = await PostAzure(userDevice.DeveiceId);
            DeviceRegistration device = new DeviceRegistration
            {
                Handle = userDevice.DeveiceId,
                Tags = new string[0]
            };
            PutAzure(RegisterationId.ToString(), device, userDevice.Fk_AppUser_Id);
        }

        public List<UserDevice> GetByUserId(string userId)
        {
            var userDevices = _dbContext.UserDevices.Where(x => x.Fk_AppUser_Id == userId).ToList();
            return userDevices;
        }
        public async Task<string> PostAzure(string handle = null)
        {
            string newRegistrationId = null;

            // make sure there are no existing registrations for this push handle (used for iOS and Android)
            if (handle != null)
            {
                var registrations = await hub.GetRegistrationsByChannelAsync(handle, 100);

                foreach (RegistrationDescription registration in registrations)
                {
                    if (newRegistrationId == null)
                    {
                        newRegistrationId = registration.RegistrationId;
                    }
                    else
                    {
                        await hub.DeleteRegistrationAsync(registration);
                    }
                }
            }

            if (newRegistrationId == null)
                newRegistrationId = await hub.CreateRegistrationIdAsync();

            return newRegistrationId;
        }

        // PUT api/register/5
        // This creates or updates a registration (with provided channelURI) at the specified id
        public async void PutAzure(string id, DeviceRegistration deviceUpdate, string username)
        {
            RegistrationDescription registration = null;
           
            registration = new FcmRegistrationDescription(deviceUpdate.Handle);

            registration.RegistrationId = id;

            // add check if user is allowed to add these tags
            registration.Tags = new HashSet<string>(deviceUpdate.Tags);
            registration.Tags.Add("username:" + username);
            
            await hub.CreateOrUpdateRegistrationAsync(registration);
        }

        // DELETE api/register/5
        public async void DeleteFromAzure(string handle)
        {
            var registrations = await hub.GetRegistrationsByChannelAsync(handle, 100);
            foreach (RegistrationDescription registration in registrations)
            {
                await hub.DeleteRegistrationAsync(registration);
            }
        }
    }
}
