﻿using Ghanim.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ghanim.Identity.BLL.IManagers
{
    public interface IPrivilegeManager
    {
        Task<Privilege> Add(Privilege privilege);
        Privilege GetById(int id);
        Task<List<string>> GetPrivelegesByRole(string roleName);
        Task<bool> IsRoleHasPrivilege(string roleName, string privilege);
        Task<bool> IsRolesHasPrivilege(List<string> roles, string privilege);
    }
}
