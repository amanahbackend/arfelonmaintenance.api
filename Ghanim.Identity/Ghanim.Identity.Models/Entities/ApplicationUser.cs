﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.AspNetCore.Identity;

namespace Ghanim.Identity.Models.Entities
{
    public class ApplicationUser : IdentityUser, IIdentityBaseEntity
    {
        public ApplicationUser() : base()
        {

        }
        public ApplicationUser(string username) : base(username)
        {

        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        /// <summary>
        /// Preference Number something like UserId
        /// </summary>
        [StringLength(50)]
        public string PF { get; set; }

        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool Deactivated { get; set; }
        public string PicturePath { get; set; }

        [NotMapped]
        public List<string> RoleNames { get; set; }

        [NotMapped]
        public string Picture { get; set; }
    }
}
