﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Identity.Models.Entities
{
    public class PasswordTokenPin
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Pin { get; set; }
    }
}
