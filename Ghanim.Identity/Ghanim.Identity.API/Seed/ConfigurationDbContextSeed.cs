﻿using Dispatching.Identity.API.Configuration;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Identity.API.Seed
{
    public class ConfigurationDbContextSeed
    {
        public async Task SeedAsync(ConfigurationDbContext context, IConfiguration configuration)
        {
            if (!context.Clients.Any())
            {
                var clients = Config.GetClients();
                foreach (var client in clients)
                {
                    if (client != null)
                    {
                        await context.Clients.AddAsync(client.ToEntity());
                    }
                }
                context.SaveChanges();
            }

            if (!context.IdentityResources.Any())
            {
                var identityResources = Config.GetIdentityResources();
                foreach (var resource in identityResources)
                {
                    await context.IdentityResources.AddAsync(resource.ToEntity());
                }
                await context.SaveChangesAsync();
            }

            if (!context.ApiResources.Any())
            {
                var apis = Config.GetApis();
                foreach (var api in apis)
                {
                    await context.ApiResources.AddAsync(api.ToEntity());
                }
                await context.SaveChangesAsync();
            }
        }

    }
}
