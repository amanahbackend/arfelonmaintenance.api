﻿using Ghanim.Identity.Models.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API
{
    public static class StaticAppSettings
    { 
        public static string ConnectionString { get; set; }

        public static ApplicationDbContext GetDbContext()
        {
            var optsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();

            optsBuilder.UseSqlServer(ConnectionString);

            return new ApplicationDbContext(optsBuilder.Options);
        }
    }
}
