﻿using AutoMapper;
using Ghanim.Identity.API.Utilities;
using Ghanim.Identity.API.ViewModels;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Identity.API.Controllers
{
    //[ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class PrivilegeController : Controller
    {
        private IPrivilegeManager _privilegeManager;
        private IApplicationUserManager _applicationUserManager;
        public PrivilegeController(IPrivilegeManager privilegeManager, IApplicationUserManager applicationUserManager)
        {
            _applicationUserManager = applicationUserManager;
            _privilegeManager = privilegeManager;
        }

        [HttpPost, Route("Add")]
        public async Task<ProcessResultViewModel<PrivilegeViewModel>> Add([FromBody] PrivilegeViewModel model)
        {
            ProcessResultViewModel<PrivilegeViewModel> result = null;
            try
            {
                var entity = Mapper.Map<PrivilegeViewModel, Privilege>(model);
                entity = await _privilegeManager.Add(entity);
                if (entity != null)
                {
                    model = Mapper.Map<Privilege, PrivilegeViewModel>(entity);
                    result = ProcessResultViewModelHelper.Succeeded(model);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<PrivilegeViewModel>(null, "Couldn't add privelege");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<PrivilegeViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetPrivilegesByRole")]
        public async Task<ProcessResultViewModel<List<string>>> GetPrivilegesByRole([FromQuery]string roleName)
        {
            ProcessResultViewModel<List<string>> result = null;
            try
            {
                var privileges = await _privilegeManager.GetPrivelegesByRole(roleName);
                result = ProcessResultViewModelHelper.Succeeded(privileges);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<string>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsUserHasPrivilege")]
        public async Task<ProcessResultViewModel<bool>> IsUserHasPrivilege(string privilege)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                var roles = await TokenManager.GetRolesFromToken(token, _applicationUserManager);
                var isRolesHasPrivilege = await _privilegeManager.IsRolesHasPrivilege(roles, privilege);
                result = ProcessResultViewModelHelper.Succeeded(isRolesHasPrivilege);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }


    }
}
