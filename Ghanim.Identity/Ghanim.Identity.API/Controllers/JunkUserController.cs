﻿using AutoMapper;
using Ghanim.Identity.API.ViewModels;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Identity.API.Controllers
{
    //[ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class JunkUserController : Controller
    {
        private IJunkUserManager _junkUserManager;

        public JunkUserController(IJunkUserManager junkUserManager)
        {
            _junkUserManager = junkUserManager;
        }

        [HttpGet, Route("GetAll")]
        public ProcessResultViewModel<List<JunkUserViewModel>> GetAll()
        {
            ProcessResultViewModel<List<JunkUserViewModel>> result = null;
            try
            {
                var junkUsers = _junkUserManager.GetAll().Data.ToList();
                var junkUsersModel = Mapper.Map<List<JunkUser>, List<JunkUserViewModel>>(junkUsers);
                result = ProcessResultViewModelHelper.Succeeded(junkUsersModel);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<JunkUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete")]
        public ProcessResultViewModel<bool> Delete([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                bool isDeleted = _junkUserManager.DeleteByUsername(username).Data;
                result = ProcessResultViewModelHelper.Succeeded(isDeleted);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }
    }
}
