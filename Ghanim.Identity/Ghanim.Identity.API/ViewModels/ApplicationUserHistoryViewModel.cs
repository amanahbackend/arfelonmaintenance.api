﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Identity.API.ViewModels
{
    public class ApplicationUserHistoryViewModel : RepoistryBaseEntity
    {
        public string UserId { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string Token { get; set; }
        public string UserType { get; set; }
        public string DeveiceId { get; set; }
    }
}
