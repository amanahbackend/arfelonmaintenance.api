﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Identity.API.ViewModels
{
    public class PhoneValidationViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        public string Code { get; set; }
    }
}
