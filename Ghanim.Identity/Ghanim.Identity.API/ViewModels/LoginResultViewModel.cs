﻿using System.Collections.Generic;

namespace Ghanim.Identity.API.ViewModels
{
    public class LoginResultViewModel
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public List<string> Roles { get; set; }
        public string UserId { get; set; }
    }
}
