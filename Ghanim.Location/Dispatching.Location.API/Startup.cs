﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using System.Net;
using DnsClient;
using Swashbuckle.AspNetCore.Swagger;
//using BuildingBlocks.ServiceDiscovery;
using Newtonsoft.Json.Serialization;
using System.Net.Sockets;

namespace Dispatching.Location.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            PACIConfig.proxyUrl = Configuration.GetSection("Location:ProxyUrl").Value;
            PACIConfig.paciServiceUrl = Configuration.GetSection("Location:PACIService:ServiceUrl").Value;
            PACIConfig.paciNumberFieldName = Configuration.GetSection("Location:PACIService:PACIFieldName").Value;
            PACIConfig.nhoodNameFieldName = Configuration.GetSection("Location:BlockService:AreaNameFieldName").Value;
            PACIConfig.blockServiceUrl = Configuration.GetSection("Location:BlockService:ServiceUrl").Value;
            PACIConfig.blockNameFieldNameBlockService = Configuration.GetSection("Location:BlockService:BlockNameFieldName").Value;
            PACIConfig.streetServiceUrl = Configuration.GetSection("Location:StreetService:ServiceUrl").Value;
            PACIConfig.blockNameFieldNameStreetService = Configuration.GetSection("Location:StreetService:BlockNameFieldName").Value;
            PACIConfig.streetNameFieldName = Configuration.GetSection("Location:StreetService:StreetFieldName").Value;


            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            //////////////////// commented the part of ServiceRegisteryPort  ////////////////////////////
            //// Dns Query for consul - service discovery
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            /////////////////////////////////////////////////////////////////////////////////////////

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "location API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                // Assign scope requirements to operations based on AuthorizeAttribute
                // options.OperationFilter<SecurityRequirementsOperationFilter>();
            });


            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseStaticFiles();
            // Autoregister using server.Features (does not work in reverse proxy mode)
            //app.UseConsulRegisterService();

            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "location API V1");
            }); // URL: /swagger

        }
    }
}
