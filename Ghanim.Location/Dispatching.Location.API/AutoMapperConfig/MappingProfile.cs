﻿using AutoMapper;
using Dispatching.Location.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Utilites.PACI.PACIHelper;

namespace Dispatching.Location.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DropPACI, LocationItemViewModel>()
                .ForMember(src => src.Id, opt => opt.MapFrom(dest => dest.Id))
                .ForMember(src => src.Name, opt => opt.MapFrom(dest => dest.Name));


            CreateMap<PACIInfo, LocationViewModel>()
                .ForMember(src => src.Area, opt => opt.MapFrom(dest => dest.Area))
                .ForMember(src => src.Block, opt => opt.MapFrom(dest => dest.Block))
                .ForMember(src => src.Governorate, opt => opt.MapFrom(dest => dest.Governorate))
                .ForMember(src => src.Street, opt => opt.MapFrom(dest => dest.Street))
                .ForMember(src => src.Lat, opt => opt.MapFrom(dest => dest.Latitude))
                .ForMember(src => src.Lng, opt => opt.MapFrom(dest => dest.Longtiude))
                .ForMember(src => src.Number, opt => opt.MapFrom(dest => dest.Number));
        }
    }
}
