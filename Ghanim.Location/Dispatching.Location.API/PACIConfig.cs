﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Location.API
{
    public static class PACIConfig
    {
        public static string proxyUrl { get; set; }
        public static string paciServiceUrl { get; set; }
        public static string paciNumberFieldName { get; set; }
        public static string blockServiceUrl { get; set; }
        public static string blockNameFieldNameBlockService { get; set; }
        public static string nhoodNameFieldName { get; set; }
        public static string streetServiceUrl { get; set; }
        public static string blockNameFieldNameStreetService { get; set; }
        public static string streetNameFieldName { get; set; }
    }
}
