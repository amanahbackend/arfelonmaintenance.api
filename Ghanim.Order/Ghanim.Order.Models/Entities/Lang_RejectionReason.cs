﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class Lang_RejectionReason : BaseEntity, ILang_RejectionReason
    {
        public int FK_RejectionReason_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public RejectionReason RejectionReason { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
