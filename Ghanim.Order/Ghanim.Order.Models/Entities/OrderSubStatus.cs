﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class OrderSubStatus : BaseEntity, IOrderSubStatus
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int StatusId { get; set; }
        public int StatusName { get; set; }
    }
}
