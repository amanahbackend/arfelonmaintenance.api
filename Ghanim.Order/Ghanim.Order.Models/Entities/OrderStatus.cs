﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class OrderStatus : BaseEntity, IOrderStatus
    {
        public string Name { get; set; }
    }
}
