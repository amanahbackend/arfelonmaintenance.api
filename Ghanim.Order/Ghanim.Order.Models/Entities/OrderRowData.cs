﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class OrderRowData :BaseEntity, IOrderRowData
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string PhoneOne { get; set; }
        public string PhoneTwo { get; set; }
        public string ContractNo { get; set; }
        public string ContractType { get; set; }
        public string ContractTypeDescription { get; set; }
        public Nullable<System.DateTime> ContractDate { get; set; }
        public Nullable<System.DateTime> ContactExpiraion { get; set; }
        public string FunctionalLocation { get; set; }
        public string PACI { get; set; }
        public Nullable<int> Governorate { get; set; }
        public Nullable<int> AreaCode { get; set; }
        public string AreaDescription { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Floor { get; set; }
        public Nullable<int> AppartmentNo { get; set; }
        public string AddressNote { get; set; }
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string OrderTypeDescription { get; set; }
        public string CompanyCode { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<int> Division { get; set; }
        public string DivisionDescription { get; set; }
        public Nullable<int> OrderPriority { get; set; }
        public string OrderPriorityDescription { get; set; }
        public string Problem { get; set; }
        public string ProblemDescription { get; set; }
        public string OrderDescription { get; set; }
        public string OrderStatus { get; set; }
        public string OrderNoteAgent { get; set; }
        public string Caller_ID { get; set; }
    }
}
