﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class Notification : BaseEntity, INotification
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
