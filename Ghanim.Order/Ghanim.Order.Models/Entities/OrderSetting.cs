﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class OrderSetting : BaseEntity, IOrderSetting
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
