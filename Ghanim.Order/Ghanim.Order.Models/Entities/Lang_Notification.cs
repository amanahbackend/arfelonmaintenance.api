﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class Lang_Notification : BaseEntity, ILang_Notification
    {
        public int FK_Notification_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        [NotMapped]
        public Notification Notification { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
