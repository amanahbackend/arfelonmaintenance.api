﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface IWorkingType : IBaseEntity
    {
        string Name { get; set; }
        string Category { get; set; }
        int CategoryCode { get; set; }
    }
}
