﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface IOrderRowData
    {
        string CustomerNo { get; set; }
        string CustomerName { get; set; }
        string PhoneOne { get; set; }
        string PhoneTwo { get; set; }
        string ContractNo { get; set; }
        string ContractType { get; set; }
        string ContractTypeDescription { get; set; }
        Nullable<System.DateTime> ContractDate { get; set; }
        Nullable<System.DateTime> ContactExpiraion { get; set; }
        string FunctionalLocation { get; set; }
        string PACI { get; set; }
        Nullable<int> Governorate { get; set; }
        Nullable<int> AreaCode { get; set; }
        string AreaDescription { get; set; }
        string Block { get; set; }
        string Street { get; set; }
        string House { get; set; }
        string Floor { get; set; }
        Nullable<int> AppartmentNo { get; set; }
        string AddressNote { get; set; }
        string OrderNo { get; set; }
        string OrderType { get; set; }
        string OrderTypeDescription { get; set; }
        string CompanyCode { get; set; }
        Nullable<System.DateTime> OrderDate { get; set; }
        Nullable<int> Division { get; set; }
        string DivisionDescription { get; set; }
        Nullable<int> OrderPriority { get; set; }
        string OrderPriorityDescription { get; set; }
        string Problem { get; set; }
        string ProblemDescription { get; set; }
        string OrderStatus { get; set; }
        string OrderNoteAgent { get; set; }
        string Caller_ID { get; set; }
        string OrderDescription { get; set; }
    }
}
