﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface IOrderSetting : IBaseEntity
    {
        string Name { get; set; }
        string Value { get; set; }
    }
}
