﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface IArchivedOrder : IBaseEntity
    {
        int OrderId { get; set; }
        // Order info
        string Code { get; set; }
        int TypeId { get; set; }
        string TypeName { get; set; }
        int StatusId { get; set; }
        string StatusName { get; set; }
        int SubStatusId { get; set; }
        string SubStatusName { get; set; }
        int ProblemId { get; set; }
        string ProblemName { get; set; }
        int PriorityId { get; set; }
        string PriorityName { get; set; }
        int CompanyCodeId { get; set; }
        string CompanyCodeName { get; set; }
        int DivisionId { get; set; }
        string DivisionName { get; set; }
        string ICAgentNote { get; set; }
        string DispatcherNote { get; set; }
        string CancellationReason { get; set; }
        string GeneralNote { get; set; }
        DateTime CreatedDate { get; set; }
        string OrderDescription { get; set; }

        // Customer info
        string CustomerCode { get; set; }
        string CustomerName { get; set; }
        string PhoneOne { get; set; }
        string PhoneTwo { get; set; }
        string Caller_ID { get; set; }
        // Address info
        string PACI { get; set; }
        string FunctionalLocation { get; set; }
        string HouseKasima { get; set; }
        string Floor { get; set; }
        string AppartmentNo { get; set; }
        int StreetId { get; set; }
        string StreetName { get; set; }
        int BlockId { get; set; }
        string BlockName { get; set; }
        int AreaId { get; set; }
        string AreaName { get; set; }
        int GovId { get; set; }
        string GovName { get; set; }
        string AddressNote { get; set; }
        decimal Long { get; set; }
        decimal Lat { get; set; }
        int BuildingTypeId { get; set; }
        string BuildingTypeName { get; set; }
        // Contract info
        string ContractCode { get; set; }
        int ContractTypeId { get; set; }
        string ContractTypeName { get; set; }
        DateTime ContractStartDate { get; set; }
        DateTime ContractExpiryDate { get; set; }
        // Reading info
        DateTime InsertionDate { get; set; }
        string FileName { get; set; }
        // Assigning info
        int SupervisorId { get; set; }
        string SupervisorName { get; set; }
        int DispatcherId { get; set; }
        string DispatcherName { get; set; }
        int TeamId { get; set; }
        AcceptenceType AcceptanceFlag { get; set; }
        int RejectionReasonId { get; set; }
        string RejectionReason { get; set; }
        AccomplishType IsAccomplish { get; set; }
        // Repeated call
        bool IsRepeatedCall { get; set; }
        // Is Exceed time
        bool IsExceedTime { get; set; }
    }
}
