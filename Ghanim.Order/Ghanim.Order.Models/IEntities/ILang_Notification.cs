﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface ILang_Notification : IBaseEntity
    {
        int FK_Notification_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Title { get; set; }
        string Body { get; set; }
    }
}
