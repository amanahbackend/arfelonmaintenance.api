﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface INotification : IBaseEntity
    {
        string Title { get; set; }
        string Body { get; set; }
    }
}
