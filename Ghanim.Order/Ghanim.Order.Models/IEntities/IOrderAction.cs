﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface IOrderAction : IBaseEntity
    {
        int? OrderId { get; set; }
        int? StatusId { get; set; }
        string StatusName { get; set; }
        int? SubStatusId { get; set; }
        string SubStatusName { get; set; }
        DateTime ActionDate { get; set; }
        DateTime ActionDay { get; set; }
        TimeSpan? ActionTime { get; set; }
        int? CreatedUserId { get; set; }
        string CreatedUser { get; set; }
        int? ActionTypeId { get; set; }
        string ActionTypeName { get; set; }
        string CostCenterName { get; set; }
        int? CostCenterId { get; set; }
        int? WorkingTypeId { get; set; }
        string WorkingTypeName { get; set; }
        string Reason { get; set; }
        string ServeyReport { get; set; }

        // assigning 
        double ActionDistance { get; set; }
        //string PF { get; set; }
        int SupervisorId { get; set; }
        string SupervisorName { get; set; }
        int DispatcherId { get; set; }
        string DispatcherName { get; set; }
        int ForemanId { get; set; }
        string ForemanName { get; set; }
        int TeamId { get; set; }
        //int DriverId { get; set; }
        //string DriverName { get; set; }
        //string DriverPFNumber { get; set; }
        
    }
}
