﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
    public interface IOrderProblem : IBaseEntity
    {
        string Name { get; set; }
        string Code { get; set; }
        int ExceedHours { get; set; }
    }
}
