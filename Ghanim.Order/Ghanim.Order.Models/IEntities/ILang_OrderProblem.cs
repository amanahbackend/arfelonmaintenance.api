﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
   public interface ILang_OrderProblem : IBaseEntity
    {
        int FK_OrderProblem_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Name { get; set; }
    }
}
