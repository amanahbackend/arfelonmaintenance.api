﻿// <auto-generated />
using System;
using Ghanim.Order.EFCore.MSSQL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    [DbContext(typeof(OrderDBContext))]
    [Migration("20181231133118_add-action-time-property-to-order-action")]
    partial class addactiontimepropertytoorderaction
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Ghanim.Order.Models.Entities.ArchivedOrder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AcceptanceFlag");

                    b.Property<string>("AddressNote");

                    b.Property<string>("AppartmentNo");

                    b.Property<int>("AreaId");

                    b.Property<string>("AreaName");

                    b.Property<int>("BlockId");

                    b.Property<string>("BlockName");

                    b.Property<int>("BuildingTypeId");

                    b.Property<string>("BuildingTypeName");

                    b.Property<string>("CancellationReason");

                    b.Property<string>("Code");

                    b.Property<int>("CompanyCodeId");

                    b.Property<string>("CompanyCodeName");

                    b.Property<string>("ContractCode");

                    b.Property<DateTime>("ContractExpiryDate");

                    b.Property<DateTime>("ContractStartDate");

                    b.Property<int>("ContractTypeId");

                    b.Property<string>("ContractTypeName");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("CurrentUserId");

                    b.Property<string>("CustomerCode");

                    b.Property<string>("CustomerName");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<int>("DispatcherId");

                    b.Property<string>("DispatcherName");

                    b.Property<string>("DispatcherNote");

                    b.Property<int>("DivisionId");

                    b.Property<string>("DivisionName");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<string>("FileName");

                    b.Property<string>("Floor");

                    b.Property<string>("FunctionalLocation");

                    b.Property<string>("GeneralNote");

                    b.Property<int>("GovId");

                    b.Property<string>("GovName");

                    b.Property<string>("HouseKasima");

                    b.Property<string>("ICAgentNote");

                    b.Property<DateTime>("InsertionDate");

                    b.Property<int>("IsAccomplish");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsExceedTime");

                    b.Property<bool>("IsRepeatedCall");

                    b.Property<decimal>("Lat");

                    b.Property<decimal>("Long");

                    b.Property<int>("OrderId");

                    b.Property<string>("PACI");

                    b.Property<string>("PhoneOne");

                    b.Property<string>("PhoneTwo");

                    b.Property<int>("PriorityId");

                    b.Property<string>("PriorityName");

                    b.Property<int>("ProblemId");

                    b.Property<string>("ProblemName");

                    b.Property<string>("RejectionReason");

                    b.Property<int>("RejectionReasonId");

                    b.Property<int>("StatusId");

                    b.Property<string>("StatusName");

                    b.Property<int>("StreetId");

                    b.Property<string>("StreetName");

                    b.Property<int>("SubStatusId");

                    b.Property<string>("SubStatusName");

                    b.Property<int>("SupervisorId");

                    b.Property<string>("SupervisorName");

                    b.Property<int>("TeamId");

                    b.Property<int>("TypeId");

                    b.Property<string>("TypeName");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("ArchivedOrders");
                });

            modelBuilder.Entity("Ghanim.Order.Models.Entities.OrderAction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("ActionDate");

                    b.Property<double>("ActionDistance");

                    b.Property<TimeSpan?>("ActionTime");

                    b.Property<int?>("ActionTypeId");

                    b.Property<string>("ActionTypeName");

                    b.Property<int?>("CostCenterId");

                    b.Property<string>("CostCenterName");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("CreatedUser");

                    b.Property<int?>("CreatedUserId");

                    b.Property<string>("CurrentUserId");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<int>("DispatcherId");

                    b.Property<string>("DispatcherName");

                    b.Property<int>("DriverId");

                    b.Property<string>("DriverName");

                    b.Property<string>("DriverPFNumber");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<int>("ForemanId");

                    b.Property<string>("ForemanName");

                    b.Property<bool>("IsDeleted");

                    b.Property<int?>("OrderId");

                    b.Property<int?>("StatusId");

                    b.Property<string>("StatusName");

                    b.Property<int?>("SubStatusId");

                    b.Property<string>("SubStatusName");

                    b.Property<int>("SupervisorId");

                    b.Property<string>("SupervisorName");

                    b.Property<int>("TeamId");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<int?>("WorkingTypeId");

                    b.Property<string>("WorkingTypeName");

                    b.HasKey("Id");

                    b.ToTable("OrderActions");
                });

            modelBuilder.Entity("Ghanim.Order.Models.Entities.OrderObject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AcceptanceFlag");

                    b.Property<string>("AddressNote");

                    b.Property<string>("AppartmentNo");

                    b.Property<int>("AreaId");

                    b.Property<string>("AreaName");

                    b.Property<int>("BlockId");

                    b.Property<string>("BlockName");

                    b.Property<int>("BuildingTypeId");

                    b.Property<string>("BuildingTypeName");

                    b.Property<string>("CancellationReason");

                    b.Property<string>("Code");

                    b.Property<int>("CompanyCodeId");

                    b.Property<string>("CompanyCodeName");

                    b.Property<string>("ContractCode");

                    b.Property<DateTime>("ContractExpiryDate");

                    b.Property<DateTime>("ContractStartDate");

                    b.Property<int>("ContractTypeId");

                    b.Property<string>("ContractTypeName");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("CurrentUserId");

                    b.Property<string>("CustomerCode");

                    b.Property<string>("CustomerName");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<int>("DispatcherId");

                    b.Property<string>("DispatcherName");

                    b.Property<string>("DispatcherNote");

                    b.Property<int>("DivisionId");

                    b.Property<string>("DivisionName");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<string>("FileName");

                    b.Property<string>("Floor");

                    b.Property<string>("FunctionalLocation");

                    b.Property<string>("GeneralNote");

                    b.Property<int>("GovId");

                    b.Property<string>("GovName");

                    b.Property<string>("HouseKasima");

                    b.Property<string>("ICAgentNote");

                    b.Property<DateTime>("InsertionDate");

                    b.Property<int>("IsAccomplish");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsExceedTime");

                    b.Property<bool>("IsRepeatedCall");

                    b.Property<decimal>("Lat");

                    b.Property<decimal>("Long");

                    b.Property<string>("PACI");

                    b.Property<string>("PhoneOne");

                    b.Property<string>("PhoneTwo");

                    b.Property<int>("PriorityId");

                    b.Property<string>("PriorityName");

                    b.Property<int>("ProblemId");

                    b.Property<string>("ProblemName");

                    b.Property<int?>("RankInDispatcher");

                    b.Property<int?>("RankInTeam");

                    b.Property<string>("RejectionReason");

                    b.Property<int>("RejectionReasonId");

                    b.Property<int>("StatusId");

                    b.Property<string>("StatusName");

                    b.Property<int>("StreetId");

                    b.Property<string>("StreetName");

                    b.Property<int>("SubStatusId");

                    b.Property<string>("SubStatusName");

                    b.Property<int>("SupervisorId");

                    b.Property<string>("SupervisorName");

                    b.Property<int>("TeamId");

                    b.Property<int>("TypeId");

                    b.Property<string>("TypeName");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("Ghanim.Order.Models.Entities.OrderSetting", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("CurrentUserId");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.ToTable("Settings");
                });
#pragma warning restore 612, 618
        }
    }
}
