﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class makedatastringinnotificationCenter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Data",
                table: "NotificationCenter",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Data",
                table: "NotificationCenter",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
