﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addSAPlocationdatatocurrentorder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SAPAppartmentNo",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPAreaName",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPBlockName",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPFloor",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPGovName",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPHouseKasima",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPPACI",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAPStreetName",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SAPAppartmentNo",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPAreaName",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPBlockName",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPFloor",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPGovName",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPHouseKasima",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPPACI",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SAPStreetName",
                table: "Orders");
        }
    }
}
