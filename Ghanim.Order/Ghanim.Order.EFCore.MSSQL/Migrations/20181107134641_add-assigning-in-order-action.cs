﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addassigninginorderaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "ActionDistance",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "DispatcherId",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DispatcherName",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DriverName",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverPFNumber",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ForemanId",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ForemanName",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SupervisorId",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SupervisorName",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionDistance",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DispatcherId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DispatcherName",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DriverName",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DriverPFNumber",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "ForemanId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "ForemanName",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "SupervisorName",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "OrderActions");
        }
    }
}
