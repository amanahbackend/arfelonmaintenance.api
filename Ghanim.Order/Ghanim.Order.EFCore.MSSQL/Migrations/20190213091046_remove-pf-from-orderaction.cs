﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class removepffromorderaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PF",
                table: "OrderActions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "OrderActions",
                nullable: true);
        }
    }
}
