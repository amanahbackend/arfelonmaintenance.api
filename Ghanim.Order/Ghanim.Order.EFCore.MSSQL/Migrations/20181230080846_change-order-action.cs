﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class changeorderaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "StatusId",
                table: "OrderActions",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "OrderId",
                table: "OrderActions",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "WorkingTypeId",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkingTypeName",
                table: "OrderActions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WorkingTypeId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "WorkingTypeName",
                table: "OrderActions");

            migrationBuilder.AlterColumn<int>(
                name: "StatusId",
                table: "OrderActions",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrderId",
                table: "OrderActions",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
