﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class fixnamingofSAPlocationdatatocurrentorder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SAPStreetName",
                table: "Orders",
                newName: "SAP_StreetName");

            migrationBuilder.RenameColumn(
                name: "SAPPACI",
                table: "Orders",
                newName: "SAP_PACI");

            migrationBuilder.RenameColumn(
                name: "SAPHouseKasima",
                table: "Orders",
                newName: "SAP_HouseKasima");

            migrationBuilder.RenameColumn(
                name: "SAPGovName",
                table: "Orders",
                newName: "SAP_GovName");

            migrationBuilder.RenameColumn(
                name: "SAPFloor",
                table: "Orders",
                newName: "SAP_Floor");

            migrationBuilder.RenameColumn(
                name: "SAPBlockName",
                table: "Orders",
                newName: "SAP_BlockName");

            migrationBuilder.RenameColumn(
                name: "SAPAreaName",
                table: "Orders",
                newName: "SAP_AreaName");

            migrationBuilder.RenameColumn(
                name: "SAPAppartmentNo",
                table: "Orders",
                newName: "SAP_AppartmentNo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SAP_StreetName",
                table: "Orders",
                newName: "SAPStreetName");

            migrationBuilder.RenameColumn(
                name: "SAP_PACI",
                table: "Orders",
                newName: "SAPPACI");

            migrationBuilder.RenameColumn(
                name: "SAP_HouseKasima",
                table: "Orders",
                newName: "SAPHouseKasima");

            migrationBuilder.RenameColumn(
                name: "SAP_GovName",
                table: "Orders",
                newName: "SAPGovName");

            migrationBuilder.RenameColumn(
                name: "SAP_Floor",
                table: "Orders",
                newName: "SAPFloor");

            migrationBuilder.RenameColumn(
                name: "SAP_BlockName",
                table: "Orders",
                newName: "SAPBlockName");

            migrationBuilder.RenameColumn(
                name: "SAP_AreaName",
                table: "Orders",
                newName: "SAPAreaName");

            migrationBuilder.RenameColumn(
                name: "SAP_AppartmentNo",
                table: "Orders",
                newName: "SAPAppartmentNo");
        }
    }
}
