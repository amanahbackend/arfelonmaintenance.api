﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addserveyreporttoorderandorderAction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Notification",
                table: "Notification");

            migrationBuilder.RenameTable(
                name: "Notification",
                newName: "Notifications");

            migrationBuilder.AddColumn<string>(
                name: "ServeyReport",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ServeyReport",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notifications",
                table: "Notifications",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Notifications",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "ServeyReport",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ServeyReport",
                table: "OrderActions");

            migrationBuilder.RenameTable(
                name: "Notifications",
                newName: "Notification");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notification",
                table: "Notification",
                column: "Id");
        }
    }
}
