﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addcaller_IdandorderDescriptiontoorder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Caller_ID",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrderDescription",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Caller_ID",
                table: "ArchivedOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrderDescription",
                table: "ArchivedOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Caller_ID",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderDescription",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Caller_ID",
                table: "ArchivedOrders");

            migrationBuilder.DropColumn(
                name: "OrderDescription",
                table: "ArchivedOrders");
        }
    }
}
