﻿using Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.Context
{
    public class OrderDBContext : DbContext
    {
        public OrderDBContext(DbContextOptions<OrderDBContext> options) : base(options)
        {
        }
        public DbSet<OrderObject> Orders { get; set; }
        public DbSet<OrderSetting> Settings { get; set; }
        public DbSet<ArchivedOrder> ArchivedOrders { get; set; }
        public DbSet<OrderAction> OrderActions { get; set; }
        public DbSet<NotificationCenter> NotificationCenter { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Lang_OrderStatus> Lang_OrderStatuses { get; set; }
        public DbSet<OrderSubStatus> OrderSubStatuses { get; set; }
        public DbSet<Lang_OrderSubStatus> Lang_OrderSubStatuses { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<Lang_OrderType> Lang_OrderTypes { get; set; }
        public DbSet<OrderPriority> OrderPriorities { get; set; }
        public DbSet<Lang_OrderPriority> Lang_OrderPriorities { get; set; }
        public DbSet<OrderProblem> OrderProblems { get; set; }
        public DbSet<Lang_OrderProblem> Lang_OrderProblems { get; set; }
        public DbSet<RejectionReason> RejectionReasons { get; set; }
        public DbSet<Lang_RejectionReason> Lang_RejectionReasons { get; set; }
        public DbSet<WorkingType> WorkingTypes { get; set; }
        public DbSet<OrderRowData> OrderRowData { get; set; }
        public DbSet<Lang_WorkingType> Lang_WorkingTypes { get; set; }
        public DbSet<Lang_Notification> Lang_Notifications { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderSettingConfiguration());
            modelBuilder.ApplyConfiguration(new ArchivedOrderConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationCenterConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationConfiguration());
            modelBuilder.ApplyConfiguration(new OrderActionConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderProblemConfiguration());
            modelBuilder.ApplyConfiguration(new OrderProblemConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderStatusConfiguraion());
            modelBuilder.ApplyConfiguration(new OrderStatusConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderSubStatusConfiguration());
            modelBuilder.ApplyConfiguration(new OrderSubStatusConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderPriorityConfiguration());
            modelBuilder.ApplyConfiguration(new OrderPriorityConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RejectionReasonConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_RejectionReasonConfiguration());
            modelBuilder.ApplyConfiguration(new OrderRowDataConfiguration());
            modelBuilder.ApplyConfiguration(new WorkingTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_WorkingTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_NotificationConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
