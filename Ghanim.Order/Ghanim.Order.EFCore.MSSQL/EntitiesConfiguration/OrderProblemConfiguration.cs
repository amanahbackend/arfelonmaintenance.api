﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    class OrderProblemConfiguration : BaseEntityTypeConfiguration<OrderProblem>, IEntityTypeConfiguration<OrderProblem>
    {
        public override void Configure(EntityTypeBuilder<OrderProblem> builder)
        {
            base.Configure(builder);
            builder.ToTable("OrderProblem");
        }
    }
}
