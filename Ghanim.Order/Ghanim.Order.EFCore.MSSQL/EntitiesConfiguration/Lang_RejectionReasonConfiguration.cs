﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_RejectionReasonConfiguration : BaseEntityTypeConfiguration<Lang_RejectionReason>, IEntityTypeConfiguration<Lang_RejectionReason>
    {
        public override void Configure(EntityTypeBuilder<Lang_RejectionReason> builder)
        {
            builder.ToTable("Lang_RejectionReason");
            builder.Property(o => o.Id).ValueGeneratedOnAdd();
            builder.Property(o => o.Name).IsRequired();
            base.Configure(builder);
        }
    }
}