﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class RejectionReasonConfiguration : BaseEntityTypeConfiguration<RejectionReason>, IEntityTypeConfiguration<RejectionReason>
    {
        public override void Configure(EntityTypeBuilder<RejectionReason> builder)
        {
            builder.ToTable("RejectionReason");
            builder.Property(o => o.Id).ValueGeneratedOnAdd();
            builder.Property(o => o.Name).IsRequired();
            base.Configure(builder);
        }
    }
}