﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_OrderProblemConfiguration : BaseEntityTypeConfiguration<Lang_OrderProblem>, IEntityTypeConfiguration<Lang_OrderProblem>
    {
        public override void Configure(EntityTypeBuilder<Lang_OrderProblem> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_OrderProblem");
        }
    }
}