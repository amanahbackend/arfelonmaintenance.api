﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    class Lang_NotificationConfiguration : BaseEntityTypeConfiguration<Lang_Notification>, IEntityTypeConfiguration<Lang_Notification>
    {
        public override void Configure(EntityTypeBuilder<Lang_Notification> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_Notification");
        }
    }
}
