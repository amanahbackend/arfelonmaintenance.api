﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderTypeConfiguration : BaseEntityTypeConfiguration<OrderType>, IEntityTypeConfiguration<OrderType>
    {
        public override void Configure(EntityTypeBuilder<OrderType> builder)
        {
            base.Configure(builder);
            builder.ToTable("OrderType");
        }
    }
}