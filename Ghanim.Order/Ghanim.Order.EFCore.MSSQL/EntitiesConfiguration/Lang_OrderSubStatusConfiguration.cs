﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_OrderSubStatusConfiguration : BaseEntityTypeConfiguration<Lang_OrderSubStatus>, IEntityTypeConfiguration<Lang_OrderSubStatus>
    {
        public override void Configure(EntityTypeBuilder<Lang_OrderSubStatus> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_OrderSubStatus");
        }
    }
}