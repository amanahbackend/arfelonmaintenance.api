﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderRowDataConfiguration : BaseEntityTypeConfiguration<OrderRowData>, IEntityTypeConfiguration<OrderRowData>
    {
        public override void Configure(EntityTypeBuilder<OrderRowData> builder)
        {
            base.Configure(builder);
            builder.ToTable("OrderRowData");
        }
    }
}