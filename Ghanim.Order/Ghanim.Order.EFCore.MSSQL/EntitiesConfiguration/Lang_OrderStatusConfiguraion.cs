﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_OrderStatusConfiguraion : BaseEntityTypeConfiguration<Lang_OrderStatus>, IEntityTypeConfiguration<Lang_OrderStatus>
    {
        public override void Configure(EntityTypeBuilder<Lang_OrderStatus> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_OrderStatus");
        }
    }
}