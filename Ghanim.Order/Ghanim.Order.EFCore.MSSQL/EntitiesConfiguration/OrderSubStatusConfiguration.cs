﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderSubStatusConfiguration : BaseEntityTypeConfiguration<OrderSubStatus>, IEntityTypeConfiguration<OrderSubStatus>
    {
        public override void Configure(EntityTypeBuilder<OrderSubStatus> builder)
        {
            base.Configure(builder);
            builder.ToTable("OrderSubStatus");
        }
    }
}
