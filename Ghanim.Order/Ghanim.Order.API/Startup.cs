﻿using AutoMapper;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.AutoMapperConfig;
using Ghanim.Order.API.Helper;
using Ghanim.Order.API.Schedulers;
using Ghanim.Order.API.Schedulers.Scheduling;
using Ghanim.Order.API.ServiceCommunications.DataManagementService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.BuildingTypesService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.ContractTypesService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.DivisionService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.GovernoratesService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.Problem;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.Order.API.ServiceCommunications.Notification;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.Order.BLL.ExcelSettings;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.BLL.Managers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.Models.IEntities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Service communication section
            services.Configure<DataManagementServiceSettings>(Configuration.GetSection("ServiceCommunications:DataManagementService"));
            services.Configure<UserManagementServiceSettings>(Configuration.GetSection("ServiceCommunications:UserManagementService"));
            services.Configure<DropoutServiceSettings>(Configuration.GetSection("ServiceCommunications:DropoutService"));
            services.Configure<IdentityServiceSettings>(Configuration.GetSection("ServiceCommunications:IdentityService"));
            FileUploadSettings.FileExtention = Configuration.GetSection("OrderFileUpload:FileExtention").Value;
            FileUploadSettings.ReadingPeriod = Configuration.GetSection("OrderFileUpload:ReadingPeriod").Value;
            FileUploadSettings.SourceFilePath = Configuration.GetSection("OrderFileUpload:SourceFilePath").Value;
            FileUploadSettings.TargetFilePath = Configuration.GetSection("OrderFileUpload:TargetFilePath").Value;
            FileUploadSettings.HostName = Configuration.GetSection("OrderFileUpload:HostName").Value;
            FileUploadSettings.Username = Configuration.GetSection("OrderFileUpload:Username").Value;
            FileUploadSettings.Password = Configuration.GetSection("OrderFileUpload:Password").Value;
            FileUploadSettings.ConnectionString = Configuration.GetSection("ConnectionString").Value;
            FileUploadSettings.proxyUrl = Configuration.GetSection("Location:ProxyUrl").Value;
            FileUploadSettings.paciServiceUrl = Configuration.GetSection("Location:PACIService:ServiceUrl").Value;
            FileUploadSettings.paciNumberFieldName = Configuration.GetSection("Location:PACIService:PACIFieldName").Value;
            FileUploadSettings.nhoodNameFieldName = Configuration.GetSection("Location:BlockService:AreaNameFieldName").Value;
            FileUploadSettings.blockServiceUrl = Configuration.GetSection("Location:BlockService:ServiceUrl").Value;
            FileUploadSettings.blockNameFieldNameBlockService = Configuration.GetSection("Location:BlockService:BlockNameFieldName").Value;
            FileUploadSettings.streetServiceUrl = Configuration.GetSection("Location:StreetService:ServiceUrl").Value;
            FileUploadSettings.blockNameFieldNameStreetService = Configuration.GetSection("Location:StreetService:BlockNameFieldName").Value;
            FileUploadSettings.streetNameFieldName = Configuration.GetSection("Location:StreetService:StreetFieldName").Value;
            StaticAppSettings.ArchivedStatusId = Configuration.GetSection("OrderActionsPrerequisite:ArchivedStatusId").Get<List<int>>();
            StaticAppSettings.TravellingStatusId = Configuration.GetSection("OrderActionsPrerequisite:TravellingStatusId").Get<List<int>>();
            StaticAppSettings.UnassignedStatusId = Configuration.GetSection("OrderActionsPrerequisite:UnassignedStatusId").Get<List<int>>();
            StaticAppSettings.CanUnassignStatusId = Configuration.GetSection("OrderActionsPrerequisite:CanUnassignStatusId").Get<List<int>>();
            StaticAppSettings.DispatchedSubStatusIds = Configuration.GetSection("SubStatusNames:DispatchedIds").Get<List<int>>();
            StaticAppSettings.ArchivedStatusName = Configuration.GetSection("OrderActionsPrerequisite:ArchivedStatusName").Get<List<string>>();
            StaticAppSettings.TravellingStatusName = Configuration.GetSection("OrderActionsPrerequisite:TravellingStatusName").Get<List<string>>();
            StaticAppSettings.UnassignedStatusName = Configuration.GetSection("OrderActionsPrerequisite:UnassignedStatusName").Get<List<string>>();
            StaticAppSettings.CanUnassignStatusName = Configuration.GetSection("OrderActionsPrerequisite:CanUnassignStatusName").Get<List<string>>();
            StaticAppSettings.DispatchedSubStatusNames = Configuration.GetSection("SubStatusNames:DispatchedNames").Get<List<string>>();
            StaticAppSettings.ConnectionString = Configuration.GetSection("ConnectionString").Get<string>();
            StaticAppSettings.ExceedTimeHours = Configuration.GetSection("OrderSettingsColumns:ExceedTimeHours").Get<string>();
            StaticAppSettings.ArchiveAfterDays = Configuration.GetSection("OrderSettingsColumns:ArchiveAfterDays").Get<string>();
            StaticAppSettings.InitialStatus = Configuration.GetSection("StatusControl:InitialStatus").Get<KeyValue>();
            StaticAppSettings.InitialSubStatus = Configuration.GetSection("StatusControl:InitialSubStatus").Get<KeyValue>();
            StaticAppSettings.DispatchedStatus = Configuration.GetSection("StatusControl:DispatchedStatus").Get<KeyValue>();
            StaticAppSettings.OnHoldStatus = Configuration.GetSection("StatusControl:OnHoldStatus").Get<KeyValue>();
            StaticAppSettings.OnTravelSubStatus = Configuration.GetSection("OrderActionsPrerequisite:OnTravelSubStatus").Get<KeyValue>();
            StaticAppSettings.SMSContent = Configuration.GetSection("SMSContent").Get<string>();
            StaticAppSettings.WorkingTypes = Configuration.GetSection("WorkingTypes").Get<List<string>>();
            StaticAppSettings.IdleHandling = Configuration.GetSection("IdleHandling").Get<List<KeyValue>>();
            StaticAppSettings.ExcludedStatusIds = Configuration.GetSection("ExcludeOrderActions:ExcludedIds").Get<List<int>>();
            StaticAppSettings.ExcludedStatusNames = Configuration.GetSection("ExcludeOrderActions:ExcludedNames").Get<List<string>>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddSingleton(provider => Configuration);

            // Add framework services.
            services.AddDbContext<OrderDBContext>(options =>
                options.UseSqlServer(Configuration["ConnectionString"],
                    sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.Order.EFCore.MSSQL")));

            services.AddAutoMapper(typeof(Startup));

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });


            #region Data & Managers
            services.AddScoped<DbContext, OrderDBContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            services.AddScoped(typeof(IOrderRowData), typeof(OrderRowData));
            services.AddScoped(typeof(IOrderObject), typeof(OrderObject));
            services.AddScoped(typeof(IOrderSetting), typeof(OrderSetting));
            services.AddScoped(typeof(IArchivedOrder), typeof(ArchivedOrder));
            services.AddScoped(typeof(IOrderAction), typeof(OrderAction));
            services.AddScoped(typeof(INotificationCenter), typeof(NotificationCenter));
            services.AddScoped(typeof(INotification), typeof(Notification));
            services.AddScoped(typeof(IOrderStatus), typeof(OrderStatus));
            services.AddScoped(typeof(IOrderSubStatus), typeof(OrderSubStatus));
            services.AddScoped(typeof(IOrderPriority), typeof(OrderPriority));
            services.AddScoped(typeof(IOrderType), typeof(OrderType));
            services.AddScoped(typeof(IOrderProblem), typeof(OrderProblem));
            services.AddScoped(typeof(ILang_OrderProblem), typeof(Lang_OrderProblem));
            services.AddScoped(typeof(ILang_OrderType), typeof(Lang_OrderType));
            services.AddScoped(typeof(ILang_OrderPriority), typeof(Lang_OrderPriority));
            services.AddScoped(typeof(ILang_OrderSubStatus), typeof(Lang_OrderSubStatus));
            services.AddScoped(typeof(ILang_OrderStatus), typeof(Lang_OrderStatus));
            services.AddScoped(typeof(IRejectionReason), typeof(RejectionReason));
            services.AddScoped(typeof(ILang_RejectionReason), typeof(Lang_RejectionReason));
            services.AddScoped(typeof(IWorkingType), typeof(WorkingType));
            services.AddScoped(typeof(ILang_WorkingType), typeof(Lang_WorkingType));
            services.AddScoped(typeof(ILang_Notification), typeof(Lang_Notification));

            services.AddScoped(typeof(IReadOrderManager), typeof(ReadOrderManager));
            services.AddScoped(typeof(IOrderRowDataManager), typeof(OrderRowDataManager));
            services.AddScoped(typeof(IOrderManager), typeof(OrderManager));
            services.AddScoped(typeof(IOrderSettingManager), typeof(OrderSettingManager));
            services.AddScoped(typeof(IArchivedOrderManager), typeof(ArchivedOrderManager));
            services.AddScoped(typeof(IOrderActionManager), typeof(OrderActionManager));
            services.AddScoped(typeof(IOrderStatusManager), typeof(OrderStatusManager));
            services.AddScoped(typeof(IOrderSubStatusManager), typeof(OrderSubStatusManager));
            services.AddScoped(typeof(IOrderProblemManager), typeof(OrderProblemManager));
            services.AddScoped(typeof(IOrderTypeManager), typeof(OrderTypeManager));
            services.AddScoped(typeof(IOrderPriorityManager), typeof(OrderPriorityManager));
            services.AddScoped(typeof(ILang_OrderSubStatusManager), typeof(Lang_OrderSubStatusManager));
            services.AddScoped(typeof(ILang_OrderStatusManager), typeof(Lang_OrderSubStatusManager));
            services.AddScoped(typeof(ILang_OrderTypeManager), typeof(Lang_OrderTypeManager));
            services.AddScoped(typeof(ILang_OrderPriorityManager), typeof(Lang_OrderPriorityManager));
            services.AddScoped(typeof(ILang_OrderProblemManager), typeof(Lang_OrderProblemManager));
            services.AddScoped(typeof(IRejectionReasonManager), typeof(RejectionReasonManager));
            services.AddScoped(typeof(ILang_RejectionReasonManager), typeof(Lang_RejectionReasonManager));
            services.AddScoped(typeof(IWorkingTypeManager), typeof(WorkingTypeManager));
            services.AddScoped(typeof(ILang_WorkingTypeManager), typeof(Lang_WorkingTypeManager));
            services.AddScoped(typeof(ILang_NotificationManager), typeof(Lang_NotificationManager));

            services.AddScoped(typeof(INotificationCenterManager), typeof(NotificationCenterManager));
            services.AddScoped(typeof(INotificationManager), typeof(NotificationManager));
            services.AddScoped(typeof(ITeamMembersService), typeof(TeamMembersService));
            services.AddScoped(typeof(INotificationService), typeof(NotificationService));
            services.AddScoped(typeof(ISMSService), typeof(SMSService));
            services.AddScoped(typeof(IProblemService), typeof(ProblemService));
            services.AddScoped(typeof(IIdentityService), typeof(IdentityService));
            services.AddScoped(typeof(IForemanService), typeof(ForemanService));
            services.AddScoped(typeof(IEngineerService), typeof(EngineerService));
            services.AddScoped(typeof(IDivisionService), typeof(DivisionService));
            services.AddScoped(typeof(IAreaService), typeof(AreaService));
            services.AddScoped(typeof(IBuildingTypesService), typeof(BuildingTypesService));
            services.AddScoped(typeof(ICompanyCodeService), typeof(CompanyCodeService));
            services.AddScoped(typeof(IContractTypesService), typeof(ContractTypesService));
            services.AddScoped(typeof(IGovernoratesService), typeof(GovernoratesService));
            services.AddScoped(typeof(IManagerService), typeof(ManagerService));
            services.AddScoped(typeof(ITechnicianService), typeof(TechnicianService));
            services.AddScoped(typeof(IDispatcherService), typeof(DispatcherService));
            services.AddScoped(typeof(ITeamService), typeof(TeamService));
            services.AddScoped(typeof(ISupervisorService), typeof(SupervisorService));
            services.AddScoped(typeof(IDispatcherSettingsService), typeof(DispatcherSettingsService));
            services.AddScoped(typeof(ISupportedLanguageService), typeof(SupportedLanguageService));
            services.AddScoped(typeof(ITechnicianStateService), typeof(TechnicianStateService));
            services.AddScoped(typeof(IAPIHelper), typeof(APIHelper));
            services.AddScoped<ReadOrdersWorkFlow>();
            services.AddHostedService<ReadingOrderTask>();

            #endregion

            services.AddDirectoryBrowser();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Order API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseStaticFiles();

            string excelSheetDirPath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\ExcelSheet");

            if (!Directory.Exists(excelSheetDirPath))
            {
                Directory.CreateDirectory(excelSheetDirPath);
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(excelSheetDirPath),
                RequestPath = new PathString("/ExcelSheet")
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(excelSheetDirPath),
                RequestPath = new PathString("/ExcelSheet")
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Order API V1");
            }); // URL: /swagger
        }
    }
}
