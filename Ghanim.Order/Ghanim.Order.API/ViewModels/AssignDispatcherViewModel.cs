﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class AssignDispatcherViewModel
    {
        public int OrderId { get; set; }
        public int DispatcherId { get; set; }
    }
}