﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class OrdersboardFilterViewModel
    {
        public string OrderCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPhone { get; set; }
        public List<int> AreaIds { get; set; }
        public string BlockName { get; set; }
        public List<int> ProblemIds { get; set; }
        public List<int> StatusIds { get; set; }  
        public int Rejection { get; set; }
        public int DispatcherId { get; set; }
    }
}
