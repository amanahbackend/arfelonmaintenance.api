﻿using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class WorkingTypeGroupViewModel
    {
        public int StateId { get; set; }
        public int TypeId { get; set; }
        public string StateName { get; set; }
        public IEnumerable<WorkingType> WorkingTypes { get; set; }
    }
}
