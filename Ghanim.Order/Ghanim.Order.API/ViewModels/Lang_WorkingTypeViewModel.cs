﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class Lang_WorkingTypeViewModel : BaseEntity
    {
        public int FK_WorkingType_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public WorkingType WorkingType { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
