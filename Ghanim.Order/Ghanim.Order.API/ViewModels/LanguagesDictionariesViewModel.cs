﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class LanguagesDictionariesViewModel : RepoistryBaseEntity
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
