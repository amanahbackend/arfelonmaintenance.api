﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class OrderTypeViewModel :BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
