﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class MapAdminRelatedViewModel : RepoistryBaseEntity
    {
        public string Mobile_Api_Key { get; set; }
        public string Web_Api_Key { get; set; }
        public int Refresh_Rate { get; set; }
    }
}
