﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.Order.API.ViewModels
{
    public class PaginatedFilterNotificationByDateViewModel
    {
        public string RecieverId { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public PaginatedItemsViewModel<NotificationCenterViewModel> PageInfo { get; set; } = null;
    }
}
