﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class WorkingTypeViewModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
