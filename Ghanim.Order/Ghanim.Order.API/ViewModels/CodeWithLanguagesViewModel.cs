﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class CodeWithLanguagesViewModel : RepoistryBaseEntity
    {
        public string Code { get; set; }
        public List<LanguagesDictionariesViewModel> languagesDictionaries { get; set; }
    }
}
