﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class OrderFilterViewModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public List<int> StatusId { get; set; }
        public List<int> ProblemId { get; set; }
        public List<int> DivisionId { get; set; }
        public List<int> DispatcherId { get; set; }
        public List<int> TechnicianId { get; set; }
        public List<int> AreaId { get; set; }
        public List<int> OrderTypeId { get; set; }
        public int Assigned{ get; set; }
    }
}
