﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class PrintOutFilterViewModel
    {
        public int DispatcherId { get; set; }
        public List<int> teamsId { get; set; }
        public bool IncludeOpenOrders { get; set; }
    }
}
