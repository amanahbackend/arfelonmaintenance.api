﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TeamViewModel : BaseEntity
    {
        public string VehicleNo { get; set; }
        //public string DriverName { get; set; }
        //public int DriverId { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int ForemanId { get; set; }
        public string ForemanName { get; set; }
        public int EngineerId { get; set; }
        public string EngineerName { get; set; }
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public string Name { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
