﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TimeSheetStructureViewModel
    {
        public DateTime DayDate { get; set; }  //key date
        public string PFNo { get; set; }    //personal number
        public string Name { get; set; }
        public Sheet Times { get; set; }
        public string DataEntryProfile { get; set; } //SERV
        public string CostCenter { get; set; }
        public string SalesOrderNumber { get; set; }
        public string SalesOrderItemNumber{ get; set; }
        public string Network { get; set; }
        public string OrderNumber { get; set; }
        public string Operation { get; set; }    //0010
        public string SubOperation { get; set; }
        public string ActivityType { get; set; }
        public string CreatedUserId { get; set; }
        public string WorkType { get; set; }  //activity type or attendance
        public TimeSpan Time { get; set; }  //hours

        //public TimeSpan ActualTime { get; set; }
        //public TimeSpan TravellingTime { get; set; }
        //public TimeSpan BreakTime { get; set; }
        //public TimeSpan IdleTime { get; set; }
        //public TimeSpan TotalHours { get; set; }
        //public TimeSpan OverTimeHours { get; set; }
    }

    public class Sheet
    {
        public string WorkType { get; set; }
        public double Time { get; set; }
    }
}
