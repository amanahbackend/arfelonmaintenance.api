﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TimeSheetStructureExcelViewModel
    {
        public string Data_Entry_Profile { get; set; } //SERV
        public DateTime Key_Date { get; set; }  //key date
        public string Personal_Number { get; set; }    //personal number
        public string Activity_Type { get; set; }
        public string Cost_Center { get; set; }
        public string Sales_Order_Number { get; set; }
        public string Sales_Order_Item_Number{ get; set; }
        public string Order_Number { get; set; }
        public string Network { get; set; }
        public string Operation { get; set; }    //0010
        public string Sub_Operation { get; set; }
        public string Attendance_or_activity_type { get; set; }  //activity type or attendance
        public double Time { get; set; }  //hours

    }
}
