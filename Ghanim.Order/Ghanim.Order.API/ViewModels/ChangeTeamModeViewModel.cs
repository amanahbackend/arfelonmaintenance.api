﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class ChangeTeamModeViewModel
    {
        public int TeamId { get; set; }
        public bool HasOrders { get; set; }
    }
}
