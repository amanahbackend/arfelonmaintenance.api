﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class NotificationViewModel : BaseEntity
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
