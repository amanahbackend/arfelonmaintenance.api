﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class OrderActionViewModel : BaseEntity
    {
        public int OrderId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int? SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        public DateTime ActionDate { get; set; }
        public DateTime ActionDay { get; set; }
        public TimeSpan? ActionTime { get; set; }
        public int? CreatedUserId { get; set; }
        public string CreatedUser { get; set; }
        public int? ActionTypeId { get; set; }
        public string ActionTypeName { get; set; }
        //public string PF { get; set; }
        public string CostCenterName { get; set; }
        public int? CostCenterId { get; set; }
        public int? WorkingTypeId { get; set; }
        public string WorkingTypeName { get; set; }
        public string Reason { get; set; }
        public string ServeyReport { get; set; }

    }
}
