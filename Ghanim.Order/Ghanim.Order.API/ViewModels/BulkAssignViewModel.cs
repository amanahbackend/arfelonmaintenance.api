﻿using CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class BulkAssignViewModel
    {
        public List<int> OrderIds { get; set; }
        public AssignType AssignType { get; set; }
        public int? TeamId { get; set; }
        public int? DispatcherId { get; set; }
    }
}
