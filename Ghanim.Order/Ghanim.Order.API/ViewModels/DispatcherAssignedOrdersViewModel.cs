﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class DispatcherAssignedOrdersViewModel
    {
        public int ForemanId{ get; set; }
        public string ForemanName{ get; set; }
        public IEnumerable<TeamOrdersViewModel> Teams { get; set; }
    }
}
