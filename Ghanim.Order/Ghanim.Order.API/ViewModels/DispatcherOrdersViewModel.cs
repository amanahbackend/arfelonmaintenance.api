﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class DispatcherOrdersViewModel : BaseEntity
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public List<OrderViewModel> Orders { get; set; }
    }
}
