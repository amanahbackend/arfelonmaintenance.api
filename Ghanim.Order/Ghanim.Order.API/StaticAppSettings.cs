﻿using Ghanim.Order.EFCore.MSSQL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API
{
    public static class StaticAppSettings
    {
        public static List<int> ArchivedStatusId { get; set; }
        public static List<string> ArchivedStatusName { get; set; }
        public static string ConnectionString { get; set; }
        public static string ArchiveAfterDays { get; set; }
        public static string ExceedTimeHours { get; set; }
        public static KeyValue InitialStatus { get; set; }
        public static KeyValue InitialSubStatus { get; set; }
        public static KeyValue DispatchedStatus { get; set; }
        public static KeyValue OnHoldStatus { get; set; }
        public static KeyValue OnTravelSubStatus { get; set; }
        public static List<int> TravellingStatusId { get; set; }
        public static List<string> TravellingStatusName { get; set; }
        public static List<int> CanUnassignStatusId { get; set; }
        public static List<string> CanUnassignStatusName { get; set; }
        public static List<int> UnassignedStatusId { get; set; }
        public static List<string> UnassignedStatusName { get; set; }
        public static List<int> DispatchedSubStatusIds { get; set; }
        public static List<string> DispatchedSubStatusNames { get; set; }
        public static List<string> WorkingTypes { get; set; }
        public static List<KeyValue> IdleHandling { get; set; }
        public static List<int> ExcludedStatusIds { get; set; }
        public static List<string> ExcludedStatusNames { get; set; }
        public static string SMSContent { get; set; }

        public static OrderDBContext GetDbContext()
        {
            var optsBuilder = new DbContextOptionsBuilder<OrderDBContext>();

            optsBuilder.UseSqlServer(ConnectionString);

            return new OrderDBContext(optsBuilder.Options);
        }
    }
}
