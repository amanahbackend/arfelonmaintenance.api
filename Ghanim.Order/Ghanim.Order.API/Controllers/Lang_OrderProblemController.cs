﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Utilities.ProcessingResult;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_OrderProblemController : BaseController<ILang_OrderProblemManager, Lang_OrderProblem, Lang_OrderProblemViewModel>
    {
        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_OrderProblemManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_OrderProblemController(IServiceProvider serviceprovider, ISupportedLanguageService _supportedLanguageService, ILang_OrderProblemManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IOrderProblemManager OrderProblemmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderProblemManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        //[HttpGet, Route("GetAll")]
        //public override ProcessResultViewModel<List<Lang_OrderProblemViewModel>> Get()
        //{
        //    return base.Get();
        //}

        [HttpGet]
        [Route("GetAllLanguagesByOrderProblemId/{OrderProblemId}")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguagesByOrderProblemId([FromRoute]int OrderProblemId)
        {
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderProblemRes = OrderProblemmanager.Get(OrderProblemId);
            var entityResult = await GetAllLanguagesByOrderProblemIdService(OrderProblemId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = OrderProblemRes.Data.Code, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succeeded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguages()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAll();
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var OrderProblemRes = OrderProblemmanager.GetAll();
            foreach (var OrderProblem in OrderProblemRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = await GetAllLanguagesByOrderProblemIdService(OrderProblem.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = OrderProblem.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderProblem.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = OrderProblem.Code, languagesDictionaries = languagesDictionary,Id = OrderProblem.Id });
            }
            var result = ProcessResultViewModelHelper.Succeeded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_OrderProblem> lstModel)
        {
            var entityResult = manager.UpdateByOrderProblem(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody]CodeWithLanguagesViewModel Model)
        {
            List<Lang_OrderProblem> lstModel = new List<Lang_OrderProblem>();
            var OrderPriorityRes = OrderProblemmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                lstModel.Add(new Lang_OrderProblem { FK_OrderProblem_ID = OrderPriorityRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByOrderProblem(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
        private async Task<ProcessResult<List<Lang_OrderProblem>>> GetAllLanguagesByOrderProblemIdService(int OrderProblemId)
        {
            List<Lang_OrderProblem> input = null;
            try
            {
                var SupportedLanguagesRess = await supportedLanguageService.GetAll();
                var SupportedLanguagesRes = SupportedLanguagesRess.Data.OrderBy(x => x.Id).ToList();
                input = manager.GetAllQuerable().Data.Where(x => x.FK_OrderProblem_ID == OrderProblemId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderProblem { FK_OrderProblem_ID = OrderProblemId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_OrderProblem>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByOrderProblemId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderProblem>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderProblemId");
            }
        }
    }
}
