﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_OrderPriorityController : BaseController<ILang_OrderPriorityManager, Lang_OrderPriority, Lang_OrderPriorityViewModel>
    {
        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_OrderPriorityManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_OrderPriorityController(IServiceProvider serviceprovider, ISupportedLanguageService _supportedLanguageService, ILang_OrderPriorityManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
           _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IOrderPriorityManager OrderPrioritymanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderPriorityManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByOrderPriorityId/{OrderPriorityId}")]
        public async Task<ProcessResultViewModel<List<LanguagesDictionariesViewModel>>> GetAllLanguagesByOrderPriorityId([FromRoute]int OrderPriorityId)
        {
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderPriorityRes = OrderPrioritymanager.Get(OrderPriorityId);
            var entityResult = manager.GetAllLanguagesByOrderPriorityId(OrderPriorityId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succeeded<List<LanguagesDictionariesViewModel>>(languagesDictionary);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public async Task<ProcessResultViewModel<List<NullWithLanguagesViewModel>>> GetAllLanguages()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAll();
            List<NullWithLanguagesViewModel> nullWithAllLanguages = new List<NullWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderPriorityRes = OrderPrioritymanager.GetAll();
            foreach (var OrderPriority in OrderPriorityRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByOrderPriorityId(OrderPriority.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = OrderPriority.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderPriority.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                nullWithAllLanguages.Add(new NullWithLanguagesViewModel { languagesDictionaries = languagesDictionary, Id = OrderPriority.Id });
            }
            var result = ProcessResultViewModelHelper.Succeeded<List<NullWithLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_OrderPriority> lstModel)
        {
            var entityResult = manager.UpdateByOrderPriority(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody]NullWithLanguagesViewModel Model)
        {
            List<Lang_OrderPriority> lstModel = new List<Lang_OrderPriority>();
            var OrderPriorityRes = OrderPrioritymanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                lstModel.Add(new Lang_OrderPriority { FK_OrderPriority_ID = OrderPriorityRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByOrderPriority(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        //[HttpGet]
        //[Route("GetLanguageByOrderPriorityId/{OrderPriorityId}")]
        //public ProcessResultViewModel<Lang_OrderPriority> GetLanguageByOrderPriorityId([FromRoute]int languageId,int orderPriorityId)
        //{
        //    var OrderPriorityRes = manager.Get(x=>x.FK_SupportedLanguages_ID==languageId&&x.FK_OrderPriority_ID== orderPriorityId);
        //    var result = ProcessResultViewModelHelper.Succeeded<Lang_OrderPriority>(OrderPriorityRes.Data);
        //    return result;
        //}

    }
}
