﻿using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderProblemController : BaseController<IOrderProblemManager, OrderProblem, OrderProblemViewModel>
    {
        public OrderProblemController(IOrderProblemManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) :
            base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
           
        }
    }
}
