﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnum;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.API.ViewModels;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class WorkingTypeController : BaseController<IWorkingTypeManager, WorkingType, WorkingTypeViewModel>
    {
        IServiceProvider _serviceprovider;
        IWorkingTypeManager manager;
        IProcessResultMapper processResultMapper;
        public WorkingTypeController(IServiceProvider serviceprovider, IWorkingTypeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        [HttpGet]
        [Route("GetAllGrouped")]
        public async Task<ProcessResult<List<WorkingTypeGroupViewModel>>> GetAllGrouped()
        {
            try
            {
                var workingTypes = manager.GetAll();
                if (workingTypes.IsSucceeded)
                {
                    var result = workingTypes.Data.GroupBy(x => x.Category, (key, group) => new WorkingTypeGroupViewModel { StateName = key, WorkingTypes = group }).ToList();
                    for (int i = 0; i < result.Count; i++)
                    {

                        if (result[i].StateName.ToLower().Contains("end"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Actual;
                            result[i].StateId = (int)TechnicianState.End;
                        }
                        else if (result[i].StateName.ToLower().Contains("work"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Actual;
                            result[i].StateId = (int)TechnicianState.Working;
                        }
                        else if (result[i].StateName.ToLower().Contains("idle"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Idle;
                            result[i].StateId = (int)TechnicianState.Idle;
                        }
                        else if (result[i].StateName.ToLower().Contains("break"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Break;
                            result[i].StateId = (int)TechnicianState.Break;
                        }
                    }
                    return ProcessResultHelper.Succeeded<List<WorkingTypeGroupViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, null, "There is somehing wrong while get all data");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, ex, "There is somehing wrong while getting data in GetAllGrouped");
            }
        }
    }
}