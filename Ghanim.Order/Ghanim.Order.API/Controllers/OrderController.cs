﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using CommonEnums;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.Helper;
using Ghanim.Order.API.ServiceCommunications.Notification;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Utilites.ProcessingResult;
using Utilities;
using Utilities.ProcessingResult;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : BaseController<IOrderManager, OrderObject, OrderViewModel>
    {
        private readonly IOrderManager _manager;
        private readonly IOrderActionManager _actionManager;
        IServiceProvider _serviceprovider;

        IProcessResultMapper _processResultMapper;
        IDispatcherService _dispatcherService;
        ISupervisorService _supervisorService;
        ITeamMembersService _teamMembersService;
        INotificationService _notificationService;
        private readonly ISMSService _smsService;
        ITeamService _teamService;
        IIdentityService _identityService;
        IForemanService _formanService;
        IEngineerService _engineerService;
        ITechnicianService _technicianService;
        IAPIHelper _helper;
        ITechnicianStateService _technicianStateService;
        INotificationCenterManager _notificationCenterManageManager;

        public OrderController(IServiceProvider serviceprovider, IOrderManager _manager,
            IOrderSettingManager settingManager,
            IOrderActionManager _actionManager,
            IAPIHelper _helper,
            IMapper mapper,
            INotificationCenterManager _notificationCenterManageManager,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService,
            IDispatcherService _dispatcherService,
            INotificationService _notificationService,
            ITeamService _teamService,
            IIdentityService _identityService,
            ISupervisorService _supervisorService,
            IForemanService _formanService,
            IEngineerService _engineerService,
            IManagerService managerService,
            ITechnicianService _technicianService,
            ITechnicianStateService _technicianStateService,
            ISMSService _smsService
        ) : base(_manager, mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            this._manager = _manager;
            this._actionManager = _actionManager;

            this._helper = _helper;
            this._notificationCenterManageManager = _notificationCenterManageManager;
            this._processResultMapper = _processResultMapper;
            this._dispatcherService = _dispatcherService;
            this._teamMembersService = _teamMembersService;
            this._notificationService = _notificationService;
            this._teamService = _teamService;
            this._supervisorService = _supervisorService;
            this._identityService = _identityService;
            this._formanService = _formanService;
            this._engineerService = _engineerService;
            this._technicianService = _technicianService;
            this._technicianStateService = _technicianStateService;
            this._smsService = _smsService;
        }

        private IOrderStatusManager OrderStatusmanager => _serviceprovider.GetService<IOrderStatusManager>();

        private IOrderSubStatusManager OrderSubStatusmanager => _serviceprovider.GetService<IOrderSubStatusManager>();

        //public override ProcessResultViewModel<OrderViewModel> Post([FromBody] OrderViewModel model)
        //{
        //    return base.Post(model);
        //}

        [HttpPost]
        [Route("SetAcceptence")]
        public async Task<ProcessResultViewModel<bool>> SetAcceptence([FromBody] AcceptenceViewModel model)
        {
            if (model.OrderId > 0)
            {
                //var entityResult = manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId, model.RejectionReason);
                //if (entityResult.IsSucceeded)
                //{
                //    var order = Get(model.OrderId).Data;
                //    var orderObject = Mapper.Map<OrderObject>(order);
                //    if (model.AcceptenceFlag == true)
                //    {
                //        await AddAction(orderObject, OrderActionType.Acceptence, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                //    }
                //    else
                //    {
                //        await AddAction(orderObject, OrderActionType.Rejection, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                //    }

                //    //notify dispatcher with acceptance or rejection

                //    var NotificationRes = await NotifyDispatcherWithOrderAcceptence(model);
                //    return ProcessResultViewModelHelper.Succeeded<bool>(true);
                //}
                //else
                //{
                //    return ProcessResultViewModelHelper.Failed<bool>(false, entityResult.Status.Message);
                //}

                var acceptenceResult = await ChangeAcceptence(model);

                if (acceptenceResult.IsSucceeded && acceptenceResult.Data)
                {
                    var notificationRes = await NotifyDispatcherWithOrderAcceptence(model);
                    return ProcessResultViewModelHelper.Succeeded(true);
                }
                return ProcessResultViewModelHelper.Failed(false, acceptenceResult.Status.Message);
            }
            return ProcessResultViewModelHelper.Failed(false, "order id is required");
        }

        private async Task<ProcessResult<bool>> ChangeAcceptence(AcceptenceViewModel model)
        {
            ProcessResult<bool> entityResult;
            if (model.AcceptenceFlag)
            {
                entityResult = _manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId,
                    model.RejectionReason, null, null, null, null);
            }
            else
            {
                entityResult = _manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId,
                    model.RejectionReason, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name,
                    StaticAppSettings.InitialSubStatus.Id, StaticAppSettings.InitialSubStatus.Name);
            }
            if (entityResult.IsSucceeded)
            {
                var order = Get(model.OrderId).Data;
                var orderObject = Mapper.Map<OrderObject>(order);
                if (model.AcceptenceFlag)
                {
                    await AddAction(orderObject, OrderActionType.Acceptence, (int)TimeSheetType.Actual,
                        EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                }
                else
                {
                    await AddAction(orderObject, OrderActionType.Rejection, (int)TimeSheetType.Actual,
                        EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                }
            }
            return entityResult;
        }

        [HttpPut]
        [Route("ChangeStatus")]
        public async Task<ProcessResult<bool>> ChangeStatus([FromBody] ChangeStatusViewModel model)
        {
            if (model.OrderId == 0)
            {
                return ProcessResultHelper.Failed(false, null, "order id is required");
            }

            var orderResult = _manager.Get(model.OrderId);
            var statusRes = OrderStatusmanager.Get(model.StatusId);
            var subStatusRes = OrderSubStatusmanager.Get(model.SubStatusId);

            if (orderResult.IsSucceeded && orderResult.Data != null)
            {
                orderResult.Data.StatusId = model.StatusId;
                orderResult.Data.StatusName = statusRes.Data.Name;
                orderResult.Data.SubStatusId = model.SubStatusId;
                orderResult.Data.SubStatusName = subStatusRes.Data.Name;
                orderResult.Data.ServeyReport = model.ServeyReport;
                model.StatusName = statusRes.Data.Name;
                model.SubStatusName = subStatusRes.Data.Name;

                if (StaticAppSettings.TravellingStatusId.Contains(model.StatusId))
                {
                    // send sms here
                    string msgContent = string.Format(StaticAppSettings.SMSContent, orderResult.Data.Code,
                        model.StatusName);
                    var sms = new SMSViewModel
                    {
                        PhoneNumber = orderResult.Data.PhoneOne,
                        Message = msgContent
                    };
                    var smsResult = await _smsService.Send(sms);
                }

                // make order unassigned in case it changed to on hold
                if (StaticAppSettings.UnassignedStatusId.Contains(model.StatusId))
                {
                    orderResult.Data.PrevTeamId = orderResult.Data.TeamId;
                    orderResult.Data.TeamId = 0;
                    orderResult.Data.RankInTeam = 0;
                }
                if (model.StatusId == 6)
                {

                    orderResult.Data.OnHoldCount = orderResult.Data.OnHoldCount + 1;
                }
                var updateResult = _manager.Update(orderResult.Data);
                if (updateResult.IsSucceeded)
                {
                    await AddAction(orderResult.Data, OrderActionType.ChangeStatus, (int)TimeSheetType.Actual,
                        EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                    //notify teamMembers and dispatcher with new status
                    var notificationRes = await NotifyDispatcherWithNewStatus(model);
                    return ProcessResultHelper.Succeeded(true);
                }
                return ProcessResultHelper.Failed(false, null, "something wrong while update order");
            }
            return ProcessResultHelper.Failed(false, null, "order id is required");
        }

        [HttpPost]
        [Route("AddMulti")]
        public ProcessResultViewModel<List<OrderViewModel>> PostMulti([FromBody] List<OrderViewModel> orders)
        {
            try
            {
                for (int i = 0; i < orders.Count; i++)
                {
                    // Check for repeated call
                    DateTime currentTime = DateTime.Now;

                    var customerOrders =
                        _manager.GetAll(x => x.CustomerCode == orders[i].CustomerCode &&
                                            (currentTime - x.CreatedDate).TotalHours <= 24);
                    if (customerOrders.IsSucceeded && customerOrders.Data.Count > 0)
                    {
                        orders[i].IsRepeatedCall = true;
                    }

                }
                var addedOrders = base.PostMulti(orders);
                if (addedOrders.IsSucceeded && addedOrders.Data.Count > 0)
                {
                    var mappedOrders = mapper.Map<List<OrderViewModel>, List<OrderObject>>(addedOrders.Data);
                    for (int i = 0; i < mappedOrders.Count; i++)
                    {
                        AddAction(mappedOrders[i], OrderActionType.AddOrder, null, null);
                    }
                }

                return addedOrders;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPut]
        [Route("BulkAssign")]
        public async Task<ProcessResult<bool>> BulkAssign([FromBody] BulkAssignViewModel model)
        {
            if (model.AssignType == AssignType.Dispatcher && model.DispatcherId == 0)
            {
                return ProcessResultHelper.Failed(false, null,
                    "dispatcher id is required while assign type is to dispatcher");
            }

            if (model.AssignType == AssignType.Team && model.TeamId == 0)
            {
                return ProcessResultHelper.Failed(false, null,
                    "team id is required while assign type is to team");
            }

            if (model.AssignType == AssignType.Dispatcher)
            {
                var dispatcher = await _dispatcherService.GetDispatcherById((int)model.DispatcherId);
                if (dispatcher.IsSucceeded && dispatcher.Data != null)
                {
                    return _manager.BulkAssign(model.OrderIds, model.AssignType, model.TeamId, model.DispatcherId,
                        dispatcher.Data.Name, dispatcher.Data.SupervisorId, dispatcher.Data.SupervisorName,
                        StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name);

                }
                return ProcessResultHelper.Failed(false, null,
                    "Something wrong while getting dispatcher information",
                    ProcessResultStatusCode.Failed, "BulkAssign Controler");
            }
            return _manager.BulkAssign(model.OrderIds, model.AssignType, model.TeamId, model.DispatcherId, null,
                null, null, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name);
        }

        [HttpPut]
        [Route("Assign")]
        public async Task<ProcessResult<bool>> Assign([FromBody] AssignViewModel model)
        {
            if (model.OrderId == 0 && model.TeamId == 0)
            {
                return ProcessResultHelper.Failed(false, null, "order id & team id is required");
            }

            var order = Get(model.OrderId).Data;
            ProcessResult<bool> assignResult;

            if (order.StatusId == StaticAppSettings.OnHoldStatus.Id)
            {
                assignResult = _manager.Assign(model.OrderId, model.TeamId, StaticAppSettings.DispatchedStatus.Id,
                    StaticAppSettings.DispatchedStatus.Name, StaticAppSettings.DispatchedSubStatusIds[1],
                    StaticAppSettings.DispatchedSubStatusNames[1]);
            }
            else
            {
                assignResult = _manager.Assign(model.OrderId, model.TeamId, StaticAppSettings.DispatchedStatus.Id,
                    StaticAppSettings.DispatchedStatus.Name, StaticAppSettings.DispatchedSubStatusIds[0],
                    StaticAppSettings.DispatchedSubStatusNames[0]);
            }

            if (assignResult.IsSucceeded & assignResult.Data)
            {
                order = Get(model.OrderId).Data;
                var orderObject = Mapper.Map<OrderObject>(order);
                await AddAction(orderObject, OrderActionType.Assign, null, null);

                var notifyTeamRes = await NotifyTeamMembersWithNewOrder(model.TeamId, order);
                return ProcessResultHelper.Succeeded(true);
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while assign order to team");
        }


        [HttpPut]
        [Route("UnAssign")]
        public async Task<ProcessResult<bool>> UnAssign([FromBody] UnAssignViewModel model)
        {
            if (model.OrderId == 0)
            {
                return ProcessResultHelper.Failed(false, null, "order id is required");
            }
            var orderRes = _manager.Get(model.OrderId);
            int teamId = orderRes.Data.TeamId;
            if (StaticAppSettings.CanUnassignStatusName.Contains(orderRes.Data.StatusName))
            {
                ProcessResult<bool> unAssignResult;

                if (orderRes.Data.StatusId == StaticAppSettings.OnHoldStatus.Id)
                {
                    orderRes.Data.PrevTeamId = orderRes.Data.TeamId;
                    unAssignResult = _manager.UnAssign(model.OrderId, StaticAppSettings.OnHoldStatus.Id,
                        StaticAppSettings.OnHoldStatus.Name);
                }
                else
                {
                    unAssignResult = _manager.UnAssign(model.OrderId, StaticAppSettings.InitialStatus.Id,
                        StaticAppSettings.InitialStatus.Name);
                }

                if (unAssignResult.IsSucceeded & unAssignResult.Data)
                {
                    var order = Get(model.OrderId).Data;
                    var orderObject = Mapper.Map<OrderObject>(order);

                    await AddAction(orderObject, OrderActionType.UnAssgin, null, null);

                    var notifyTeamRes = await NotifyTeamMembersWithUnassignOrder(teamId, model.OrderId);
                    return ProcessResultHelper.Succeeded(true);
                }
                return ProcessResultHelper.Failed(false, null, "something wrong while unassign order");
            }
            return ProcessResultHelper.Failed(false, null, "couldn't un-assign this order with this status");
        }

        [HttpPut]
        [Route("BulkUnAssign")]
        public ProcessResult<bool> BulkUnAssign([FromBody] List<int> model)
        {
            if (model == null || model.Count == 0)
            {
                return ProcessResultHelper.Failed(false, null, "order ids is required while unassign orders");
            }

            return _manager.BulkUnAssign(model, StaticAppSettings.InitialStatus.Id,
                StaticAppSettings.InitialStatus.Name);
        }

        [HttpPut]
        [Route("ChangeTeamRank")]
        public async Task<ProcessResult<bool>> ChangeTeamRank([FromBody] ChangeRankInTeamViewModel model)
        {
            List<int> travellingStatusId = StaticAppSettings.TravellingStatusId;
            if (model == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed(false, null, "team id and order id and rank are required");
            }

            var teamOrders = _manager.GetAll(x => x.TeamId == model.TeamId &&
                                                 !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
            {
                foreach (var teamorder in teamOrders.Data)
                {
                    if (travellingStatusId.Contains(teamorder.StatusId))
                    {
                        return ProcessResultHelper.Failed(false, null,
                            "You can't change order rank when technician has order with on travel or reached status.",
                            ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
                    }
                }

                var result = _manager.ChangeOrderRank(teamOrders.Data, model.Orders);
                if (result.IsSucceeded)
                {
                    AcceptenceViewModel acceptenceModel =
                        new AcceptenceViewModel
                        {
                            AcceptenceFlag = true,
                            OrderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key
                        };
                    var notificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                }

                return result;
            }

            return ProcessResultHelper.Failed(false, null, "something wrong while getting team orders",
                ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }

        [HttpGet]
        [Route("GetTeamOrders")]
        public ProcessResult<List<OrderViewModel>> GetTeamOrders([FromQuery] int teamId)
        {
            if (teamId > 0)
            {
                var orders = _manager.GetAllQuerable().Data
                    .Where(x => x.TeamId == teamId && x.AcceptanceFlag != AcceptenceType.Rejected &&
                                !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId)).OrderBy(x => x.RankInTeam)
                    .ToList();
                var result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders);

                return ProcessResultHelper.Succeeded(result);
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "team id is required",
                ProcessResultStatusCode.Failed, "GetTeamOrders Controller");
        }

        [HttpGet]
        [Route("GetAssignTypes")]
        public ProcessResult<List<EnumEntity>> GetAssignTypes()
        {
            return ProcessResultHelper.Succeeded(EnumManager<AssignType>.GetEnumList());
        }

        [HttpGet]
        [Route("GetAcceptenceTypes")]
        public ProcessResult<List<EnumEntity>> GetAcceptenceTypes()
        {
            return ProcessResultHelper.Succeeded(EnumManager<AcceptenceType>.GetEnumList());
        }

        [HttpGet]
        [Route("GetAccomplishTypes")]
        public ProcessResult<List<EnumEntity>> GetAccomplishTypes()
        {
            return ProcessResultHelper.Succeeded(EnumManager<AccomplishType>.GetEnumList());
        }

        [HttpPost]
        [Route("Search")]
        public ProcessResult<List<OrderViewModel>> Search([FromBody] FilterOrderViewModel model)
        {

            if (model != null && model.Filters.Count > 0)
            {
                Expression<Func<OrderObject, bool>> predicateFilter = null;
                if (model != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters, model.IncludeUnAssign);
                }

                var result = _manager.GetAll(predicateFilter);
                if (result.IsSucceeded && result.Data.Count > 0)
                {
                    List<OrderViewModel> searchResult =
                        mapper.Map<List<OrderObject>, List<OrderViewModel>>(result.Data);
                    return ProcessResultHelper.Succeeded(searchResult);
                }
                return ProcessResultHelper.Succeeded(new List<OrderViewModel>());
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null,
                "filter properties are required at least one property");
        }

        [HttpGet]
        [Route("GetUnAssignedOrdersForDispatcher")]
        public ProcessResult<List<OrderViewModel>> GetUnAssignedOrdersForDispatcher([FromQuery] int dispatcherId)
        {

            if (dispatcherId > 0)
            {
                var orders = _manager.GetAll(x => x.DispatcherId == dispatcherId &&
                                                 StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succeeded(result);
                }
                return ProcessResultHelper.Succeeded(new List<OrderViewModel>());
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "dispatcher id is required");
        }

        [HttpGet]
        [Route("GetUnAssignedOrdersForSupervisor")]
        public ProcessResult<List<OrderViewModel>> GetUnAssignedOrdersForSupervisor([FromQuery] int supervisrId)
        {
            if (supervisrId > 0)
            {
                var orders = _manager.GetAll(x => x.SupervisorId == supervisrId && x.DispatcherId == 0 &&
                                                 StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);

                    return ProcessResultHelper.Succeeded(result);
                }

                return ProcessResultHelper.Succeeded(new List<OrderViewModel>());
            }

            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "supervisor id is required");
        }

        [HttpGet]
        [Route("GetAssignedOrdersForDispatcher")]
        public async Task<ProcessResult<List<DispatcherAssignedOrdersViewModel>>> GetAssignedOrdersForDispatcher(
            [FromQuery] int dispatcherId)
        {
            try
            {
                if (dispatcherId > 0)
                {
                    List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
                    deactiveStatusesId.Add(StaticAppSettings.InitialStatus.Id);

                    var orders = _manager.GetAll(x => x.DispatcherId == dispatcherId && x.TeamId > 0 &&
                                                     !deactiveStatusesId.Contains(x.StatusId));
                    var dispatcherTeams = await _teamService.GetTeamsByDispatcherId(dispatcherId);
                    var currentTeams = mapper.Map<List<TeamViewModel>, List<TeamOrdersViewModel>>(dispatcherTeams.Data);

                    if (orders.IsSucceeded)
                    {
                        if (orders.Data.Count > 0)
                        {
                            List<OrderViewModel> result =
                                mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                            var groups = result.GroupBy(x => x.TeamId,
                                (key, group) => new TeamOrdersViewModel
                                {
                                    TeamId = key,
                                    Orders = group.OrderBy(x => x.RankInTeam)
                                }).ToList();

                            if (groups.Count > 0)
                            {
                                if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                                {

                                    for (int i = 0; i < currentTeams.Count; i++)
                                    {
                                        currentTeams[i].Orders = new List<OrderViewModel>();
                                        currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                                        currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;

                                        for (int j = 0; j < groups.Count; j++)
                                        {
                                            if (currentTeams[i].TeamId == groups[j].TeamId)
                                            {
                                                currentTeams[i].Orders = groups[j].Orders;
                                            }
                                        }
                                    }
                                }
                            }

                            // here add foreman group binding
                            var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName },
                                (key, group) => new DispatcherAssignedOrdersViewModel
                                {
                                    ForemanId = key.ForemanId,
                                    ForemanName = key.ForemanName,
                                    Teams = group
                                }).ToList();

                            return ProcessResultHelper.Succeeded(newGroup);
                        }
                        else
                        {
                            if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                            {

                                for (int i = 0; i < currentTeams.Count; i++)
                                {
                                    currentTeams[i].Orders = new List<OrderViewModel>();
                                    currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                                    currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                                }
                            }
                            // here add foreman group binding
                            var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName },
                                (key, group) => new DispatcherAssignedOrdersViewModel
                                {
                                    ForemanId = key.ForemanId,
                                    ForemanName = key.ForemanName,
                                    Teams = group
                                }).ToList();

                            return ProcessResultHelper.Succeeded(newGroup);
                        }
                    }

                    return ProcessResultHelper.Succeeded(new List<DispatcherAssignedOrdersViewModel>());
                }

                return ProcessResultHelper.Failed<List<DispatcherAssignedOrdersViewModel>>(
                    null, null, "dispatcher id is required");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherAssignedOrdersViewModel>>(
                    null, null, ex.StackTrace);
            }
        }

        [HttpGet]
        [Route("GetAssignedOrdersForDispatchersBySupervisor")]
        public ProcessResult<List<DispatchersUnAssignedOrdersViewModel>> GetAssignedOrdersForDispatchersBySupervisor(
            [FromQuery] int supervisorId)
        {
            if (supervisorId > 0)
            {
                var orders = _manager.GetAll(x => x.SupervisorId == supervisorId && x.DispatcherId > 0 &&
                                                 x.StatusId == StaticAppSettings.InitialStatus.Id);

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    var groups = result.GroupBy(x => x.DispatcherId,
                            (key, group) => new DispatchersUnAssignedOrdersViewModel
                            {
                                DispatcherId = key,
                                Orders = group
                            })
                        .ToList();
                    return ProcessResultHelper.Succeeded(groups);
                }
                return ProcessResultHelper.Succeeded<List<DispatchersUnAssignedOrdersViewModel>>(null);
            }
            return ProcessResultHelper.Failed<List<DispatchersUnAssignedOrdersViewModel>>(null, null,
                "supervisor id is required");
        }

        [HttpGet]
        [Route("GetMapOrdersForDispatcher")]
        public ProcessResult<List<OrderViewModel>> GetMapOrdersForDispatcher([FromQuery] int dispatcherId)
        {
            if (dispatcherId > 0)
            {
                List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;

                var orders = _manager.GetAll(x => x.DispatcherId == dispatcherId && x.Long > 0 && x.Lat > 0 &&
                                                 !deactiveStatusesId.Contains(x.StatusId));
                if (orders.IsSucceeded)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succeeded(result);
                }
                return ProcessResultHelper.Failed<List<OrderViewModel>>(null, orders.Exception,
                    "Something wrong while getting orders");
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "dispatcher id is required");
        }

        [HttpPost]
        [Route("SetWorkingAction")]
        public async Task<ProcessResultViewModel<bool>> SetWorkingAction([FromBody] WorkingViewModel model)
        {
            var actionType = OrderActionType.Work;
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
            }


            var addedOrderAction = await AddAction(null, actionType, model.WorkingTypeId, model.WorkingTypeName);
            if (addedOrderAction.IsSucceeded)
            {
                var technicianState = mapper.Map<WorkingViewModel, TechnicianStateViewModel>(model);
                var result = await _technicianStateService.Add(technicianState);
                if (result.IsSucceeded && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succeeded(true);
                }
                return ProcessResultViewModelHelper.Failed(false, result.Status.Message);
            }
            return ProcessResultViewModelHelper.Failed(false, addedOrderAction.Status.Message);
        }

        [HttpPost]
        [Route("SetWorkingState")]
        public async Task<ProcessResultViewModel<bool>> SetWorkingState([FromBody] WorkingViewModel model)
        {

            if (model.State == TechnicianState.Working && model.OrderId == 0)
            {
                return ProcessResultViewModelHelper.Failed(false,
                    "Order id is required to change team state to working");
            }
            if (model.WorkingTypeName == "Reject first order" && string.IsNullOrEmpty(model.RejectionReason))
            {
                return ProcessResultViewModelHelper.Failed(false,
                    "Rejection reason is required to reject the order");
            }

            var actionType = OrderActionType.Work;
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
            }

            OrderObject orderObject = new OrderObject();
            if (model.OrderId > 0)
            {
                var order = Get(model.OrderId).Data;
                orderObject = Mapper.Map<OrderObject>(order);
            }


            if (model.State == TechnicianState.Working)
            {
                string acceptValue = StaticAppSettings.WorkingTypes[0];
                string rejectValue = StaticAppSettings.WorkingTypes[1];
                string continueValue = StaticAppSettings.WorkingTypes[2];

                if (acceptValue == model.WorkingTypeName)
                {
                    // accept order and set on travel
                    var acceptAndSetTravelling = _manager.AcceptOrderAndSetOnTravel(model.OrderId,
                        StaticAppSettings.TravellingStatusName[0], StaticAppSettings.TravellingStatusId[0],
                        StaticAppSettings.OnTravelSubStatus.Name, StaticAppSettings.OnTravelSubStatus.Id);
                    if (!(acceptAndSetTravelling.IsSucceeded && acceptAndSetTravelling.Data))
                    {
                        return ProcessResultViewModelHelper.Failed(false, acceptAndSetTravelling.Status.Message);
                    }
                    // notify dispatcher with the order changes
                    ChangeStatusViewModel changeStatusView = new ChangeStatusViewModel
                    {
                        OrderId = orderObject.Id,
                        StatusId = orderObject.StatusId,
                        StatusName = orderObject.StatusName,
                        SubStatusId = orderObject.SubStatusId,
                        SubStatusName = orderObject.SubStatusName
                    };
                    var notificationRes = await NotifyDispatcherWithNewStatus(changeStatusView);

                    // send SMS
                    string msgContent = string.Format(StaticAppSettings.SMSContent, orderObject.Code,
                        orderObject.StatusName);
                    var sms = new SMSViewModel
                    {
                        PhoneNumber = orderObject.PhoneOne,
                        Message = msgContent
                    };
                    var smsResult = await _smsService.Send(sms);
                }
                else if (rejectValue == model.WorkingTypeName)
                {
                    // make order as rejected
                    AcceptenceViewModel acceptenceViewModel = new AcceptenceViewModel
                    {
                        AcceptenceFlag = false,
                        OrderId = model.OrderId,
                        RejectionReason = model.RejectionReason,
                        RejectionReasonId = model.RejectionReasonId
                    };
                    var acceptenceResult = await ChangeAcceptence(acceptenceViewModel);
                    if (!(acceptenceResult.IsSucceeded && acceptenceResult.Data))
                    {
                        return ProcessResultViewModelHelper.Failed(false, acceptenceResult.Status.Message);
                    }
                    var notificationRes = await NotifyDispatcherWithOrderAcceptence(acceptenceViewModel);
                }
                else if (continueValue == model.WorkingTypeName)
                {

                }
            }

            ProcessResult<OrderAction> addedOrderAction = new ProcessResult<OrderAction>();
            if (model.OrderId > 0)
            {
                var order = Get(model.OrderId).Data;
                orderObject = Mapper.Map<OrderObject>(order);
                addedOrderAction = await AddAction(orderObject, actionType, model.WorkingTypeId, model.WorkingTypeName);
            }
            else
            {
                addedOrderAction = await AddAction(null, actionType, model.WorkingTypeId, model.WorkingTypeName);
            }

            if (addedOrderAction.IsSucceeded)
            {
                var technicianState = mapper.Map<WorkingViewModel, TechnicianStateViewModel>(model);
                var result = await _technicianStateService.Add(technicianState);
                if (result.IsSucceeded && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succeeded(true);
                }
                return ProcessResultViewModelHelper.Failed(false, result.Status.Message);
            }
            return ProcessResultViewModelHelper.Failed(false, addedOrderAction.Status.Message);
        }

        [HttpGet]
        [Route("GetOrderActionTypes")]
        public ProcessResult<List<EnumEntity>> GetOrderActionTypes()
        {
            return ProcessResultHelper.Succeeded(EnumManager<OrderActionType>.GetEnumList());
        }

        [HttpPost]
        [Route("OrderFilter")]
        public async Task<ProcessResultViewModel<List<OrderViewModel>>> OrderFilter(
            [FromBody] OrderFilterViewModel model)
        {
            var predicate = PredicateBuilder.True<OrderObject>();
            List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;
            List<int> teamsId = new List<int>();
            List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
            List<int> unassignedStatusesId = StaticAppSettings.UnassignedStatusId;

            if (model.AreaId != null && model.AreaId.Any())
            {
                predicate = predicate.And(r => model.AreaId.Contains(r.AreaId));
            }
            if (model.DispatcherId != null && model.DispatcherId.Any())
            {
                predicate = predicate.And(r => model.DispatcherId.Contains(r.DispatcherId));
            }
            if (model.DivisionId != null && model.DivisionId.Any())
            {
                predicate = predicate.And(r => model.DivisionId.Contains(r.DivisionId));
            }
            if (model.OrderTypeId != null && model.OrderTypeId.Any())
            {
                predicate = predicate.And(r => model.OrderTypeId.Contains(r.TypeId));
            }
            if (model.ProblemId != null && model.ProblemId.Any())
            {
                predicate = predicate.And(r => model.ProblemId.Contains(r.ProblemId));
            }
            if (model.Assigned == (int)OrderStatusEnum.Assigned)
            {
                predicate = predicate.And(r => !deactiveStatusesId.Contains(r.StatusId) &&
                                               !unassignedStatusesId.Contains(r.StatusId));
            }
            else if (model.Assigned == (int)OrderStatusEnum.Unassigned)
            {
                predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId));
            }
            else if (model.Assigned == (int)OrderStatusEnum.Both)
            {
                predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId) ||
                                               !deactiveStatusesId.Contains(r.StatusId) &&
                                               !unassignedStatusesId.Contains(r.StatusId));
            }
            if (model.StatusId != null && model.StatusId.Any())
            {
                predicate = predicate.And(r => model.StatusId.Contains(r.StatusId));
            }
            if (model.TechnicianId != null && model.TechnicianId.Any())
            {
                foreach (var technician in model.TechnicianId)
                {
                    var res = await _teamMembersService.GetTeamIdByTechnicianMember(technician);
                    teamsId.Add(res.Data.FirstOrDefault().TeamId);
                }
                predicate = predicate.And(r => teamsId.Contains(r.TeamId));
            }

            if (model.DateFrom != null && model.DateTo != null)
            {
                predicate = predicate.And(x => (x.CreatedDate >= model.DateFrom) && (x.CreatedDate <= model.DateTo));
            }
            else if (model.DateFrom == null && model.DateTo == null)
            {
                predicate = predicate.And(x => true);
            }
            else if (model.DateFrom == null)
            {
                predicate = predicate.And(x => x.CreatedDate <= model.DateTo);
            }
            else if (model.DateTo == null)
            {
                predicate = predicate.And(x => x.CreatedDate >= model.DateFrom);
            }
            var paginatedRes = _manager.GetAll(predicate);

            return _processResultMapper.Map<List<OrderObject>, List<OrderViewModel>>(paginatedRes);
        }

        [HttpPost]
        [Route("UnassignedOrdersBoardFilter")]
        public ProcessResultViewModel<List<OrderViewModel>> UnassignedOrdersBoardFilter(
            [FromBody] OrdersboardFilterViewModel model)
        {
            var predicate = PredicateBuilder.True<OrderObject>();

            List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;

            if (model.OrderCode != null && model.OrderCode != "")
            {
                predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            }

            if (model.CustomerCode != null && model.CustomerCode != "")
            {
                predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            }

            if (model.CustomerName != null && model.CustomerName != "")
            {
                predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            }

            if (model.CustomerPhone != null && model.CustomerPhone != "")
            {
                predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) ||
                                               r.PhoneTwo.Contains(model.CustomerPhone) ||
                                               r.Caller_ID.Contains(model.CustomerPhone));
            }

            if (model.AreaIds != null && model.AreaIds.Any())
            {
                predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            }

            if (model.BlockName != null && model.BlockName != "")
            {
                predicate = predicate.And(r => r.BlockName.Contains(model.BlockName));
            }

            if (model.ProblemIds != null && model.ProblemIds.Any())
            {
                predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId));
            }

            if (model.StatusIds != null && model.StatusIds.Any())
            {
                predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            }

            if (model.Rejection == (int)RejectionEnum.NoAction)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            }
            else if (model.Rejection == (int)RejectionEnum.Rejected)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            }
            else if (model.Rejection == (int)RejectionEnum.Accepted)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            }
            else if (model.Rejection == (int)RejectionEnum.All)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected ||
                                               r.AcceptanceFlag == AcceptenceType.Accepted ||
                                               r.AcceptanceFlag == AcceptenceType.NoAction);
            }

            predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId));
            predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);

            var paginatedRes = _manager.GetAll(predicate);

            return _processResultMapper.Map<List<OrderObject>, List<OrderViewModel>>(paginatedRes);
        }

        [HttpPost]
        [Route("assignedOrdersBoardFilter")]
        public async Task<ProcessResult<List<DispatcherAssignedOrdersViewModel>>> AssignedOrdersBoardFilter(
            [FromBody] OrdersboardFilterViewModel model)
        {
            var predicate = PredicateBuilder.True<OrderObject>();
            List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
            List<int> unassignedStatusesId = StaticAppSettings.UnassignedStatusId;

            if (!string.IsNullOrEmpty(model.OrderCode))
            {
                predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            }

            if (!string.IsNullOrEmpty(model.CustomerCode))
            {
                predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            }

            if (!string.IsNullOrEmpty(model.CustomerName))
            {
                predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            }

            if (model.CustomerPhone != null && model.CustomerPhone != "")
            {
                predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) ||
                                               r.PhoneTwo.Contains(model.CustomerPhone) ||
                                               r.Caller_ID.Contains(model.CustomerPhone));
            }

            if (model.AreaIds != null && model.AreaIds.Any())
            {
                predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            }

            if (!string.IsNullOrEmpty(model.BlockName))
            {
                predicate = predicate.And(r => r.BlockName.Contains(model.BlockName));
            }

            if (model.ProblemIds != null && model.ProblemIds.Any())
            {
                predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId));
            }

            if (model.StatusIds != null && model.StatusIds.Any())
            {
                predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            }

            if (model.Rejection == (int)RejectionEnum.NoAction)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            }
            else if (model.Rejection == (int)RejectionEnum.Rejected)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            }
            else if (model.Rejection == (int)RejectionEnum.Accepted)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            }
            else if (model.Rejection == (int)RejectionEnum.All)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected ||
                                               r.AcceptanceFlag == AcceptenceType.Accepted ||
                                               r.AcceptanceFlag == AcceptenceType.NoAction);
            }

            predicate = predicate.And(r => !deactiveStatusesId.Contains(r.StatusId) &&
                                           !unassignedStatusesId.Contains(r.StatusId));
            predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);

            var ordersRes = _manager.GetAll(predicate);
            var dispatcherTeams = await _teamService.GetTeamsByDispatcherId(model.DispatcherId);
            var currentTeams = mapper.Map<List<TeamViewModel>, List<TeamOrdersViewModel>>(dispatcherTeams.Data);

            if (ordersRes.IsSucceeded)
            {
                if (ordersRes.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(ordersRes.Data);
                    var groups = result.GroupBy(x => x.TeamId,
                        (key, group) => new TeamOrdersViewModel
                        {
                            TeamId = key,
                            Orders = group.OrderBy(x => x.RankInTeam)
                        }).ToList();

                    if (groups.Count > 0)
                    {
                        if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                        {

                            for (int i = 0; i < currentTeams.Count; i++)
                            {
                                currentTeams[i].Orders = new List<OrderViewModel>();
                                currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                                currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                                for (int j = 0; j < groups.Count; j++)
                                {
                                    if (currentTeams[i].TeamId == groups[j].TeamId)
                                    {
                                        currentTeams[i].Orders = groups[j].Orders;
                                    }
                                }
                            }
                        }
                    }
                    // here add foreman group binding
                    var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName },
                        (key, group) => new DispatcherAssignedOrdersViewModel
                        {
                            ForemanId = key.ForemanId,
                            ForemanName = key.ForemanName,
                            Teams = group
                        }).ToList();
                    return ProcessResultHelper.Succeeded(newGroup);
                }
                else
                {
                    if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                    {

                        for (int i = 0; i < currentTeams.Count; i++)
                        {
                            currentTeams[i].Orders = new List<OrderViewModel>();
                            currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                            currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                        }
                    }
                    // here add foreman group binding
                    var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName },
                        (key, group) => new DispatcherAssignedOrdersViewModel
                        {
                            ForemanId = key.ForemanId,
                            ForemanName = key.ForemanName,
                            Teams = group
                        }).ToList();
                    return ProcessResultHelper.Succeeded(newGroup);
                }
            }

            return ProcessResultHelper.Succeeded(
                new List<DispatcherAssignedOrdersViewModel>());
        }

        private Expression<Func<OrderObject, bool>> CreateOrdersFilterPredicate(List<Filter> filters,
            bool includeUnAssign)
        {
            var predicate = PredicateBuilder.True<OrderObject>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(
                        PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<OrderObject>(filter.PropertyName,
                            filter.Value));
                }
                else
                {
                    predicate = predicate.And(
                        PredicateBuilder.CreateEqualSingleExpression<OrderObject>(filter.PropertyName, filter.Value));
                }
            }
            if (includeUnAssign)
            {
                predicate = predicate.And(x => !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            }
            else
            {
                List<int> excludeStatusIds = StaticAppSettings.ArchivedStatusId;
                excludeStatusIds.Add(StaticAppSettings.InitialStatus.Id);
                predicate = predicate.And(x => !excludeStatusIds.Contains(x.StatusId));
            }
            return predicate;
        }

        private async Task<ProcessResult<OrderAction>> AddAction(OrderObject order, OrderActionType actionType,
            int? workingTypeId, string workingTypeName)
        {
            ///// NOTE :: We have to do the below on all of team in case the user is technician
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);
            if (string.IsNullOrEmpty(currentUserId))
            {
                return ProcessResultHelper.Failed<OrderAction>(null, null, "Unauthorized");
            }
            var userRes = await _identityService.GetUserById(currentUserId);
            string costCenterName = null;
            int? costCenterId = 0;
            int teamId = 0;
            // get user data
            var orderAction = new OrderAction();
            var orderActions = new List<OrderAction>();

            orderAction.ActionDate = DateTime.Now;
            orderAction.ActionDay = DateTime.Now.Date;
            //if (UserRes != null && UserRes.Data != null)
            //{
            var roleNames = userRes.Data.RoleNames.FirstOrDefault();
            // var roleNames = "Technician";
            switch (roleNames)
            {
                case "Engineer":
                    var engineer = await _engineerService.GetEngineerByUserId(currentUserId);
                    if (engineer != null && engineer.Data != null)
                    {
                        costCenterId = engineer.Data.CostCenterId;
                        costCenterName = engineer.Data.CostCenterName;
                        orderAction.CreatedUser = engineer.Data.Name;
                        orderAction.TeamId = order.TeamId;
                        CurrentUserId = currentUserId;
                    }
                    break;

                case "Technician":
                    var technician = await _technicianService.GetTechnicianByUserId(currentUserId);
                    if (technician?.Data != null)
                    {
                        costCenterId = technician.Data.CostCenterId;
                        costCenterName = technician.Data.CostCenterName;
                        orderAction.TeamId = technician.Data.TeamId;
                        teamId = technician.Data.TeamId;
                        orderAction.CreatedUser = technician.Data.Name;
                        orderAction.CurrentUserId = technician.Data.UserId;
                        CurrentUserId = currentUserId;

                        var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(teamId);
                        if (teamMembers.IsSucceeded && teamMembers.Data != null)
                        {
                            for (int i = 0; i < teamMembers.Data.Count; i++)
                            {
                                if (technician.Data.Name != teamMembers.Data[i].MemberParentName)
                                {
                                    orderActions.Add(new OrderAction
                                    {
                                        CostCenterId = teamMembers.Data[i].CostCenterId,
                                        CostCenterName = teamMembers.Data[i].CostCenter,
                                        TeamId = teamMembers.Data[i].TeamId,
                                        ActionDate = orderAction.ActionDate,
                                        CreatedUser = teamMembers.Data[i].MemberParentName,
                                        CurrentUserId = teamMembers.Data[i].UserId,
                                        ActionDay = DateTime.Now.Date
                                    });
                                }
                            }
                        }

                    }
                    break;

                case "Foreman":
                    var foreman = await _formanService.GetForemanByUserId(currentUserId);
                    if (foreman?.Data != null)
                    {
                        costCenterId = foreman.Data.CostCenterId;
                        costCenterName = foreman.Data.CostCenterName;
                        orderAction.CreatedUser = foreman.Data.Name;
                        orderAction.TeamId = order.TeamId;
                        CurrentUserId = currentUserId;
                    }
                    break;

                case "Supervisor":
                    var supervisor = await _supervisorService.GetSupervisorByUserId(currentUserId);
                    if (supervisor != null && supervisor.Data != null)
                    {
                        costCenterId = supervisor.Data.CostCenterId;
                        costCenterName = supervisor.Data.CostCenterName;
                        orderAction.CreatedUser = supervisor.Data.Name;
                        orderAction.TeamId = order.TeamId;
                        CurrentUserId = currentUserId;
                    }
                    break;

                case "Dispatcher":
                    var dispatcher = await _dispatcherService.GetDispatcherByUserId(currentUserId);
                    if (dispatcher != null && dispatcher.Data != null)
                    {
                        costCenterId = dispatcher.Data.CostCenterId;
                        costCenterName = dispatcher.Data.CostCenterName;
                        orderAction.CreatedUser = dispatcher.Data.Name;
                        orderAction.TeamId = order.TeamId;
                        CurrentUserId = currentUserId;
                    }
                    break;

                default:
                    break;
                    //}
            }

            // update action time in last
            if (orderAction.TeamId > 0)
            {
                var lastActions = _actionManager.GetAll(x => x.TeamId == orderAction.TeamId && x.ActionTime == null).Data
                    .ToList();
                if (lastActions.Count > 0)
                {
                    for (int i = 0; i < lastActions.Count; i++)
                    {
                        lastActions[i].ActionTime = orderAction.ActionDate - lastActions[i].ActionDate;
                    }
                    _actionManager.Update(lastActions);
                }
            }

            //if (actionType == OrderActionType.Acceptence || actionType == OrderActionType.Rejection)
            //{

            //}

            // binding order action details
            if (order != null)
            {
                orderAction.ServeyReport = order.ServeyReport;
                orderAction.StatusId = order.StatusId;
                orderAction.StatusName = order.StatusName;
                orderAction.OrderId = order.Id;
                orderAction.SubStatusId = order.SubStatusId;
                orderAction.SubStatusName = order.SubStatusName;
                orderAction.ActionTypeId = (int)actionType;
                orderAction.ActionTypeName = EnumManager<OrderActionType>.GetName(actionType);
                orderAction.SupervisorId = order.SupervisorId;
                orderAction.DispatcherId = order.DispatcherId;
                orderAction.DispatcherName = order.DispatcherName;
                orderAction.SupervisorName = order.SupervisorName;
                orderAction.TeamId = orderAction.TeamId;
                orderAction.CurrentUserId = currentUserId;
                orderAction.CostCenterId = costCenterId;
                orderAction.CostCenterName = costCenterName;

            }
            else
            {
                orderAction.ActionTypeId = (int)actionType;
                orderAction.ActionTypeName = EnumManager<OrderActionType>.GetName(actionType);
                orderAction.CurrentUserId = currentUserId;
                orderAction.CostCenterId = costCenterId;
                orderAction.CostCenterName = costCenterName;
            }



            if (order != null)
            {
                if (actionType == OrderActionType.Acceptence || actionType == OrderActionType.Rejection)
                {
                    if (orderAction.TeamId > 0)
                    {
                        var lastActions = _actionManager
                            .GetAll(x => x.TeamId == orderAction.TeamId &&
                                         x.ActionTypeId != (int)OrderActionType.Assign &&
                                         x.ActionTypeId != (int)OrderActionType.UnAssgin).Data
                            .OrderBy(r => r.CreatedDate).ToList();
                        orderAction.WorkingTypeId = lastActions.LastOrDefault().WorkingTypeId;
                        orderAction.WorkingTypeName = lastActions.LastOrDefault().WorkingTypeName;
                        orderAction.Reason = lastActions.LastOrDefault().Reason;
                    }

                }
                else if (StaticAppSettings.TravellingStatusId.Contains(order.StatusId))
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Traveling;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling);

                }
                else
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
                }
            }
            else
            {
                orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
                orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
                ///// for time sheet groups
                if (actionType == OrderActionType.Idle)
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Idle;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle);
                    orderAction.Reason = workingTypeName;
                }
                else if (actionType == OrderActionType.Break)
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Break;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Break);
                    orderAction.Reason = workingTypeName;
                }
            }
            
            if (workingTypeName == "End Work")
            {
                orderAction.Reason = "End Work";
            }

            // binding order actions on list
            if (orderActions.Count > 0)
            {
                for (int i = 0; i < orderActions.Count; i++)
                {
                    if (order != null)
                    {
                        orderActions[i].StatusId = order.StatusId;
                        orderActions[i].StatusName = order.StatusName;
                        orderActions[i].OrderId = order.Id;
                        orderActions[i].SubStatusId = order.SubStatusId;
                        orderActions[i].SubStatusName = order.SubStatusName;
                        orderActions[i].SupervisorId = order.SupervisorId;
                        orderActions[i].DispatcherId = order.DispatcherId;
                        orderActions[i].DispatcherName = order.DispatcherName;
                        orderActions[i].SupervisorName = order.SupervisorName;
                        orderActions[i].ServeyReport = order.ServeyReport;

                    }
                    orderActions[i].ActionTypeId = orderAction.ActionTypeId;
                    orderActions[i].ActionTypeName = orderAction.ActionTypeName;
                    orderActions[i].TeamId = orderAction.TeamId;
                    orderActions[i].WorkingTypeId = orderAction.WorkingTypeId;
                    orderActions[i].WorkingTypeName = orderAction.WorkingTypeName;
                    orderActions[i].CurrentUserId = currentUserId;
                }
                _actionManager.Add(orderActions);
            }

            return _actionManager.Add(orderAction);
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithNewOrders(int teamId,
            List<int> orderIds)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            if (teamMembers?.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    foreach (var orderId in orderIds)
                    {
                        var orderRes = _manager.Get(orderId);
                        var dispatcherRes = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
                        notificationModels.Add(new SendNotificationViewModel
                        {
                            UserId = item.UserId,
                            Title = "New Order",
                            Body =
                                $"Dispatcher {dispatcherRes.Data.Name} assigned New order No. {orderRes.Data.Code} to your team",
                            Data = new { OrderId = orderId, NotificationType = "New Order" }
                        });
                        _notificationCenterManageManager.Add(new NotificationCenter
                        {
                            RecieverId = item.UserId,
                            Title = "New Order",
                            Body =
                                $"Dispatcher {dispatcherRes.Data.Name} assigned New order No. {orderRes.Data.Code} to your team",
                            Data = orderId.ToString(),
                            NotificationType = "New Order",
                            IsRead = false
                        });
                    }

                }

                notificationRes = await _notificationService.SendNotifications(notificationModels);
            }
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithNewOrder(int teamId,
            OrderViewModel order)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(teamId);
            var dispatcherRes = await _dispatcherService.GetDispatcherById(order.DispatcherId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();

            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    notificationModels.Add(new SendNotificationViewModel
                    {
                        UserId = item.UserId,
                        Title = "New Order",
                        Body = $"Dispatcher {dispatcherRes.Data.Name} assigned New order No. {order.Code} to your team",
                        Data = new { OrderId = order.Id, NotificationType = "New Order" }
                    });

                    _notificationCenterManageManager.Add(new NotificationCenter
                    {
                        RecieverId = item.UserId,
                        Title = "New Order",
                        Body = $"Dispatcher {dispatcherRes.Data.Name} assigned New order No. {order.Code} to your team",
                        Data = order.Id.ToString(),
                        NotificationType = "New Order",
                        IsRead = false
                    });
                }

                notificationRes = await _notificationService.SendNotifications(notificationModels);
            }
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithUnassignOrder(int teamId, int orderId)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var orderRes = _manager.Get(orderId);
            var dispatcherRes = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();

            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    notificationModels.Add(new SendNotificationViewModel
                    {
                        UserId = item.UserId,
                        Title = "Unassigned Order",
                        Body =
                            $"Dispatcher {dispatcherRes.Data.Name} unassigned order No. {orderRes.Data.Code} to another team",
                        Data = new
                        {
                            OrderId = orderId,
                            NotificationType = "Unassigned Order"
                        }
                    });

                    _notificationCenterManageManager.Add(new NotificationCenter
                    {
                        RecieverId = item.UserId,
                        Title = "Un assign Order",
                        Body =
                            $"Dispatcher {dispatcherRes.Data.Name} unassigned order No. {orderRes.Data.Code} to another team",
                        Data = orderId.ToString(),
                        NotificationType = "Unassigned Order",
                        IsRead = false
                    });
                }

                notificationRes = await _notificationService.SendNotifications(notificationModels);
            }

            return notificationRes;
        }

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewOrders(int dispatcherId, List<int> OrderIds)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var dispatcher = await dispatcherService.GetDispatcherById(dispatcherId);
        //    List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
        //    if (dispatcher != null && dispatcher.Data != null)
        //    {
        //        foreach (var OrderId in OrderIds)
        //        {
        //            notificationModels.Add(new SendNotificationViewModel()
        //            {
        //                UserId = dispatcher.Data.UserId,
        //                Title = "New Order",
        //                Body = "There is new Order With Id: " + OrderId,
        //                Data = new { OrderId = OrderId }
        //            });
        //        }
        //        notificationRes = await notificationService.SendNotifications(notificationModels);
        //    }
        //    return notificationRes;
        //}

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewOrder(int dispatcherId, int orderId)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var dispatcher = await dispatcherService.GetDispatcherById(dispatcherId);
        //    SendNotificationViewModel notificationModel = new SendNotificationViewModel()
        //    {
        //        UserId = dispatcher.Data.UserId,
        //        Title = "New Order",
        //        Body = "There is new Order With Id: " + orderId,
        //        Data = new { OrderId = orderId }
        //    };
        //    notificationRes = await notificationService.SendNotification(notificationModel);
        //    return notificationRes;
        //}

        private async Task<ProcessResultViewModel<string>>
            NotifyDispatcherWithOrderAcceptence(AcceptenceViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);
            if (string.IsNullOrEmpty(currentUserId))
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var userRes = await _identityService.GetUserById(currentUserId);
            var technicianRes = await _technicianService.GetTechnicianByUserId(currentUserId);
            var orderRes = _manager.Get(model.OrderId);
            var dispatcher = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            SendNotificationViewModel notificationModel;
            var userId = dispatcher.Data.UserId;
            var orderId = model.OrderId;
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (technicianRes != null && technicianRes.Data != null && dispatcher != null && dispatcher.Data != null)
            {
                if (model.AcceptenceFlag)
                {
                    notificationModel = new SendNotificationViewModel
                    {
                        UserId = dispatcher.Data.UserId,
                        Title = "Acceptance Order",
                        Body = $"Technician {technicianRes.Data.Name} Accepted order No. {orderRes.Data.Code}.",
                        Data = new
                        {
                            order = JsonConvert.DeserializeObject(jsonOrder),
                            NotificationType = "Acceptance Order"
                        }
                    };
                    _notificationCenterManageManager.Add(new NotificationCenter
                    {
                        RecieverId = userId,
                        Title = "Acceptance Order",
                        Body = $"Technician {technicianRes.Data.Name} Accepted order No. {orderRes.Data.Code}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Acceptance Order",
                        IsRead = false
                    });
                }
                else
                {
                    notificationModel = new SendNotificationViewModel
                    {
                        UserId = dispatcher.Data.UserId,
                        Title = "Acceptance Order",
                        Body =
                            $"Technician {technicianRes.Data.Name} rejected order No. {orderRes.Data.Code} as {model.RejectionReason}.",
                        Data = new
                        {
                            order = JsonConvert.DeserializeObject(jsonOrder),
                            NotificationType = "Acceptance Order"
                        }
                    };
                    _notificationCenterManageManager.Add(new NotificationCenter
                    {
                        RecieverId = userId,
                        Title = "Acceptance Order",
                        Body =
                            $"Technician {technicianRes.Data.Name} rejected order No. {orderRes.Data.Code} as {model.RejectionReason}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Acceptance Order",
                        IsRead = false
                    });
                }
                notificationRes = await _notificationService.SendNotification(notificationModel);
            }
            return notificationRes;
        }

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderChange(AcceptenceViewModel model)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var orderRes = manager.Get(model.OrderId);
        //    var dispatcher = await dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
        //    SendNotificationViewModel notificationModel;
        //    var userId = dispatcher.Data.UserId;
        //    var orderId = model.OrderId;
        //    if (model.AcceptenceFlag)
        //    {
        //        notificationModel = new SendNotificationViewModel()
        //        {
        //            UserId = userId,
        //            Title = "Acceptence Order",
        //            Body = $"The order With Id: { orderId } is accepted",
        //            Data = new { Order = orderRes }
        //        };
        //    }
        //    else
        //    {
        //        notificationModel = new SendNotificationViewModel()
        //        {
        //            UserId = dispatcher.Data.UserId,
        //            Title = "Acceptence Order",
        //            Body = "The order With Id: " + model.OrderId + "is rejected as " + model.RejectionReason,
        //            Data = new { Order = orderRes }
        //        };
        //    }
        //    notificationRes = await notificationService.SendNotification(notificationModel);
        //    return notificationRes;
        //}

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersandDispatcherWithNewStatus(
            ChangeStatusViewModel model)
        {
            ProcessResultViewModel<string> notificationRes;
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);

            if (string.IsNullOrEmpty(currentUserId))
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }

            var userRes = await _identityService.GetUserById(currentUserId);
            var technicianRes = await _technicianService.GetTechnicianByUserId(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = _manager.Get(model.OrderId);
            var dispatcherRes = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);

            if (orderRes.Data.TeamId != 0)
            {
                var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId);

                if (teamMembers != null && teamMembers.Data != null)
                {
                    foreach (var item in teamMembers.Data)
                    {
                        notificationModels.Add(new SendNotificationViewModel
                        {
                            UserId = item.UserId,
                            Title = "New Status",
                            Body =
                                $"Dispatcher {dispatcherRes.Data.Name} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                            Data = new
                            {
                                OrderId = model.OrderId,
                                NotificationType = "New Status"
                            }
                        });

                        _notificationCenterManageManager.Add(new NotificationCenter
                        {
                            RecieverId = item.UserId,
                            Title = "New Status",
                            Body =
                                $"Dispatcher {dispatcherRes.Data.Name} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                            Data = model.OrderId.ToString(),
                            NotificationType = "New Status",
                            IsRead = false
                        });
                    }
                }

            }
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (dispatcherRes != null && dispatcherRes.Data != null)
            {
                notificationModels.Add(new SendNotificationViewModel
                {
                    UserId = dispatcherRes.Data.UserId,
                    Title = "New Status",
                    Body =
                        $"Technician {technicianRes.Data.Name} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }
                });
                _notificationCenterManageManager.Add(new NotificationCenter
                {
                    RecieverId = dispatcherRes.Data.UserId,
                    Title = "New Status",
                    Body =
                        $"Technician {technicianRes.Data.Name} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                    Data = model.OrderId.ToString(),
                    NotificationType = "",
                    IsRead = false
                });
            }
            notificationRes = await _notificationService.SendNotifications(notificationModels);
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewStatus(ChangeStatusViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var userRes = await _identityService.GetUserById(currentUserId);
            var technicianRes = await _technicianService.GetTechnicianByUserId(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = _manager.Get(model.OrderId);
            var dispatcherRes = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (dispatcherRes != null && dispatcherRes.Data != null && technicianRes != null &&
                technicianRes.Data != null)
            {
                notificationModels.Add(new SendNotificationViewModel
                {
                    UserId = dispatcherRes.Data.UserId,
                    Title = "New Status",
                    Body =
                        $"Technician {technicianRes.Data.Name} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }
                });
                _notificationCenterManageManager.Add(new NotificationCenter
                {
                    RecieverId = dispatcherRes.Data.UserId,
                    Title = "New Status",
                    Body =
                        $"Technician {technicianRes.Data.Name} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                    Data = model.OrderId.ToString(),
                    NotificationType = "",
                    IsRead = false
                });
                notificationRes = await _notificationService.SendNotifications(notificationModels);
            }
            return notificationRes;
        }

        [HttpPost]
        [Route("NotifyDispatcherWithOrderChangeRank")]
        public async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderChangeRank(
            [FromBody] ChangeRankInTeamViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);
            if (string.IsNullOrEmpty(currentUserId))
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var userRes = await _identityService.GetUserById(currentUserId);
            var technicianRes = await _technicianService.GetTechnicianByUserId(currentUserId);
            var orderRes = _manager.Get(model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key);
            var dispatcher = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            SendNotificationViewModel notificationModel;
            var userId = dispatcher.Data.UserId;

            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            notificationModel = new SendNotificationViewModel
            {
                UserId = userId,
                Title = "Change order priority",
                Body =
                    $"Technician {technicianRes.Data.Name} requests changing the priority of order No. {orderRes.Data.Code} to be able to work on it.",
                Data = new
                {
                    order = JsonConvert.DeserializeObject(jsonOrder),
                    NotificationType = "Change order priority",
                    rankInTeamModel = model
                }
            };
            _notificationCenterManageManager.Add(new NotificationCenter
            {
                RecieverId = userId,
                Title = "Change order priority",
                Body =
                    $"Technician {technicianRes.Data.Name} requests changing the priority of order No. {orderRes.Data.Code} to be able to work on it.",
                Data = JsonConvert.SerializeObject(model),
                NotificationType = "Change order priority",
                IsRead = false
            });

            notificationRes = await _notificationService.SendNotification(notificationModel);
            return notificationRes;
        }

        [HttpPut]
        [Route("ChangeOrderRank/{acceptChange}")]
        public async Task<ProcessResult<bool>> ChangeOrderRankWeb([FromRoute] bool acceptChange,
            [FromBody] ChangeRankInTeamViewModel model)
        {
            if (model == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed(false, null, "team id and order id and rank are required");
            }
            if (acceptChange)
            {

                var teamOrders = _manager.GetAll(x => x.TeamId == model.TeamId &&
                                                     !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
                {
                    var changeRes = _manager.ChangeOrderRank(teamOrders.Data, model.Orders);
                    teamOrders = _manager.GetAll(x => x.TeamId == model.TeamId &&
                                                     !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                    if (changeRes.IsSucceeded && changeRes.Data)
                    {
                        await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);
                        AcceptenceViewModel acceptenceModel =
                            new AcceptenceViewModel
                            {
                                AcceptenceFlag = true,
                                OrderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key
                            };
                        var notificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                        return ProcessResultHelper.Succeeded(true);
                    }
                    return ProcessResultHelper.Failed(false, changeRes.Exception);
                }
            }
            else
            {
                AcceptenceViewModel acceptenceModel =
                    new AcceptenceViewModel
                    {
                        AcceptenceFlag = false,
                        OrderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key
                    };
                var notificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                return ProcessResultHelper.Succeeded(true);
            }

            return ProcessResultHelper.Failed(false, null, "something wrong while getting team orders",
                ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }

        [HttpGet]
        [Route("GetDispatcherOrders")]
        public ProcessResult<List<DispatcherOrdersViewModel>> GetDispatcherOrders(int supervisorId)
        {
            var dispatchersOrdersList = new List<DispatcherOrdersViewModel>();
            var orders = _manager.GetDispatcherOrders(supervisorId, StaticAppSettings.InitialStatus.Id);
            if (orders.Data != null)
            {
                var dispatchersOrders = orders.Data.GroupBy(x => new { x.DispatcherId, x.DispatcherName }).ToList();
                foreach (var item in dispatchersOrders)
                {
                    foreach (var order in item)
                    {
                        var orderList = new List<OrderViewModel>();
                        var orderItem = Mapper.Map<OrderViewModel>(order);
                        orderList.Add(orderItem);

                        var dispatchersOrder = new DispatcherOrdersViewModel
                        {
                            DispatcherId = item.Key.DispatcherId,
                            DispatcherName = item.Key.DispatcherName,
                            Orders = orderList
                        };
                        dispatchersOrdersList.Add(dispatchersOrder);
                    }
                }
                return ProcessResultHelper.Succeeded(dispatchersOrdersList);
            }
            return ProcessResultHelper.Failed<List<DispatcherOrdersViewModel>>(null, null,
                "there are no dispatchers for this supervisor",
                ProcessResultStatusCode.Failed, "GetDispatcherOrders Controller");
        }
        //[HttpPut]
        //[Route("UpdateOrdersWithOnholdCount")]
        //public async Task<ProcessResult<bool>> UpdateOrdersWithOnholdCOunt()
        //{
        //     var OnholdOrdersActionsRes = actionManager.GetAll(x=>x.StatusId==6);
        //     if (OnholdOrdersActionsRes.IsSucceeded && OnholdOrdersActionsRes.Data.Count()>0)
        //     {
        //        foreach (var order in OnholdOrdersActionsRes.Data)
        //        {
        //            var orderRes = manager.Get(order.OrderId);
        //            orderRes.Data.OnHoldCount = orderRes.Data.OnHoldCount + 1;
        //            var changeOnholdCount = manager.Update(orderRes.Data);
        //        }
        //         return ProcessResultHelper.Succeeded<bool>(true);
        //     }
        //     else
        //     {
        //         return ProcessResultHelper.Failed<bool>(false, OnholdOrdersActionsRes.Exception);
        //     }
        //}


        private async Task<ProcessResultViewModel<string>> NotifyTechnicianWithOrderAcceptencechange(
            AcceptenceViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var orderRes = _manager.Get(model.OrderId);
            var dispatcherRes = await _dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            List<SendNotificationViewModel> notificationModelLst = new List<SendNotificationViewModel>();
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (model.AcceptenceFlag)
            {
                if (orderRes.Data.TeamId != 0)
                {
                    var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId);
                    if (teamMembers != null && teamMembers.Data != null && dispatcherRes != null &&
                        dispatcherRes.Data != null)
                    {
                        foreach (var item in teamMembers.Data)
                        {
                            notificationModelLst.Add(new SendNotificationViewModel
                            {
                                UserId = item.UserId,
                                Title = "Change order priority",
                                Body =
                                    $"Dispatcher {dispatcherRes.Data.Name} changed the priority of order No. {orderRes.Data.Code}.",
                                Data = new
                                {
                                    order = JsonConvert.DeserializeObject(jsonOrder),
                                    NotificationType = "Change order priority"
                                }
                            });
                            _notificationCenterManageManager.Add(new NotificationCenter
                            {
                                RecieverId = item.UserId,
                                Title = "Change order priority",
                                Body =
                                    $"Dispatcher {dispatcherRes.Data.Name} changed the priority of order No. {orderRes.Data.Code}.",
                                Data = orderRes.Data.Id.ToString(),
                                NotificationType = "Change order priority",
                                IsRead = false
                            });
                        }
                    }
                }
            }
            else
            {
                if (orderRes.Data.TeamId != 0)
                {
                    var teamMembers = await _teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId);
                    if (teamMembers != null && teamMembers.Data != null && dispatcherRes != null &&
                        dispatcherRes.Data != null)
                    {
                        foreach (var item in teamMembers.Data)
                        {
                            notificationModelLst.Add(new SendNotificationViewModel
                            {
                                UserId = item.UserId,
                                Title = "Change order priority",
                                Body =
                                    $"Dispatcher {dispatcherRes.Data.Name} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                Data = new
                                {
                                    order = JsonConvert.DeserializeObject(jsonOrder),
                                    NotificationType = "Change order priority"
                                }
                            });
                            _notificationCenterManageManager.Add(new NotificationCenter
                            {
                                RecieverId = item.UserId,
                                Title = "Change order priority",
                                Body =
                                    $"Dispatcher {dispatcherRes.Data.Name} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                Data = orderRes.Data.Id.ToString(),
                                NotificationType = "Change order priority",
                                IsRead = false
                            });
                        }
                    }
                }
            }
            if (notificationModelLst.Count > 0)
            {
                notificationRes = await _notificationService.SendNotifications(notificationModelLst);
            }
            return notificationRes;
        }

        [HttpPost]
        [Route("PrintOut")]
        public async Task<ProcessResultViewModel<List<PrintOutResultViewModel>>> PrintOut(
            [FromBody] PrintOutFilterViewModel model)
        {
            ProcessResultViewModel<List<PrintOutResultViewModel>> result =
                new ProcessResultViewModel<List<PrintOutResultViewModel>>();
            List<PrintOutResultViewModel> modelLst = new List<PrintOutResultViewModel>();
            List<ForemenWithOrders> formanWithOrderLst = new List<ForemenWithOrders>();
            List<ForemenWithOrders> ordersRes = new List<ForemenWithOrders>();
            try
            {
                if (model.IncludeOpenOrders)
                {
                    if (model.teamsId == null)
                    {
                        ordersRes = _manager.GetAll().Data
                            .Where(x => (x.DispatcherId == model.DispatcherId) &&
                                        (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId)))
                            .GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId },
                                (key, group) => new ForemenWithOrders
                                {
                                    AreaId = key.AreaId,
                                    AreaName = key.AreaName,
                                    PrevTeamId = key.PrevTeamId,
                                    Orders = group.Select(q => Mapper.Map<OrderViewModel>(q))
                                }).ToList();
                    }
                    else
                    {
                        ordersRes = _manager.GetAll().Data
                            .Where(x => (x.DispatcherId == model.DispatcherId) &&
                                        (model.teamsId.Contains(x.PrevTeamId) || x.PrevTeamId == 0) && x.TeamId == 0 &&
                                        (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId)))
                            .GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId },
                                (key, group) => new ForemenWithOrders
                                {
                                    AreaId = key.AreaId,
                                    AreaName = key.AreaName,
                                    PrevTeamId = key.PrevTeamId,
                                    Orders = group.Select(q => Mapper.Map<OrderViewModel>(q))
                                }).ToList();
                    }
                    foreach (var item in ordersRes.ToList())
                    {
                        foreach (var orderItem in item.Orders)
                        {
                            var orderActions = _actionManager.GetAll().Data.Where(x => x.OrderId == orderItem.Id)
                                .ToList();
                            foreach (var actionItem in orderActions)
                            {
                                orderItem.TechId = actionItem.CreatedUserId;
                                orderItem.TechName = actionItem.CreatedUser;
                            }
                        }
                        var teamRes = await _teamService.GetTeamById(item.PrevTeamId);
                        if (teamRes != null && teamRes.Data != null)
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders
                            {
                                ForemanId = teamRes.Data.ForemanId,
                                ForemanName = teamRes.Data.ForemanName,
                                PrevTeamId = item.PrevTeamId,
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = item.Orders
                            });
                        }
                        else
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders
                            {
                                ForemanId = 0,
                                ForemanName = "",
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = item.Orders
                            });
                        }
                    }

                    modelLst.Add(new PrintOutResultViewModel
                    {
                        DispatcherId = model.DispatcherId,
                        foremenWithOrders = formanWithOrderLst
                    });
                }
                else
                {
                    if (model.teamsId == null)
                    {
                        ordersRes = _manager.GetAll().Data
                            .Where(x => (x.DispatcherId == model.DispatcherId) &&
                                        (x.StatusId == StaticAppSettings.OnHoldStatus.Id))
                            .GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId },
                                (key, group) => new ForemenWithOrders
                                {
                                    AreaId = key.AreaId,
                                    AreaName = key.AreaName,
                                    PrevTeamId = key.PrevTeamId,
                                    Orders = group.Select(q => Mapper.Map<OrderViewModel>(q))
                                }).ToList();
                    }
                    else
                    {
                        ordersRes = _manager.GetAll().Data
                            .Where(x => (x.DispatcherId == model.DispatcherId) &&
                                        (model.teamsId.Contains(x.PrevTeamId)) && x.TeamId == 0 &&
                                        (x.StatusId == StaticAppSettings.OnHoldStatus.Id))
                            .GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId },
                                (key, group) => new ForemenWithOrders
                                {
                                    AreaId = key.AreaId,
                                    AreaName = key.AreaName,
                                    PrevTeamId = key.PrevTeamId,
                                    Orders = group.Select(q => Mapper.Map<OrderViewModel>(q))
                                }).ToList();
                    }
                    foreach (var item in ordersRes.ToList())
                    {
                        var teamRes = await _teamService.GetTeamById(item.PrevTeamId);
                        if (teamRes?.Data != null)
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders
                            {
                                ForemanId = teamRes.Data.ForemanId,
                                ForemanName = teamRes.Data.ForemanName,
                                PrevTeamId = item.PrevTeamId,
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = item.Orders
                            });
                        }
                        else
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders
                            {
                                ForemanId = 0,
                                ForemanName = "",
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = item.Orders
                            });
                        }
                    }
                    modelLst.Add(new PrintOutResultViewModel
                    {
                        DispatcherId = model.DispatcherId,
                        foremenWithOrders = formanWithOrderLst
                    });
                }

                return ProcessResultViewModelHelper.Succeeded(modelLst, "PrintOut");
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Succeeded<List<PrintOutResultViewModel>>(null, ex.Message);

            }
        }

        [HttpPut]
        [Route("AssignDispatcher")]
        public ProcessResult<bool> AssignDispatcher([FromBody] AssignDispatcherViewModel model)
        {
            if (model.OrderId == 0 && model.DispatcherId == 0)
            {
                return ProcessResultHelper.Failed(false, null, "order id & dispatcher id are required");
            }
            var assigned = _manager.AssignDispatcher(model.OrderId, model.DispatcherId);

            if (assigned.Data)
            {
                return ProcessResultHelper.Succeeded(true);
            }
            return ProcessResultHelper.Failed(false, null,
                "something wrong while assign order to dispatcher");
        }

        [HttpPut]
        [Route("UnAssignDispatcher")]
        public ProcessResult<bool> UnAssignDispatcher([FromBody] AssignDispatcherViewModel model)
        {
            if (model.OrderId == 0)
            {
                return ProcessResultHelper.Failed(false, null, "order id is required");
            }
            var unAssigned = _manager.UnAssignDispatcher(model.OrderId);

            if (unAssigned.Data)
            {
                return ProcessResultHelper.Succeeded(true);
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while unassign order");
        }
    }
}