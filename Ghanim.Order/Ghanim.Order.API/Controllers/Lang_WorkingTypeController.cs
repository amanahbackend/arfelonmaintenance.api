﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_WorkingTypeController : BaseController<ILang_WorkingTypeManager, Lang_WorkingType, Lang_WorkingTypeViewModel>
    {
        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_WorkingTypeManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_WorkingTypeController(IServiceProvider serviceprovider,ISupportedLanguageService _supportedLanguageService, ILang_WorkingTypeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IWorkingTypeManager WorkingTypemanager
        {
            get
            {
                return _serviceprovider.GetService<IWorkingTypeManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByWorkingTypeId/{WorkingTypeId}")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguagesByWorkingTypeId([FromRoute]int WorkingTypeId)
        {
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var WorkingTypeRes = WorkingTypemanager.Get(WorkingTypeId);
            var entityResult = manager.GetAllLanguagesByWorkingTypeId(WorkingTypeId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = WorkingTypeRes.Data.Category, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succeeded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguages()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAll();
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var WorkingTypeRes = WorkingTypemanager.GetAll();
            foreach (var WorkingType in WorkingTypeRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByWorkingTypeId(WorkingType.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = WorkingType.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = WorkingType.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = WorkingType.Category, languagesDictionaries = languagesDictionary, Id = WorkingType.Id });
            }
            var result = ProcessResultViewModelHelper.Succeeded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_WorkingType> lstModel)
        {
            var entityResult = manager.UpdateByWorkingType(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody]CodeWithLanguagesViewModel Model)
        {
            List<Lang_WorkingType> lstModel = new List<Lang_WorkingType>();
            var WorkingTypeRes = WorkingTypemanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                lstModel.Add(new Lang_WorkingType { FK_WorkingType_ID = WorkingTypeRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByWorkingType(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}