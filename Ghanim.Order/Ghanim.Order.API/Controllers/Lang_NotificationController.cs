﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using AutoMapper;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_NotificationController : BaseController<ILang_NotificationManager, Lang_Notification, Lang_NotificationViewModel>
    {
        public ILang_NotificationManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;

        public Lang_NotificationController(ILang_NotificationManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
    }
}