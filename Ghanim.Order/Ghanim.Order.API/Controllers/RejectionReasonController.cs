﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class RejectionReasonController : BaseController<IRejectionReasonManager, RejectionReason, RejectionReasonViewModel>
    {
        IServiceProvider _serviceprovider;
        IRejectionReasonManager manager;
        IProcessResultMapper processResultMapper;
        public RejectionReasonController(IServiceProvider serviceprovider, IRejectionReasonManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
    }
}