﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderSubStatusController : BaseController<IOrderSubStatusManager, OrderSubStatus, OrderSubStatusViewModel>
    {
        IOrderSubStatusManager manager;
        IServiceProvider _serviceprovider;

        public OrderSubStatusController(IServiceProvider serviceprovider, IOrderSubStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            _serviceprovider = serviceprovider;
        }
        private ILang_OrderSubStatusManager Lang_OrderSubStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_OrderSubStatusManager>();
            }
        }
        [HttpGet]
        [Route("GetByStatusId")]
        public ProcessResult<List<OrderSubStatus>> GetByStatusId([FromQuery] int id)
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            var actions = manager.GetByStatusId(id);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderSubStatusRes = Lang_OrderSubStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderSubStatus_ID == item.Id);
                    if (OrderSubStatusRes != null && OrderSubStatusRes.Data != null)
                    {
                        item.Name = OrderSubStatusRes.Data.Name;
                    }
                }
            }
            return actions;
        }

    }
}