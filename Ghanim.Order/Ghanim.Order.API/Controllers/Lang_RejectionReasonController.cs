﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_RejectionReasonController : BaseController<ILang_RejectionReasonManager, Lang_RejectionReason, Lang_RejectionReasonViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_RejectionReasonManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_RejectionReasonController(IServiceProvider serviceprovider, ILang_RejectionReasonManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IRejectionReasonManager RejectionReasonmanager
        {
            get
            {
                return _serviceprovider.GetService<IRejectionReasonManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

    }
}