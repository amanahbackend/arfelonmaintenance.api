﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;
using CommonEnum;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService;
using CommonEnums;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService;
using Utilities.Utilites.GenericListToExcel;
using Ghanim.Order.BLL.ExcelSettings;
using System.IO;
using Utilites;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using System.Text.RegularExpressions;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderActionController : BaseController<IOrderActionManager, OrderAction, OrderActionViewModel>
    {
        public IOrderActionManager manager;
        public readonly new IMapper mapper;
        private readonly IHostingEnvironment _hostingEnv;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider _serviceprovider;
        ITeamMembersService teamMembersService;
        ITechnicianStateService technicianStateService;
        ITechnicianService technicianService;
        public OrderActionController(IServiceProvider serviceprovider, IHostingEnvironment hostingEnv, IOrderActionManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            ITechnicianStateService _technicianStateService,
            ITechnicianService _technicianService,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            _hostingEnv = hostingEnv;
             manager = _manager;
            mapper = _mapper;
            technicianStateService = _technicianStateService;
            technicianService = _technicianService;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            teamMembersService = _teamMembersService;
        }
        private IOrderManager orderManager
        {
            get
            {
                return _serviceprovider.GetService<IOrderManager>();
            }
        }
        [HttpPost]
        [Route("GetOrderProgress")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderActionViewModel>>> GetOrderProgress([FromBody]PaginatedFilterOrderActionViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<OrderActionViewModel>, PaginatedItems<OrderAction>>(model.PageInfo);
            Expression<Func<OrderAction, bool>> predicateFilter = null;
            if (model.Filters != null)
            {
                predicateFilter = CreateFilterPredicate(model.Filters);
            }
            var paginatedActions = manager.GetAllPaginated(action, predicateFilter);
            return processResultMapper.Map<PaginatedItems<OrderAction>, PaginatedItemsViewModel<OrderActionViewModel>>(paginatedActions);
        }

        [HttpGet]
        [Route("GetOrderProgress/{orderId}")]
        public async Task<ProcessResult<List<OrderActionViewModel>>> GetOrderProgressByOrderId([FromRoute]int orderId)
        {
            var result = new List<OrderActionViewModel>();
            var actions = manager.GetAll(x => x.OrderId == orderId);
            if (actions.IsSucceeded)
            {
                result = mapper.Map<List<OrderAction>, List<OrderActionViewModel>>(actions.Data);
                return ProcessResultHelper.Succeeded<List<OrderActionViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderActionViewModel>>(result, actions.Exception, "something wrong while getting data");
        }

        [HttpGet]
        [Route("GetTeamMode/{teamId}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode([FromRoute]int teamId)
        {
            var result = new TeamModeViewModel();
            var actions = manager.GetAll(x => x.TeamId == teamId && (x.ActionTypeId != (int)OrderActionType.Assign && x.ActionTypeId != (int)OrderActionType.UnAssgin));
            if (actions.IsSucceeded)
            {
                if (actions.Data != null)
                {
                    var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(actions.Data.LastOrDefault());
                    if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual && lastAction.OrderId == 0)
                    {
                        result.Mode = "Stopped";
                    }
                    else if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual)
                    {
                        result.Mode = "Working";
                    }
                    else
                    {
                        result.Mode = $"{lastAction.WorkingTypeName} - {lastAction.Reason}";
                    }
                    return ProcessResultHelper.Succeeded<TeamModeViewModel>(result);
                }
                return ProcessResultHelper.Succeeded<TeamModeViewModel>(result);
            }
            return ProcessResultHelper.Failed<TeamModeViewModel>(result, actions.Exception, "something wrong while getting data");
        }

        [HttpGet]
        [Route("ChangeTeamMode/{teamId}/{isHasOrder}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode([FromRoute]int teamId, [FromRoute] bool isHasOrder)
        {
            var members = await teamMembersService.GetMemberUsersByTeamId(teamId);
            List<OrderAction> actionList = new List<OrderAction>();
            OrderAction model = new OrderAction();
            if (members.IsSucceeded && members.Data != null)
            {
                for (int i = 0; i < members.Data.Count; i++)
                {
                    model = new OrderAction()
                    {
                        ActionDate = DateTime.Now,
                        ActionDay = DateTime.Now.Date,
                        CostCenterId = members.Data[i].CostCenterId,
                        CostCenterName = members.Data[i].CostCenter,
                        TeamId = teamId,
                        CreatedUser = members.Data[i].MemberParentName,
                        CurrentUserId = members.Data[i].UserId,
                        ActionTypeId = (int)OrderActionType.Idle,
                        ActionTypeName = EnumManager<OrderActionType>.GetName(OrderActionType.Idle),
                        WorkingTypeId = (int)TimeSheetType.Idle,
                        WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle)
                    };

                    if (isHasOrder)
                    {
                        model.Reason = StaticAppSettings.IdleHandling[2].Name;
                    }
                    else
                    {
                        model.Reason = StaticAppSettings.IdleHandling[0].Name;
                    }
                    var addedOrderAction = manager.Add(model);

                    if (addedOrderAction.IsSucceeded)
                    {
                        TechnicianStateViewModel technicianStatemodel = new TechnicianStateViewModel();
                        technicianStatemodel.State = TechnicianState.Idle;
                        technicianStatemodel.TeamId = teamId;
                        technicianStatemodel.ActionDate = DateTime.Now;
                        technicianStatemodel.Long = 00.00m;
                        technicianStatemodel.Lat = 00.00m;
                        technicianStatemodel.TechnicianId = members.Data[i].MemberParentId;
                        var addedTechnicianStateRes = await technicianStateService.Add(technicianStatemodel);
                    }
                }
            }

            TeamModeViewModel result = new TeamModeViewModel();
            if (string.IsNullOrEmpty(model.WorkingTypeName))
            {
                result.Mode = "End Work";
            }
            else
            {
                if (string.IsNullOrEmpty(model.Reason))
                {
                    result.Mode = $"{model.WorkingTypeName}";
                }
                else
                {
                    result.Mode = $"{model.WorkingTypeName} - {model.Reason}";
                }
            }

            return ProcessResultHelper.Succeeded<TeamModeViewModel>(result);

        }

        //[HttpPost]
        //[Route("GetTimeSheet")]
        //public async Task<ProcessResult<List<TimeSheetStructureViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        //{
        //    if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
        //    {
        //        return ProcessResultHelper.Failed<List<TimeSheetStructureViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
        //    }
        //    else
        //    {
        //        var predicate = PredicateBuilder.True<OrderAction>();

        //        //if (!string.IsNullOrEmpty(model.PFNo))
        //        //{
        //        //    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("PF", model.PFNo));
        //        //}

        //        if (!string.IsNullOrEmpty(model.CostCenter))
        //        {
        //            predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("CostCenterName", model.CostCenter));
        //        }

        //        predicate = predicate.And(x => x.ActionDay.Date >= model.DateFrom.Date && x.ActionDay.Date <= model.DateTo);
        //        predicate = predicate.And(x => x.ActionTime != null);

        //        var timeSheetRow = manager.GetAll(predicate);

        //        if (timeSheetRow.IsSucceeded && timeSheetRow.Data != null)
        //        {

        //            //var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName }).Select(async cl => new TimeSheetStructureViewModel()
        //            //{
        //            //    DayDate = cl.First().ActionDay.Date,
        //            //    Name = cl.First().CreatedUser.ToString(),
        //            //    Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
        //            //    WorkType = cl.First().WorkingTypeName,
        //            //    //CostCenter = "",
        //            //    //DataEntryProfile = "SERV",
        //            //    //ActivityType = cl.First().ActionTypeName,
        //            //    //OrderNumber = orderManager.Get(cl.First().OrderId).Data.Code,
        //            //    //Operation = "0010",
        //            //    //SubOperation = "",
        //            //    //SalesOrderItemNumber = "",
        //            //    //SalesOrderNumber = "",
        //            //    //Network = "",
        //            //    //PFNo = (await teamMembersService.GetMembersByTeamId(orderManager.Get(cl.First().OrderId).Data.TeamId)).Data.FirstOrDefault().PF
        //            //});
        //            //// here the magic :D 
        //           // var xx = (await Task.WhenAll(result)).ToList();

        //            var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName }).Select(cl => new TimeSheetStructureViewModel()
        //            {
        //                DayDate = cl.First().ActionDay.Date,
        //                Name = cl.First().CreatedUser.ToString(),
        //                Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
        //                WorkType = cl.First().WorkingTypeName
        //            }).ToList();
        //            return ProcessResultHelper.Succeeded<List<TimeSheetStructureViewModel>>(result);
        //            //return ProcessResultHelper.Succeeded<List<TimeSheetStructureViewModel>>(xx);
        //        }
        //        else
        //        {
        //            return ProcessResultHelper.Failed<List<TimeSheetStructureViewModel>>(null, null, "Something wrong while getting data from order action manager");
        //        }
        //    }
        //}

        [HttpPost]
        [Route("GetTimeSheet")]
        public async Task<ProcessResult<List<TimeSheetStructureExcelViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        {
            if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
            {
                return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
            }
            else
            {
                List<TimeSheetStructureExcelViewModel> timeSheetStructureExcelViewLst = new List<TimeSheetStructureExcelViewModel>();
                var predicate = PredicateBuilder.True<OrderAction>();

                //if (!string.IsNullOrEmpty(model.PFNo))
                //{
                //    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("PF", model.PFNo));
                //}

                if (!string.IsNullOrEmpty(model.CostCenter))
                {
                    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("CostCenterName", model.CostCenter));
                }

                predicate = predicate.And(x => x.ActionDay.Date >= model.DateFrom.Date && x.ActionDay.Date <= model.DateTo);
                predicate = predicate.And(x => x.ActionTime != null);

                var timeSheetRow = manager.GetAll(predicate);

                if (timeSheetRow.IsSucceeded && timeSheetRow.Data != null)
                {
                    var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName }).Select(cl => new TimeSheetStructureViewModel()
                    {
                        DayDate = cl.First().ActionDay.Date,
                        Name = cl.First().CreatedUser.ToString(),
                        Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
                        WorkType = cl.First().WorkingTypeName,
                        CostCenter = "",
                        DataEntryProfile = "SERV",
                        OrderNumber = cl.First().OrderId.ToString(),
                        ActivityType = cl.First().ActionTypeName,
                        Operation = "0010",
                        SubOperation = "",
                        SalesOrderItemNumber = "",
                        SalesOrderNumber = "",
                        Network = "",
                        CreatedUserId = cl.First().CurrentUserId
                    });

                    List<TimeSheetStructureViewModel> timeSheetStructureViewLst = result.ToList();
                    List<string> DispatcherUserIdLst = new List<string>();

                    foreach (var mdl in timeSheetStructureViewLst.ToList())
                    {
                        if (mdl.OrderNumber != null && mdl.OrderNumber != "")
                        {
                            var orderRes = orderManager.Get(int.Parse(mdl.OrderNumber));
                            if (orderRes.IsSucceeded && orderRes.Data != null)
                            {
                                mdl.OrderNumber = orderRes.Data.Code;
                            }
                        }
                        if (mdl.CreatedUserId != null && mdl.CreatedUserId != "")
                        {
                            var techRes = await technicianService.GetTechnicianByUserId(mdl.CreatedUserId);
                            if (techRes.IsSucceeded && techRes.Data != null)
                            {
                                mdl.PFNo = techRes.Data.PF;
                            }
                            else
                            {
                                DispatcherUserIdLst.Add(mdl.CreatedUserId);
                            }
                        }
                        else
                        {
                            timeSheetStructureViewLst.Remove(mdl);
                        }
                    }
                    timeSheetStructureViewLst.RemoveAll(r => DispatcherUserIdLst.Contains(r.CreatedUserId));

                    foreach (var item in timeSheetStructureViewLst)
                    {
                        timeSheetStructureExcelViewLst.Add(new TimeSheetStructureExcelViewModel
                        {
                            Data_Entry_Profile = item.DataEntryProfile,
                            Key_Date = item.DayDate,
                            Personal_Number = item.PFNo,
                            Activity_Type = item.ActivityType,
                            Cost_Center = item.CostCenter,
                            Sales_Order_Number = item.SalesOrderNumber,
                            Sales_Order_Item_Number = item.SalesOrderItemNumber,
                            Order_Number = item.OrderNumber,
                            Network = item.Network,
                            Operation = item.Operation,
                            Sub_Operation = item.SubOperation,
                            Attendance_or_activity_type = item.WorkType,
                            Time = item.Time.TotalHours
                        });
                    }
                    return ProcessResultHelper.Succeeded<List<TimeSheetStructureExcelViewModel>>(timeSheetStructureExcelViewLst);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "Something wrong while getting data from order action manager");
                }
            }
        }

        private Expression<Func<OrderAction, bool>> CreateFilterPredicate(List<Filter> filters)
        {
            var predicate = PredicateBuilder.True<OrderAction>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<OrderAction>(filter.PropertyName, filter.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<OrderAction>(filter.PropertyName, filter.Value));
                }
            }
            return predicate;
        }

        //[HttpPost]
        //[Route("GetTimeSheet")]
        //public async Task<ProcessResult<List<TimeSheetStructureExcelViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        //{
        //    if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
        //    {
        //        return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
        //    }
        //    else
        //    {
        //        List<TimeSheetStructureExcelViewModel> timeSheetStructureExcelViewLst = new List<TimeSheetStructureExcelViewModel>();
        //        var predicate = PredicateBuilder.True<OrderAction>();

        //        //if (!string.IsNullOrEmpty(model.PFNo))
        //        //{
        //        //    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("PF", model.PFNo));
        //        //}

        //        if (!string.IsNullOrEmpty(model.CostCenter))
        //        {
        //            predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("CostCenterName", model.CostCenter));
        //        }

        //        predicate = predicate.And(x => x.ActionDay.Date >= model.DateFrom.Date && x.ActionDay.Date <= model.DateTo);
        //        predicate = predicate.And(x => x.ActionTime != null);

        //        var timeSheetRow = manager.GetAll(predicate);

        //        if (timeSheetRow.IsSucceeded && timeSheetRow.Data != null)
        //        {
        //            var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName}).Select(cl => new TimeSheetStructureViewModel()
        //            {
        //                DayDate = cl.First().ActionDay.Date,
        //                Name = cl.First().CreatedUser.ToString(),
        //                Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
        //                WorkType = cl.First().WorkingTypeName,
        //                CostCenter = "",
        //                DataEntryProfile = "SERV",
        //                OrderNumber = cl.First().OrderId.ToString(),
        //                ActivityType = cl.First().ActionTypeName,
        //                Operation = "0010",
        //                SubOperation = "",
        //                SalesOrderItemNumber = "",
        //                SalesOrderNumber = "",
        //                Network = "",
        //                CreatedUserId = cl.First().CurrentUserId
        //            });

        //            List<TimeSheetStructureViewModel> timeSheetStructureViewLst = result.ToList();
        //            List<string> DispatcherUserIdLst = new List<string>();

        //            foreach (var mdl in timeSheetStructureViewLst.ToList())
        //            {
        //                if (mdl.OrderNumber != null && mdl.OrderNumber != "")
        //                {
        //                    var orderRes = orderManager.Get(int.Parse(mdl.OrderNumber));
        //                    if (orderRes.IsSucceeded && orderRes.Data != null)
        //                    {
        //                        mdl.OrderNumber = orderRes.Data.Code;
        //                    }
        //                }
        //                if (mdl.CreatedUserId != null && mdl.CreatedUserId != "")
        //                {
        //                    var techRes = await technicianService.GetTechnicianByUserId(mdl.CreatedUserId);
        //                    if (techRes.IsSucceeded && techRes.Data != null)
        //                    {
        //                        mdl.PFNo = techRes.Data.PF;
        //                    }
        //                    else
        //                    {
        //                        DispatcherUserIdLst.Add(mdl.CreatedUserId);
        //                    }
        //                }
        //                else
        //                {
        //                    timeSheetStructureViewLst.Remove(mdl);
        //                }

        //                if (mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual)|| mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Break)|| mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle)|| mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling))
        //                {
        //                    mdl.WorkType = "ATRH";
        //                }
        //            }                    
        //            timeSheetStructureViewLst.RemoveAll(r => DispatcherUserIdLst.Contains(r.CreatedUserId));
        //            timeSheetStructureExcelViewLst = timeSheetStructureViewLst.GroupBy(x => x.DayDate).Select(cl => new TimeSheetStructureExcelViewModel() {
        //                Data_Entry_Profile = cl.First().DataEntryProfile,
        //                Key_Date = cl.First().DayDate,
        //                Personal_Number = cl.First().PFNo,
        //                Activity_Type = cl.First().ActivityType,
        //                Cost_Center = cl.First().CostCenter,
        //                Sales_Order_Number = cl.First().SalesOrderNumber,
        //                Sales_Order_Item_Number = cl.First().SalesOrderItemNumber,
        //                Order_Number = cl.First().OrderNumber,
        //                Network = cl.First().Network,
        //                Operation = cl.First().Operation,
        //                Sub_Operation = cl.First().SubOperation,
        //                Attendance_or_activity_type = cl.First().WorkType,
        //                Time = cl.First().Time.TotalHours
        //            }).ToList();

        //            return ProcessResultHelper.Succeeded<List<TimeSheetStructureExcelViewModel>>(timeSheetStructureExcelViewLst);
        //        }
        //        else
        //        {
        //            return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "Something wrong while getting data from order action manager");
        //        }
        //    }
        //}


        [HttpPost]
        [Route("GetTimeSheetExcel")]
        public string GetTimeSheetExcel([FromBody]List<TimeSheetStructureExcelViewModel> modelLst)
        {
            //string[] files = Directory.GetFiles(_hostingEnv.WebRootPath + "\\ExcelSheet", $"*.xls");
            //foreach (var item in files.ToList())
            //{
            //    if (item== "ExcelSheet")
            //    {
                    DateTime dateTime = DateTime.UtcNow.Date;
                    string newFile = _hostingEnv.WebRootPath + "\\ExcelSheet\\" + "TimeSheet " + dateTime.ToString("dd-MM-yyyy")+".csv";
                    var result = ListToExcelHelper.WriteObjectsToExcel(modelLst, newFile);
                    
                    if (result != null && result != "")
                    {
                        result = result.Replace(".csv", ".xlsx");
                        return result;
                    }
                //}
            //}
            
            return null;
        }

        [HttpGet]
        [Route("Test")]
        public ProcessResultViewModel<bool> Test()
        {
            //string[] files = Directory.GetFiles(_hostingEnv.WebRootPath + "\\ExcelSheet", $"*.xls");
            //foreach (var item in files.ToList())
            //{
            //    if (item== "ExcelSheet")
            //    {
            try {
               
             string[] files = Directory.GetFiles(FileUploadSettings.SourceFilePath, $"*.{FileUploadSettings.FileExtention}");

            if (files.Length > 0)
            {
                    
                return ProcessResultViewModelHelper.Succeeded<bool>(true, files[0]);
            }

            return ProcessResultViewModelHelper.Failed<bool>(false, _hostingEnv.WebRootPath + "\\GhanimSAPFiles\\");
            }
            catch(Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }
    }
}