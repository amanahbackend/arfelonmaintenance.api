﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderStatusController : BaseController<IOrderStatusManager, OrderStatus, OrderStatusViewModel>
    {
        IServiceProvider _serviceprovider;
        IOrderStatusManager _orderStatusManager;
        IMapper _mapper;

        public OrderStatusController(IServiceProvider serviceprovider, IOrderStatusManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _orderStatusManager = _manger;
            this._mapper = _mapper;
            _serviceprovider = serviceprovider;
        }

        private ILang_OrderStatusManager LangOrderStatusmanager => _serviceprovider.GetService<ILang_OrderStatusManager>();

        [HttpGet]
        [Route("GetAllExcluded")]
        public ProcessResult<List<OrderStatusViewModel>> GetAllExcluded()
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            var actions = _orderStatusManager.GetAllExclude(StaticAppSettings.ExcludedStatusIds);

            if (!string.IsNullOrEmpty(languageIdValue))
            {
                foreach (var item in actions.Data)
                {
                    var orderStatusRes = LangOrderStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderStatus_ID == item.Id);
                    if (orderStatusRes != null && orderStatusRes.Data != null)
                    {
                        item.Name = orderStatusRes.Data.Name;
                    }
                }
            }

            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = _mapper.Map<List<OrderStatus>, List<OrderStatusViewModel>>(actions.Data);
                return ProcessResultHelper.Succeeded(result);
            }
            return ProcessResultHelper.Failed<List<OrderStatusViewModel>>(null, null, "something wrong while getting data from get all excluded");
        }
    }
}