﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class NotificationCenterController : BaseController<INotificationCenterManager, NotificationCenter, NotificationCenterViewModel>
    {
        public INotificationCenterManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public NotificationCenterController(INotificationCenterManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }

        [HttpPost]
        [Route("GetByRecieverIdAndIsRead")]
        public  ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>> GetByRecieverIdAndIsRead([FromBody]PaginatedFilterNotificationViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
            var predicate = PredicateBuilder.True<NotificationCenter>();
            if (model.RecieverId!=null)
            {
                predicate = predicate.And(r => r.RecieverId==model.RecieverId);
            }
            if (model.IsRead!=null)
            {
                predicate = predicate.And(r => r.IsRead == model.IsRead);
            }

            var paginatedRes = manager.GetAllPaginated(action,predicate,x=>x.CreatedDate);

            return processResultMapper.Map<PaginatedItems<NotificationCenter>, PaginatedItemsViewModel<NotificationCenterViewModel>>(paginatedRes);
        }
        [HttpGet]
        [Route("CountByRecieverId/{recieverId}")]
        public ProcessResultViewModel<int> CountByRecieverId([FromRoute]string recieverId)
        {
            if (recieverId !=null)
            {
                var result = manager.GetAllQuerable().Data.Where(x => x.RecieverId == recieverId && x.IsRead==false ).ToList().Count;

                return ProcessResultViewModelHelper.Succeeded<int>(result);
            }
            return ProcessResultViewModelHelper.Failed<int>(0 , "reciever Id is required");

        }

        [HttpPost]
        [Route("FilterByDate")]
        public ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>> FilterByDate([FromBody]PaginatedFilterNotificationByDateViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
            var predicate = PredicateBuilder.True<NotificationCenter>();
            
            if (model.RecieverId != null)
            {
                predicate = predicate.And(r => r.RecieverId == model.RecieverId);
            }

            if (model.startDate != null && model.endDate != null)
            {
                predicate = predicate.And(x => (x.CreatedDate >= model.startDate) && (x.CreatedDate <= model.endDate));
            }
            else if (model.startDate == null && model.endDate == null)
            {
                predicate = predicate.And(x => true);
            }
            else if (model.startDate == null)
            {
                predicate = predicate.And(x => x.CreatedDate <= model.endDate);
            }
            else if (model.endDate == null)
            {
                predicate = predicate.And(x => x.CreatedDate >= model.startDate);
            }
            

            var paginatedRes = manager.GetAllPaginated(action, predicate,x => x.CreatedDate);

            return processResultMapper.Map<PaginatedItems<NotificationCenter>, PaginatedItemsViewModel<NotificationCenterViewModel>>(paginatedRes);
        }
        [HttpPut]
        [Route("MarkAllAsRead")]
        public ProcessResult<bool> MarkAllAsRead([FromRoute]string recieverId)
        {
            if (string.IsNullOrEmpty(recieverId))
            {
                return ProcessResultHelper.Failed<bool>(false, null, "reciever id is required");
            }
            var result = manager.MarkAllAsRead(recieverId);

            if (result.Data)
            {
                return ProcessResultHelper.Succeeded<bool>(true);
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "something wrong happened when trying to mark all notifications as read");
            }
        }

    }
}