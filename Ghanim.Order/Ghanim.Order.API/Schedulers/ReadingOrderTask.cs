﻿using Ghanim.Order.API.Schedulers.Scheduling;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ghanim.Order.API.Schedulers
{
    public class ReadingOrderTask : IHostedService//, IDisposable
    {
        private readonly ILogger _logger;
        private Timer _timer;
        private ReadOrdersWorkFlow _readOrdersWorkFlow;
        public ReadingOrderTask(ILogger<ReadingOrderTask> logger,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                _readOrdersWorkFlow = scope.ServiceProvider.GetRequiredService<ReadOrdersWorkFlow>();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(300));

            return Task.CompletedTask;                                                             
        }

        private void DoWork(object state)
        {
            _logger.LogInformation("Timed Background Service is working.");
            _readOrdersWorkFlow.ReadFile();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
