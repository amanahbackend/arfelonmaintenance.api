﻿using AutoMapper;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.Problem;
using Ghanim.Order.BLL.Managers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.Schedulers.TaskFlow
{
    public class TimeExceedFlow
    {
        OrderDBContext context;
        private readonly ILogger<TimeExceedFlow> logger;
        private readonly IMapper mapper;

        //ArchivedOrderManager archivedOrderManager;
        OrderManager orderManager;
        OrderSettingManager orderSettingManager;

        IProblemService problemService;

        public TimeExceedFlow(IServiceProvider _serviceProvider, IMapper _mapper)
        {
            context = StaticAppSettings.GetDbContext();
            //  archivedOrderManager = new ArchivedOrderManager(context);
            orderManager = new OrderManager(context);
            orderSettingManager = new OrderSettingManager(context);
            mapper = _mapper;

            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                logger = _serviceProvider.GetService<ILogger<TimeExceedFlow>>();
                problemService = _serviceProvider.GetService<IProblemService>();
            }
        }

        public async void TimeExceedChecker()
        {
            var problems = await problemService.GetProblems();

            DateTime currentTime = DateTime.Now;

            if (problems != null)
            {
                if (problems.IsSucceeded && problems.Data != null && problems.Data.Count > 0)
                {
                    for (int i = 0; i < problems.Data.Count; i++)
                    {
                        var orders = orderManager.GetAllQuerable().Data.Where(x => x.ProblemId == problems.Data[i].Id && (currentTime - x.CreatedDate).TotalHours >= problems.Data[i].ExceedHours && x.IsExceedTime == false).ToList();
                        if (orders.Count > 0)
                        {
                            for (int j = 0; j < orders.Count; j++)
                            {
                                orders[j].IsExceedTime = true;
                            }
                            orderManager.Update(orders);
                        }
                    }
                }
            }
        }
    }
}
