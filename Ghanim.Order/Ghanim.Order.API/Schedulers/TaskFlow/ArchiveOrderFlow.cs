﻿using AutoMapper;
using Ghanim.Order.BLL.Managers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ghanim.Order.API.Schedulers.TaskFlow
{
    public class ArchiveOrderFlow
    {
        OrderDBContext context;
        private readonly ILogger<ArchiveOrderFlow> logger;

        private readonly IMapper _mapper;

        ArchivedOrderManager archivedOrderManager;
        OrderManager orderManager;
        OrderSettingManager orderSettingManager;

        public ArchiveOrderFlow(IMapper mapper1)
        {
            context = StaticAppSettings.GetDbContext();
            archivedOrderManager = new ArchivedOrderManager(context);
            orderManager = new OrderManager(context);
            orderSettingManager = new OrderSettingManager(context);
            _mapper = mapper1;
        }

        public async void Archive()
        {
            var archiveAfterDays = orderSettingManager.GetAllQuerable().Data.Where(x => x.Name == StaticAppSettings.ArchiveAfterDays).FirstOrDefault().Value;
            int daysAfter = Convert.ToInt32(archiveAfterDays);

            DateTime currentTime = DateTime.Now;

            var orders = orderManager.GetAllQuerable().Data.Where(x => StaticAppSettings.ArchivedStatusId.Contains(x.StatusId) && (currentTime - x.CreatedDate).TotalDays >= daysAfter).ToList();

            if (orders.Count > 0)
            {
                var archivedOrders = _mapper.Map<List<OrderObject>, List<ArchivedOrder>>(orders);
                var archiveResult = archivedOrderManager.Add(archivedOrders);
                if (archiveResult.IsSucceeded)
                {
                    orderManager.Delete(orders);
                }
            }
        }

    }
}
