﻿using CommonEnum;
using Ghanim.Order.API.Controllers;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.BuildingTypesService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.ContractTypesService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.DivisionService;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.GovernoratesService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.ExcelSettings;
using Ghanim.Order.BLL.Managers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilites.PACI;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace Ghanim.Order.API.Schedulers.Scheduling
{
    public class ReadOrdersWorkFlow //: IReadOrderManager
    {
        OrderTypeManager orderTypeManager;
        OrderStatusManager orderStatusManager;
        OrderSubStatusManager orderSubStatusManager;
        OrderPriorityManager orderPriorityManager;
        OrderProblemManager orderProblemManager;
        OrderManager orderManager;
        IContractTypesService contractTypesService;
        IAreaService areaService;
        IBuildingTypesService buildingTypesService;
        ICompanyCodeService companyCodeService;
        IGovernoratesService governoratesService;
        IDivisionService divisionService;
        OrderRowDataManager orderRowDataManager;
        IDispatcherService dispatcherService;
        IDispatcherSettingsService dispatcherSettingsService;
        ISupervisorService supervisorService;
        OrderDBContext context;
        ILogger<ReadOrdersWorkFlow> logger;
        private readonly IHostingEnvironment _hostingEnv;
        public ReadOrdersWorkFlow(IServiceProvider serviceProvider, IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                context = FileUploadSettings.GetDbContext();

                orderTypeManager = new OrderTypeManager(context);
                orderStatusManager = new OrderStatusManager(context);
                orderSubStatusManager = new OrderSubStatusManager(context);
                orderPriorityManager = new OrderPriorityManager(context);
                divisionService = serviceProvider.GetService<IDivisionService>();
                contractTypesService = serviceProvider.GetService<IContractTypesService>();
                areaService =  serviceProvider.GetService<IAreaService>();
                buildingTypesService =  serviceProvider.GetService<IBuildingTypesService>();
                governoratesService =  serviceProvider.GetService<IGovernoratesService>();
                companyCodeService =  serviceProvider.GetService<ICompanyCodeService>();
                orderProblemManager = new OrderProblemManager(context);
                orderRowDataManager = new OrderRowDataManager(context);
                logger = serviceProvider.GetService<ILogger<ReadOrdersWorkFlow>>();
                orderManager = new OrderManager(context);
                dispatcherService = serviceProvider.GetService<IDispatcherService>();
                dispatcherSettingsService = serviceProvider.GetService<IDispatcherSettingsService>();
                supervisorService = serviceProvider.GetService<ISupervisorService>();
            }
            PACIHelper.Intialization(FileUploadSettings.proxyUrl,
                FileUploadSettings.paciServiceUrl,
                FileUploadSettings.paciNumberFieldName,
                FileUploadSettings.blockServiceUrl,
                FileUploadSettings.blockNameFieldNameBlockService,
                FileUploadSettings.nhoodNameFieldName,
                FileUploadSettings.streetServiceUrl,
                FileUploadSettings.blockNameFieldNameStreetService,
                FileUploadSettings.streetNameFieldName);

        }

        private  void MoveFile(string fileName)
        {
            try
            {
                var sourceFolder = FileUploadSettings.SourceFilePath;
                var targetFolder = FileUploadSettings.TargetFilePath;

                Uri uriSource = new Uri(FileUploadSettings.HostName + "/" + sourceFolder + "/" + fileName);
                Uri uriDestination = new Uri(FileUploadSettings.HostName + "/" + targetFolder + "/" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + fileName);

                Uri targetUriRelative = uriSource.MakeRelativeUri(uriDestination);

                WebClient request = new WebClient();
                request.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                var fileData = request.DownloadData(uriSource.AbsoluteUri);

                //var url = new Uri(FileUploadSettings.HostName + "/" + sourceFolder + "/" + fileName);
                FtpWebRequest ftpRequestMove = (FtpWebRequest)WebRequest.Create(uriDestination.AbsoluteUri);
                ftpRequestMove.EnableSsl = true;
                ftpRequestMove.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                ftpRequestMove.UsePassive = true;
                ftpRequestMove.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequestMove.ContentLength = fileData.Length;
                Stream requestStream = ftpRequestMove.GetRequestStream();
                requestStream.Write(fileData, 0, fileData.Length);
                requestStream.Close();
                ftpRequestMove.GetResponse().Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Move Error");
                Console.WriteLine(ex.Message);
            }
        }

        private OrderRowData GenerateOrderRowData(string[] values)
        {
            OrderRowData orderRow = new OrderRowData();

            orderRow.AppartmentNo = values[(int)ExcelSheetProperties.ApartmentNo].ToInt32();
            orderRow.AreaCode = values[(int)ExcelSheetProperties.Area].ToInt32();
            orderRow.AreaDescription = values[(int)ExcelSheetProperties.AreaDescription].ToString();
            orderRow.Block = values[(int)ExcelSheetProperties.Block].ToString();
            orderRow.CompanyCode = values[(int)ExcelSheetProperties.OrderCompanyCode].ToString();
            if (values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != null && values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != "")
            {
                orderRow.ContactExpiraion = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractExpirationDate].ToString(), "dd.MM.yyyy", null);
            }
            if (values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != null && values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != "")
            {
                orderRow.ContractDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom].ToString(), "dd.MM.yyyy", null);
            }
            orderRow.ContractNo = values[(int)ExcelSheetProperties.ContractNo].ToString();
            orderRow.ContractType = values[(int)ExcelSheetProperties.ContractType].ToString();
            orderRow.ContractTypeDescription = values[(int)ExcelSheetProperties.ContractTypeDescription].ToString();
            orderRow.CustomerName = values[(int)ExcelSheetProperties.CustomerName].ToString();
            orderRow.CustomerNo = values[(int)ExcelSheetProperties.CustomerId].ToString();
            orderRow.Division = values[(int)ExcelSheetProperties.OrderDivision].ToInt32();
            orderRow.DivisionDescription = values[(int)ExcelSheetProperties.OrderDivisionDescription].ToString();
            orderRow.Floor = values[(int)ExcelSheetProperties.Floor].ToString();
            orderRow.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
            orderRow.House = values[(int)ExcelSheetProperties.HouseKasima].ToString();
            if (values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != null && values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != "")
            {
                orderRow.OrderDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString(), "dd.MM.yyyy HH:mm:ss", null);
            }
            orderRow.OrderNo = values[(int)ExcelSheetProperties.OrderNo].ToString();
            orderRow.OrderNoteAgent = values[(int)ExcelSheetProperties.OrderNoteICAgent].ToString();
            orderRow.OrderPriority = values[(int)ExcelSheetProperties.OrderPriority].ToInt32();
            orderRow.OrderPriorityDescription = values[(int)ExcelSheetProperties.OrderPriorityDescription].ToString();
            orderRow.OrderStatus = values[(int)ExcelSheetProperties.OrderStatusDescription].ToString();
            orderRow.OrderType = values[(int)ExcelSheetProperties.OrderType].ToString();
            orderRow.OrderTypeDescription = values[(int)ExcelSheetProperties.OrderTypeDescription].ToString();
            orderRow.PACI = values[(int)ExcelSheetProperties.Paci].ToString();
            orderRow.PhoneOne = values[(int)ExcelSheetProperties.Phone1].ToString();
            orderRow.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].ToString();
            orderRow.Problem = values[(int)ExcelSheetProperties.OrderProblem1].ToString();
            orderRow.ProblemDescription = values[(int)ExcelSheetProperties.OrderProblem1Description].ToString();
            orderRow.Street = values[(int)ExcelSheetProperties.StreetJaddah].ToString();
            orderRow.Caller_ID = values[(int)ExcelSheetProperties.Caller_ID].ToString();
            orderRow.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].ToString();

            return orderRow;
        }

        private async Task<OrderObject> GenerateOrderObject(string[] values, string fileName)
        {
            OrderObject temp = new OrderObject();

            #region OrderViewModelInitialization
            DateTime? cDate = null;
            if (values[5] != "")
            {
                var date = values[5].ToString();
                int len = date.Length;
                cDate = DateTime.ParseExact(values[5].ToString(), "dd.MM.yyyy HH:mm:ss", null);
            }

            temp.Code = values[(int)ExcelSheetProperties.OrderNo].ToString();
            temp.TypeName = values[(int)ExcelSheetProperties.OrderTypeDescription].ToString();
            temp.Caller_ID = values[(int)ExcelSheetProperties.Caller_ID].ToString();
            temp.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].ToString();

            try
            {
                var type = orderTypeManager.Get(x => x.Name == temp.TypeName).Data;
                temp.TypeId = (type != null) ? type.Id : 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in type binding");
            }

            temp.StatusName = orderStatusManager.GetAll().Data.FirstOrDefault().Name;
            temp.StatusId = orderStatusManager.GetAll().Data.FirstOrDefault().Id;
            temp.SubStatusId = orderSubStatusManager.GetAll().Data.FirstOrDefault().Id;
            temp.SubStatusName = orderSubStatusManager.GetAll().Data.FirstOrDefault().Name;

            temp.PriorityName = values[(int)ExcelSheetProperties.OrderPriorityDescription].ToString();

            try
            {
                var priority = orderPriorityManager.Get(x => x.Name == temp.PriorityName).Data;
                temp.PriorityId = (priority != null) ? priority.Id : 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in priority binding");
            }

            temp.CompanyCodeName = values[(int)ExcelSheetProperties.OrderCompanyCode].ToString();

            try
            {
                var companyCodeRes = await companyCodeService.GetByCode(temp.CompanyCodeName);
                if (companyCodeRes != null && companyCodeRes.Data != null)
                {
                    var companyCode = companyCodeRes.Data;
                    temp.CompanyCodeId = (companyCode != null) ? companyCode.Id : 0;
                }

            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in company code binding");
            }
            if (values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != null && values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != "")
            {
                temp.CreatedDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString(), "dd.MM.yyyy HH:mm:ss", null);
            }
            temp.DivisionName = values[(int)ExcelSheetProperties.OrderDivisionDescription].ToString();

            try
            {
                var divisionRes = await divisionService.GetByName(temp.DivisionName);
                if (divisionRes != null && divisionRes.Data != null)
                {
                    var division = divisionRes.Data;
                    temp.DivisionId = (division != null) ? division.Id : 0;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in division binding");
            }

            temp.ProblemName = values[(int)ExcelSheetProperties.OrderProblem1Description].ToString();

            try
            {
                var problem = orderProblemManager.Get(x => x.Name == temp.ProblemName).Data;
                temp.ProblemId = (problem != null) ? problem.Id : 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in problem binding");
            }

            temp.ICAgentNote = values[(int)ExcelSheetProperties.OrderNoteICAgent].ToString();
            temp.ContractCode = values[(int)ExcelSheetProperties.ContractNo].ToString();
            temp.ContractTypeName = values[(int)ExcelSheetProperties.ContractTypeDescription].ToString();

            try
            {
                var contractRes = await contractTypesService.GetByName(temp.ContractTypeName);
                if (contractRes != null && contractRes.Data != null)
                {
                    var contract = contractRes.Data;
                    temp.ContractTypeId = (contract != null) ? contract.Id : 0;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in contract binding");
            }
            if (values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != null && values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != "")
            {
                temp.ContractStartDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom].ToString(), "dd.MM.yyyy", null);
            }
            if (values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != null && values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != "")
            {
                temp.ContractExpiryDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractExpirationDate].ToString(), "dd.MM.yyyy", null);
            }
            temp.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
            temp.CustomerCode = values[(int)ExcelSheetProperties.CustomerId].ToString();
            temp.CustomerName = values[(int)ExcelSheetProperties.CustomerName].ToString();
            temp.PhoneOne = values[(int)ExcelSheetProperties.Phone1].ToString();
            temp.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].ToString();
            temp.SAP_PACI = values[(int)ExcelSheetProperties.Paci].ToString();
            temp.SAP_AreaName = values[(int)ExcelSheetProperties.AreaDescription].ToString();

            //try
            //{
            //    var area = areasManager.Get(x => x.Name == temp.AreaName).Data;
            //    temp.AreaId = (area != null) ? area.Id : 0;
            //}
            //catch (Exception ex)
            //{
            //    logger.LogError(ex.Message, "error in area binding");
            //}

            //temp.BlockId = values[(int)ExcelSheetProperties.Block].ToInt32();
            temp.SAP_BlockName = values[(int)ExcelSheetProperties.BlockDescription].ToString();
            temp.SAP_StreetName = values[(int)ExcelSheetProperties.StreetJaddah].ToString();
            temp.SAP_HouseKasima = values[(int)ExcelSheetProperties.HouseKasima].ToString();
            temp.SAP_Floor = values[(int)ExcelSheetProperties.Floor].ToString();
            temp.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
            temp.SAP_AppartmentNo = values[(int)ExcelSheetProperties.ApartmentNo].ToString();
            temp.BuildingTypeName = values[(int)ExcelSheetProperties.BuildingType].ToString();
            var buildingTypeRes = await buildingTypesService.GetByName(temp.BuildingTypeName);
            if (buildingTypeRes != null && buildingTypeRes.Data != null) { 
                 var buildingType = buildingTypeRes.Data;
                 temp.BuildingTypeId = (buildingType != null) ? buildingType.Id : 0;
             }
            temp.FileName = fileName;
            temp.InsertionDate = DateTime.Now;
            temp.AcceptanceFlag = AcceptenceType.NoAction;
            temp.IsAccomplish = AccomplishType.Working;
            #endregion OrderViewModelInitialization

            //Address intialization from PACI
            #region AddressIntialization
            if (!string.IsNullOrEmpty(temp.SAP_PACI))
            {
                int round = 1;
                var paciData = new PACIHelper.PACIInfo();
                while (round < 3 && paciData.Latitude == null && paciData.Longtiude == null)
               {
                    paciData = PACIHelper.GetLocationByPACI(Convert.ToInt32(temp.SAP_PACI));
                   round++;
                }
                if (paciData != null)
                {
                    temp.PACI = temp.SAP_PACI;
                    temp.Lat = (decimal)paciData.Latitude;
                    temp.Long = (decimal)paciData.Longtiude;
                    temp.StreetName = paciData.Street?.Name;
                    temp.StreetId = Convert.ToInt32(paciData.Street?.Id);
                    temp.BlockName = paciData.Block?.Name;
                    temp.BlockId = Convert.ToInt32(paciData.Block?.Id);

                    try
                    {
                        var areaRes =await areaService.GetByName(paciData.Area.Name);
                        if (areaRes != null && areaRes.Data != null)
                        {
                            var area = areaRes.Data;
                            if (area == null)
                            {
                                areaRes = await areaService.Add(new AreasViewModel() { Name = paciData.Area.Name });
                                area = areaRes.Data;
                            }
                            temp.AreaName = paciData.Area?.Name;
                            temp.AreaId = (area != null) ? area.Id : 0;
                        }
                        else
                        {
                            areaRes = await areaService.Add(new AreasViewModel() { Name = paciData.Area.Name });
                            var area = areaRes.Data;
                            temp.AreaName = paciData.Area?.Name;
                            temp.AreaId = (area != null) ? area.Id : 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex.Message, "error in area binding");
                    }

                    try
                    {
                        var govRes =await governoratesService.GetByName(paciData.Governorate.Name);
                        var gov = govRes.Data;
                        temp.GovName = (gov != null) ? gov.Name : null;
                        temp.GovId = (gov != null) ? gov.Id : 0;
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex.Message, "error in gov binding");
                    }
                }
            }
            #endregion AddressIntialization

            //Assign to Dispatcher
            #region AssignToDispatcher
            SettingsViewModel orderSettings = new SettingsViewModel()
            {
                AreaId = temp.AreaId,
                DivisionId = temp.DivisionId,
                ProblemId = temp.ProblemId
            };
            var dispatchers = await dispatcherSettingsService.GetDispatcherSettingsByOrder(orderSettings);
            if (dispatchers.IsSucceeded && dispatchers.Data != null)
            {
                Random randNum = new Random();

                temp.DispatcherId = dispatchers.Data[randNum.Next(dispatchers.Data.Count)].Id;

                var dispatcherDetails = await dispatcherService.GetDispatcherById(temp.DispatcherId);
                if (dispatcherDetails.IsSucceeded && dispatcherDetails.Data != null)
                {
                    temp.DispatcherName = dispatcherDetails.Data.Name;
                    temp.SupervisorId = dispatcherDetails.Data.SupervisorId;
                    temp.SupervisorName = dispatcherDetails.Data.SupervisorName;
                }
            }
            else
            {
                //Handle assign to supervisor
                var supervisors = await supervisorService.GetSupervisorsByDivisionId(temp.DivisionId);
                if (supervisors.IsSucceeded && supervisors.Data.Count > 0)
                {
                    Random randNum = new Random();

                    temp.SupervisorId = supervisors.Data[randNum.Next(supervisors.Data.Count)].Id;
                    temp.SupervisorName = supervisors.Data[randNum.Next(supervisors.Data.Count)].Name;
                }
            }
            #endregion AssignToDispatcher
            return temp;
        }

        public async void ReadFile()
        {
            try
            {
                //string[] files = Directory.GetFiles(FileUploadSettings.SourceFilePath, $"*.{FileUploadSettings.FileExtention}");
                // string[] files = Directory.GetFiles(_hostingEnv.WebRootPath + "\\GhanimSAPFiles\\", $"*.{FileUploadSettings.FileExtention}");
                var ftpPath = FileUploadSettings.HostName + "/" + FileUploadSettings.TargetFilePath;
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath));
                ftpRequest.EnableSsl = true;
                ftpRequest.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                List<string> directories = new List<string>();

                string streamLine = streamReader.ReadLine();
                while (!string.IsNullOrEmpty(streamLine))
                {
                    if (streamLine.Contains(FileUploadSettings.FileExtention))
                    {
                        directories.Add(streamLine);
                    }
                    streamLine = streamReader.ReadLine();
                }

                string[] files = directories.ToArray();
                streamReader.Close();

                if (files.Length > 0)
                {

                    WebClient request = new WebClient();
                    for (int j = 0; j < files.Length; j++)
                    {
                        var fileName = files[j];
                        FtpWebRequest ftpRequestRead = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath + "/" + fileName));
                        ftpRequestRead.EnableSsl = true;
                        ftpRequestRead.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                        ftpRequestRead.Method = WebRequestMethods.Ftp.DownloadFile;
                        FtpWebResponse responseRead = (FtpWebResponse)ftpRequestRead.GetResponse();
                        StreamReader reader = new StreamReader(responseRead.GetResponseStream());

                        //var reader = new StreamReader(File.OpenRead(fileName),System.Text.Encoding.UTF8);
                        List<OrderObject> orderList = new List<OrderObject>();
                        List<OrderRowData> orderRowDataList = new List<OrderRowData>();
                        bool isFirst = true;
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');

                            if (isFirst == false)
                            {
                                if (values != null && values.Length > 0)
                                {

                                    try
                                    {
                                        var orderObject =await GenerateOrderObject(values, fileName);
                                        orderList.Add(orderObject);

                                        var orderRow = GenerateOrderRowData(values);
                                        orderRowDataList.Add(orderRow);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("Entity of type \"{0}\" with error \"{1}\"", e.GetType(), e.Message);
                                    }
                                }
                            }
                            else
                            {
                                isFirst = false;
                            }
                        }

                        try
                        {
                            orderManager.Add(orderList);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        orderRowDataManager.Add(orderRowDataList);

                        Console.WriteLine(string.Format("End of file {0}", files[j]));
                        reader.Dispose();
                        MoveFile(files[j]);
                        Console.WriteLine(" \r \n Hi  \r \n  Task Finish press any key to close. \r \n Thanks");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("ISSUE");
            }
        }
    }
}
