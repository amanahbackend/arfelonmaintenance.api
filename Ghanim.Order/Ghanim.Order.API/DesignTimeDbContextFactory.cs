﻿using Ghanim.Order.EFCore.MSSQL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<OrderDBContext>
    {
        public OrderDBContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<OrderDBContext>();

            // var connectionString = configuration.GetConnectionString("ConnectionString");
            builder.UseSqlServer("Data Source=.\\SQLEXPRESS; Initial Catalog=ArfelonMaintenanceDev; Integrated Security = true;");

            return new OrderDBContext(builder.Options);
        }
    }
}
