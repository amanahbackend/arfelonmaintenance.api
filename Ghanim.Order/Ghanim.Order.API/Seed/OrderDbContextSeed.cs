﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.Seed
{
    public class OrderDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(OrderDBContext context, IHostingEnvironment env,
            ILogger<OrderDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                // settings = appSettings.Value;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;

                var settings = GetDefaultSettings();
                var lstRejectionReasons = GetDefaultRejectionReasons();
                var orderTypes = GetDefaultOrderType();
                var orderPriorities = GetDefaultOrderPriority();
                var orderProblems = GetDefaultOrderProblem();
                var orderStatuses = GetDefaultOrderStatus();
                var orderSubStatuses = GetDefaultOrderSubStatus();
                var workingTypes = GetDefaultWorkingType();

                using (var transaction = context.Database.BeginTransaction())
                {
                    await SeedEntityAsync(context, settings, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Settings ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Settings OFF");

                    await SeedEntityAsync(context, lstRejectionReasons, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT RejectionReason ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT RejectionReason OFF");

                    await SeedEntityAsync(context, orderStatuses, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderStatus ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderStatus OFF");

                    await SeedEntityAsync(context, orderSubStatuses, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderSubStatus ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderSubStatus OFF");

                    await SeedEntityAsync(context, orderPriorities, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderPriority ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderPriority OFF");

                    await SeedEntityAsync(context, orderProblems, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderProblem ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderProblem OFF");

                    await SeedEntityAsync(context, orderTypes, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderType ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT OrderType OFF");

                    await SeedEntityAsync(context, workingTypes, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT WorkingType ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT WorkingType OFF");

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for OrderDbContext");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }

        private List<OrderSetting> GetDefaultSettings()
        {
            return new List<OrderSetting>
            {
                new OrderSetting
                {
                    Id = 1,
                    Name = "ExceedTimeHours",
                    Value = "24"
                },
                new OrderSetting
                {
                    Id = 2,
                    Name = "ArchiveAfterDays",
                    Value = "15"
                }
            };
        }
        private List<RejectionReason> GetDefaultRejectionReasons()
        {
            List<RejectionReason> result = new List<RejectionReason>
            {
                new RejectionReason {Id=1, Name="Traffic Jam"},
                new RejectionReason {Id=2, Name="Re-Schedule to next day"}
            };
            return result;
        }
        private List<OrderPriority> GetDefaultOrderPriority()
        {
            List<OrderPriority> result = new List<OrderPriority>
            {
                new OrderPriority {Id=1, Name="Low"},
                new OrderPriority {Id=2, Name="Medium"},
                new OrderPriority {Id=3, Name="High"  },
                new OrderPriority {Id=4, Name="Very High"}
            };
            return result;
        }
        private List<OrderStatus> GetDefaultOrderStatus()
        {
            List<OrderStatus> result = new List<OrderStatus>
            {
                new OrderStatus {Id=1, Name="Open"},
                new OrderStatus {Id=2, Name="Dispatched"},
                new OrderStatus {Id=3, Name="On-Travel"},
                new OrderStatus {Id=4, Name="Reached"},
                new OrderStatus {Id=5, Name="Started"},
                new OrderStatus {Id=6, Name="On-Hold"},
                new OrderStatus {Id=7, Name="Complete"},
                new OrderStatus {Id=8, Name="Canceled"}
            };
            return result;
        }
        private List<OrderProblem> GetDefaultOrderProblem()
        {
            List<OrderProblem> result = new List<OrderProblem>
            {
                new OrderProblem {Id=1, Code="CAC",Name="Commercial CAC",ExceedHours =24},
                new OrderProblem {Id=2,Code="CHI", Name="Chillers",ExceedHours =24},
                new OrderProblem {Id=3,Code="CHS", Name="CHS",ExceedHours =24},
                new OrderProblem {Id=4,Code="CLN", Name="Cleaning Equipment",ExceedHours =24},
                new OrderProblem {Id=5,Code="ELECTRICAL", Name="Electrical",ExceedHours =24},
                new OrderProblem {Id=6,Code="ZELEVATOR", Name="Elevators",ExceedHours =24},
                new OrderProblem {Id=7,Code="ZFIREFIGHTING", Name="Fire Fighting",ExceedHours =24},
                new OrderProblem {Id=8,Code="FLT", Name="Fork Lifts",ExceedHours =24},
                new OrderProblem {Id=9,Code="GAC", Name="Government Service",ExceedHours =24},
                new OrderProblem {Id=10,Code="NSD", Name="Non Res Non Std",ExceedHours =24},
                new OrderProblem {Id=11,Code="PLB", Name="Plumbing",ExceedHours =24},
                new OrderProblem {Id=12,Code="RAC", Name="Residential CAC",ExceedHours =24},
                new OrderProblem {Id=13,Code="REL", Name="Residential Electrical Service",ExceedHours =24},
                new OrderProblem {Id=14,Code="RPB", Name="Residential Plumbing Service",ExceedHours =24},
                new OrderProblem {Id=15,Code="RSW", Name="Residential Swimming Pool Serv",ExceedHours =24},
                new OrderProblem {Id=16,Code="SAV", Name="Security Audio Visual",ExceedHours =24},
                new OrderProblem {Id=17,Code="SRA", Name="SRAC Service",ExceedHours =24},
                new OrderProblem {Id=18,Code="STD", Name="Non Res Standard",ExceedHours =24},
                new OrderProblem {Id=19,Code="STO", Name="Stationed operator",ExceedHours =24},
                new OrderProblem {Id=20,Code="SUB", Name="Subcontractor",ExceedHours =24},
                new OrderProblem {Id=21,Code="TEC", Name="Technology",ExceedHours =24},
                new OrderProblem {Id=22,Code="VDN", Name="Video Data Network",ExceedHours =24},
                new OrderProblem {Id=23,Code="WTC", Name="Water Cooler",ExceedHours =24}
            };
            return result;
        }
        private List<OrderType> GetDefaultOrderType()
        {
            List<OrderType> result = new List<OrderType>
            {
                new OrderType {Id=1, Code= "ZS01", Name= "Contract Service Order" },
                new OrderType {Id=2,Code= "ZS03", Name= "Enhancement Job Order" },
                new OrderType {Id=3,Code= "ZS06", Name= "Preventive Maintenance Order" },
                new OrderType {Id=4,Code= "ZS07", Name= "MC Potential Orders" },
                new OrderType {Id=5,Code= "ZS20", Name= "Cash Compressor" },
                new OrderType {Id=6,Code= "ZS02", Name= "Cash call Order" },
                new OrderType {Id=7,Code= "ZS05", Name= "SRAC Installation Order" },
                new OrderType {Id=8,Code= "ZS08", Name= "Special Services Order" },
                new OrderType {Id=9,Code= "ZS09", Name="Sub order for quotation creation"}
            };
            return result;
        }
        private List<OrderSubStatus> GetDefaultOrderSubStatus()
        {
            List<OrderSubStatus> result = new List<OrderSubStatus>
            {
                new OrderSubStatus {Id=1,Code="OPEN",Name ="Released",StatusId=1},
                new OrderSubStatus {Id=2,Code="DSSC",Name ="Scheduled",StatusId=2},
                new OrderSubStatus {Id=3,Code="DSRS",Name ="Re-Scheduled",StatusId=2},
                new OrderSubStatus {Id=4,Code="DSOT",Name ="On-Travel",StatusId=3},
                new OrderSubStatus {Id=5,Code="DSCH",Name ="Reached",StatusId=4},
                new OrderSubStatus {Id=6,Code="DSST",Name ="Started",StatusId=5},
                new OrderSubStatus {Id=7,Code="OHQC",Name ="Quotation - Waiting customer approval",StatusId=6},
                new OrderSubStatus {Id=8,Code="OHGL",Name ="Gas Leak",StatusId=6},
                new OrderSubStatus {Id=9,Code="OHWL",Name ="Water Leak",StatusId=6},
                new OrderSubStatus {Id=10,Code="OHSC",Name ="Compressor Cash / warranty",StatusId=6},
                new OrderSubStatus {Id=11,Code="OHCW",Name ="Compressor Warranty",StatusId=6},
                new OrderSubStatus {Id=12,Code="OHMB",Name ="Motor Bracket",StatusId=6},
                new OrderSubStatus {Id=13,Code="OHTY",Name ="Tray",StatusId=6},
                new OrderSubStatus {Id=14,Code="OHEX",Name ="E.X.V",StatusId=6},
                new OrderSubStatus {Id=15,Code="OHCP",Name ="Capacitor",StatusId=6},
                new OrderSubStatus {Id=16,Code="OHTH",Name ="Thermostat",StatusId=6},
                new OrderSubStatus {Id=17,Code="OHFB",Name ="Fan blade",StatusId=6},
                new OrderSubStatus {Id=18,Code="OHBR",Name ="Breaker",StatusId=6},
                new OrderSubStatus {Id=19,Code="OHIM",Name ="I/D Motor",StatusId=6},
                new OrderSubStatus {Id=20,Code="OHOM",Name ="O/D Motor",StatusId=6},
                new OrderSubStatus {Id=21,Code="COMR",Name ="Motor replaced",StatusId=7},
                new OrderSubStatus {Id=22,Code="COBR",Name ="Belt replaced",StatusId=7},
                new OrderSubStatus {Id=23,Code="CNEC",Name ="E.C.S",StatusId=8},
                new OrderSubStatus {Id=24,Code="CNCP",Name ="Customer has not paid",StatusId=8}
            };
            return result;
        }
        private List<WorkingType> GetDefaultWorkingType()
        {
            List<WorkingType> result = new List<WorkingType>
            {
                new WorkingType {Id=1,Category="Work",Name ="Accept & set first order On-Travel",CategoryCode= 0},
                new WorkingType {Id=2,Category="Break hours",Name ="Pray",CategoryCode= 4},
                new WorkingType {Id=3,Category="Break hours",Name ="Food",CategoryCode= 4 },
                new WorkingType {Id=4,Category="Idle Time hours",Name ="Stores time",CategoryCode= 3},
                new WorkingType {Id=5,Category="Idle Time hours",Name ="No work assigned",CategoryCode= 3},
                new WorkingType {Id=6,Category="Idle Time hours",Name ="Petty Cash purchase",CategoryCode= 3},
                new WorkingType {Id=7,Category="Idle Time hours",Name ="Meetings",CategoryCode= 3},
                new WorkingType {Id=8,Category="Work",Name ="Reject first order",CategoryCode= 0},
                new WorkingType {Id=9,Category="Work",Name ="Continue working",CategoryCode= 0},
                new WorkingType {Id=10,Category="End Work",Name ="End Work",CategoryCode= 0},
                new WorkingType {Id=11,Category="Idle Time hours",Name ="Waiting for Service orders assignment",CategoryCode= 3},
                new WorkingType {Id=12,Category="Idle Time hours",Name ="Pick materials from stores",CategoryCode= 3}
            };
            return result;
        }

    }
}
