﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.Helper
{
    public enum OrderStatusEnum
    {
        Assigned=1,
        Unassigned=2,
        Both=3
    }
}
