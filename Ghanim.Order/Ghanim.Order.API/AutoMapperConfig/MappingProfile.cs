﻿using AutoMapper;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites;

namespace Ghanim.Order.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<OrderObject, OrderViewModel>();
            CreateMap<OrderViewModel, OrderObject>()
                .IgnoreBaseEntityProperties();

            CreateMap<OrderObject, ArchivedOrder>()
               .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<ArchivedOrder, OrderObject>()
                .IgnoreBaseEntityProperties();
            CreateMap<OrderProblem, OrderProblemViewModel>();
            CreateMap<OrderProblemViewModel, OrderProblem>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderType, OrderTypeViewModel>();
            CreateMap<OrderTypeViewModel, OrderType>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderStatus, OrderStatusViewModel>();
            CreateMap<OrderStatusViewModel, OrderStatus>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderPriority, OrderPriorityViewModel>();
            CreateMap<OrderPriorityViewModel, OrderPriority>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderSubStatus, OrderSubStatusViewModel>();
            CreateMap<OrderSubStatusViewModel, OrderSubStatus>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderStatus, Lang_OrderStatusViewModel>();
            CreateMap<Lang_OrderStatusViewModel, Lang_OrderStatus>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderProblem, Lang_OrderProblemViewModel>();
            CreateMap<Lang_OrderProblemViewModel, Lang_OrderProblem>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderType, Lang_OrderTypeViewModel>();
            CreateMap<Lang_OrderTypeViewModel, Lang_OrderType>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_WorkingType, Lang_WorkingTypeViewModel>();
            CreateMap<Lang_WorkingTypeViewModel, Lang_WorkingType>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderPriority, Lang_OrderPriorityViewModel>();
            CreateMap<Lang_OrderPriorityViewModel, Lang_OrderPriority>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Notification, Lang_NotificationViewModel>();
            CreateMap<Lang_NotificationViewModel, Lang_Notification>()
           .IgnoreBaseEntityProperties();

            CreateMap<Notification, NotificationViewModel>();
            CreateMap<NotificationViewModel, Notification>()
           .IgnoreBaseEntityProperties();

            CreateMap<WorkingViewModel, TechnicianStateViewModel>();

            CreateMap<TeamViewModel, TeamOrdersViewModel>()
                .ForMember(dest=>dest.TeamId , opt=>opt.MapFrom(src=>src.Id))
                .ForMember(dest=>dest.TeamName , opt=>opt.MapFrom(src=>src.Name))
                .ForMember(dest=>dest.ForemanId , opt=>opt.MapFrom(src=>src.ForemanId))
                .ForMember(dest=>dest.ForemanName , opt=>opt.MapFrom(src=>src.ForemanName))
                .ForMember(dest=>dest.Orders , opt=> opt.Ignore());

        }
    }
}
