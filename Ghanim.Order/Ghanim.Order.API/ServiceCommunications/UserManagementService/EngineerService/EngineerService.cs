﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.EngineerService
{
    public class EngineerService : DefaultHttpClientCrud<UserManagementServiceSettings, EngineerViewModel, EngineerViewModel>,
       IEngineerService
    {
        UserManagementServiceSettings _settings;

        public EngineerService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<EngineerViewModel>> GetEngineerByUserId(string userId)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetEngineerByUserId;
                var url = $"{baseUrl}/{requestedAction}/{userId}";
                return await GetByUriCustomized<EngineerViewModel>(url);
            }
            return null;
        }
    }
}
