﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.IdentityService
{
    public class IdentityService : DefaultHttpClientCrud<IdentityServiceSettings, IdentityViewModel, IdentityViewModel>,
       IIdentityService
    {
        IdentityServiceSettings _settings;

        public IdentityService(IOptions<IdentityServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<IdentityViewModel>> GetUserById(string userId)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetUserById;
                var url = $"{baseUrl}/{requestedAction}?id=" + userId;
                return await GetByUriCustomized<IdentityViewModel>(url);
            }
            return null;
        }
     
    }
}
