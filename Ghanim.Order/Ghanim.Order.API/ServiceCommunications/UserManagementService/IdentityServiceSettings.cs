﻿using DispatchProduct.HttpClient;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        public string GetUserById { get; set; }
    }
}