﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService
{
    public class TechnicianService : DefaultHttpClientCrud<UserManagementServiceSettings, TechnicianViewModel, TechnicianViewModel>,
       ITechnicianService
    {
        UserManagementServiceSettings _settings;

        public TechnicianService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<TechnicianViewModel>> GetTechnicianByUserId(string userId)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetTechnicianByUserId;
                var url = $"{baseUrl}/{requestedAction}/{userId}";
                return await GetByUriCustomized<TechnicianViewModel>(url);
            }
            return null;
        }
    }
}
