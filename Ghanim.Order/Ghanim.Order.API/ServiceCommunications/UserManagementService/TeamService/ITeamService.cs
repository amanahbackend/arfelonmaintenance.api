﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamService
{
    public interface ITeamService : IDefaultHttpClientCrud<UserManagementServiceSettings, TeamViewModel, TeamViewModel>
    {
        Task<ProcessResultViewModel<List<TeamViewModel>>> GetTeamsByDispatcherId(int dispatcherId);
        Task<ProcessResultViewModel<TeamViewModel>> GetTeamById(int Id);
    }
}
