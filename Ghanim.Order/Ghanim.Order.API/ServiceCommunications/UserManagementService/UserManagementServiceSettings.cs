﻿using DispatchProduct.HttpClient;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService
{
    public class UserManagementServiceSettings : DefaultHttpClientSettings
    {
        public string GetDispatchersByDivision { get; set; }
        public string GetUserById { get; set; }
        public string GetDispatcherSettings { get; set; }
        public string GetDispatcherById { get; set; }
        public string GetTeamById { get; set; }
        public string GetDispatcherByUserId { get; set; }
        public string GetEngineerByUserId { get; set; }
        public string GetForemanByUserId { get; set; }
        public string GetSupervisorByUserId { get; set; }
        public string GetTechnicianByUserId { get; set; }
        public string GetManagerByUserId { get; set; }
        public string GetTeamIdByTechnicianMember { get; set; }
        public string GetSupervisorByDivisionId { get; set; }
        public string GetSupervisorsByDivisionId { get; set; }
        public string GetMembersByTeamId { get; set; }
        public string GetMemberUsersByTeamId { get; set; }
        public string GetUserIdByMemberId { get; set; }
        public string GetTeamsByDispatcherId { get; set; }
        public string AddTechnicianState { get; set; }
    }
}