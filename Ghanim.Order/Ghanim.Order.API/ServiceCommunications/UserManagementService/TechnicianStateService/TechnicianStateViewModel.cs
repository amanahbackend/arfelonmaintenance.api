﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService
{
    public class TechnicianStateViewModel : BaseEntity
    {
        public int TechnicianId { get; set; }
        public int TeamId { get; set; }
        public DateTime ActionDate { get; set; }
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public TechnicianState State { get; set; }
    }
}
