﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService
{
    public class TechnicianStateService : DefaultHttpClientCrud<UserManagementServiceSettings, TechnicianStateViewModel, TechnicianStateViewModel>,
       ITechnicianStateService
    {
        UserManagementServiceSettings _settings;

        public TechnicianStateService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<TechnicianStateViewModel>> Add(TechnicianStateViewModel model)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddTechnicianState;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<TechnicianStateViewModel,TechnicianStateViewModel>(url, model);
            }
            return null;
        }
    }
}
