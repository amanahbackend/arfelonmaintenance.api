﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService
{
    public class SettingsViewModel
    {
        public int? DivisionId { get; set; }
        public int? AreaId { get; set; }
        public int? ProblemId { get; set; }
    }
}
