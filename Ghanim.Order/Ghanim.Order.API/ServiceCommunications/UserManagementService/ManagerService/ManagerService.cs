﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.ManagerService
{
    public class ManagerService : DefaultHttpClientCrud<UserManagementServiceSettings, ManagerViewModel, ManagerViewModel>,
       IManagerService
    {
        UserManagementServiceSettings _settings;

        public ManagerService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<ManagerViewModel>> GetManagerByUserId(string userId)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetManagerByUserId;
                var url = $"{baseUrl}/{requestedAction}/{userId}";
                return await GetByUriCustomized<ManagerViewModel>(url);
            }
            return null;
        }
    }
}
