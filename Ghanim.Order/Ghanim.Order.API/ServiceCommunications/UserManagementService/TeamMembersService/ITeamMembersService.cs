﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Order.API.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService
{
    public interface ITeamMembersService : IDefaultHttpClientCrud<UserManagementServiceSettings, TeamMemberViewModel, TeamMemberViewModel>
    {
        Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMembersByTeamId(int teamId);
        Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMemberUsersByTeamId(int teamId);
        Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetTeamIdByTechnicianMember(int Id);
    }
}