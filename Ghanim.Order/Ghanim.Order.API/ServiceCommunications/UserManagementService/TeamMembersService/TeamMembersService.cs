﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService
{
    public class TeamMembersService : DefaultHttpClientCrud<UserManagementServiceSettings, TeamMemberViewModel, TeamMemberViewModel>,
       ITeamMembersService
    {
        UserManagementServiceSettings _settings;

        public TeamMembersService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMembersByTeamId(int teamId)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetMembersByTeamId;
                var url = $"{baseUrl}/{requestedAction}/{teamId}";
                return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMemberUsersByTeamId(int teamId)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetMemberUsersByTeamId;
                var url = $"{baseUrl}/{requestedAction}/{teamId}";
                return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetTeamIdByTechnicianMember(int Id)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetTeamIdByTechnicianMember;
                var url = $"{baseUrl}/{requestedAction}/{Id}";
                return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            }
            return null;
        }

    }
}