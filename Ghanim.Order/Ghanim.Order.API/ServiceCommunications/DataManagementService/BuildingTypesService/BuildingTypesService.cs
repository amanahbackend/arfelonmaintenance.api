﻿using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.BuildingTypesService
{
    public class BuildingTypesService : DefaultHttpClientCrud<DataManagementServiceSettings, BuildingTypesViewModel, BuildingTypesViewModel>,
       IBuildingTypesService
    {
        DataManagementServiceSettings _settings;

        public BuildingTypesService(IOptions<DataManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<BuildingTypesViewModel>> GetByName(string name)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetBuildingTypes;
                var url = $"{baseUrl}/{requestedAction}/{name}";
                return await GetByUriCustomized<BuildingTypesViewModel>(url);
            }
            return null;
        }
    }
}
