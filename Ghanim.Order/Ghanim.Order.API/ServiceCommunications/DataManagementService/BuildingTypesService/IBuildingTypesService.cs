﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.BuildingTypesService
{
    public interface IBuildingTypesService : IDefaultHttpClientCrud<DataManagementServiceSettings, BuildingTypesViewModel, BuildingTypesViewModel>
    {
        Task<ProcessResultViewModel<BuildingTypesViewModel>> GetByName(string name);
    }
}