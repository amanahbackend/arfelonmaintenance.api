﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.GovernoratesService
{
    public class GovernoratesService : DefaultHttpClientCrud<DataManagementServiceSettings, GovernoratesViewModel, GovernoratesViewModel>,
       IGovernoratesService
    {
        DataManagementServiceSettings _settings;

        public GovernoratesService(IOptions<DataManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<GovernoratesViewModel>> GetByName(string name)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetGovernorates;
                var url = $"{baseUrl}/{requestedAction}/{name}";
                return await GetByUriCustomized<GovernoratesViewModel>(url);
            }
            return null;
        }
        
    }
}
