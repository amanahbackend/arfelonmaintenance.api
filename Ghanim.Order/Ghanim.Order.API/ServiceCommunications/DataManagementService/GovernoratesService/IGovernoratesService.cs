﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.GovernoratesService
{
    public interface IGovernoratesService : IDefaultHttpClientCrud<UserManagementServiceSettings, GovernoratesViewModel, GovernoratesViewModel>
    {
        Task<ProcessResultViewModel<GovernoratesViewModel>> GetByName(string name);
    }
}