﻿using DispatchProduct.HttpClient;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService
{
    public class DataManagementServiceSettings : DefaultHttpClientSettings
    {
        public string GetGovernorates { get; set; }
        public string GetDivisions { get; set; }
        public string GetAreas { get; set; }
        public string GetBuildingTypes { get; set; }
        public string GetSupportedLanguages { get; set; }
        public string GetSupportedLanguage { get; set; }
        public string GetSupportedLanguageByName { get; set; }
        public string GetCompanyCode { get; set; }
        public string GetContractTypes { get; set; }
        public string GetProblems { get; set; }
        public string AddArea { get; set; }

    }
}