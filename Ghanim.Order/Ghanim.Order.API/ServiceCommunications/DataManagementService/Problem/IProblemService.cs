﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.Problem
{
    public interface IProblemService : IDefaultHttpClientCrud<DataManagementServiceSettings, OrderProblemViewModel, OrderProblemViewModel>
    {
        Task<ProcessResultViewModel<List<OrderProblemViewModel>>> GetProblems();
    }
}