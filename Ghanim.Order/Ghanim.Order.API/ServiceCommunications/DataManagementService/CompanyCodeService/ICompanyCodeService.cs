﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService
{
    public interface ICompanyCodeService : IDefaultHttpClientCrud<DataManagementServiceSettings, CompanyCodeViewModel, CompanyCodeViewModel>
    {
        Task<ProcessResultViewModel<CompanyCodeViewModel>> GetByCode(string code);
    }
}