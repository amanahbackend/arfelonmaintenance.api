﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.CompanyCodeService
{
    public class CompanyCodeService : DefaultHttpClientCrud<DataManagementServiceSettings, CompanyCodeViewModel, CompanyCodeViewModel>,
       ICompanyCodeService
    {
        DataManagementServiceSettings _settings;

        public CompanyCodeService(IOptions<DataManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<CompanyCodeViewModel>> GetByCode(string code)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetCompanyCode;
                var url = $"{baseUrl}/{requestedAction}/{code}";
                return await GetByUriCustomized<CompanyCodeViewModel>(url);
            }
            return null;
        }
    }
}
