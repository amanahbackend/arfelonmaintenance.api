﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService
{
    public class SupportedLanguagesViewModel : RepoistryBaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }

    }
}
