﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService
{
    public class SupportedLanguageService : DefaultHttpClientCrud<DataManagementServiceSettings, SupportedLanguagesViewModel, SupportedLanguagesViewModel>,
       ISupportedLanguageService
    {
        DataManagementServiceSettings _settings;

        public SupportedLanguageService(IOptions<DataManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<List<SupportedLanguagesViewModel>>> GetAll()
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetSupportedLanguages;
                var url = $"{baseUrl}/{requestedAction}";
                return await GetByUriCustomized<List<SupportedLanguagesViewModel>>(url);
            }
            return null;

        }

        public async Task<ProcessResultViewModel<SupportedLanguagesViewModel>> Get(int id)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetSupportedLanguage;
                var url = $"{baseUrl}/{requestedAction}/{id}";
                return await GetByUriCustomized<SupportedLanguagesViewModel>(url);
            }
            return null;

        }

        public async Task<ProcessResultViewModel<SupportedLanguagesViewModel>> GetByName(string name)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetSupportedLanguageByName;
                var url = $"{baseUrl}/{requestedAction}/{name}";
                return await GetByUriCustomized<SupportedLanguagesViewModel>(url);
            }
            return null;

        }

    }
}
