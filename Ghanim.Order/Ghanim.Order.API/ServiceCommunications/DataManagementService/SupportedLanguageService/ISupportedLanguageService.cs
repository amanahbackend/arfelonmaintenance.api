﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.SupportedLanguageService
{
    public interface ISupportedLanguageService : IDefaultHttpClientCrud<DataManagementServiceSettings, SupportedLanguagesViewModel, SupportedLanguagesViewModel>
    {
        Task<ProcessResultViewModel<List<SupportedLanguagesViewModel>>> GetAll();
        Task<ProcessResultViewModel<SupportedLanguagesViewModel>> Get(int id);
        Task<ProcessResultViewModel<SupportedLanguagesViewModel>> GetByName(string name);
    }
}