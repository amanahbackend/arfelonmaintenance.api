﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.ContractTypesService
{
    public interface IContractTypesService : IDefaultHttpClientCrud<DataManagementServiceSettings, ContractTypesViewModel, ContractTypesViewModel>
    {
          Task<ProcessResultViewModel<ContractTypesViewModel>> GetByName(string name);
    }
}
