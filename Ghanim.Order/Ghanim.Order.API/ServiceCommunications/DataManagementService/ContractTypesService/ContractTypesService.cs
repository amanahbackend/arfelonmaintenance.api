﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.ContractTypesService
{
    public class ContractTypesService : DefaultHttpClientCrud<DataManagementServiceSettings, ContractTypesViewModel, ContractTypesViewModel>,
       IContractTypesService
    {
        DataManagementServiceSettings _settings;

        public ContractTypesService(IOptions<DataManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<ContractTypesViewModel>> GetByName(string name)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetContractTypes;
                var url = $"{baseUrl}/{requestedAction}/{name}";
                return await GetByUriCustomized<ContractTypesViewModel>(url);
            }
            return null;
        }
        
    }
}
