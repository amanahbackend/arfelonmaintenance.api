﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.DivisionService
{
    public interface IDivisionService : IDefaultHttpClientCrud<UserManagementServiceSettings, DivisionViewModel, DivisionViewModel>
    {
        Task<ProcessResultViewModel<DivisionViewModel>> GetByName(string name);
    }
}