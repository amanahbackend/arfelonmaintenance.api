﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService
{
    public interface IAreaService : IDefaultHttpClientCrud<DataManagementServiceSettings, AreasViewModel, AreasViewModel>
    {
        Task<ProcessResultViewModel<AreasViewModel>> GetByName(string name);
        Task<ProcessResultViewModel<AreasViewModel>> Add(AreasViewModel model);

    }
}