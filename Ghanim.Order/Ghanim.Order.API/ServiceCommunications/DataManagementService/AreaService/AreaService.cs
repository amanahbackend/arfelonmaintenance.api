﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.DataManagementService.AreaService
{
    public class AreaService : DefaultHttpClientCrud<DataManagementServiceSettings, AreasViewModel, AreasViewModel>,
       IAreaService
    {
        DataManagementServiceSettings _settings;

        public AreaService(IOptions<DataManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<AreasViewModel>> GetByName(string name)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetAreas;
                var url = $"{baseUrl}/{requestedAction}/{name}";
                return await GetByUriCustomized<AreasViewModel>(url);
            }
            return null;

        }

        public async Task<ProcessResultViewModel<AreasViewModel>> Add(AreasViewModel model)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddArea;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<AreasViewModel, AreasViewModel>(url, model);
            }
            return null;
        }
    }
}
