﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.Notification
{
    public class SMSService : DefaultHttpClientCrud<DropoutServiceSettings, SMSViewModel, bool>, ISMSService
    {
        DropoutServiceSettings _settings;

        public SMSService(IOptions<DropoutServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<bool>> Send(SMSViewModel model)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.SendSMS;
                var url = $"{baseUrl}/{requestedAction}";

                return await PostCustomize<SMSViewModel, bool>(url, model);
            }
            return null;
        }
    }
}
