﻿//using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;

namespace Ghanim.Order.API.ServiceCommunications.Notification
{
    public class DropoutServiceSettings : DefaultHttpClientSettings
    {
        public string SendNotifications { get; set; }
        public string SendNotification { get; set; }
        public string SendSMS { get; set; }
    }
}
