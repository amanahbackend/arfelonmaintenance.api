﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.Notification
{
    public class NotificationService : DefaultHttpClientCrud<DropoutServiceSettings, SendNotificationViewModel, string>, INotificationService
    {
        DropoutServiceSettings _settings;

        public NotificationService(IOptions<DropoutServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<string>> SendNotifications(List<SendNotificationViewModel> lstModel)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.SendNotifications;
                var url = $"{baseUrl}/{requestedAction}";

                return await PostCustomize<List<SendNotificationViewModel>, string>(url, lstModel);
            }
            return null;
        }
        public async Task<ProcessResultViewModel<string>> SendNotification(SendNotificationViewModel Model)
        {
            string baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.SendNotification;
                var url = $"{baseUrl}/{requestedAction}";

                return await PostCustomize<SendNotificationViewModel, string>(url, Model);
            }
            return null;
        }
    }
}
