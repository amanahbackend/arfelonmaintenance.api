﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.ExcelSettings
{
    public enum ExcelSheetProperties
    {
        OrderNo = 0,
        OrderType = 1,
        OrderTypeDescription = 2,
        OrderDescription = 3,
        OrderCompanyCode = 4,
        OrderCreatedDateTime = 5,
        OrderDivision = 6,
        OrderDivisionDescription = 7,
        OrderPriority = 8,
        OrderPriorityDescription = 9,
        OrderProblem1 =  10,
        OrderProblem1Description =  11,
        OrderProblem2 =  12,
        OrderProblem2Description =  13,
        OrderProblem3 =  14,
        OrderProblem3Description =  15,
        OrderProblem4 =  16,
        OrderProblem4Description =  17,
        OrderStatus = 18,
        OrderStatusDescription = 19,
        OrderNoteICAgent = 20,
        ContractNo=21,
        ContractType=22,
        ContractTypeDescription=23,
        ContractValidFrom =24,
        ContractExpirationDate = 25,
        FunctionalLocation = 26,
        FunctionalLocationDescription = 27,
        CustomerId = 28,
        CustomerName = 29,
        Phone1 = 30,
        Phone2 = 31,
        Paci = 32,
        BuildingType = 33,
        Area = 34,
        AreaDescription = 35,
        Block = 36,
        BlockDescription = 37,
        StreetJaddah = 38,
        HouseKasima = 39,
        Floor = 40,
        ApartmentNo = 41,
        Caller_ID=42
    }
}
