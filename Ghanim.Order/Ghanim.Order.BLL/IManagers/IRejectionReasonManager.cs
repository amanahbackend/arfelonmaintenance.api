﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.IManagers
{
    public interface IRejectionReasonManager : IRepository<RejectionReason>
    {
    }
}
