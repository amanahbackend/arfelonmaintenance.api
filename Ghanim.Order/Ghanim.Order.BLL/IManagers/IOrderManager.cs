﻿using CommonEnum;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.IManagers
{
    public interface IOrderManager : IRepository<OrderObject>
    {
        ProcessResult<bool> SetAcceptence(int orderId, bool acceptenceFlag, int? rejectionReasonId, string rejectionReason, int? initialStatusId, string initialStatusName, int? initialSubStatusId, string initialSubStatusName);
        ProcessResult<bool> AcceptOrderAndSetOnTravel(int orderId, string statusName, int statusId, string subStatusName, int subStatusId);
        ProcessResult<bool> BulkAssign(List<int> orderIds, AssignType assignType, int? teamId, int? dispatcherId, string dispatcherName, int? supervisorId, string supervisorName, int statusId, string statusName);
        ProcessResult<bool> BulkUnAssign(List<int> orderIds, int statusId, string statusName);
        ProcessResult<bool> Assign(int orderId, int teamId, int nextStatusId, string nextStatusName, int nextSubStatusId, string nextSubStatusName);
        ProcessResult<bool> UnAssign(int orderId, int nextStatusId, string nextStatusName);
        ProcessResult<bool> ChangeOrderRank(List<OrderObject> teamOrders, Dictionary<int, int> orders);
        ProcessResult<List<OrderObject>> Add(List<OrderObject> ordersLst);
        ProcessResult<List<OrderObject>> GetDispatcherOrders(int supervisorId, int state);
        ProcessResult<bool> AssignDispatcher(int orderId, int dispatcherId);
        ProcessResult<bool> UnAssignDispatcher(int orderId);
    }
}