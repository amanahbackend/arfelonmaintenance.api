﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.IManagers
{
    public interface ILang_OrderTypeManager : IRepository<Lang_OrderType>
    {
        ProcessResult<List<Lang_OrderType>> GetAllLanguagesByOrderTypeId(int OrderTypeId);
        ProcessResult<bool> UpdateByOrderType(List<Lang_OrderType> entities);
    }
}

