﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.IManagers
{
    public interface ILang_OrderStatusManager : IRepository<Lang_OrderStatus>
    {
        ProcessResult<List<Lang_OrderStatus>> GetAllLanguagesByOrderStatusId(int OrderStatusId);
        ProcessResult<bool> UpdateByOrderStatus(List<Lang_OrderStatus> entities);
    }
}