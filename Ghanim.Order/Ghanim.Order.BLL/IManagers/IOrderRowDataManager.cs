﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.IManagers
{
    public interface IOrderRowDataManager : IRepository<OrderRowData>
    {
    }
}
