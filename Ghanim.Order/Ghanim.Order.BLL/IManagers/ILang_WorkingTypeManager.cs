﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.IManagers
{
    public interface ILang_WorkingTypeManager : IRepository<Lang_WorkingType>
    {
        ProcessResult<List<Lang_WorkingType>> GetAllLanguagesByWorkingTypeId(int WorkingTypeId);
        ProcessResult<bool> UpdateByWorkingType(List<Lang_WorkingType> entities);
    }
}

