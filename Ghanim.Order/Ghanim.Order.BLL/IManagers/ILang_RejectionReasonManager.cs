﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.IManagers
{
    public interface ILang_RejectionReasonManager : IRepository<Lang_RejectionReason>
    {
    }
}
