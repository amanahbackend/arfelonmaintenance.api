﻿using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace Ghanim.Order.BLL.IManagers
{
    public interface IReadOrderManager
    {
        ProcessResult<List<OrderObject>> Process(string path, UploadFile Uploadfile);
        void ReadFile();
    }
}