﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.IManagers
{
    public interface IOrderSubStatusManager : IRepository<OrderSubStatus>
    {
        ProcessResult<List<OrderSubStatus>> GetByStatusId(int statusId);
    }
}
