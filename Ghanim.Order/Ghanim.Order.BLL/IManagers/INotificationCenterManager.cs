﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.IManagers
{
    public interface INotificationCenterManager : IRepository<NotificationCenter>
    {
        ProcessResult<List<NotificationCenter>> FilterByDate(DateTime? startDate, DateTime? endDate);
        ProcessResult<bool> MarkAllAsRead(string recieverId);
    }
}