﻿using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.Order.BLL.ExcelSettings;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace Ghanim.Order.BLL.Managers
{
    public class ReadOrderManager : IReadOrderManager
    {
        private IOrderTypeManager orderTypeManager;
        private IOrderStatusManager orderStatusManager;
        private IOrderPriorityManager orderPriorityManager;
        private IDivisionManager divisionManager;
        private IOrderProblemManager orderProblemManager;
        private IContractTypesManager contractTypesManager;
        private IAreasManager areasManager;

        public ReadOrderManager(IOrderTypeManager _orderTypeManager,
            IOrderStatusManager _orderStatusManager,
            IOrderPriorityManager _orderPriorityManager,
            IDivisionManager _divisionManager,
            IOrderProblemManager _orderProblemManager,
            IContractTypesManager _contractTypesManager,
            IAreasManager _areasManager)
        {
            orderTypeManager = _orderTypeManager;
            orderStatusManager = _orderStatusManager;
            orderPriorityManager = _orderPriorityManager;
            divisionManager = _divisionManager;
            orderProblemManager = _orderProblemManager;
            contractTypesManager = _contractTypesManager;
            areasManager = _areasManager;
        }


        //private static FileUploadSettings fileUploadSettings;

        public static OrderObject GetItems(IList<string> rowData, IList<string> columnNames)
        {
            //public int TypeId { get; set; }
            //public string TypeName { get; set; }
            //public int StatusId { get; set; }
            //public string StatusName { get; set; }
            //public int SubStatusId { get; set; }
            //public string SubStatusName { get; set; }
            //public int ProblemId { get; set; }
            //public string ProblemName { get; set; }
            //public int PriorityId { get; set; }
            //public string PriorityName { get; set; }
            //public int CompanyCodeId { get; set; }
            //public string CompanyCodeName { get; set; }
            //public int DivisionId { get; set; }
            //public string DivisionName { get; set; }
            //public string ICAgentNote { get; set; }
            //public string DispatcherNote { get; set; }
            //public string CancellationReason { get; set; }
            //public string GeneralNote { get; set; }
            //public string OrderDescription { get; set; }
            //// Customer info
            //public string CustomerCode { get; set; }
            //public string CustomerName { get; set; }
            //public string PhoneOne { get; set; }
            //public string PhoneTwo { get; set; }
            // public string Caller_ID { get; set; }
            //// Address info
            //public string PACI { get; set; }
            //public string FunctionalLocation { get; set; }
            //public string HouseKasima { get; set; }
            //public string Floor { get; set; }
            //public string AppartmentNo { get; set; }
            //public int StreetId { get; set; }
            //public string StreetName { get; set; }
            //public int BlockId { get; set; }
            //public string BlockName { get; set; }
            //public int AreaId { get; set; }
            //public string AreaName { get; set; }
            //public int GovId { get; set; }
            //public string GovName { get; set; }
            //public string AddressNote { get; set; }
            //public decimal Long { get; set; }
            //public decimal Lat { get; set; }
            //// Contract info
            //public string ContracCode { get; set; }
            //public int ContractTypeId { get; set; }
            //public string ContractTypeName { get; set; }
            //public DateTime ContractStartDate { get; set; }
            //public DateTime ContractExpiryDate { get; set; }
            var orderObject = new OrderObject()
            {
                //Code = rowData[(int)ExcelSheetProperties.OrderNo].ToString(),
                //TypeId = rowData[(int)ExcelSheetProperties.OrderType]
            };
            return orderObject;
        }

        public  ProcessResult<List<OrderObject>> Process(string path, UploadFile Uploadfile)
        {
            ProcessResult<List<OrderObject>> result = new ProcessResult<List<OrderObject>>();
            try
            {
                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                //Uploadfile = mapper.Map<UploadFileViewModel, UploadFile>(file);
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(Uploadfile.FileName, path);
                    IList<OrderObject> dataList = ExcelReader.GetDataToList(excelPath, GetItems);
                    result.Data = SaveItems(dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }
        private static List<OrderObject> SaveItems(IList<OrderObject> dataList)
        {
            List<OrderObject> result = new List<OrderObject>();
            foreach (var item in dataList)
            {
                result.Add(item);
            }
            return result;
        }

        public static void MoveFile(string fileName, StreamReader reader)
        {
            //var sourceFolder = fileUploadSettings.SourceFilePath;
            //var targetFolder = fileUploadSettings.TargetFilePath;

            //string source = System.IO.Path.Combine(sourceFolder, fileName);
            //string target = System.IO.Path.Combine(targetFolder, fileName);
            try
            {
                reader.Dispose();
                //File.Move(source, target);
            }
            catch
            {
                reader.Dispose();
            }
        }

        public void ReadFile()
        {
            try
            {
                string[] files = Directory.GetFiles(FileUploadSettings.SourceFilePath, $"*.{FileUploadSettings.FileExtention}");

                if (files.Length > 0)
                {

                    for (int i = 0; i < files.Length; i++)
                        files[i] = Path.GetFileName(files[i]);

                    for (int j = 0; j < files.Length; j++)
                    {
                        var fileName = System.IO.Path.Combine(FileUploadSettings.SourceFilePath, files[j]);

                        var reader = new StreamReader(File.OpenRead(fileName));
                        List<OrderObject> orderList = new List<OrderObject>();
                        bool isFirst = true;
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');

                            if (isFirst == false)
                            {
                                if (values != null && values.Length > 0)
                                {

                                    try
                                    {
                                        DateTime? cDate = null;
                                        if (values[5] != "")
                                        {
                                            var date = values[5].ToString();
                                            int len = date.Length;
                                            cDate = DateTime.ParseExact(values[5].ToString(), "dd.MM.yyyy HH:mm:ss", null);
                                        }

                                        OrderObject temp = new OrderObject();
                                        temp.Code = values[(int)ExcelSheetProperties.OrderNo].ToString();
                                        temp.TypeName = values[(int)ExcelSheetProperties.OrderType].ToString();
                                        temp.TypeId = orderTypeManager.Get(x => x.Code == temp.TypeName).Data.Id;
                                        temp.StatusName = values[(int)ExcelSheetProperties.OrderStatus].ToString();
                                        temp.StatusId = orderStatusManager.Get(x => x.Name == temp.StatusName).Data.Id;
                                        temp.PriorityName = values[(int)ExcelSheetProperties.OrderPriority].ToString();
                                        temp.PriorityId = orderPriorityManager.Get(x => x.Name == temp.PriorityName).Data.Id;
                                        temp.CompanyCodeName = values[(int)ExcelSheetProperties.OrderCompanyCode].ToString();
                                        temp.CreatedDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString(), "dd.MM.yyyy HH:mm:ss", null);
                                        temp.DivisionName = values[(int)ExcelSheetProperties.OrderDivisionDescription].ToString();
                                        temp.DivisionId = divisionManager.Get(x => x.Name == temp.DivisionName).Data.Id;
                                        temp.ProblemName = values[(int)ExcelSheetProperties.OrderProblem1Description].ToString();
                                        temp.ProblemId = orderProblemManager.Get(x => x.Name == temp.ProblemName).Data.Id;
                                        temp.ICAgentNote = values[(int)ExcelSheetProperties.OrderNoteICAgent].ToString();
                                        temp.ContractCode = values[(int)ExcelSheetProperties.ContractNo].ToString();
                                        temp.ContractTypeName = values[(int)ExcelSheetProperties.ContractTypeDescription].ToString();
                                        temp.ContractTypeId = contractTypesManager.Get(x => x.Code == temp.ContractTypeName).Data.Id;
                                        temp.ContractStartDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom].ToString(), "dd.MM.yyyy", null);
                                        temp.ContractExpiryDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractExpirationDate].ToString(), "dd.MM.yyyy", null);
                                        temp.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
                                        temp.CustomerCode = values[(int)ExcelSheetProperties.CustomerId].ToString();
                                        temp.CustomerName = values[(int)ExcelSheetProperties.CustomerName].ToString();
                                        temp.PhoneOne = values[(int)ExcelSheetProperties.Phone1].ToString();
                                        temp.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].ToString();
                                        temp.PACI = values[(int)ExcelSheetProperties.Paci].ToString();
                                        temp.AreaName = values[(int)ExcelSheetProperties.AreaDescription].ToString();
                                        temp.AreaId = areasManager.Get(x => x.Name == temp.AreaName).Data.Id;
                                        temp.BlockId = values[(int)ExcelSheetProperties.Block].ToInt32();
                                        temp.BlockName = values[(int)ExcelSheetProperties.BlockDescription].ToString();
                                        temp.StreetName = values[(int)ExcelSheetProperties.StreetJaddah].ToString();
                                        temp.HouseKasima = values[(int)ExcelSheetProperties.HouseKasima].ToString();
                                        temp.Floor = values[(int)ExcelSheetProperties.Floor].ToString();
                                        temp.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
                                        temp.AppartmentNo = values[(int)ExcelSheetProperties.ApartmentNo].ToString();
                                        temp.Caller_ID= values[(int)ExcelSheetProperties.Caller_ID].ToString();
                                        temp.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].ToString();
                                        orderList.Add(temp);

                                    }
                                    catch (Exception e)
                                    {

                                        Console.WriteLine("Entity of type \"{0}\" with error \"{1}\"",
                                            e.GetType(), e.Message);
                                    }
                                }
                            }
                            else
                            {
                                isFirst = false;
                            }

                            //if (temp.PACI != null)
                            //{
                            //    PACIModel tempPaci = GetLocationByPACI(temp.PACI);
                            //    if (tempPaci != null && tempPaci.features != null && tempPaci.features.Count > 0)
                            //    {
                            //        tempAddress.PACI = tempPaci.features[0].attributes.civilid;
                            //        temp.GovName = tempPaci.features[0].attributes.governorateenglish;
                            //        temp.BlockName = tempPaci.features[0].attributes.blockenglish;
                            //        temp.StreetName = tempPaci.features[0].attributes.streetenglish;
                            //        var areaid = tempPaci.features[0].attributes.neighborhoodid;
                            //        var areaname = tempPaci.features[0].attributes.neighborhoodenglish;
                            //        tempAddress.AppartmentNo = tempPaci.features[0].attributes.houseenglish;
                            //        tempAddress.Floor = tempPaci.features[0].attributes.floor_no;
                            //        tempAddress.House_Kasima = tempPaci.features[0].attributes.houseenglish;

                            //        int? areaID;
                            //        if (areaid != null)
                            //        {
                            //            int id = Convert.ToInt32(areaid);
                            //            var areaTemp = db.Areas.Where(x => x.OriginalId == id).FirstOrDefault();
                            //            if (areaTemp != null)
                            //            {
                            //                areaID = areaTemp.Id;
                            //                tempAddress.AreaId = areaID;
                            //            }
                            //            else
                            //            {
                            //                Area tempArea = new Area()
                            //                {
                            //                    Name = areaname,
                            //                    OriginalId = id
                            //                };
                            //                db.Areas.Add(tempArea);
                            //                db.SaveChanges();
                            //                tempAddress.AreaId = tempArea.Id;
                            //            }
                            //        }

                            //        if (temp.GovName != null)
                            //        {
                            //            var name = temp.GovName;
                            //            var govTemp = db.Governorates.Where(x => x.Name == name).FirstOrDefault();
                            //            if (govTemp != null)
                            //            {
                            //                tempAddress.GovId = govTemp.Id;
                            //            }
                            //            else
                            //            {
                            //                Governorate tempGov = new Governorate()
                            //                {
                            //                    Name = temp.GovName
                            //                };
                            //                db.Governorates.Add(tempGov);
                            //                db.SaveChanges();
                            //                tempAddress.GovId = tempGov.Id;
                            //            }
                            //        }
                            //        else if (temp.GovId != null)
                            //        {
                            //            var id = temp.GovId;
                            //            var govTemp = db.Governorates.Where(x => x.Id == id).FirstOrDefault();
                            //            if (govTemp != null)
                            //            {
                            //                tempAddress.GovId = govTemp.Id;
                            //            }
                            //            else
                            //            {
                            //                Governorate tempGov = new Governorate()
                            //                {
                            //                    Name = temp.GovName
                            //                };
                            //                db.Governorates.Add(tempGov);
                            //                db.SaveChanges();
                            //                tempAddress.GovId = tempGov.Id;
                            //            }
                            //        }

                            //        if (temp.BlockName != null)
                            //        {
                            //            var name = temp.BlockName;
                            //            var blockTemp = db.Blocks.Where(x => x.Name == name).FirstOrDefault();
                            //            if (blockTemp != null)
                            //            {
                            //                tempAddress.BlockId = blockTemp.Id;
                            //            }
                            //            else
                            //            {
                            //                Block tempBlock = new Block()
                            //                {
                            //                    Name = temp.BlockName,
                            //                    BlockNo = temp.BlockName
                            //                };
                            //                db.Blocks.Add(tempBlock);
                            //                db.SaveChanges();
                            //                tempAddress.BlockId = tempBlock.Id;
                            //            }
                            //        }
                            //        else if (temp.BlockId > 0)
                            //        {
                            //            var id = temp.BlockId;
                            //            var blockTemp = db.Blocks.Where(x => x.Name == id.ToString()).FirstOrDefault();
                            //            if (blockTemp != null)
                            //            {
                            //                tempAddress.BlockId = blockTemp.Id;
                            //            }
                            //            else
                            //            {
                            //                Block tempBlock = new Block()
                            //                {
                            //                    Name = temp.BlockName,
                            //                    BlockNo = temp.BlockName
                            //                };
                            //                db.Blocks.Add(tempBlock);
                            //                db.SaveChanges();
                            //                tempAddress.BlockId = temp.Id;
                            //            }
                            //        }

                            //        if (temp.StreetName != null)
                            //        {
                            //            var name = temp.StreetName;
                            //            var streetTemp = db.Street_Jaddah.Where(x => x.Name == name).FirstOrDefault();
                            //            if (streetTemp != null)
                            //            {
                            //                tempAddress.StreetId = streetTemp.Id;
                            //            }
                            //            else
                            //            {
                            //                Street_Jaddah tempStreet = new Street_Jaddah()
                            //                {
                            //                    Name = temp.StreetName
                            //                };
                            //                db.Street_Jaddah.Add(tempStreet);
                            //                db.SaveChanges();
                            //                tempAddress.StreetId = tempStreet.Id;
                            //            }
                            //        }

                            //        tempAddress.Lat = tempPaci.features[0].attributes.lat;
                            //        tempAddress.Long = tempPaci.features[0].attributes.lon;
                            //    }
                            //    else
                            //    {

                            //        //int? areaID;
                            //        //if (temp.AreaIdFile != null)
                            //        //{
                            //        //	var areaTemp = db.Areas.Where(x => x.OriginalId == temp.AreaIdFile).FirstOrDefault();
                            //        //	if (areaTemp != null)
                            //        //	{
                            //        //		areaID = areaTemp.Id;
                            //        //		tempAddress.AreaId = areaID;
                            //        //	}
                            //        //}

                            //        //tempAddress.BlockId = temp.BlockId;
                            //        //tempAddress.GovId = temp.GovId;
                            //        //tempAddress.Floor = temp.Floor;
                            //        ////tempAddress.PACI = temp.PACI;
                            //        //tempAddress.StreetId = temp.StreetId;
                            //        //tempAddress.House_Kasima = temp.House_Kasima;
                            //    }
                            //}
                            //else
                            //{

                            //    //int? areaID;
                            //    //if (temp.AreaIdFile != null)
                            //    //{
                            //    //	var areaTemp = db.Areas.Where(x => x.OriginalId == temp.AreaIdFile).FirstOrDefault();
                            //    //	if (areaTemp != null)
                            //    //	{
                            //    //		areaID = areaTemp.Id;
                            //    //		tempAddress.AreaId = areaID;
                            //    //	}
                            //    //}
                            //    //tempAddress.BlockId = temp.BlockId;
                            //    //tempAddress.GovId = temp.GovId;
                            //    //tempAddress.Floor = temp.Floor;
                            //    //tempAddress.PACI = temp.PACI;
                            //    //tempAddress.StreetId = temp.StreetId;
                            //    //tempAddress.House_Kasima = temp.House_Kasima;

                            //}





                        }


                        Console.WriteLine(string.Format("End of file {0}", files[j]));
                        reader.Dispose();
                        MoveFile(files[j], reader);
                        Console.WriteLine(" \r \n Hi  \r \n  Task Finish press any key to close. \r \n Thanks");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("ISSUE");
            }
        }
    }
}
