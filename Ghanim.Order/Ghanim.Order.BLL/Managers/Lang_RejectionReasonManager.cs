﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_RejectionReasonManager : Repository<Lang_RejectionReason>, ILang_RejectionReasonManager
    {
        public Lang_RejectionReasonManager(OrderDBContext context)
            : base(context)
        {
        }
    }
}