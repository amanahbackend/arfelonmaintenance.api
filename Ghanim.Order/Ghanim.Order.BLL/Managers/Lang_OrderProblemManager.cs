﻿using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.DataManagement.BLL.IManagers;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_OrderProblemManager : Repository<Lang_OrderProblem>, ILang_OrderProblemManager
    {
        IServiceProvider _serviceprovider;

        public Lang_OrderProblemManager(IServiceProvider serviceprovider, OrderDBContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager => _serviceprovider.GetService<ISupportedLanguagesManager>();

        private IOrderProblemManager orderProblemManager => _serviceprovider.GetService<IOrderProblemManager>();

        public ProcessResult<List<Lang_OrderProblem>> GetAllLanguagesByOrderProblemId(int OrderProblemId)
        {
            List<Lang_OrderProblem> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_OrderProblem_ID == OrderProblemId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderProblem { FK_OrderProblem_ID = OrderProblemId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded(input, null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByOrderProblemId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex, null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderProblemId");
            }
        }

        public ProcessResult<bool> UpdateByOrderProblem(List<Lang_OrderProblem> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var orderProblemRes = orderProblemManager.Get(item.FK_OrderProblem_ID);
                        if (orderProblemRes != null && orderProblemRes.Data != null)
                        {
                            orderProblemRes.Data.Name = item.Name;
                            var updateRes = orderProblemManager.Update(orderProblemRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderProblem = GetByOrderProblemIdAndsupprotedLangId(item.FK_OrderProblem_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderProblem != null && prevLangOrderProblem.Data != null && prevLangOrderProblem.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded(result, null, ProcessResultStatusCode.Succeeded, "UpdateByOrderProblem");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded(result, null, ProcessResultStatusCode.Succeeded, "UpdateByOrderProblem");
            }
        }

        private ProcessResult<List<Lang_OrderProblem>> GetByOrderProblemIdAndsupprotedLangId(int OrderProblemId, int supprotedLangId)
        {
            List<Lang_OrderProblem> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_OrderProblem_ID == OrderProblemId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded(input, null, ProcessResultStatusCode.Succeeded, "GetByOrderProblemIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex, null, ProcessResultStatusCode.Failed, "GetByOrderProblemIdAndsupprotedLangId");
            }
        }
    }
}