﻿using CommonEnum;
using CommonEnums;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.Managers
{
    public class OrderManager : Repository<OrderObject>, IOrderManager
    {
        public IOrderActionManager actionManager;

        public OrderManager(OrderDBContext context)
            : base(context)
        {
        }

        public OrderManager(OrderDBContext context, IOrderActionManager _actionManager)
           : base(context)
        {
            actionManager = _actionManager;
        }

        public ProcessResult<bool> SetAcceptence(int orderId, bool acceptenceFlag, int? rejectionReasonId, string rejectionReason, int? initialStatusId, string initialStatusName, int? initialSubStatusId, string initialSubStatusName)
        {
            try
            {
                var order = Get(x => x.Id == orderId);
                if (order.IsSucceeded)
                {
                    if (acceptenceFlag)
                    {
                        order.Data.AcceptanceFlag = AcceptenceType.Accepted;
                    }
                    else if (acceptenceFlag == false)
                    {
                        if (rejectionReasonId < 1)
                        {
                            return ProcessResultHelper.Failed(false, null, "rejection reason id and name are required!");
                        }
                        order.Data.AcceptanceFlag = AcceptenceType.Rejected;
                        order.Data.RejectionReason = rejectionReason;
                        order.Data.RejectionReasonId = (int)rejectionReasonId;
                        order.Data.StatusId = (int)initialStatusId;
                        order.Data.SubStatusId = (int)initialSubStatusId;
                        order.Data.StatusName = initialStatusName;
                        order.Data.SubStatusName = initialSubStatusName;
                        order.Data.TeamId = 0;
                        order.Data.RankInTeam = 0;
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, null, "Acceptance flag must be a boolean type");
                    }

                    var updateResult = Update(order.Data);
                    if (updateResult.IsSucceeded)
                    {
                        //if (acceptenceFlag)
                        //{
                        //    AddAction(order.Data, OrderActionType.Acceptence);
                        //}
                        //else
                        //{
                        //    AddAction(order.Data, OrderActionType.Rejection);
                        //}

                        return ProcessResultHelper.Succeeded(true);
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, null, updateResult.Status.Message);
                    }
                }
                return ProcessResultHelper.Failed(false, null, order.Status.Message);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }

        public ProcessResult<bool> AcceptOrderAndSetOnTravel(int orderId, string statusName, int statusId, string subStatusName, int subStatusId)
        {
            try
            {
                var order = Get(x => x.Id == orderId);
                if (order.IsSucceeded)
                {
                    order.Data.AcceptanceFlag = AcceptenceType.Accepted;
                    order.Data.StatusId = statusId;
                    order.Data.SubStatusId = subStatusId;
                    order.Data.StatusName = statusName;
                    order.Data.SubStatusName = subStatusName;

                    var updateResult = Update(order.Data);
                    if (updateResult.IsSucceeded)
                    {
                        // AddAction(order.Data, OrderActionType.FirstOrderWork);
                        return ProcessResultHelper.Succeeded(true);
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, null, updateResult.Status.Message);
                    }
                }
                return ProcessResultHelper.Failed(false, null, order.Status.Message);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }

        public ProcessResult<bool> BulkAssign(List<int> orderIds, AssignType assignType, int? teamId, int? dispatcherId, string dispatcherName, int? supervisorId, string supervisorName, int statusId, string statusName)
        {
            var tempOrders = GetAll(x => orderIds.Contains(x.Id));
            if (tempOrders.IsSucceeded && tempOrders.Data.Count > 0)
            {
                for (int i = 0; i < tempOrders.Data.Count; i++)
                {
                    if (assignType == AssignType.Dispatcher)
                    {
                        tempOrders.Data[i].DispatcherId = (int)dispatcherId;
                        tempOrders.Data[i].DispatcherName = dispatcherName;
                        tempOrders.Data[i].SupervisorId = (int)supervisorId;
                        tempOrders.Data[i].SupervisorName = supervisorName;
                        tempOrders.Data[i].StatusId = statusId;
                        tempOrders.Data[i].StatusName = statusName;
                        tempOrders.Data[i].TeamId = 0;
                    }
                    else if (assignType == AssignType.Team)
                    {
                        tempOrders.Data[i].TeamId = (int)teamId;
                        tempOrders.Data[i].StatusId = statusId;
                        tempOrders.Data[i].StatusName = statusName;
                    }
                }
                var updatedOrders = Update(tempOrders.Data);
                if (updatedOrders.IsSucceeded && updatedOrders.Data)
                {
                    for (int i = 0; i < tempOrders.Data.Count; i++)
                    {
                        AddAction(tempOrders.Data[i], OrderActionType.BulkAssign);
                    }
                }

                return updatedOrders;
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while get all orders", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "BulkAssign Manager");
        }

        public ProcessResult<bool> Assign(int orderId, int teamId, int nextStatusId, string nextStatusName, int nextSubStatusId, string nextSubStatusName)
        {
            var tempOrder = Get(x => x.Id == orderId);

            var orders = GetAll(x => x.TeamId == teamId).Data;
            int orderRank = 1;
            if (orders.Count > 0)
            {
                var rankedOrders = orders.OrderByDescending(x => x.RankInTeam).ToList();
                orderRank = (int)rankedOrders.FirstOrDefault().RankInTeam + 1;
            }
            if (tempOrder.IsSucceeded && tempOrder.Data != null)
            {
                tempOrder.Data.TeamId = teamId;
                tempOrder.Data.RankInTeam = orderRank;
                // check status before assign
                // in case it was open
                // in the below check it will be dispatched
                //if (tempOrder.Data.StatusId < nextStatusId)
                //{
                tempOrder.Data.StatusId = nextStatusId;
                tempOrder.Data.StatusName = nextStatusName;
                //}
                // here we need to make it dispatched ==> and sub status as rescheduled
                // when it was hold and in column un assign
                //if (tempOrder.Data.SubStatusId < nextStatusId)
                //{
                tempOrder.Data.SubStatusId = nextSubStatusId;
                tempOrder.Data.SubStatusName = nextSubStatusName;
                tempOrder.Data.AcceptanceFlag = AcceptenceType.NoAction;
                //}

                var updatedOrder = Update(tempOrder.Data);
                if (updatedOrder.IsSucceeded && updatedOrder.Data)
                {
                    // AddAction(tempOrder.Data, OrderActionType.Assign);
                }
                return updatedOrder;
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while get order data", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "Assign Manager");
        }

        public ProcessResult<bool> UnAssign(int orderId, int nextStatusId, string nextStatusName)
        {
            var tempOrder = Get(x => x.Id == orderId);
            if (tempOrder.IsSucceeded && tempOrder.Data != null)
            {
                tempOrder.Data.TeamId = 0;
                // check status before un assign
                // in case it was Dispatched
                // in the below check it will be Open
                //if (tempOrder.Data.StatusId == nextStatusId + 1)
                //{
                tempOrder.Data.StatusId = nextStatusId;
                tempOrder.Data.StatusName = nextStatusName;
                tempOrder.Data.AcceptanceFlag = AcceptenceType.NoAction;

                //}
                // here we need to make it Hold ==> and sub status as the same reason
                // when it was hold and in the team column

                var updatedOrder = Update(tempOrder.Data);
                if (updatedOrder.IsSucceeded && updatedOrder.Data)
                {
                    //AddAction(tempOrder.Data, OrderActionType.UnAssgin);
                }
                return updatedOrder;
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while get order data", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "Assign Manager");
        }

        public ProcessResult<bool> BulkUnAssign(List<int> orderIds, int statusId, string statusName)
        {
            var tempOrders = GetAll(x => orderIds.Contains(x.Id));
            if (tempOrders.IsSucceeded && tempOrders.Data.Count > 0)
            {
                for (int i = 0; i < tempOrders.Data.Count; i++)
                {
                    tempOrders.Data[i].TeamId = 0;
                    tempOrders.Data[i].StatusId = statusId;
                    tempOrders.Data[i].StatusName = statusName;
                }
                var updatedOrders = Update(tempOrders.Data);
                if (updatedOrders.IsSucceeded && updatedOrders.Data)
                {
                    for (int i = 0; i < tempOrders.Data.Count; i++)
                    {
                        AddAction(tempOrders.Data[i], OrderActionType.BulkUnAssign);
                    }
                }

                return updatedOrders;
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while get all orders", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "BulkUnAssign Manager");
        }

        public ProcessResult<bool> ChangeOrderRank(List<OrderObject> teamOrders, Dictionary<int, int> orders)
        {
            try
            {
                for (int i = 0; i < teamOrders.Count; i++)
                {
                    teamOrders[i].RankInTeam = orders[teamOrders[i].Id];
                }
                var updatedOrders = Update(teamOrders);

                if (updatedOrders.IsSucceeded && updatedOrders.Data)
                {
                    for (int i = 0; i < teamOrders.Count; i++)
                    {
                        //  AddAction(teamOrders[i], OrderActionType.ChangeTeamRank);
                    }
                }
                return updatedOrders;

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex, "something wrong", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "BulkUnAssign Manager");
            }
        }

        private void AddAction(OrderObject order, OrderActionType actionType)
        {
            actionManager.Add(new OrderAction()
            {
                ActionDate = DateTime.Now,
                StatusId = order.StatusId,
                StatusName = order.StatusName,
                OrderId = order.Id,
                SubStatusId = order.SubStatusId,
                SubStatusName = order.SubStatusName,
                ActionTypeId = (int)actionType,
                ActionTypeName = EnumManager<OrderActionType>.GetName(actionType),
                SupervisorId = order.SupervisorId,
                DispatcherId = order.DispatcherId,
                DispatcherName = order.DispatcherName,
                SupervisorName = order.SupervisorName,
                TeamId = order.TeamId
            });
        }

        public ProcessResult<List<OrderObject>> Add(List<OrderObject> ordersLst)
        {
            try
            {
                for (int i = 0; i < ordersLst.Count; i++)
                {
                    // Check for repeated call
                    DateTime currentTime = DateTime.Now;

                    var customerOrders = GetAll(x => x.CustomerCode == ordersLst[i].CustomerCode && (currentTime - x.CreatedDate).TotalHours <= 24);
                    if (customerOrders.IsSucceeded && customerOrders.Data.Count > 0)
                    {
                        ordersLst[i].IsRepeatedCall = true;
                    }
                }
                var addedOrders = base.Add(ordersLst);
                if (addedOrders.IsSucceeded && addedOrders.Data.Count > 0)
                {

                    for (int i = 0; i < addedOrders.Data.Count; i++)
                    {
                        AddAction(addedOrders.Data[i], OrderActionType.AddOrder);
                    }
                }
                return addedOrders;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProcessResult<List<OrderObject>> GetDispatcherOrders(int SupervisorId, int state)
        {
            var orders = GetAll(x => x.SupervisorId == SupervisorId && x.DispatcherId != 0 && x.StatusId == state);
            return orders;
        }

        public ProcessResult<bool> AssignDispatcher(int orderId, int dispatcherId)
        {
            var orders = Get(x => x.Id == orderId);

            if (orders.Data != null)
            {
                orders.Data.DispatcherId = dispatcherId;

                var updatedOrder = Update(orders.Data);

                return updatedOrder;
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while get order data", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "Assign Manager");
        }

        public ProcessResult<bool> UnAssignDispatcher(int orderId)
        {
            var orders = Get(x => x.Id == orderId);

            if (orders.Data != null)
            {
                orders.Data.DispatcherId = 0;

                var updatedOrder = Update(orders.Data);

                return updatedOrder;
            }
            return ProcessResultHelper.Failed(false, null, "something wrong while get order data", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "Assign Manager");
        }
    }
}