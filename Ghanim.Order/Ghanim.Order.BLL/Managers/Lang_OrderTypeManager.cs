﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.BLL.IManagers;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_OrderTypeManager : Repository<Lang_OrderType>, ILang_OrderTypeManager
    {
        IServiceProvider _serviceprovider;

        public Lang_OrderTypeManager(IServiceProvider serviceprovider, OrderDBContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IOrderTypeManager ordertypeManager
        {
            get
            {
                return _serviceprovider.GetService<IOrderTypeManager>();
            }
        }
        public ProcessResult<List<Lang_OrderType>> GetAllLanguagesByOrderTypeId(int OrderTypeId)
        {
            List<Lang_OrderType> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_OrderType_ID == OrderTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderType { FK_OrderType_ID = OrderTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_OrderType>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByOrderTypeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderTypeId");
            }
        }

        public ProcessResult<bool> UpdateByOrderType(List<Lang_OrderType> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var ordertypeRes = ordertypeManager.Get(item.FK_OrderType_ID);
                        if (ordertypeRes != null && ordertypeRes.Data != null)
                        {
                            ordertypeRes.Data.Name = item.Name;
                            var updateRes = ordertypeManager.Update(ordertypeRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty)
                    {
                        var prevLangOrderType = GetByOrderTypeIdAndsupprotedLangId(item.FK_OrderType_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderType != null && prevLangOrderType.Data != null && prevLangOrderType.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByOrderType");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByOrderType");
            }
        }

        private ProcessResult<List<Lang_OrderType>> GetByOrderTypeIdAndsupprotedLangId(int OrderTypeId, int supprotedLangId)
        {
            List<Lang_OrderType> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_OrderType_ID == OrderTypeId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_OrderType>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByOrderTypeIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByOrderTypeIdAndsupprotedLangId");
            }
        }
    }
}