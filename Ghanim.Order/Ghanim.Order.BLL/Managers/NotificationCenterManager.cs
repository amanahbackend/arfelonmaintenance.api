﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.Managers
{
    public class NotificationCenterManager : Repository<NotificationCenter>, INotificationCenterManager
    {
        public NotificationCenterManager(OrderDBContext context)
            : base(context)
        {
        }

        public ProcessResult<List<NotificationCenter>> FilterByDate(DateTime? startDate, DateTime? endDate)
        {
            try
            {
                List<NotificationCenter> result = new List<NotificationCenter>();
                if ((endDate == null && startDate == null))
                {
                    result = GetAll().Data.ToList();
                }
                else if (startDate == null)
                {
                    result = GetAllQuerable().Data.Where(x => x.CreatedDate <= endDate).ToList();
                }
                else if (endDate == null)
                {
                    result = GetAllQuerable().Data.Where(x => x.CreatedDate >= startDate).ToList();
                }
                else
                {
                    result = GetAllQuerable().Data.Where(x => (x.CreatedDate >= startDate) && (x.CreatedDate <= endDate)).ToList();
                }
                return ProcessResultHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<NotificationCenter>>(null, ex);
            }
        }
        public ProcessResult<bool> MarkAllAsRead(string recieverId)
        {
            try
            {
                var result = GetAllQuerable().Data.Where(x => x.RecieverId == recieverId && x.IsRead == false).ToList();

                if(result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        item.IsRead = true;
                    }

                    var updatedOrder = Update(result);

                    return ProcessResultHelper.Succeeded(true);
                }
                return ProcessResultHelper.Succeeded(true);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}
