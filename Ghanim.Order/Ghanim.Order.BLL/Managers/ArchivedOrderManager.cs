﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;

namespace Ghanim.Order.BLL.Managers
{
    public class ArchivedOrderManager : Repository<ArchivedOrder>, IArchivedOrderManager
    {
        public ArchivedOrderManager(OrderDBContext context): base(context)
        {
        }
    }
}
