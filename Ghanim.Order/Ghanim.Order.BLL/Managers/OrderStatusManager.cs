﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.Managers
{
    public class OrderStatusManager : Repository<OrderStatus>, IOrderStatusManager
    {
        public OrderStatusManager(OrderDBContext context)
            : base(context)
        {
        }

        public ProcessResult<List<OrderStatus>> GetAllExclude(List<int> excludedStatuseIds)
        {
            return GetAll(x => !excludedStatuseIds.Contains(x.Id));
        }
    }
}