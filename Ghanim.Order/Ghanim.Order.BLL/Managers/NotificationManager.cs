﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Ghanim.Order.EFCore.MSSQL.Context;

namespace Ghanim.Order.BLL.Managers
{
    public class NotificationManager : Repository<Notification>, INotificationManager
    {
        public NotificationManager(OrderDBContext context) : base(context)
        {
        }
    }
}
