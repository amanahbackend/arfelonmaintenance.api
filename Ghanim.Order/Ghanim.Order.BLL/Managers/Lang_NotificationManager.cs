﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Ghanim.Order.EFCore.MSSQL.Context;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_NotificationManager : Repository<Lang_Notification>, ILang_NotificationManager
    {
        public Lang_NotificationManager(OrderDBContext context) : base(context)
        {
        }
    }
}
