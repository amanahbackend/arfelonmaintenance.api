﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.Managers
{
    public class RejectionReasonManager : Repository<RejectionReason>, IRejectionReasonManager
    {
        public RejectionReasonManager(OrderDBContext context)
            : base(context)
        {
        }
    }
}