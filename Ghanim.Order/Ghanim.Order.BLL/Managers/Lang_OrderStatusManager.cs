﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.EFCore.MSSQL.Context;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_OrderStatusManager : Repository<Lang_OrderStatus>, ILang_OrderStatusManager
    {
        IServiceProvider _serviceprovider;

        public Lang_OrderStatusManager(IServiceProvider serviceprovider, OrderDBContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IOrderStatusManager OrderStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderStatusManager>();
            }
        }

        public ProcessResult<List<Lang_OrderStatus>> GetAllLanguagesByOrderStatusId(int orderStatusId)
        {
            List<Lang_OrderStatus> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_OrderStatus_ID == orderStatusId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderStatus { FK_OrderStatus_ID = orderStatusId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }

        public ProcessResult<bool> UpdateByOrderStatus(List<Lang_OrderStatus> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var orderstatusRes = OrderStatusmanager.Get(item.FK_OrderStatus_ID);
                        if (orderstatusRes != null && orderstatusRes.Data != null)
                        {
                            orderstatusRes.Data.Name = item.Name;
                            var updateRes = OrderStatusmanager.Update(orderstatusRes.Data);
                        }
                    }

                    if (item.Id == 0 && !string.IsNullOrEmpty(item.Name))
                    {
                        var prevLangOrderStatus = GetByOrderStatusIdAndsupprotedLangId(item.FK_OrderStatus_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderStatus != null && prevLangOrderStatus.Data != null && prevLangOrderStatus.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(result, ex);
            }
        }

        private ProcessResult<List<Lang_OrderStatus>> GetByOrderStatusIdAndsupprotedLangId(int orderStatusId, int supprotedLangId)
        {
            List<Lang_OrderStatus> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_OrderStatus_ID == orderStatusId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded(input);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(input, ex);
            }
        }
    }
}