﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.Managers
{
    public class OrderTypeManager : Repository<OrderType>, IOrderTypeManager
    {
        public OrderTypeManager(OrderDBContext context)
            : base(context)
        {
        }
    }
}
