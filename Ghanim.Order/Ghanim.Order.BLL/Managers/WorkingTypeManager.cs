﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.BLL.Managers
{
    public class WorkingTypeManager : Repository<WorkingType>, IWorkingTypeManager
    {
        public WorkingTypeManager(OrderDBContext context)
            : base(context)
        {
        }
    }
}
