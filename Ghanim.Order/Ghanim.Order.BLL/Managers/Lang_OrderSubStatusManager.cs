﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.BLL.IManagers;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_OrderSubStatusManager : Repository<Lang_OrderSubStatus>, ILang_OrderSubStatusManager
    {
        IServiceProvider _serviceprovider;

        public Lang_OrderSubStatusManager(IServiceProvider serviceprovider, OrderDBContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private ISupportedLanguagesManager ordersubstatusManager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        public ProcessResult<List<Lang_OrderSubStatus>> GetAllLanguagesByOrderSubStatusId(int OrderSubStatusId)
        {
            List<Lang_OrderSubStatus> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_OrderSubStatus_ID == OrderSubStatusId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderSubStatus { FK_OrderSubStatus_ID = OrderSubStatusId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_OrderSubStatus>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByOrderSubStatusId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderSubStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderSubStatusId");
            }
        }

        public ProcessResult<bool> UpdateByOrderSubStatus(List<Lang_OrderSubStatus> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var ordersubstatusRes = ordersubstatusManager.Get(item.FK_OrderSubStatus_ID);
                        if (ordersubstatusRes != null && ordersubstatusRes.Data != null)
                        {
                            ordersubstatusRes.Data.Name = item.Name;
                            var updateRes = ordersubstatusManager.Update(ordersubstatusRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderSubStatus = GetByOrderSubStatusIdAndsupprotedLangId(item.FK_OrderSubStatus_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderSubStatus != null && prevLangOrderSubStatus.Data != null && prevLangOrderSubStatus.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByOrderSubStatus");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByOrderSubStatus");
            }
        }

        private ProcessResult<List<Lang_OrderSubStatus>> GetByOrderSubStatusIdAndsupprotedLangId(int OrderSubStatusId, int supprotedLangId)
        {
            List<Lang_OrderSubStatus> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_OrderSubStatus_ID == OrderSubStatusId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_OrderSubStatus>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByOrderSubStatusIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderSubStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByOrderSubStatusIdAndsupprotedLangId");
            }
        }
    }
}
