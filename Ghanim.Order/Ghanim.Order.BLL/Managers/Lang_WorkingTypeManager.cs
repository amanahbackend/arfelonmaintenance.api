﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.BLL.IManagers;

namespace Ghanim.Order.BLL.Managers
{
    public class Lang_WorkingTypeManager : Repository<Lang_WorkingType>, ILang_WorkingTypeManager
    {
        IServiceProvider _serviceprovider;

        public Lang_WorkingTypeManager(IServiceProvider serviceprovider, OrderDBContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IWorkingTypeManager workingtypeManager
        {
            get
            {
                return _serviceprovider.GetService<IWorkingTypeManager>();
            }
        }
        public ProcessResult<List<Lang_WorkingType>> GetAllLanguagesByWorkingTypeId(int WorkingTypeId)
        {
            List<Lang_WorkingType> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_WorkingType_ID == WorkingTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_WorkingType { FK_WorkingType_ID = WorkingTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_WorkingType>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByWorkingTypeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_WorkingType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByWorkingTypeId");
            }
        }

        public ProcessResult<bool> UpdateByWorkingType(List<Lang_WorkingType> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var workingtypeRes = workingtypeManager.Get(item.FK_WorkingType_ID);
                        if (workingtypeRes != null && workingtypeRes.Data != null)
                        {
                            workingtypeRes.Data.Name = item.Name;
                            var updateRes = workingtypeManager.Update(workingtypeRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty)
                    {
                        var prevLangWorkingType = GetByWorkingTypeIdAndsupprotedLangId(item.FK_WorkingType_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangWorkingType != null && prevLangWorkingType.Data != null && prevLangWorkingType.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByOrderType");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByOrderType");
            }
        }

        private ProcessResult<List<Lang_WorkingType>> GetByWorkingTypeIdAndsupprotedLangId(int WorkingTypeId, int supprotedLangId)
        {
            List<Lang_WorkingType> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_WorkingType_ID == WorkingTypeId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_WorkingType>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByWorkingTypeIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_WorkingType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByWorkingTypeIdAndsupprotedLangId");
            }
        }
    }
}