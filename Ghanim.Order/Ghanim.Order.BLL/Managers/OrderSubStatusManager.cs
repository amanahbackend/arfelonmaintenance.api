﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.Order.BLL.Managers
{
    public class OrderSubStatusManager : Repository<OrderSubStatus>, IOrderSubStatusManager
    {
        public OrderSubStatusManager(OrderDBContext context)
            : base(context)
        {
        }

        public ProcessResult<List<OrderSubStatus>> GetByStatusId(int statusId)
        {
            return GetAll(x => x.StatusId == statusId);
        }
    }
}