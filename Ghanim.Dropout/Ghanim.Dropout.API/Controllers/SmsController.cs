﻿using Ghanim.Dropout.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Dropout.API.Controllers
{
    [Route("api/[controller]")]
    public class SmsController : Controller
    {
        SMSKey _SMSKey;
        public SmsController(IOptions<SMSKey> SMSKey)
        {
            _SMSKey = SMSKey.Value;
        }

        //[HttpPost, Route("Send")]
        //public ProcessResultViewModel<bool> Send([FromBody] SendSmsViewModel model)
        //{
        //    var smsUrl = string.Format("http://smsbox.com/smsgateway/services/messaging.asmx/Http_SendSMS?username=alghanim&password=alghanim@321&customerid=994&sendertext=A ENG&messagebody={0}&recipientnumbers={1}&defdate=&isblink=false&isflash=false", model.Message, model.PhoneNumber);
        //    WebRequest request = WebRequest.Create(smsUrl);
        //    request.Method = "POST";
        //    request.ContentLength = 0;
        //    WebResponse response = request.GetResponse();
        //    return ProcessResultViewModelHelper.Succeeded(true);
        //}

        [HttpPost, Route("SendTestAccount")]
        public ProcessResultViewModel<bool> SendTestAccount([FromBody] SendSmsViewModel model)
        {
            var smsUrl = string.Format(_SMSKey.URL + "G=965{0}&M={1}&P={2}", model.PhoneNumber, model.Message, _SMSKey.Key);
            WebRequest request = WebRequest.Create(smsUrl);
            request.Method = "GET";

            //     string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(_SMSKey.Name + ":" + _SMSKey.Key));
            //   request.Headers.Add("Authorization", "Basic " + encoded);

            request.ContentLength = 0;
            WebResponse response = request.GetResponse();
            return ProcessResultViewModelHelper.Succeeded(true);
        }
    }
}
