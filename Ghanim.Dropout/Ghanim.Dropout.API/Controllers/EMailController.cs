﻿using Ghanim.Dropout.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Dropout.API.Controllers
{
    [Route("api/[controller]")]
    public class EMailController : Controller
    {
        IConfigurationRoot _configuration;
        public EMailController(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        [HttpPost, Route("Send")]
        public ProcessResultViewModel<bool> Send([FromBody]SendMailViewModel model)
        {
            if (model.Subject == null)
            {
                return ProcessResultViewModelHelper.Failed(false, "Subject field is required.");
            }
            if (model.Body == null)
            {
                return ProcessResultViewModelHelper.Failed(false, "Body field is required.");
            }
            if (model.To == null || model.To.Count == 0)
            {
                return ProcessResultViewModelHelper.Failed(false, "To field must have at least one mail.");
            }
            string smtpServer = _configuration["MailSettings:SmtpServer"];
            string username = _configuration["MailSettings:SmtpUsername"];
            string password = _configuration["MailSettings:SmtpPassword"];
            string from = _configuration["MailSettings:SmtpFrom"];
            int port = Convert.ToInt32(_configuration["MailSettings:SmtpPort"]);

            SmtpClient client = new SmtpClient(smtpServer)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(username, password),
                Port = port,
                EnableSsl = true
            };

            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(from),
                Body = model.Body,
                Subject = model.Subject,
                IsBodyHtml = true
            };
            foreach (var item in model.To)
            {
                mailMessage.To.Add(item);
            }
            if (model.CC != null)
            {
                foreach (var item in model.CC)
                {
                    mailMessage.CC.Add(item);
                }
            }
            if (model.Bcc != null)
            {
                foreach (var item in model.Bcc)
                {
                    mailMessage.Bcc.Add(item);
                }
            }
            client.Send(mailMessage);
            return ProcessResultViewModelHelper.Succeeded(true);
        }

        [HttpPost, Route("Receive")]
        public ProcessResultViewModel<bool> Receive([FromBody]SendMailViewModel model)
        {
            if (model.Subject == null)
            {
                return ProcessResultViewModelHelper.Failed(false, "Subject field is required.");
            }
            if (model.Body == null)
            {
                return ProcessResultViewModelHelper.Failed(false, "Body field is required.");
            }
            string smtpServer = _configuration["MailSettings:SmtpServer"];
            string username = _configuration["MailSettings:SmtpUsername"];
            string password = _configuration["MailSettings:SmtpPassword"];
            string from = _configuration["MailSettings:SmtpFrom"];
            string to = _configuration["MailSettings:SmtpTo"];
            int port = Convert.ToInt32(_configuration["MailSettings:SmtpPort"]);

            SmtpClient client = new SmtpClient(smtpServer)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(username, password),
                Port = port,
                EnableSsl = true
            };

            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(from),
                Body = model.Body,
                Subject = model.Subject,
                IsBodyHtml = true
            };
            mailMessage.To.Add(to);
            client.Send(mailMessage);
            return ProcessResultViewModelHelper.Succeeded(true);
        }
    }
}
