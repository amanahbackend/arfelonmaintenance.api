﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Ghanim.Dropout.API.Models;
using Ghanim.Dropout.API.ServiceCommunications.Identity;
using Ghanim.Dropout.API.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Ghanim.Dropout.API.Controllers
{
    [Route("api/[controller]")]
    public class NotificationCenterController : Controller
    {
        private readonly IConfigurationRoot _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IIdentityService _identityService;

        public NotificationCenterController(IConfigurationRoot configuration, IHostingEnvironment hostingEnvironment,
            IIdentityService identityService)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _identityService = identityService;
        }

        //[HttpPost, Route("Send"), MapToApiVersion("1.0")]
        //public async Task<IActionResult> Send([FromBody]SendNotificationViewModel model)
        //{
        //    //var accKeyFilePath = _configuration["FcmSettings:AccountKeyFilePath"];

        //    var accKeyFilePath = Path.Combine(_hostingEnvironment.ContentRootPath, _configuration["FcmSettings:AccountKeyFilePath"]);
        //    var projectId = _configuration["FcmSettings:ProjectId"];

        //    // Read the Credentials from a File, which is not under Version Control:
        //    var settings = FileBasedFcmClientSettings.CreateFromFile(projectId, accKeyFilePath);

        //    // Construct the Client:
        //    using (var client = new FcmClient(settings))
        //    {
        //        // Construct the Data Payload to send:

        //        var data = new Dictionary<string, string>
        //        {
        //            {nameof(model.NotificationType), Enum.GetName(typeof(NotificationType), model.NotificationType)}
        //        };
        //        foreach (var item in model.Data)
        //        {
        //            data.Add(item.Key, item.Value);
        //        }

        //        var result = new FcmMessageResponse();
        //        var userDevicesResult = await _identityService.GetUserDevices("v1", model.UserId);
        //        if (userDevicesResult != null && userDevicesResult.IsSucceeded)
        //        {
        //            var userDevices = userDevicesResult.Data;
        //            foreach (var item in userDevices)
        //            {
        //                var registrationId = item.DeveiceId;
        //                // The Message should be sent to the given token:
        //                var message = new FcmMessage()
        //                {
        //                    ValidateOnly = false,
        //                    Message = new Message
        //                    {
        //                        Token = registrationId,
        //                        Data = data
        //                    }
        //                };
        //                // Finally send the Message and wait for the Result:
        //                var cts = new CancellationTokenSource();
        //                // Send the Message and wait synchronously:
        //                result = client.SendAsync(message, cts.Token).GetAwaiter().GetResult();
        //            }
        //        }
        //        // Print the Result to the Console:
        //        return Ok(result);
        //    }
        //}

        [HttpPost, Route("Send")]
        public async Task<IProcessResultViewModel<string>> Send([FromBody]SendNotificationViewModel model)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            var userDevices = await _identityService.GetUserDevices(model.UserId);
            foreach (var item in userDevices.Data)
            {
                result = SendPushNotification(item.DeveiceId, model.Title, model.Body, model.Data, apiKey);
            }
            return ProcessResultViewModelHelper.Succeeded(result);
        }

        [HttpPost, Route("SendAzure")]
        public async Task<IProcessResultViewModel<string>> SendAzure([FromBody]SendNotificationViewModel model)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            if (!string.IsNullOrEmpty(model.UserId))
            {
                result = await SendPushNotificationAzureAsync(model.UserId, model.Title, model.Body, model.Data, apiKey);
            }
            return ProcessResultViewModelHelper.Succeeded(result);
        }

        [HttpPost, Route("SendToMultiUsers")]
        public async Task<IProcessResultViewModel<string>> SendToMultiUsers([FromBody]List<SendNotificationViewModel> lstmodel)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            foreach (var model in lstmodel)
            {
                if (!string.IsNullOrEmpty(model.UserId))
                {
                    var userDevices = await _identityService.GetUserDevices(model.UserId);
                    if (userDevices.IsSucceeded && userDevices.Data != null && userDevices.Data.Count > 0)
                    {
                        foreach (var item in userDevices.Data)
                        {
                            result = SendPushNotification(item.DeveiceId, model.Title, model.Body, model.Data, apiKey);
                        }
                    }
                }

            }

            return ProcessResultViewModelHelper.Succeeded(result);
        }

        [HttpPost, Route("SendToMultiUsersAzure")]
        public async Task<IProcessResultViewModel<string>> SendToMultiUsersAzure([FromBody]List<SendNotificationViewModel> lstmodel)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            foreach (var model in lstmodel)
            {
                if (!string.IsNullOrEmpty(model.UserId))
                {
                    result = await SendPushNotificationAzureAsync(model.UserId, model.Title, model.Body, model.Data, apiKey);
                }

            }

            return ProcessResultViewModelHelper.Succeeded(result);
        }

        public static string SendPushNotification(string deviceId, string title, string body,
            dynamic dataObj, string ApplicationId)
        {
            string str;
            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body,
                        title,
                        sound = "Enabled"
                    },
                    data = dataObj
                };
                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }

        public static async Task<string> SendPushNotificationAzureAsync(string userId, string title, string body,
            dynamic dataObj, string ApplicationId)
        {
            string str = "error";
            string[] userTag = new string[1];
            userTag[0] = "username:" + userId;
            //userTag[1] = "from:" + user;

            Microsoft.Azure.NotificationHubs.NotificationOutcome outcome;

            try
            {
                // Android
                var notif = "{ \"data\" : {\"message\":\"" + body + "\"}}";
                outcome = await Notifications.Instance.Hub.SendFcmNativeNotificationAsync(notif, userTag);

                if (outcome != null)
                {
                    if (!((outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Abandoned) ||
                        (outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Unknown)))
                    {
                        str = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }

        [HttpGet, Route("CountByRecieverId/{recieverId}")]
        public ProcessResultViewModel<int> CountByRecieverId([FromRoute]string recieverId)
        {
            if (recieverId != null)
            {
                var result = 0;// manager.GetAllQuerable().Data.Where(x => x.RecieverId == recieverId && x.IsRead == false).ToList().Count;

                return ProcessResultViewModelHelper.Succeeded<int>(result);
            }
            return ProcessResultViewModelHelper.Failed(0, "Receiver Id is required");

        }
    }
}
