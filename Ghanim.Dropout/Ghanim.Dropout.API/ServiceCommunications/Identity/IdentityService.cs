﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Dropout.API.ServiceSettings;
using Ghanim.Dropout.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Dropout.API.ServiceCommunications.Identity
{
    public class IdentityService : DefaultHttpClientCrud<IdentityServiceSettings, string, List<UserDeviceViewModel>>, IIdentityService
    {
        private readonly IdentityServiceSettings _settings;

        public IdentityService(IOptions<IdentityServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<List<UserDeviceViewModel>>> GetUserDevices(string userId)
        {
            var baseUrl =  _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!string.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = _settings.GetUserDevicesAction;
                var url = $"{baseUrl}/{requestedAction}/{userId}";
                var result = await Get(url);
                return result;
            }
            return null;
        }
    }
}
