﻿using Amanah.Microservices.CommonLib;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Arfelon.OrderManagement.BLL.Orders
{
    [Table("OrderPriority", Schema = "orderMgt")]
    public class OrderPriority : BaseEntity
    {
        [StringLength(20)]
        public string Code { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
    }
}
