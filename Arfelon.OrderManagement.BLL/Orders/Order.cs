﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Amanah.Microservices.CommonLib;
using Arfelon.OrderManagement.BLL.Orders.OrderDivisions;
using Arfelon.OrderManagement.BLL.Orders.OrderTypes;

namespace Arfelon.OrderManagement.BLL.Orders
{
    [Table("Order", Schema = "orderMgt")]
    public class Order : BaseEntity
    {
        [StringLength(20)]
        public string OrderNo { get; set; }

        [StringLength(250)]
        public string OrderDescription { get; set; }

        public int? CustomerId { get; set; }

        public int? OrderTypeId { get; set; }
        public virtual OrderType OrderType { get; set; }

        [StringLength(20)]
        public string CompanyCode { get; set; }

        public int? OrderStatusId { get; set; }
        public virtual OrderType OrderStatus { get; set; }

        public int? OrderDivisionId { get; set; }
        public virtual OrderDivision OrderDivision { get; set; }

        public int? OrderPriorityId { get; set; }
        public virtual OrderPriority OrderPriority { get; set; }

        public List<OrderProblems.OrderProblems> OrderProblems { get; set; }

        [StringLength(200)]
        public string OrderNotes { get; set; }

        // Address of the order
        [StringLength(10)]
        public string PACI { get; set; }

        [StringLength(20)]
        public string BuildingType { get; set; }

        [StringLength(20)]
        public string Area { get; set; }

        [StringLength(20)]
        public string AreaDesc { get; set; }

        [StringLength(20)]
        public string Block { get; set; }
        
        /// <summary>
        /// Street / Jaddah
        /// </summary>
        [StringLength(100)]
        public string Street { get; set; }

        /// <summary>
        /// House / Kasima
        /// </summary>
        [StringLength(10)]
        public string House { get; set; }

        public int? Floor { get; set; }

        public int? ApartmentNo { get; set; }
    }
}
