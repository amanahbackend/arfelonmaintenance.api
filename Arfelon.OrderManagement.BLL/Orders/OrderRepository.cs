﻿using Amanah.Microservices.CommonLib;
using Microsoft.EntityFrameworkCore;

namespace Arfelon.OrderManagement.BLL.Orders
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(DbContext context) : base(context)
        {
        }
    }
}
