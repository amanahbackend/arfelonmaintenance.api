﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Amanah.Microservices.CommonLib;

namespace Arfelon.OrderManagement.BLL.Orders.OrderDivisions
{
    [Table("OrderDivision", Schema = "orderMgt")]
    public class OrderDivision : BaseEntity
    {
        [StringLength(20)]
        public string Code { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
    }
}
