﻿using System.ComponentModel.DataAnnotations.Schema;
using Amanah.Microservices.CommonLib;

namespace Arfelon.OrderManagement.BLL.Orders.OrderProblems
{
    [Table("OrderProblems", Schema = "orderMgt")]
    public class OrderProblems : BaseEntity
    {
        public int? OrderId { get; set; }

        public int? OrderProblemId { get; set; }
        public virtual OrderProblem.OrderProblem OrderProblem { get; set; }
    }
}
