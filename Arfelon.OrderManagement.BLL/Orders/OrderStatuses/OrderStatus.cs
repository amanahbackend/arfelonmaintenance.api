﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Amanah.Microservices.CommonLib;

namespace Arfelon.OrderManagement.BLL.Orders.OrderStatuses
{
    [Table("OrderStatus", Schema = "orderMgt")]
    public class OrderStatus : BaseEntity
    {
        [StringLength(20)]
        public string Code { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
    }
}
