﻿using Amanah.Microservices.CommonLib;

namespace Arfelon.OrderManagement.BLL.Orders
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
