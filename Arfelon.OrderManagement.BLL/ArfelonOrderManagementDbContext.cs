﻿using Arfelon.OrderManagement.BLL.Orders;
using Arfelon.OrderManagement.BLL.Orders.OrderDivisions;
using Arfelon.OrderManagement.BLL.Orders.OrderProblem;
using Arfelon.OrderManagement.BLL.Orders.OrderStatuses;
using Arfelon.OrderManagement.BLL.Orders.OrderTypes;
using Microsoft.EntityFrameworkCore;

namespace Arfelon.OrderManagement.BLL
{
    public class ArfelonOrderManagementDbContext : DbContext
    {
        public ArfelonOrderManagementDbContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<OrderType> OrderType { get; set; }
        public DbSet<OrderDivision> OrderDivision { get; set; }
        public DbSet<OrderPriority> OrderPriority { get; set; }
        public DbSet<OrderProblem> OrderProblem { get; set; }

        public DbSet<Order> Order { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
