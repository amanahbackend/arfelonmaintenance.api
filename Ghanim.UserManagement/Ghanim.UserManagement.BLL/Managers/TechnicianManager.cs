﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class TechnicianManager : Repository<Technician>, ITechnicianManager
    {
        IServiceProvider serviceProvider;
        public TechnicianManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<Technician> GetByUserId(string userId)
        {
            Technician input = null;
            try
            {
                input = GetAll().Data.Find(x => x.UserId == userId);
                return ProcessResultHelper.Succeeded<Technician>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<Technician>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
        }

        public ProcessResult<List<Technician>> Search(string word)
        {
            List<Technician> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.PF.Contains(word)).Take(100).ToList();
                return ProcessResultHelper.Succeeded<List<Technician>>(input, (string)null, ProcessResultStatusCode.Succeeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Technician>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }
    }
}