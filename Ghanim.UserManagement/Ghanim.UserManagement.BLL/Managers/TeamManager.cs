﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.BLL.NotMappedModels;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;


namespace Ghanim.UserManagement.BLL.Managers
{
    public class TeamManager : Repository<Team>, ITeamManager
    {
        IServiceProvider serviceProvider;
        public TeamManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<Team>> GetByDispatcherId(int dispatcherId)
        {
            List<Team> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.DispatcherId == dispatcherId).ToList();
                return ProcessResultHelper.Succeeded<List<Team>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }

        public ProcessResult<List<DispatcherWithTeamsViewModel>> GetByDispatchersId(List<int> dispatcherIds)
        {
            try
            {
                var result = GetAllQuerable().Data.Where(x => dispatcherIds.Contains(x.DispatcherId)).GroupBy(x => new { x.DispatcherId, x.DispatcherName }, (key, group) => new DispatcherWithTeamsViewModel { DispatcherId = key.DispatcherId, DispatcherName = key.DispatcherName, Teams = group }).ToList();
                return ProcessResultHelper.Succeeded<List<DispatcherWithTeamsViewModel>>(result, (string)null, ProcessResultStatusCode.Succeeded, "GetByDispatchersId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherWithTeamsViewModel>>(new List<DispatcherWithTeamsViewModel>(), ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatchersId");
            }
        }

        public ProcessResult<List<Team>> GetByFormansId(List<int> formansId)
        {
            try
            {
                var result = GetAllQuerable().Data.Where(x => formansId.Contains(x.DispatcherId)).ToList();
                return ProcessResultHelper.Succeeded<List<Team>>(result, (string)null, ProcessResultStatusCode.Succeeded, "GetByFormansId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(null, ex, (string)null, ProcessResultStatusCode.Failed, "GetByFormansId");
            }
        }

        public ProcessResult<List<Team>> SearchByTeamName(string word)
        {
            List<Team> input = null;
            try
            {
                if (word != null)
                {
                    input = GetAllQuerable().Data.Where(x => x.Name.ToString().Contains(word)).ToList();
                }
                else
                {
                    input = GetAllQuerable().Data.Take(100).ToList();
                }
                return ProcessResultHelper.Succeeded<List<Team>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
    }
}