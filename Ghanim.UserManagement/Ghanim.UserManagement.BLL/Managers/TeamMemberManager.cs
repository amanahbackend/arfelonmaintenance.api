﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using CommonEnums;
using Ghanim.UserManagement.BLL.NotMappedModels;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class TeamMemberManager : Repository<TeamMember>, ITeamMemberManager
    {
        IServiceProvider serviceProvider;

        public TeamMemberManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<TeamMember>> GetUnassignedMember()
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.TeamId == null && x.MemberType != (int)MemberType.Vehicle).ToList();
                return ProcessResultHelper.Succeeded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetUnassignedMember");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetUnassignedMember");
            }
        }

        public ProcessResult<List<TeamMember>> GetUnassignedMemberByDivisionId(int divisionId)
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => (x.TeamId == 0 && x.MemberType != (int)MemberType.Vehicle && x.DivisionId != null && x.DivisionId == divisionId) || (x.TeamId == null && x.MemberType != (int)MemberType.Vehicle && x.DivisionId != null && x.DivisionId == divisionId)).ToList();
                return ProcessResultHelper.Succeeded(input, (string)null, ProcessResultStatusCode.Succeeded, "GetUnassignedMember");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(null, ex, (string)null, ProcessResultStatusCode.Failed, "GetUnassignedMember");
            }
        }

        public ProcessResult<List<TeamMember>> GetUnassignedVehicleMember()
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => (x.TeamId == null && x.MemberType == (int)MemberType.Vehicle) || (x.TeamId == 0 && x.MemberType == (int)MemberType.Vehicle)).ToList();
                return ProcessResultHelper.Succeeded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetUnassignedMember");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetUnassignedMember");
            }
        }

        public ProcessResult<bool> AssignedMemberToTeam(int teamId, int MemberId)
        {
            bool result = false;
            try
            {
                var teamMemberRes = GetAll().Data.Find(x => x.Id == MemberId);
                if (teamMemberRes != null)
                {
                    teamMemberRes.TeamId = teamId;
                }
                result = Update(teamMemberRes).Data;

                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "assignedMemberToTeam");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "assignedMemberToTeam");
            }
        }

        public ProcessResult<TeamMember> GetByMemberType_TeamId(int teamId, int memberType)
        {
            TeamMember input = null;
            try
            {
                input = GetAll().Data.Find(x => x.MemberType == memberType && x.TeamId == teamId);
                return ProcessResultHelper.Succeeded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByMemberType_TeamId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByMemberType_TeamId");
            }
        }

        public ProcessResult<TeamMember> GetByMemberParentId(int memberParentId)
        {
            TeamMember input = null;
            try
            {
                input = GetAll().Data.Find(x => x.MemberParentId == memberParentId);
                return ProcessResultHelper.Succeeded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByMemberParentId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByMemberParentId");
            }
        }

        public ProcessResult<List<TeamMember>> GetByTeamId(int teamId)
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAll().Data.Where(x => x.TeamId == teamId).ToList();
                return ProcessResultHelper.Succeeded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByMemberType_TeamId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByMemberType_TeamId");
            }
        }

        public ProcessResult<bool> SortMemeber(int teamId, List<TeamMemberRanks> teamMembers)
        {
            bool result = false;
            try
            {
                foreach (var item in teamMembers)
                {
                    var teamMemberRes = GetAll().Data.Find(x => x.Id == item.Id);
                    teamMemberRes.Rank = item.Rank;
                    result = Update(teamMemberRes).Data;
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "SortMemeber");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "SortMemeber");
            }

        }

        public ProcessResult<List<TeamMember>> SearchTech(string word)
        {
            List<TeamMember> input = null;
            try
            {
                if (word != null)
                {
                    input = GetAllQuerable().Data.Where(x => x.MemberParentName.Contains(word) && x.MemberType == (int)MemberType.Technician).Take(100).ToList();
                }
                else
                {
                    input = GetAllQuerable().Data.Take(100).ToList();
                }
                return ProcessResultHelper.Succeeded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }

        public ProcessResult<List<TeamMember>> SearchVec(string word)
        {
            List<TeamMember> input = null;
            try
            {
                if (word != null)
                {
                    input = GetAllQuerable().Data.Where(x => x.MemberParentName.Contains(word) && x.MemberType == (int)MemberType.Vehicle).Take(100).ToList();
                }
                else
                {
                    input = GetAllQuerable().Data.Take(100).ToList();
                }
                return ProcessResultHelper.Succeeded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }
    }
}