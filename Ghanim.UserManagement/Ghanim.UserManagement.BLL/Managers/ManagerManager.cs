﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class ManagerManager :Repository<Manager> ,IManagerManager
    {
        IServiceProvider serviceProvider;
        public ManagerManager(IServiceProvider _serviceProvidor,UserManagementContext context) :base(context)
        {
            serviceProvider = _serviceProvidor;
        }

        public ProcessResult<Manager> GetByUserId(string userId)
        {
            Manager input = null;
            try
            {
                input = GetAll().Data.Find(x => x.UserId == userId);
                return ProcessResultHelper.Succeeded<Manager>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<Manager>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
        }
    }
}
