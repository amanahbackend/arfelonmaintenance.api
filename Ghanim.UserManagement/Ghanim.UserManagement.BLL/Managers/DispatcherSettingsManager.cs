﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.BLL.NotMappedModels;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
   public class DispatcherSettingsManager : Repository<DispatcherSettings>, IDispatcherSettingsManager
    {
        IServiceProvider serviceProvider;
        public DispatcherSettingsManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<DispatcherSettings>> GetByDispatcherId(int dispatcherId)
        {
            List<DispatcherSettings> input = null;
            try
            {                                                                 
                input = GetAllQuerable().Data.Where(x => x.DispatcherId == dispatcherId).ToList();
                return ProcessResultHelper.Succeeded<List<DispatcherSettings>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherSettings>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
        public ProcessResult<List<DispatcherSettings>> GetbyGroupId(int groupId)
        {
            List<DispatcherSettings> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.GroupId == groupId).ToList();
                return ProcessResultHelper.Succeeded<List<DispatcherSettings>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherSettings>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
    }
}