﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class VehicleManager : Repository<Vehicle>, IVehicleManager
    {
        IServiceProvider serviceProvider;
        public VehicleManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<Vehicle>> Search(string word)
        {
            List<Vehicle> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Plate_no.Contains(word)).Take(100).ToList();
                return ProcessResultHelper.Succeeded<List<Vehicle>>(input, (string)null, ProcessResultStatusCode.Succeeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Vehicle>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }
    }
}