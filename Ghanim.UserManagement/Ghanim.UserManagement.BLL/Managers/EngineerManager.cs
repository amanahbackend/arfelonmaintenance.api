﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
   public class EngineerManager : Repository<Engineer>, IEngineerManager
    {
        IServiceProvider serviceProvider;
        public EngineerManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<Engineer> GetByUserId(string userId)
        {
            Engineer input = null;
            try
            {
                input = GetAll().Data.Find(x => x.UserId == userId);
                return ProcessResultHelper.Succeeded<Engineer>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<Engineer>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
        }
    }
}