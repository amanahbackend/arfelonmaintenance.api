﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class TechniciansStateManager : Repository<TechniciansState>, ITechniciansStateManager
    {
        IServiceProvider serviceProvider;
        public TechniciansStateManager(IServiceProvider _serviceProvidor, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvidor;
        }

        public ProcessResult<TechniciansState> GetLastStateByTechnicianId(int technicianId)
        {
            var technicianStates = base.GetAll(x => x.TechnicianId == technicianId).Data.LastOrDefault();
            return ProcessResultHelper.Succeeded<TechniciansState>(technicianStates, (string)null, ProcessResultStatusCode.Succeeded, "GetLastStateByTechnicianId");
        }
    }
}
