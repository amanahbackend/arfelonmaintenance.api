﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
   public class DispatcherManager : Repository<Dispatcher>, IDispatcherManager
    {
        IServiceProvider serviceProvider;
        public DispatcherManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<Dispatcher> GetByUserId(string userId)
        {
            Dispatcher input = null;
            try
            {
                input = GetAll().Data.Find(x => x.UserId == userId);
                return ProcessResultHelper.Succeeded<Dispatcher>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<Dispatcher>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByUserId");
            }
        }
    }
}