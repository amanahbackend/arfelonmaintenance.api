﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.BLL.NotMappedModels
{
    public class AreaWithOrderProblems
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public List<Problems> OrderProblems { get; set; }
        public int GroupId { get; set; }
    }
}
