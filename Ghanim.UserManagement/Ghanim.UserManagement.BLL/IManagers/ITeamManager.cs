﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.NotMappedModels;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.BLL.IManagers
{
    public interface ITeamManager : IRepository<Team>
    {
        ProcessResult<List<Team>> GetByDispatcherId(int dispatcherId);
        ProcessResult<List<DispatcherWithTeamsViewModel>> GetByDispatchersId(List<int> dispatcherIds);
        ProcessResult<List<Team>> GetByFormansId(List<int> formansId);
        ProcessResult<List<Team>> SearchByTeamName(string word);

    }
}