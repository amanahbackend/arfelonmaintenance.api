﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.BLL.IManagers
{
    public interface ITechnicianManager : IRepository<Technician>
    {
         ProcessResult<Technician> GetByUserId(string userId);
         ProcessResult<List<Technician>> Search(string word);
    }
}
