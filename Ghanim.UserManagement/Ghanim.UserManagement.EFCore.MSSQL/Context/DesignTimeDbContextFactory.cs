﻿using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.EFCore.MSSQL.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<UserManagementContext>
    {
        public UserManagementContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<UserManagementContext>();

            // var connectionString = configuration.GetConnectionString("ConnectionString");
            builder.UseSqlServer("Data Source=.\\SQLEXPRESS; Initial Catalog=ArfelonMaintenanceDev; Integrated Security = true;");

            return new UserManagementContext(builder.Options);
        }
    }
}
