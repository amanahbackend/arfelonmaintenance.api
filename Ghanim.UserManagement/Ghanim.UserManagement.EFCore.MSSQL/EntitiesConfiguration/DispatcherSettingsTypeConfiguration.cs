﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class DispatcherSettingsTypeConfiguration : BaseEntityTypeConfiguration<DispatcherSettings>
    {
        public override void Configure(EntityTypeBuilder<DispatcherSettings> DispatcherSettings)
        {
            base.Configure(DispatcherSettings);

            DispatcherSettings.ToTable("DispatcherSettings");
            DispatcherSettings.HasKey(o => o.Id);
            DispatcherSettings.Property(o => o.Id)
                                .ForSqlServerUseSequenceHiLo("DispatcherSettingsseq");
            DispatcherSettings.Property(o => o.AreaId).IsRequired();
            DispatcherSettings.Property(o => o.AreaName).IsRequired();
            DispatcherSettings.Property(o => o.DispatcherId).IsRequired();
            DispatcherSettings.Property(o => o.DispatcherName).IsRequired();
            DispatcherSettings.Property(o => o.OrderProblemId).IsRequired();
            DispatcherSettings.Property(o => o.OrderProblemName).IsRequired();
        }
    }
}