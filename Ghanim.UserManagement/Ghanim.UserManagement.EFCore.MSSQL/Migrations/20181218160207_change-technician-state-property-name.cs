﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class changetechnicianstatepropertyname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TechnicianType",
                table: "TechniciansStates");

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "TechniciansStates",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "TechniciansStates");

            migrationBuilder.AddColumn<int>(
                name: "TechnicianType",
                table: "TechniciansStates",
                nullable: false,
                defaultValue: 0);
        }
    }
}
