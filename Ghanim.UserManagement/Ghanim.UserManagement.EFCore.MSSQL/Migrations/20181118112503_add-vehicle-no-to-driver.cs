﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class addvehiclenotodriver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "VehicleNo",
                table: "Drivers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VehicleNo",
                table: "Drivers");
        }
    }
}
