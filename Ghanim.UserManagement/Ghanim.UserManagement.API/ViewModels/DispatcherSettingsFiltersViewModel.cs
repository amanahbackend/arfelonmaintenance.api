﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class DispatcherSettingsFiltersViewModel
    {
        public List<string> DispatcherNames { get; set; }
        public List<string> OrderProblemNames { get; set; }
        public List<string> AreaNames { get; set; }
        public PaginatedItemsViewModel<DispatcherSettingsViewModel> PageInfo { get; set; } = null;
    }
}
