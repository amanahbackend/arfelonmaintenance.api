﻿using CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class LoginResultViewModel
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public List<string> Roles { get; set; }
        public string UserId { get; set; }
        public int TeamId { get; set; }
        public int Id { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public TechnicianState LastState { get; set; }
    }
}
