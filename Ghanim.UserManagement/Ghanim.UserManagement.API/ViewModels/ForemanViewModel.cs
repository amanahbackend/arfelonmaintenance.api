﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class ForemanViewModel : BaseEntity
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int EngineerId { get; set; }
        public string EngineerName { get; set; }
        public string PF { get; set; }
    }
}
