﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class LogoutViewModel
    {
        [Required]
       public string UserId { get; set; }
        [Required]
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public string DeviceId { get; set; }
    }
}
