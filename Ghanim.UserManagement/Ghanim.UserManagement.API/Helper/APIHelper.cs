﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.Helper
{
    public class APIHelper :IAPIHelper
    {
        public string GetUserIdFromToken(string token)
        {
            JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
            string userId = jwtTokenObj.Payload.Jti;
            return userId;
        }

        public string GetAuthHeader(HttpRequest request)
        {
            string authHeader = request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(authHeader))
            {
                authHeader = authHeader.Replace("bearer ", "");
                authHeader = authHeader.Replace("bearer ", "");
            }
            return authHeader;
        }
    }
}
