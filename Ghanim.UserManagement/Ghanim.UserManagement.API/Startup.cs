﻿using AutoMapper;
using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.API.AutoMapperConfig;
using Ghanim.UserManagement.API.Helper;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.BLL.Managers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using Ghanim.UserManagement.Models.IEntities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IdentityServiceSettings>
             (Configuration.GetSection("ServiceCommunications:IdentityService"));

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);
            // Add framework services.
            services.AddDbContext<UserManagementContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.UserManagement.EFCore.MSSQL")));


            services.AddAutoMapper(typeof(Startup));
            //Mapper.AssertConfigurationIsValid();

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            //services.AddIdentityServer();
            //services.Configure<VehicleAppSettings>(Configuration);
            #region Data & Managers
            services.AddScoped<DbContext, UserManagementContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));

            services.AddScoped(typeof(ISupervisor), typeof(Supervisor));
            services.AddScoped(typeof(IDispatcher), typeof(Dispatcher));
            services.AddScoped(typeof(IDispatcherSettings), typeof(DispatcherSettings));
            services.AddScoped(typeof(IForeman), typeof(Foreman));
            services.AddScoped(typeof(ITechnician), typeof(Technician));
            services.AddScoped(typeof(IDriver), typeof(Driver));
            services.AddScoped(typeof(IEngineer), typeof(Engineer));
            services.AddScoped(typeof(ITeamMember), typeof(TeamMember));
            services.AddScoped(typeof(ITeam), typeof(Team));
            services.AddScoped(typeof(IAttendance), typeof(Attendence));
            services.AddScoped(typeof(IManager), typeof(Manager));
            services.AddScoped(typeof(IMaterialController), typeof(MaterialController));
            services.AddScoped(typeof(ITechniciansState), typeof(TechniciansState));
            services.AddScoped(typeof(IVehicle), typeof(Vehicle));

            services.AddScoped(typeof(ISupervisorManager), typeof(SupervisorManager));
            services.AddScoped(typeof(IDispatcherManager), typeof(DispatcherManager));
            services.AddScoped(typeof(IForemanManager), typeof(ForemanManager));
            services.AddScoped(typeof(ITechnicianManager), typeof(TechnicianManager));
            services.AddScoped(typeof(IDriverManager), typeof(DriverManager));
            services.AddScoped(typeof(IEngineerManager), typeof(EngineerManager));
            services.AddScoped(typeof(ITeamMemberManager), typeof(TeamMemberManager));
            services.AddScoped(typeof(ITeamManager), typeof(TeamManager));
            services.AddScoped(typeof(IAttenenceManager), typeof(AttendenceManager));
            services.AddScoped(typeof(IDispatcherSettingsManager), typeof(DispatcherSettingsManager));
            services.AddScoped(typeof(IManagerManager), typeof(ManagerManager));
            services.AddScoped(typeof(IMaterialControllerManager), typeof(MaterialControllerManager));
            services.AddScoped(typeof(ITechniciansStateManager), typeof(TechniciansStateManager));
            services.AddScoped(typeof(IVehicleManager), typeof(VehicleManager));
            services.AddScoped(typeof(IAPIHelper), typeof(APIHelper));
            services.AddScoped<IIdentityUserService, IdentityUserService>();
            #endregion

            //   services.AddDbContext<DataContext>(options =>
            //options.UseSqlServer(Configuration["ConnectionString"],
            // sqlOptions => sqlOptions.MigrationsAssembly("Sprintoo.UserManagement.EFCore.MSSQL")));


            //////////////////// commented the part of ServiceRegisteryPort  ////////////////////////////
            //// Dns Query for consul - service discovery
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            ///////////////////////////////////////////////////////////////////////////////////

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "User Management API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                // Assign scope requirements to operations based on AuthorizeAttribute
                // options.OperationFilter<SecurityRequirementsOperationFilter>();
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseStaticFiles();
            // Auto register using server.Features (does not work in reverse proxy mode)
            // app.UseConsulRegisterService();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "User Management API V1");
            }); // URL: /swagger
        }
    }
}
