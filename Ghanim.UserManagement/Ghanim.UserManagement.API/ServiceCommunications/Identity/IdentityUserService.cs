﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.Api.HttpClient;
using Utilites.ProcessingResult;
using DispatchProduct.HttpClient;
using Ghanim.UserManagement.API.ViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Ghanim.UserManagement.API.ServiceCommunications.Identity
{
    public class IdentityUserService
       : DefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>,
       IIdentityUserService
    {
        IdentityServiceSettings _settings;

        public IdentityUserService(IOptions<IdentityServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string userId)
        {
            string baseUrl = _settings.Uri;

            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }

            if (!string.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetUserById;
                var url = $"{baseUrl}/{requestedAction}?id={userId}";
                return await Get(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(List<string> ids)
        {
            string baseUrl = _settings.Uri;

            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }

            if (!string.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetByIdsAction;
                var url = $"{baseUrl}/{requestedAction}";

                return await PostCustomize<List<string>, List<ApplicationUserViewModel>>(url, ids);
            }

            return null;
        }

        public async Task<ProcessResultViewModel<bool>> ResetPassword(ResetPasswordViewModel model)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!string.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.ResetPassword;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<ResetPasswordViewModel, bool>(url, model);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login(LoginViewModel model)
        {
            string baseUrl = _settings.Uri;

            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }

            if (!string.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.Login;
                var url = $"{baseUrl}/{requestedAction}";

                return await PostCustomize<LoginViewModel, LoginResultViewModel>(url, model);
                //var jsonString = PostCustomize1(url, model).Result;
                //ProcessResultViewModel<LoginResultViewModel> result = JsonConvert.DeserializeObject<ProcessResultViewModel<LoginResultViewModel>>(jsonString);

                //return result;
            }

            return null;
        }

        //private async Task<string> PostCustomize1<TInCustomize>(string requestUri, TInCustomize model, string auth = "")
        //{
        //    var httpResponseMessage = await HttpRequestFactory.Post(requestUri, model, auth);

        //    string jsonString = httpResponseMessage.Content.ReadAsStringAsync()
        //        .Result.Replace("\\", "").Trim(new[] { '"' });

        //    //LogToFile(requestUri, jsonString);

        //    return jsonString;
        //}

        /// <summary>
        /// For testing purpose only.
        /// </summary>
        private void LogToFile(string requestUri, string jsonString)
        {
            Directory.CreateDirectory("c:\\test");
            StreamWriter streamWriter = new StreamWriter("c:\\test\\file1.txt", true);

            StringBuilder sb = new StringBuilder();
            sb.Append("RequestUri=").AppendLine(requestUri);
            sb.Append("JsonString returned: ").AppendLine(jsonString);
            streamWriter.Write(sb.ToString());
            streamWriter.Close();
        }

        public async Task<ProcessResultViewModel<bool>> Logout(string userId, string deviceId)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!string.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.Logout + "?userId=" + userId + "&deviceId=" + deviceId;
                var url = $"{baseUrl}/{requestedAction}";
                return await Put(url);
            }
            return null;
        }
    }
}
