﻿using DispatchProduct.HttpClient;
using Ghanim.UserManagement.API.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.ServiceCommunications.Identity
{
    public interface IIdentityUserService
       : IDefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>
    {
        Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string userId);

        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(List<string> ids);

        Task<ProcessResultViewModel<bool>> ResetPassword(ResetPasswordViewModel model);

        Task<ProcessResultViewModel<LoginResultViewModel>> Login(LoginViewModel model);

        Task<ProcessResultViewModel<bool>> Logout(string userId, string deviceId);
    }
}
