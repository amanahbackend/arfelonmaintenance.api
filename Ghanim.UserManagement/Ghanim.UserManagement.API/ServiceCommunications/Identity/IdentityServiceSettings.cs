﻿using DispatchProduct.HttpClient;

namespace Ghanim.UserManagement.API.ServiceCommunications.Identity
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        //private new string Uri { get => base.Uri; set => base.Uri = value; }

        public string GetUserById { get; set; }

        public string GetByIdsAction { get; set; }

        public string SearchAction { get; set; }

        public string ResetPassword { get; set; }

        public string Login { get; set; }

        public string Logout { get; set; }

        //public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        //{
        //    return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        //}
    }
}
