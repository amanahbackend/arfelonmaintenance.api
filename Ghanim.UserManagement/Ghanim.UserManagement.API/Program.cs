﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Utilities.Utilites.SerilogExtensions;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.UserManagement.EFCore.MSSQL.Context;

namespace Ghanim.UserManagement.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.With<CustomExceptionEnricher>()
                .MinimumLevel.Error()
                .Enrich.FromLogContext()
                .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                .CreateLogger();

            BuildWebHost(args)
                .MigrateDbContext<UserManagementContext>((context, services) =>
                {
                    var logger = services.GetService<ILogger<DataContextSeed>>();
                    //var env = services.GetService<IHostingEnvironment>();
                    //var languageManager = services.GetService<ILanguageManager>();
                    //var countryManager = services.GetService<ICountryManager>();
                    //var customerTypeManager = services.GetService<ICustomerTypeManager>();
                    //new DataContextSeed(customerTypeManager, languageManager, countryManager)
                    //   .SeedAsync(context, env, logger)
                    //.Wait();
                }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .UseSerilog()
              .Build();
    }
}
