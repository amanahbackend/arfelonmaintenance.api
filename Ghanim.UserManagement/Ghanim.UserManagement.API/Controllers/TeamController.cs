﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.BLL.NotMappedModels;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class TeamController : BaseController<ITeamManager, Team, TeamViewModel>
    {
        public ITeamMemberManager teamMemberManger;
        public IDispatcherManager dispatcherManger;
        public readonly new IMapper mapper;
        IServiceProvider serviceProvider;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public TeamController(IServiceProvider _serviceProvider, ITeamManager _manger,
            ITeamMemberManager _teamMemberManger, IDispatcherManager _dispatcherManager,
            IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityUserService _identityUserService) :
            base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManger = _teamMemberManger;
            dispatcherManger = _dispatcherManager;
            serviceProvider = _serviceProvider;
        }

        private IForemanManager formanManager
        {
            get { return serviceProvider.GetService<IForemanManager>(); }
        }

        [HttpPost, Route("ResetPassword")]
        public async Task<ProcessResultViewModel<bool>> ResetPassword([FromBody] TeamResetPasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                if (model != null && !string.IsNullOrEmpty(model.NewPassword) && model.TeamId > 0)
                {
                    var members = teamMemberManger.GetAll().Data.Where(x => x.TeamId == model.TeamId)
                        .Select(x => x.MemberParentName).ToList();
                    ResetPasswordViewModel resetPasswordModel = new ResetPasswordViewModel()
                    {
                        NewPassword = model.NewPassword,
                        Usernames = members
                    };
                    result = await identityUserService.ResetPassword(resetPasswordModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Team Reset Password View Model id is invalid");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<TeamViewModel> Post([FromBody] TeamViewModel model)
        {
            try
            {
                var formanRes = formanManager.Get(model.ForemanId);
                if (formanRes != null && formanRes.Data != null)
                {
                    model.DispatcherId = formanRes.Data.DispatcherId;
                    model.DispatcherName = formanRes.Data.DispatcherName;
                    model.EngineerId = formanRes.Data.EngineerId;
                    model.EngineerName = formanRes.Data.EngineerName;
                    model.SupervisorId = formanRes.Data.SupervisorId;
                    model.SupervisorName = formanRes.Data.SupervisorName;
                    return base.Post(model);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<TeamViewModel>(null,
                        "there isn't forman with Id:" + model.ForemanId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TeamViewModel>(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] TeamViewModel model)
        {
            try
            {
                var formanRes = formanManager.Get(model.ForemanId);
                if (formanRes != null && formanRes.Data != null)
                {
                    model.DispatcherId = formanRes.Data.DispatcherId;
                    model.DispatcherName = formanRes.Data.DispatcherName;
                    model.EngineerId = formanRes.Data.EngineerId;
                    model.EngineerName = formanRes.Data.EngineerName;
                    model.SupervisorId = formanRes.Data.SupervisorId;
                    model.SupervisorName = formanRes.Data.SupervisorName;
                    return base.Put(model);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false,
                        "there isn't forman with Id:" + model.ForemanId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetByDispatcherId/{dispatcherId}")]
        public ProcessResultViewModel<List<TeamViewModel>> GetByDispatcherId([FromRoute] int dispatcherId)
        {
            var entityResult = manger.GetByDispatcherId(dispatcherId);
            var result = processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByFormanId")]
        public ProcessResultViewModel<List<TeamViewModel>> GetByFormanId([FromQuery] List<int> formansId)
        {
            var entityResult = manger.GetByFormansId(formansId);
            var result = processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            return result;
        }

        [HttpPut]
        [Route("ReassignTeamToDispatcher")]
        public ProcessResultViewModel<bool> ReassignTeamToDispatcher([FromQuery] int teamId, int dispatcherId)
        {
            ProcessResultViewModel<bool> result = null;
            ;

            var dispatcherRes = dispatcherManger.Get(dispatcherId);
            var teamRes = manger.Get(teamId);
            if (teamRes != null && teamRes.Data != null && dispatcherRes != null && dispatcherRes.Data != null)
            {
                teamRes.Data.DispatcherId = dispatcherRes.Data.Id;
                teamRes.Data.DispatcherName = dispatcherRes.Data.Name;
                teamRes.Data.SupervisorId = dispatcherRes.Data.SupervisorId;
                teamRes.Data.SupervisorName = dispatcherRes.Data.SupervisorName;
                var entityResult = manger.Update(teamRes.Data);
                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>) null);
            }

            return result;
        }

        [HttpGet]
        [Route("GetAllWithDispatcher")]
        public ProcessResultViewModel<List<DispatcherWithTeamsViewModel>> GetAllWithDispatcher()
        {
            List<DispatcherWithTeamsViewModel> result = null;
            var dispatchers = dispatcherManger.GetAll();
            if (dispatchers.IsSucceeded && dispatchers.Data.Count > 0)
            {
                List<int> dispatcherIds = dispatchers.Data.Select(x => x.Id).ToList();
                result = manger.GetByDispatchersId(dispatcherIds).Data;
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        foreach (var team in item.Teams)
                        {
                            var teamMemberRes = teamMemberManger.GetByTeamId(team.Id);
                            if (teamMemberRes != null && teamMemberRes.Data.Count() > 0)
                            {
                                team.Members = teamMemberRes.Data;
                            }
                        }
                    }
                }
                for (int i = 0; i < dispatchers.Data.Count; i++)
                {
                    var isFound = result.Find(x => x.DispatcherId == dispatchers.Data[i].Id);
                    if (isFound == null)
                    {
                        result.Add(new DispatcherWithTeamsViewModel()
                        {
                            DispatcherId = dispatchers.Data[i].Id,
                            DispatcherName = dispatchers.Data[i].Name,
                            Teams = new List<Team>()
                        });
                    }
                }
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            return ProcessResultViewModelHelper.Failed<List<DispatcherWithTeamsViewModel>>(null,
                "something wrong with dispatchers");
        }

        [HttpGet]
        [Route("SearchByTeamName")]
        public ProcessResultViewModel<List<TeamViewModel>> SearchByTeamName([FromQuery] string word)
        {
            var entityResult = manger.SearchByTeamName(word);
            var result = processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            return result;
        }
    }
}