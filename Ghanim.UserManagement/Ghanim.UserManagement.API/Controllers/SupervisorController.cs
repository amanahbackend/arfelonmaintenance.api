﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class SupervisorController : BaseController<ISupervisorManager, Supervisor, SupervisorViewModel>
    {
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public SupervisorController(ISupervisorManager _manger, IIdentityUserService _identityUserService,
            IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper,
            _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
        }

        [HttpGet]
        [Route("GetSupervisorUser/{id}")]
        public async Task<ProcessResultViewModel<SupervisorUserViewModel>> GetSupervisorUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Supervisor> supervisorTemp = manger.Get(id);
                if (supervisorTemp.IsSucceeded && supervisorTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp =
                        await identityUserService.GetById(supervisorTemp.Data.UserId);
                    SupervisorUserViewModel result =
                        mapper.Map<Supervisor, SupervisorUserViewModel>(supervisorTemp.Data);
                    mapper.Map<ApplicationUserViewModel, SupervisorUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<SupervisorUserViewModel>(null, "invalid supervisor id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<SupervisorUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetSupervisorUsers")]
        public async Task<ProcessResultViewModel<List<SupervisorUserViewModel>>> GetSupervisorUsers()
        {
            try
            {
                ProcessResult<List<Supervisor>> supervisorsTemp = manger.GetAll();
                if (supervisorsTemp.IsSucceeded && supervisorsTemp.Data.Count > 0)
                {
                    List<string> userIds = supervisorsTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<SupervisorUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<SupervisorUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Supervisor, SupervisorUserViewModel>(supervisorsTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<SupervisorUserViewModel>(),
                        "no supervisor found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<SupervisorUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetSupervisorsByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<SupervisorViewModel>>> GetSupervisorsByDivisionId(
            [FromRoute] int divisionId)
        {
            try
            {
                List<Supervisor> supervisorsTemp = manger.GetAll().Data.Where(x => x.DivisionId == divisionId).ToList();
                if (supervisorsTemp != null && supervisorsTemp.Count > 0)
                {
                    //List<string> userIds = supervisorsTemp.Data.Select(x => x.UserId).ToList();
                    //ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<SupervisorViewModel> result =
                        mapper.Map<List<Supervisor>, List<SupervisorViewModel>>(supervisorsTemp);
                    // YARAB SAM7NY .. Fawzy
                    //for (int i = 0; i < result.Count; i++)
                    //{
                    //    mapper.Map<Supervisor, SupervisorUserViewModel>(supervisorsTemp.Data[i], result[i]);
                    //}
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded<List<SupervisorViewModel>>(null,
                        "no supervisor found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<SupervisorViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetSupervisorByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<SupervisorViewModel>> GetSupervisorByDivisionId([FromRoute] int divisionId)
        {
            try
            {
                Supervisor supervisorTemp = manger.Get(x => x.DivisionId == divisionId).Data;
                if (supervisorTemp != null)
                {
                    //List<string> userIds = supervisorsTemp.Data.Select(x => x.UserId).ToList();
                    //ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    SupervisorViewModel result = mapper.Map<Supervisor, SupervisorViewModel>(supervisorTemp);
                    // YARAB SAM7NY .. Fawzy
                    //for (int i = 0; i < result.Count; i++)
                    //{
                    //    mapper.Map<Supervisor, SupervisorUserViewModel>(supervisorsTemp.Data[i], result[i]);
                    //}
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded<SupervisorViewModel>(null, "no supervisor found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<SupervisorViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetSupervisorByUserId/{userId}")]
        public ProcessResultViewModel<SupervisorViewModel> GetSupervisorByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.GetAll().Data.Where(x => x.UserId == userId).FirstOrDefault();
                SupervisorViewModel result = mapper.Map<Supervisor, SupervisorViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<SupervisorViewModel>(null, ex.Message);
            }
        }
    }
}