﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;

using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class DriverController : BaseController<IDriverManager, Driver, DriverViewModel>
    {
        public ITeamMemberManager teamMemberManager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public DriverController(IDriverManager _manger, ITeamMemberManager _teamMemberManager, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService _identityUserService) : base(_manger, _mapper, _processResultMapper,
            _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManager = _teamMemberManager;
        }

        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<DriverViewModel> Post([FromBody] DriverViewModel model)
        {
            ProcessResultViewModel<DriverViewModel> resultViewModel = base.Post(model);
            teamMemberManager.Add(new TeamMember()
            {
                MemberParentId = resultViewModel.Data.Id,
                MemberParentName = resultViewModel.Data.Name,
                MemberType = (int) MemberType.Driver,
                MemberTypeName = "Driver",
                UserId = resultViewModel.Data.UserId,
                PF = resultViewModel.Data.PF
            });
            return resultViewModel;
        }

        [HttpPost]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<DriverViewModel>> PostMulti(
            [FromBody] List<DriverViewModel> lstmodel)
        {
            ProcessResultViewModel<List<DriverViewModel>> resultViewModel = base.PostMulti(lstmodel);
            foreach (var item in resultViewModel.Data)
            {
                teamMemberManager.Add(new TeamMember()
                {
                    MemberParentId = item.Id,
                    MemberParentName = item.Name,
                    MemberType = (int) MemberType.Driver,
                    UserId = item.UserId,
                    MemberTypeName = "Driver",
                    PF = item.PF
                });
            }
            return resultViewModel;
        }

        [HttpGet]
        [Route("GetDriverUser/{id}")]
        public async Task<ProcessResultViewModel<DriverUserViewModel>> GetDriverUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Driver> driverTemp = manger.Get(id);
                if (driverTemp.IsSucceeded && driverTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp =
                        await identityUserService.GetById(driverTemp.Data.UserId);
                    DriverUserViewModel result = mapper.Map<Driver, DriverUserViewModel>(driverTemp.Data);
                    mapper.Map<ApplicationUserViewModel, DriverUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<DriverUserViewModel>(null, "invalid driver id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<DriverUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDriverUsers")]
        public async Task<ProcessResultViewModel<List<DriverUserViewModel>>> GetDriverUsers()
        {
            try
            {
                ProcessResult<List<Driver>> driversTemp = manger.GetAll();
                if (driversTemp.IsSucceeded && driversTemp.Data.Count > 0)
                {
                    List<string> userIds = driversTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<DriverUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<DriverUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Driver, DriverUserViewModel>(driversTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<DriverUserViewModel>(), "no drivers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DriverUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDriverUsersByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<DriverUserViewModel>>> GetDriverUsersByDivisionId(
            [FromRoute] int divisionId)
        {
            try
            {
                ProcessResult<List<Driver>> engineersTemp = manger.GetAll(x => x.DivisionId == divisionId);
                if (engineersTemp.IsSucceeded && engineersTemp.Data.Count > 0)
                {
                    List<string> userIds = engineersTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<DriverUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<DriverUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Driver, DriverUserViewModel>(engineersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<DriverUserViewModel>(), "no drivers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DriverUserViewModel>>(null, ex.Message);
            }
        }
    }
}