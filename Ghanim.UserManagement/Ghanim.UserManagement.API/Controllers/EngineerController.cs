﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class EngineerController : BaseController<IEngineerManager, Engineer, EngineerViewModel>
    {
        public IEngineerManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;
        public EngineerController(IEngineerManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityUserService _identityUserService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
        }


        [HttpGet]
        [Route("GetEngineerUser/{id}")]
        public async Task<ProcessResultViewModel<EngineerUserViewModel>> GetEngineerUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Engineer> engineerTemp = manger.Get(id);
                if (engineerTemp.IsSucceeded && engineerTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(engineerTemp.Data.UserId);
                    EngineerUserViewModel result = mapper.Map<Engineer, EngineerUserViewModel>(engineerTemp.Data);
                    mapper.Map<ApplicationUserViewModel, EngineerUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<EngineerUserViewModel>(null, "invalid engineer id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<EngineerUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetEngineerUsersByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<EngineerUserViewModel>>> GetEngineerUserByDivisionId([FromRoute] int divisionId)
        {
            try
            {
                ProcessResult<List<Engineer>> engineersTemp = manger.GetAll(x => x.DivisionId == divisionId);
                if (engineersTemp.IsSucceeded && engineersTemp.Data.Count > 0)
                {
                    List<string> userIds = engineersTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<EngineerUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<EngineerUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Engineer, EngineerUserViewModel>(engineersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<EngineerUserViewModel>(), "no engineers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<EngineerUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetEngineerUsers")]
        public async Task<ProcessResultViewModel<List<EngineerUserViewModel>>> GetEngineerUsers()
        {
            try
            {
                ProcessResult<List<Engineer>> engineersTemp = manger.GetAll();
                if (engineersTemp.IsSucceeded && engineersTemp.Data.Count > 0)
                {
                    List<string> userIds = engineersTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<EngineerUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<EngineerUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Engineer, EngineerUserViewModel>(engineersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<EngineerUserViewModel>(), "no engineers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<EngineerUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetEngineerByUserId/{userId}")]
        public ProcessResultViewModel<EngineerViewModel> GetEngineerByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.GetAll().Data.Where(x => x.UserId == userId).FirstOrDefault();
                EngineerViewModel result = mapper.Map<Engineer, EngineerViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<EngineerViewModel>(null, ex.Message);
            }
        }
    }
}