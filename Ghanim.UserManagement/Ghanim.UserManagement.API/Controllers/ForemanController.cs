﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class ForemanController : BaseController<IForemanManager, Foreman, ForemanViewModel>
    {
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public ForemanController(IForemanManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityUserService _identityUserService) :
            base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
        }

        [HttpGet]
        [Route("GetForemanUser/{id}")]
        public async Task<ProcessResultViewModel<ForemanUserViewModel>> GetForemanUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Foreman> foremanTemp = manger.Get(id);
                if (foremanTemp.IsSucceeded && foremanTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp =
                        await identityUserService.GetById(foremanTemp.Data.UserId);
                    ForemanUserViewModel result = mapper.Map<Foreman, ForemanUserViewModel>(foremanTemp.Data);
                    mapper.Map<ApplicationUserViewModel, ForemanUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<ForemanUserViewModel>(null, "invalid engineer id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ForemanUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetForemansByDispatcherId/{dispatcherId}")]
        public async Task<ProcessResultViewModel<List<ForemanUserViewModel>>> GetForemansByDispatcherId(
            [FromRoute] int dispatcherId)
        {
            try
            {
                ProcessResult<List<Foreman>> foremansTemp = manger.GetAll(x => x.DispatcherId == dispatcherId);
                if (foremansTemp.IsSucceeded && foremansTemp.Data != null)
                {
                    List<string> userIds = foremansTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<ForemanUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<ForemanUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Foreman, ForemanUserViewModel>(foremansTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<ForemanUserViewModel>(),
                        "no foremans found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<ForemanUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetForemanUsers")]
        public async Task<ProcessResultViewModel<List<ForemanUserViewModel>>> GetForemanUsers()
        {
            try
            {
                ProcessResult<List<Foreman>> foremansTemp = manger.GetAll();
                if (foremansTemp.IsSucceeded && foremansTemp.Data.Count > 0)
                {
                    List<string> userIds = foremansTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<ForemanUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<ForemanUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Foreman, ForemanUserViewModel>(foremansTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<ForemanUserViewModel>(),
                        "no foremans found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<ForemanUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetForemanByUserId/{userId}")]
        public ProcessResultViewModel<ForemanViewModel> GetForemanByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.GetAll().Data.Where(x => x.UserId == userId).FirstOrDefault();
                ForemanViewModel result = mapper.Map<Foreman, ForemanViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ForemanViewModel>(null, ex.Message);
            }
        }
    }
}