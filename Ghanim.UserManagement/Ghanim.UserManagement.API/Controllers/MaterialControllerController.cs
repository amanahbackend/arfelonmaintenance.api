﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/Material")]
    public class MaterialControllerController : BaseController<IMaterialControllerManager,MaterialController,MaterialControllerViewModel>
    {
        public IMaterialControllerManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public MaterialControllerController(IMaterialControllerManager _manager, IMapper _mapper, 
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,IIdentityUserService _identityuserservice) 
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityuserservice;
        }

        [HttpGet]
        [Route("GetMaterialControllerUser/{id}")]
        public async Task<ProcessResultViewModel<MaterialControllerUserViewModel>> GetMaterialControllerUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<MaterialController> materialTemp = manager.Get(id);
                if (materialTemp.IsSucceeded && materialTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(materialTemp.Data.UserId);
                    MaterialControllerUserViewModel result = mapper.Map<MaterialController, MaterialControllerUserViewModel>(materialTemp.Data);
                    mapper.Map<ApplicationUserViewModel, MaterialControllerUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<MaterialControllerUserViewModel>(null, "invalid material controller id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<MaterialControllerUserViewModel>(null, ex.Message);
            }
        }
    }
}