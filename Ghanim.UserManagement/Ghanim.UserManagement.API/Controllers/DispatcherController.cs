﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.Filters;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class DispatcherController : BaseController<IDispatcherManager, Dispatcher, DispatcherViewModel>
    {
        public readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public DispatcherController(IDispatcherManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService _identityUserService) : base(_manger, _mapper, _processResultMapper,
            _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
        }

        [HttpGet]
        [Route("GetDispatcherUser/{id}")]
        public async Task<ProcessResultViewModel<DispatcherUserViewModel>> GetDispatcherUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Dispatcher> dispatcherTemp = manger.Get(id);
                if (dispatcherTemp.IsSucceeded && dispatcherTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp =
                        await identityUserService.GetById(dispatcherTemp.Data.UserId);
                    DispatcherUserViewModel result =
                        mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatcherTemp.Data);
                    mapper.Map<ApplicationUserViewModel, DispatcherUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<DispatcherUserViewModel>(null, "invalid dispatcher id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<DispatcherUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDispatcherUsers")]
        public async Task<ProcessResultViewModel<List<DispatcherUserViewModel>>> GetDispatcherUsers()
        {
            try
            {
                ProcessResult<List<Dispatcher>> dispatchersTemp = manger.GetAll();
                if (dispatchersTemp.IsSucceeded && dispatchersTemp.Data.Count > 0)
                {
                    List<string> userIds = dispatchersTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<DispatcherUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<DispatcherUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatchersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<DispatcherUserViewModel>(),
                        "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDispatcherUsersByDivisonId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatcherUsers(int divisionId)
        {
            try
            {
                List<Dispatcher> dispatchersTemp = manger.GetAll().Data.Where(x => x.DivisionId == divisionId).ToList();
                if (dispatchersTemp.Count > 0)
                {
                    //List<string> userIds = dispatchersTemp.Select(x => x.UserId).ToList();
                    //ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<DispatcherViewModel> result =
                        mapper.Map<List<Dispatcher>, List<DispatcherViewModel>>(dispatchersTemp);
                    //// YARAB SAM7NY .. Fawzy
                    //for (int i = 0; i < result.Count; i++)
                    //{
                    //    mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatchersTemp[i], result[i]);
                    //}
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<DispatcherViewModel>(),
                        "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDispatcherUsersBySupervisorId/{supervisorId}")]
        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>>
            GetDispatcherUsersBySupervisorId(int supervisorId)
        {
            try
            {
                List<Dispatcher> dispatchersTemp = manger.GetAll().Data.Where(x => x.SupervisorId == supervisorId)
                    .ToList();
                if (dispatchersTemp.Count > 0)
                {
                    //List<string> userIds = dispatchersTemp.Select(x => x.UserId).ToList();
                    //ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<DispatcherViewModel> result =
                        mapper.Map<List<Dispatcher>, List<DispatcherViewModel>>(dispatchersTemp);
                    //// YARAB SAM7NY .. Fawzy
                    //for (int i = 0; i < result.Count; i++)
                    //{
                    //    mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatchersTemp[i], result[i]);
                    //}
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<DispatcherViewModel>(),
                        "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDispatcherByUserId/{userId}")]
        public ProcessResultViewModel<DispatcherViewModel> GetDispatcherByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.GetAll().Data.Where(x => x.UserId == userId).FirstOrDefault();
                DispatcherViewModel result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<DispatcherViewModel>(null, ex.Message);
            }
        }
    }
}