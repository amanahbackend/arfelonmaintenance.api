﻿using AutoMapper;
using CommonEnums;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.BLL.NotMappedModels;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class TeamMemberController : BaseController<ITeamMemberManager, TeamMember, TeamMemberViewModel>
    {
        public readonly new IMapper mapper;
        IServiceProvider serviceProvider;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public TeamMemberController(IServiceProvider _serviceProvider,
            ITeamMemberManager _manger,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService _identityUserService
            ) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            serviceProvider = _serviceProvider;
        }

        private ITeamManager teamManager
        {
            get
            {
                return serviceProvider.GetService<ITeamManager>();
            }
        }
        private IVehicleManager vehicleManager
        {
            get
            {
                return serviceProvider.GetService<IVehicleManager>();
            }
        }

        private ITechnicianManager technicianManager
        {
            get
            {
                return serviceProvider.GetService<ITechnicianManager>();
            }
        }

        [HttpGet]
        [Route("GetUnassignedMember")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetUnassignedMember()
        {
            var entityResult = manger.GetUnassignedMember();
            var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetUnassignedMemberByDivisionId/{divisionId}")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetUnassignedMemberByDivisionId([FromRoute]int divisionId)
        {
            try
            {
                var entityResult = manger.GetUnassignedMemberByDivisionId(divisionId);
                var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception : " + FlattenException(ex, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }

        private string FlattenException(Exception exception, string Method)
        {
            string logFormat;
            logFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString();
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(logFormat);
            stringBuilder.AppendLine("********************************");

            var st = new StackTrace(exception, true);
            var frame = st.GetFrame(0);
            stringBuilder.AppendLine("Line Number: " + frame.GetFileLineNumber());
            stringBuilder.AppendLine("File Name: " + frame.GetFileName());
            stringBuilder.AppendLine("Method Name: " + frame.GetMethod());
            stringBuilder.AppendLine(Method);

            while (exception != null)
            {
                stringBuilder.AppendLine("-------------------------------------");
                stringBuilder.AppendLine("\n Message=" + exception.Message);
                stringBuilder.AppendLine("\n StackTrace=" + exception.StackTrace);
                stringBuilder.AppendLine("\n Exception=" + exception.ToString());
                stringBuilder.AppendLine("----------------------------------------------------------");
                exception = exception.InnerException;
            }
            stringBuilder.AppendLine("**************END******************");
            return stringBuilder.ToString();
        }

        [HttpGet]
        [Route("GetUnassignedVehicleMember")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetUnassignedVehicleMember()
        {
            var entityResult = manger.GetUnassignedVehicleMember();
            var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetMembersByTeamId/{teamId}")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetMembersByTeamId([FromRoute]int teamId)
        {
            var entityResult = manger.GetByTeamId(teamId);

            var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);

            if (result.IsSucceeded && result.Data.Count > 0)
            {
                if (result.Data.Count > 0)
                {
                    result = BindingTechnicianDataToMember(result, entityResult);
                }

            }

            return result;
        }

        [HttpGet]
        [Route("GetTeamIdByTechnicianMember/{Id}")]
        public ProcessResult<List<TeamMemberViewModel>> GetTeamIdByTechnicianMember([FromRoute]int Id)
        {
            var entityResult = manger.GetAll().Data.Where(x=>x.MemberType==(int)MemberType.Technician&&x.MemberParentId==Id).ToList();
            var result = mapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);           
            return ProcessResultHelper.Succeeded<List<TeamMemberViewModel>>(result);
        }

        private ProcessResultViewModel<List<TeamMemberViewModel>> BindingTechnicianDataToMember(ProcessResultViewModel<List<TeamMemberViewModel>> models, ProcessResult<List<TeamMember>> enitityModel)
        {
            for (int i = 0; i < models.Data.Count; i++)
            {
                if (models.Data[i].MemberTypeName == "Technician")
                {
                    var technician = technicianManager.Get(enitityModel.Data[i].MemberParentId);
                    if (technician.IsSucceeded && technician.Data != null)
                    {
                        models.Data[i].CostCenter = technician.Data.CostCenterName;
                        models.Data[i].CostCenterId = technician.Data.CostCenterId;
                        models.Data[i].DivisionId = technician.Data.DivisionId;
                        models.Data[i].DivisionName = technician.Data.DivisionName;
                        models.Data[i].PF = technician.Data.PF;
                        models.Data[i].UserId = technician.Data.UserId;
                    }
                }
            }
            return models;
        }
        [HttpGet]
        [Route("GetMemberUsersByTeamId/{teamId}")]
        public ProcessResult<List<TeamMemberUserViewModel>> GetMemberUsersByTeamId(int teamId)
        {

            var members = manger.GetAll(x => x.TeamId == teamId&& x.MemberType!=(int)MemberType.Vehicle).Data;

            if (members.Count > 0)
            {
                var result = mapper.Map<List<TeamMember>, List<TeamMemberUserViewModel>>(members);
                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i].MemberTypeName == "Technician")
                    {
                        var technician = technicianManager.Get(result[i].MemberParentId);
                        result[i].UserId = technician.Data.UserId;
                        result[i].PF = technician.Data.PF;
                        result[i].CostCenter = technician.Data.CostCenterName;
                        result[i].CostCenterId = technician.Data.CostCenterId;
                    }
                    else
                    {

                    }
                }
                return ProcessResultHelper.Succeeded<List<TeamMemberUserViewModel>>(result);
            }
            else
            {
                return ProcessResultHelper.Succeeded<List<TeamMemberUserViewModel>>(null);
            }
        }

        [HttpPut]
        [Route("assignedMemberToTeam/{teamId}")]
        public ProcessResultViewModel<bool> AssignedMemberToTeam([FromRoute] int teamId, int memberId)
        {
            ProcessResultViewModel<bool> result = new ProcessResultViewModel<bool> { Data = false }; var memberRes = manger.Get(memberId);
            var teamRes = teamManager.Get(teamId);
            var technicianRes = technicianManager.Get(memberRes.Data.MemberParentId);

            if (memberRes.Data.MemberType == (int)MemberType.Technician)
            {
                if (teamRes != null && teamRes.Data != null && technicianRes != null && technicianRes.Data != null)
                {
                    if (teamRes.Data.DivisionId == technicianRes.Data.DivisionId)
                    {
                        var entityResult = manger.AssignedMemberToTeam(teamId, memberId);
                        result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed(false, "Technician and Team Should be in the same Division.");

                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed(false, "There isn't team with this teamId or there  isn't technician with this technicianId.");

                }
            }
            else if (memberRes.Data.MemberType == (int)MemberType.Vehicle)
            {
                var vehicleMemberRes = manger.GetByMemberType_TeamId(teamId, memberRes.Data.MemberType);
                var vehicleRes = vehicleManager.Get(memberRes.Data.MemberParentId);
                // there is Vehicle in this team
                if (vehicleMemberRes != null && vehicleMemberRes.Data != null)
                {
                    return ProcessResultViewModelHelper.Failed(false, "There is another Vehicle for this team.");
                }
                else
                {
                    // no Vehicle in team 
                    if (teamRes != null && teamRes.Data != null && vehicleRes != null && vehicleRes.Data != null)
                    {
                        var entityResult = manger.AssignedMemberToTeam(teamId, memberId);
                        if (entityResult.Data)
                        {
                            vehicleRes.Data.Isassigned = true;
                            var vehicleUpdateRes = vehicleManager.Update(vehicleRes.Data);
                        }
                        result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed(false, "There isn't team with this teamId or there  isn't Vehicle with this Plate No.");
                    }
                }
            }
            return result;
        }

        [HttpPut]
        [Route("assignedMembersToTeam/{teamId}")]
        public ProcessResultViewModel<bool> AssignedMembersToTeam([FromRoute] int teamId, [FromBody] List<int> membersIds)
        {
            ProcessResultViewModel<bool> result = new ProcessResultViewModel<bool> { Data = false };
            // check if there are old members
            var oldMembersRes = GetMembersByTeamId(teamId);

            // unassign old members
            if (oldMembersRes != null && oldMembersRes.Data.Count > 0)
            {
                var oldMembersIds = oldMembersRes.Data.Select(x => x.Id).ToList();
                var UnassignedMembersRes = UnassignedMembers(oldMembersIds);
            }
            // assign new members to team
            foreach (var memberId in membersIds)
            {
                var memberRes = manger.Get(memberId);
                var teamRes = teamManager.Get(teamId);
                var technicianRes = technicianManager.Get(memberRes.Data.MemberParentId);
                if (memberRes.Data.MemberType == (int)MemberType.Technician)
                {
                    if (teamRes != null && teamRes.Data != null && technicianRes != null && technicianRes.Data != null)
                    {
                        if (teamRes.Data.DivisionId == technicianRes.Data.DivisionId)
                        {
                            var entityResult = manger.AssignedMemberToTeam(teamId, memberId);
                            if (entityResult.Data)
                            {
                                technicianRes.Data.TeamId = teamId;
                                var technicianUpdateRes = technicianManager.Update(technicianRes.Data);
                            }
                            result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed(false, "Technician and Team Should be in the same Division.");

                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed(false, "There isn't team with this teamId or there  isn't technician with this technicianId.");

                    }
                }
                else if (memberRes.Data.MemberType == (int)MemberType.Vehicle)
                {
                    var vehicleMemberRes = manger.GetByMemberType_TeamId(teamId, memberRes.Data.MemberType);
                    var vehicleRes = vehicleManager.Get(memberRes.Data.MemberParentId);
                    // there is Vehicle in this team
                    if (vehicleMemberRes != null && vehicleMemberRes.Data != null)
                    {
                        return ProcessResultViewModelHelper.Failed(false, "There is another Vehicle for this team.");
                    }
                    else
                    {
                        // no Vehicle in team 
                        if (teamRes != null && teamRes.Data != null && vehicleRes != null && vehicleRes.Data != null)
                        {
                            var entityResult = manger.AssignedMemberToTeam(teamId, memberId);
                            if (entityResult.Data)
                            {
                                vehicleRes.Data.Isassigned = true;
                                vehicleRes.Data.TeamId = teamId;
                                var vehicleUpdateRes = vehicleManager.Update(vehicleRes.Data);
                            }
                            result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed(false, "There isn't team with this teamId or there  isn't Vehicle with this VehicleId.");
                        }
                    }
                }
            }
            return result;
        }

        [HttpPut]
        [Route("UnassignedMember")]
        public ProcessResultViewModel<bool> ReassignTeamToDispatcher([FromQuery] int memberId)
        {
            ProcessResultViewModel<bool> result = null; ;

            var memberRes = manger.Get(memberId);
            if (memberRes != null && memberRes.Data != null)
            {
                memberRes.Data.TeamId = null;
                if (memberRes.Data.MemberType == (int)MemberType.Vehicle)
                {
                    var vehicleMemberRes = vehicleManager.Get(memberRes.Data.MemberParentId);
                    vehicleMemberRes.Data.Isassigned = false;
                    vehicleMemberRes.Data.TeamId = 0;
                    var vehicleResult = vehicleManager.Update(vehicleMemberRes.Data);
                }
                else if (memberRes.Data.MemberType == (int)MemberType.Technician)
                {
                    var technicianMemberRes = technicianManager.Get(memberRes.Data.MemberParentId);
                    technicianMemberRes.Data.TeamId = 0;
                    var vehicleResult = technicianManager.Update(technicianMemberRes.Data);
                }

                var entityResult = manger.Update(memberRes.Data);
                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            }

            return result;
        }

        [HttpPut]
        [Route("UnassignedMembers")]
        public ProcessResultViewModel<bool> UnassignedMembers([FromBody] List<int> membersIds)
        {
            ProcessResultViewModel<bool> result = null;
            ProcessResult<bool> entityResult = new ProcessResult<bool> { Data = false };
            foreach (var memberId in membersIds)
            {
                var memberRes = manger.Get(memberId);
                if (memberRes != null && memberRes.Data != null)
                {
                    memberRes.Data.TeamId = null;
                    if (memberRes.Data.MemberType == (int)MemberType.Vehicle)
                    {
                        var vehicleMemberRes = vehicleManager.Get(memberRes.Data.MemberParentId);
                        vehicleMemberRes.Data.Isassigned = false;
                        vehicleMemberRes.Data.TeamId = 0;
                        var vehicleResult = vehicleManager.Update(vehicleMemberRes.Data);
                    }
                    else if (memberRes.Data.MemberType == (int)MemberType.Technician)
                    {
                        var technicianMemberRes = technicianManager.Get(memberRes.Data.MemberParentId);
                        technicianMemberRes.Data.TeamId = 0;
                        var vehicleResult = technicianManager.Update(technicianMemberRes.Data);
                    }

                    entityResult = manger.Update(memberRes.Data);
                }
                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            }
            return result;
        }

        [HttpPut]
        [Route("SortMemeber/{teamId}")]
        public ProcessResultViewModel<bool> SortMemeber([FromRoute]int teamId, [FromBody] List<TeamMemberRanks> teamMembers)
        {
            ProcessResultViewModel<bool> result;
            var entityResult = manger.SortMemeber(teamId, teamMembers);
            result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [HttpGet]
        [Route("SearchVec")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> SearchVec([FromQuery]string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return ProcessResultViewModelHelper.Succeeded(new List<TeamMemberViewModel>(), "word can not be empty");
            }
            else
            {
                var entityResult = manger.SearchVec(word);
                var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
                return result;
            }
        }


        [HttpGet]
        [Route("SearchTech")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> SearchTech([FromQuery]string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return ProcessResultViewModelHelper.Succeeded(new List<TeamMemberViewModel>(), "word can not be empty");
            }
            else
            {
                var entityResult = manger.SearchTech(word);
                var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
                return result;
            }
        }
    }
}