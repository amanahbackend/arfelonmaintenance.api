﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class AttendenceController : BaseController<IAttenenceManager, Attendence, AttendenceViewModel>
    {
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;

        public AttendenceController(IAttenenceManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) :
            base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }

    }
}