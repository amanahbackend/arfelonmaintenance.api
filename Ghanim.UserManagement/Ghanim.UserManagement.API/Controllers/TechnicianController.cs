﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;
using Ghanim.UserManagement.API.Helper;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class TechnicianController : BaseController<ITechnicianManager, Technician, TechnicianViewModel>
    {
        public ITeamMemberManager teamMemberManager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;
        ITechniciansStateManager techniciansStateManager;
        IAPIHelper helper;

        public TechnicianController(ITechnicianManager _manger,
            IAPIHelper _helper,
            ITeamMemberManager _teamMemberManager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService _identityUserService,
            ITechniciansStateManager _techniciansStateManager) : base(_manger, _mapper, _processResultMapper,
            _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            helper = _helper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManager = _teamMemberManager;
            techniciansStateManager = _techniciansStateManager;
        }

        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<TechnicianViewModel> Post([FromBody] TechnicianViewModel model)
        {
            try
            {
                ProcessResultViewModel<TechnicianViewModel> resultViewModel = base.Post(model);
                teamMemberManager.Add(new TeamMember()
                {
                    MemberParentId = resultViewModel.Data.Id,
                    MemberParentName = resultViewModel.Data.Name,
                    MemberType = (int) MemberType.Technician,
                    MemberTypeName = "Technician",
                    UserId = resultViewModel.Data.UserId,
                    PF = resultViewModel.Data.PF,
                    DivisionId = resultViewModel.Data.DivisionId,
                    DivisionName = resultViewModel.Data.DivisionName,
                    IsDriver = resultViewModel.Data.IsDriver
                });

                return resultViewModel;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianViewModel>(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<TechnicianViewModel>> PostMulti(
            [FromBody] List<TechnicianViewModel> lstmodel)
        {
            try
            {
                ProcessResultViewModel<List<TechnicianViewModel>> resultViewModel = base.PostMulti(lstmodel);
                foreach (var item in resultViewModel.Data)
                {
                    var res = teamMemberManager.Add(new TeamMember()
                    {
                        MemberParentId = item.Id,
                        MemberParentName = item.Name,
                        MemberType = (int) MemberType.Technician,
                        MemberTypeName = "Technician",
                        PF = item.PF,
                        UserId = item.UserId,
                        DivisionId = item.DivisionId,
                        DivisionName = item.DivisionName,
                        IsDriver = item.IsDriver
                    });
                }
                return resultViewModel;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetTechnicianUser/{id}")]
        public async Task<ProcessResultViewModel<TechnicianUserViewModel>> GetTechnicianUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Technician> technicianTemp = manger.Get(id);
                if (technicianTemp.IsSucceeded && technicianTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp =
                        await identityUserService.GetById(technicianTemp.Data.UserId);
                    TechnicianUserViewModel result =
                        mapper.Map<Technician, TechnicianUserViewModel>(technicianTemp.Data);
                    mapper.Map<ApplicationUserViewModel, TechnicianUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<TechnicianUserViewModel>(null, "invalid technician id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetTechnicianUsers")]
        public async Task<ProcessResultViewModel<List<TechnicianUserViewModel>>> GetTechnicianUsers()
        {
            try
            {
                ProcessResult<List<Technician>> techniciansTemp = manger.GetAll();
                if (techniciansTemp.IsSucceeded && techniciansTemp.Data.Count > 0)
                {
                    List<string> userIds = techniciansTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp =
                        await identityUserService.GetByUserIds(userIds);
                    List<TechnicianUserViewModel> result =
                        mapper.Map<List<ApplicationUserViewModel>, List<TechnicianUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Technician, TechnicianUserViewModel>(techniciansTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succeeded(new List<TechnicianUserViewModel>(),
                        "no technicians found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianUserViewModel>>(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody] LoginViewModel model)
        {

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.Username))
            {
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null,
                    "User name & password are required");
            }

            ProcessResultViewModel<LoginResultViewModel> loginResult = await identityUserService.Login(model);

            if (loginResult.IsSucceeded && loginResult.Data != null)
            {
                var technicianResult = manger.Get(x => x.UserId == loginResult.Data.UserId);
                if (technicianResult.IsSucceeded && technicianResult.Data != null)
                {
                    var teamMemberResult = teamMemberManager.Get(x => x.MemberParentId == technicianResult.Data.Id);

                    if (teamMemberResult.IsSucceeded && teamMemberResult.Data != null)
                    {
                        if (teamMemberResult.Data.TeamId != null)
                        {
                            loginResult.Data.TeamId = (int) teamMemberResult.Data.TeamId;
                            loginResult.Data.Id = (int) technicianResult.Data.Id;
                            loginResult.Data.FK_SupportedLanguages_ID =
                                (int) technicianResult.Data.FK_SupportedLanguages_ID;
                            var lastState =
                                techniciansStateManager.GetLastStateByTechnicianId(technicianResult.Data.Id);

                            if (lastState.IsSucceeded && lastState.Data != null)
                            {
                                loginResult.Data.LastState = lastState.Data.State;
                            }

                            return ProcessResultViewModelHelper.Succeeded(loginResult.Data);
                        }

                        return ProcessResultViewModelHelper.Succeeded(loginResult.Data,
                            "this technician is not assigned to team");
                    }

                    return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null,
                        "something wrong while fetching technician's member user");
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null,
                        "something wrong while fetching technician data");
                }
            }
            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null,
                "something wrong while login through identity service");
        }

        [HttpPost]
        [Route("Logout")]
        public async Task<ProcessResultViewModel<bool>> Logout([FromBody] LogoutViewModel model)
        {

            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.DeviceId))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "username & password are required");
            }
            else
            {
                ProcessResultViewModel<bool> logoutResult =
                    await identityUserService.Logout(model.UserId, model.DeviceId);

                if (logoutResult.IsSucceeded)
                {
                    var technicianResult = manger.Get(x => x.UserId == model.UserId);
                    if (technicianResult.IsSucceeded && technicianResult.Data != null)
                    {
                        var teamMemberResult = teamMemberManager.Get(x => x.MemberParentId == technicianResult.Data.Id);
                        if (teamMemberResult.IsSucceeded && teamMemberResult.Data != null)
                        {
                            if (teamMemberResult.Data.TeamId != null)
                            {
                                var technicianStateRes = techniciansStateManager.Add(new TechniciansState
                                {
                                    Lat = model.Lat,
                                    Long = model.Long,
                                    TeamId = technicianResult.Data.TeamId,
                                    TechnicianId = technicianResult.Data.Id,
                                    State = 0
                                });
                                return ProcessResultViewModelHelper.Succeeded<bool>(true);
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.Succeeded<bool>(false,
                                    "this technican is not assigned to team");
                            }
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<bool>(false,
                                "something wrong while fetching technician's member user");
                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false,
                            "something wrong while fetching technician data");
                    }
                }
                return ProcessResultViewModelHelper.Failed<bool>(false,
                    "something wrong while logout through identity service");
            }
        }

        [HttpGet]
        [Route("GetTechnicianByUserId/{userId}")]
        public ProcessResultViewModel<TechnicianViewModel> GetTechnicianByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.GetAll().Data.Where(x => x.UserId == userId).FirstOrDefault();
                TechnicianViewModel result = mapper.Map<Technician, TechnicianViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("Search")]
        public ProcessResultViewModel<List<TechnicianViewModel>> Search(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return ProcessResultViewModelHelper.Succeeded(new List<TechnicianViewModel>(), "word can not be empty");
            }
            else
            {
                var entityResult = manger.Search(word);
                var result = processResultMapper.Map<List<Technician>, List<TechnicianViewModel>>(entityResult);
                return result;
            }
        }

        [HttpPut]
        [Route("UpdateTechnicianLanguage/{languageId}")]
        public IProcessResultViewModel<bool> UpdateTechnicianLanguage(int languageId)
        {
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "UnAuthorized");
            }
            var technicianRes = manger.GetAll().Data.Where(x => x.UserId == currentUserId).FirstOrDefault();
            technicianRes.FK_SupportedLanguages_ID = languageId;
            var result = manger.Update(technicianRes);
            if (result.IsSucceeded)
            {
                return ProcessResultViewModelHelper.Succeeded<bool>(true);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Language of the Technician doesn't change.");
            }
        }
    }
}