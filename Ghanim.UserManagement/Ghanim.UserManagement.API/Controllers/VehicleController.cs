﻿using System.Collections.Generic;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class VehicleController : BaseController<IVehicleManager, Vehicle, VehicleViewModel>
    {
        public ITeamMemberManager teamMemberManager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public VehicleController(IVehicleManager _manger,
            ITeamMemberManager _teamMemberManager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService _identityUserService,
            ITechniciansStateManager _techniciansStateManager) : base(_manger, _mapper, _processResultMapper,
            _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManager = _teamMemberManager;
        }

        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<VehicleViewModel> Post([FromBody] VehicleViewModel model)
        {
            ProcessResultViewModel<VehicleViewModel> resultViewModel = base.Post(model);
            teamMemberManager.Add(new TeamMember()
            {
                MemberParentId = resultViewModel.Data.Id,
                MemberParentName = resultViewModel.Data.Plate_no,
                MemberType = (int) MemberType.Vehicle,
                MemberTypeName = "Vehicle"
            });
            return resultViewModel;
        }

        [HttpPut]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] VehicleViewModel model)
        {
            ProcessResultViewModel<bool> resultViewModel = base.Put(model);
            var vehicleMemeber =
                teamMemberManager.GetAll(x => x.MemberParentId == model.Id && x.MemberType == (int) MemberType.Vehicle);

            if (vehicleMemeber.IsSucceeded && vehicleMemeber.Data.Count > 0)
            {
                vehicleMemeber.Data[0].MemberParentName = model.Plate_no;
                teamMemberManager.Update(vehicleMemeber.Data[0]);
            }
            return resultViewModel;
        }

        [HttpPost]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<VehicleViewModel>> PostMulti(
            [FromBody] List<VehicleViewModel> lstmodel)
        {
            ProcessResultViewModel<List<VehicleViewModel>> resultViewModel = base.PostMulti(lstmodel);
            foreach (var item in resultViewModel.Data)
            {
                var rest = teamMemberManager.Add(new TeamMember()
                {
                    MemberParentId = item.Id,
                    MemberParentName = item.Plate_no,
                    MemberType = (int) MemberType.Vehicle,
                    MemberTypeName = "Vehicle"
                });
            }

            return resultViewModel;
        }

        [HttpGet]
        [Route("Search")]
        public ProcessResultViewModel<List<VehicleViewModel>> Search(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return ProcessResultViewModelHelper.Succeeded(new List<VehicleViewModel>(), "word can not be empty");
            }

            var entityResult = manger.Search(word);
            var result = processResultMapper.Map<List<Vehicle>, List<VehicleViewModel>>(entityResult);
            return result;
        }
    }
}