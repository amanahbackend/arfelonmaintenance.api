﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class ManagerController : BaseController<IManagerManager,Manager,ManagerViewModel>
    {
        public IManagerManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper ProcessResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;

        public ManagerController(IManagerManager _manager,IMapper _mapper,IProcessResultMapper _processResultMapper,IProcessResultPaginatedMapper _processResultPaginatedMapper,IIdentityUserService _identityUserService):base(_manager,_mapper,_processResultMapper,_processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            ProcessResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
        }

        [HttpGet]
        [Route("GetManagerByUserId/{userId}")]
        public ProcessResultViewModel<ManagerViewModel> GetManagerByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.GetAll().Data.Where(x => x.UserId == userId).FirstOrDefault();
                ManagerViewModel result = mapper.Map<Manager, ManagerViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succeeded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ManagerViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetManagerUser/{id}")]
        public async Task<ProcessResultViewModel<ManagerUserViewModel>> GetSupervisorUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Manager> managerTemp = manger.Get(id);
                if (managerTemp.IsSucceeded && managerTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(managerTemp.Data.UserId);
                    ManagerUserViewModel result = mapper.Map<Manager, ManagerUserViewModel>(managerTemp.Data);
                    mapper.Map<ApplicationUserViewModel, ManagerUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succeeded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<ManagerUserViewModel>(null, "invalid manager id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ManagerUserViewModel>(null, ex.Message);
            }
        }
    }
}