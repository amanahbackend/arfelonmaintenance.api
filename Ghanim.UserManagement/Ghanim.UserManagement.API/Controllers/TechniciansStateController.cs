﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using CommonEnums;
using CommonServicesAPI.ServicesCommunication.Identity;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[Controller]")]
    public class TechniciansStateController : BaseController<ITechniciansStateManager,TechniciansState,TechniciansStateViewModel>
    {
        public ITechniciansStateManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper ProcessResultMapper;
        IProcessResultPaginatedMapper ProcessResultPaginatedMapper;
        //private readonly IIdentityUserService identityUserService;

        public TechniciansStateController(ITechniciansStateManager _manager,IMapper _mapper,IProcessResultMapper _processResultMapper,IProcessResultPaginatedMapper _processResultPaginatedMapper):base(_manager,_mapper,_processResultMapper,_processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            ProcessResultMapper = _processResultMapper;
            ProcessResultPaginatedMapper = _processResultPaginatedMapper;
            //identityUserService = _identityUserService;
        }
        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<TechniciansStateViewModel> Post([FromBody] TechniciansStateViewModel model)
        {
            model.ActionDate = DateTime.UtcNow;
            ProcessResultViewModel<TechniciansStateViewModel> resultViewModel = base.Post(model);            
            return resultViewModel;
        }

        [HttpPost]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<TechniciansStateViewModel>> PostMulti([FromBody] List<TechniciansStateViewModel> lstmodel)
        {
            foreach (var item in lstmodel)
            {
                item.ActionDate = DateTime.UtcNow;
            }
            ProcessResultViewModel<List<TechniciansStateViewModel>> resultViewModel = base.PostMulti(lstmodel);
            
            return resultViewModel;
        }

        [HttpGet]
        [Route("GetTechnicianStates")]
        public ProcessResult<List<EnumEntity>> GetTechnicianStates()
        {
            return ProcessResultHelper.Succeeded<List<EnumEntity>>(EnumManager<TechnicianState>.GetEnumList());
        }
    }
}