﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.UserManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.UserManagement.Models.Entities
{
    public class Technician : BaseEntity, ITechnician
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string PF { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public bool IsDriver { get; set; }
        public int TeamId { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
        [NotMapped]
        public Team Team { get; set; }

        //public int SupervisorId { get; set; }
        //public string SupervisorName { get; set; }
        //public int DispatcherId { get; set; }
        //public string DispatcherName { get; set; }
        //public int ForemanId { get; set; }
        //public string ForemanName { get; set; }
    }
}
