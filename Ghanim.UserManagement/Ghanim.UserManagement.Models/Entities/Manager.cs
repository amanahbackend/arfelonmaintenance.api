﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.IEntities;

namespace Ghanim.UserManagement.Models.Entities
{
    public class Manager: BaseEntity , IManager
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string PF { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
    }
}
