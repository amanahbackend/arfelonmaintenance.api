﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.Entities
{
    public class Attendence : BaseEntity,IAttendance
    {
        public string AttendedId { get; set; }
        public string AttendedName { get; set; }
        public int StatusId { get; set; }
        public int StatusName { get; set; }
        public DateTime AttendedDate { get; set; }
    }
}
