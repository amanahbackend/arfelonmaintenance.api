﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.IEntities
{
    public interface ITeam : IBaseEntity
    {
        string VehicleNo { get; set; }
        //string DriverName { get; set; }
        //int DriverId { get; set; }
        int DivisionId { get; set; }
        string DivisionName { get; set; }
        int ForemanId { get; set; }
        string ForemanName { get; set; }
        int EngineerId { get; set; }
        string EngineerName { get; set; }
        int DispatcherId { get; set; }
        string DispatcherName { get; set; }
        int SupervisorId { get; set; }
        string SupervisorName { get; set; }
        string Name { get; set; }
        int StatusId { get; set; }
        string StatusName { get; set; }
        int ShiftId { get; set; }
    }
}