﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.IEntities
{
    public interface ITechniciansState :IBaseEntity
    {
        int TechnicianId { get; set; }
        int TeamId { get; set; }
        DateTime ActionDate { get; set; }
        TechnicianState State { get; set; }
        decimal Long { get; set; }
        decimal Lat { get; set; }

    }
}
