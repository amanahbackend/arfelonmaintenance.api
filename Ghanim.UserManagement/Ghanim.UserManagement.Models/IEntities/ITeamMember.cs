﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.IEntities
{
    public interface ITeamMember : IBaseEntity
    {
        int? TeamId { get; set; }
        int MemberParentId { get; set; }
        string MemberParentName { get; set; }
        string PF { get; set; }
        int Rank { get; set; }
        int MemberType { get; set; }
        string MemberTypeName { get; set; }
        string UserId { get; set; }
        int? DivisionId { get; set; }
        string DivisionName { get; set; }
        bool IsDriver { get; set; }
    }
}
