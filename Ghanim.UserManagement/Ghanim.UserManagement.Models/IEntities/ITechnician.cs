﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.IEntities
{
    public interface ITechnician : IBaseEntity
    {
        string UserId { get; set; }
        string Name { get; set; }
        string PF { get; set; }
        int DivisionId { get; set; }
        string DivisionName { get; set; }
        int CostCenterId { get; set; }
        string CostCenterName { get; set; }
        int TeamId { get; set; }
        Team Team { get; set; }
        bool IsDriver { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        SupportedLanguages SupportedLanguages { get; set; }
        //int SupervisorId { get; set; }
        //string SupervisorName { get; set; }
        //int DispatcherId { get; set; }
        //string DispatcherName { get; set; }
        //int ForemanId { get; set; }
        //string ForemanName { get; set; }
    }
}