﻿CREATE TABLE [dbo].[Customer] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    [Code]             NVARCHAR (20)  NULL,
    [FirstName]        NVARCHAR (20)  NULL,
    [SecondName]       NVARCHAR (20)  NULL,
    [ThirdName]        NVARCHAR (20)  NULL,
    [FourthName]       NVARCHAR (20)  NULL,
    [Phone1]           NVARCHAR (15)  NULL,
    [Phone2]           NVARCHAR (15)  NULL,
    [Address]          NVARCHAR (200) NULL,
    [Email]            NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([Id] ASC)
);

