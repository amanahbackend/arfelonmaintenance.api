﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface IShift : IBaseEntity
    {
        TimeSpan FromTime { get; set; }
        TimeSpan ToTime { get; set; }
    }
}
