﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface ILang_Shift : IBaseEntity
    {
        int FK_Shift_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        TimeSpan FromTime { get; set; }
        TimeSpan ToTime { get; set; }
        Shift Shift { get; set; }
        SupportedLanguages SupportedLanguages { get; set; }
    }
}
