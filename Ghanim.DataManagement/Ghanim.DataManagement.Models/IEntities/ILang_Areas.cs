﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface ILang_Areas : IBaseEntity
    {
        int FK_Area_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Name { get; set; }
        Areas Areas { get; set; }
        SupportedLanguages SupportedLanguages { get; set; }
    }
}
