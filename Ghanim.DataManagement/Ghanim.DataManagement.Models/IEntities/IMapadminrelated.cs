﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface IMapAdminRelated: IBaseEntity
    {
        string Mobile_Api_Key { get; set; }
        string Web_Api_Key { get; set; }
        int Refresh_Rate { get; set; }

    }
}
