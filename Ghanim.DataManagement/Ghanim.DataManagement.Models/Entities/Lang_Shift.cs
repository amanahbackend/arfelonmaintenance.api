﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ghanim.DataManagement.Models.Entities
{
    public class Lang_Shift : BaseEntity, ILang_Shift
    {
        public int FK_Shift_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        [NotMapped]
        public Shift Shift { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
