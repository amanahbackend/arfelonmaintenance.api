﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class MapAdminRelated : BaseEntity, IMapAdminRelated
    {
        public string Mobile_Api_Key { get; set; }

        public string Web_Api_Key { get; set; }

        public int Refresh_Rate { get; set; }

    }
}
