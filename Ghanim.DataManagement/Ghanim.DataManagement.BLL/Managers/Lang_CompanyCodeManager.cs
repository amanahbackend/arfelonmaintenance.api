﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_CompanyCodeManager : Repository<Lang_CompanyCode>, ILang_CompanyCodeManager
    {
        IServiceProvider _serviceprovider;
        public Lang_CompanyCodeManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }

        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private ICompanyCodeManager companyCodeManager
        {
            get
            {
                return _serviceprovider.GetService<ICompanyCodeManager>();
            }
        }
        public ProcessResult<List<Lang_CompanyCode>> GetAllLanguagesByCompanyCodeId(int CompanyCodeId)
        {
            List<Lang_CompanyCode> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_CompanyCode_ID == CompanyCodeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_CompanyCode { FK_CompanyCode_ID = CompanyCodeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_CompanyCode>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByCompanyCodeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CompanyCode>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByCompanyCodeId");
            }
        }

        public ProcessResult<bool> UpdateByCompanyCode(List<Lang_CompanyCode> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var companyCodeRes = companyCodeManager.Get(item.FK_CompanyCode_ID);
                        if (companyCodeRes != null && companyCodeRes.Data != null)
                        {
                            companyCodeRes.Data.Name = item.Name;
                            var updateRes = companyCodeManager.Update(companyCodeRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangCompanyCode = GetByCompanyCodeIdAndsupprotedLangId(item.FK_CompanyCode_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangCompanyCode != null && prevLangCompanyCode.Data != null && prevLangCompanyCode.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByCompanyCodeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByCompanyCodeId");
            }
        }

        private ProcessResult<List<Lang_CompanyCode>> GetByCompanyCodeIdAndsupprotedLangId(int CompanyCodeId, int supprotedLangId)
        {
            List<Lang_CompanyCode> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_CompanyCode_ID == CompanyCodeId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_CompanyCode>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByCompanyCodeIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CompanyCode>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByCompanyCodeIdAndsupprotedLangId");
            }
        }
    }
}
