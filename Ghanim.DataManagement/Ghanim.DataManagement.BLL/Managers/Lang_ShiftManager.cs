﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_ShiftManager : Repository<Lang_Shift>, ILang_ShiftManager
    {
        public Lang_ShiftManager(LookUpDbContext context)
            : base(context)
        {
        }
        public ProcessResult<List<Lang_Shift>> GetAllLanguagesByShiftId(int ShiftId)
        {
            List<Lang_Shift> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Shift_ID == ShiftId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Shift>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByShiftId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Shift>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByShiftId");
            }
        }

        public ProcessResult<bool> UpdateByShift(List<Lang_Shift> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    result = Update(item).Data;
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByShiftId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByShiftId");
            }
        }
    }
}
