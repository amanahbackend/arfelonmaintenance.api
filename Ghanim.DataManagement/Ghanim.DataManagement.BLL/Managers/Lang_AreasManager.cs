﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_AreasManager : Repository<Lang_Areas>, ILang_AreasManager
    {
        IServiceProvider _serviceprovider;

        public Lang_AreasManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IAreasManager areasManager
        {
            get
            {
                return _serviceprovider.GetService<IAreasManager>();
            }
        }

        public ProcessResult<List<Lang_Areas>> GetAllLanguagesByAreaId(int AreaId)
        {
            List<Lang_Areas> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x=>x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_Area_ID == AreaId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res= SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_Areas { FK_Area_ID = AreaId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });               
                }
                input=input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Areas>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByAreaId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Areas>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByAreaId");
            }
        }

        public  ProcessResult<bool> UpdateByArea(List<Lang_Areas> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes!=null && supportedLanguageRes.Data !=null && supportedLanguageRes.Data.Name == "English" )
                    {
                        var AreaRes = areasManager.Get(item.FK_Area_ID);
                        if (AreaRes != null && AreaRes.Data != null)
                        {
                            AreaRes.Data.Name = item.Name;
                             var updateRes = areasManager.Update(AreaRes.Data);
                        } 
                    }
                    if (item.Id==0&&item.Name!=string.Empty&& item.Name != null)
                    {
                        var prevLangAreas = GetByAreaIdAndsupprotedLangId(item.FK_Area_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangAreas!=null && prevLangAreas.Data != null && prevLangAreas.Data.Count==0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if(item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                         result = true;
                    }
                  
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByAreaId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByAreaId");
            }
        }

        private ProcessResult<List<Lang_Areas>> GetByAreaIdAndsupprotedLangId(int areaId,int supprotedLangId)
        {
            List<Lang_Areas> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Area_ID == areaId && x.FK_SupportedLanguages_ID== supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Areas>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByAreaIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Areas>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByAreaIdAndsupprotedLangId");
            }
        }

    }
}
