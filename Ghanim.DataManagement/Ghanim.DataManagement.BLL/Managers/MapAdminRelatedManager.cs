﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class MapAdminRelatedManager : Repository<MapAdminRelated>, IMapAdminRelatedManager
    {
        public MapAdminRelatedManager(LookUpDbContext context) : base(context)
        {
        }
    }
}
