﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class AreasManager : Repository<Areas>, IAreasManager
    {
        IServiceProvider _serviceprovider;
        public AreasManager(LookUpDbContext context)
            : base(context)
        {
        }

        //public AreasManager(IServiceProvider serviceprovider, LookUpDbContext context)
        //  : base(context)
        //{
        //    _serviceprovider = serviceprovider;
        //}

        //private ILang_AreasManager lang_AreasManager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ILang_AreasManager>();
        //    }
        //}

        //private ISupportedLanguagesManager supportedLanguagesmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ISupportedLanguagesManager>();
        //    }
        //}

        public ProcessResult<List<Areas>> GetAreasByGovId(int govId)
        {
            List<Areas> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Fk_Governorate_Id == govId).ToList();
                return ProcessResultHelper.Succeeded<List<Areas>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAreasByGovId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Areas>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }

        //public override ProcessResult<List<Areas>> Add(List<Areas> entityLst)
        //{
        //    ProcessResult<List<Areas>> result = new ProcessResult<List<Areas>>() { Data = null };
        //    List<Lang_Areas> Lang_Areas = new List<Lang_Areas>();
        //    try
        //    {
        //        var supportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.ToList();
        //        result = base.Add(entityLst);
        //        foreach (var item in result.Data)
        //        {
        //            foreach (var supportedLanguage in supportedLanguagesRes)
        //            {
        //                if (supportedLanguage.Name == "English")
        //                {
        //                    Lang_Areas.Add(new Lang_Areas { FK_Area_ID = item.Id, FK_SupportedLanguages_ID = supportedLanguage.Id, Name = item.Name });
        //                }
        //                else
        //                {
        //                    Lang_Areas.Add(new Lang_Areas { FK_Area_ID = item.Id, FK_SupportedLanguages_ID = supportedLanguage.Id, Name = null });
        //                }
        //            }
        //        }
                
        //        //var lang_AreasRes = lang_AreasManager.Add(Lang_Areas);
               
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultHelper.Failed<List<Areas>>(result.Data, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
        //    }
        //}

        public ProcessResult<Areas> GetAreasByArea_No(int AreaNo) {
            Areas input = null;
            try
            {
                input = GetAll().Data.Find(x => x.Area_No == AreaNo);
                return ProcessResultHelper.Succeeded<Areas>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAreasByGovId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<Areas>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }
    }
}
