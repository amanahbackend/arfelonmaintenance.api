﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_BuildingTypesManager : Repository<Lang_BuildingTypes>, ILang_BuildingTypesManager
    {
        IServiceProvider _serviceprovider;
        public Lang_BuildingTypesManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IBuildingTypesManager buildingTypesManager
        {
            get
            {
                return _serviceprovider.GetService<IBuildingTypesManager>();
            }
        }

        public ProcessResult<List<Lang_BuildingTypes>> GetAllLanguagesByBuildingTypesId(int BuildingTypesId)
        {
            List<Lang_BuildingTypes> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_BuildingTypes_ID == BuildingTypesId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_BuildingTypes { FK_BuildingTypes_ID = BuildingTypesId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_BuildingTypes>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByAvailabilityId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_BuildingTypes>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByAvailabilityId");
            }
        }

        public ProcessResult<bool> UpdateByBuildingTypes(List<Lang_BuildingTypes> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var buildingTypeRes = buildingTypesManager.Get(item.FK_BuildingTypes_ID);
                        if (buildingTypeRes != null && buildingTypeRes.Data != null)
                        {
                            buildingTypeRes.Data.Name = item.Name;
                            var updateRes = buildingTypesManager.Update(buildingTypeRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangBuildingTypes = GetByBuildingTypesIdAndsupprotedLangId(item.FK_BuildingTypes_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangBuildingTypes != null && prevLangBuildingTypes.Data != null && prevLangBuildingTypes.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByBuildingTypesId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByBuildingTypesId");
            }
        }

        private ProcessResult<List<Lang_BuildingTypes>> GetByBuildingTypesIdAndsupprotedLangId(int BuildingTypesId, int supprotedLangId)
        {
            List<Lang_BuildingTypes> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_BuildingTypes_ID == BuildingTypesId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_BuildingTypes>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByBuildingTypesIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_BuildingTypes>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByBuildingTypesIdAndsupprotedLangId");
            }
        }
    }
}
