﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_CostCenterManager : Repository<Lang_CostCenter>, ILang_CostCenterManager
    {
        public Lang_CostCenterManager(LookUpDbContext context)
            : base(context)
        {
        }
        public ProcessResult<List<Lang_CostCenter>> GetAllLanguagesByCostCenterId(int CostCenterId)
        {
            List<Lang_CostCenter> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_CostCenter_ID == CostCenterId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_CostCenter>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByCostCenterId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CostCenter>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByCostCenterId");
            }
        }

        public ProcessResult<bool> UpdateByCostCenter(List<Lang_CostCenter> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    result = Update(item).Data;
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByCostCenter");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByCostCenter");
            }
        }
    }
}
