﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_AvailabilityManager : Repository<Lang_Availability>, ILang_AvailabilityManager
    {
        IServiceProvider _serviceprovider;
        public Lang_AvailabilityManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IAvailabilityManager availabilityManager
        {
            get
            {
                return _serviceprovider.GetService<IAvailabilityManager>();
            }
        }
        public ProcessResult<List<Lang_Availability>> GetAllLanguagesByAvailabilityId(int AvailabilityId)
        {
            List<Lang_Availability> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_Availability_ID == AvailabilityId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_Availability { FK_Availability_ID = AvailabilityId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Availability>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByAvailabilityId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Availability>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByAvailabilityId");
            }
        }

        public ProcessResult<bool> UpdateByAvailability(List<Lang_Availability> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var availabilityRes = availabilityManager.Get(item.FK_Availability_ID);
                        if (availabilityRes != null && availabilityRes.Data != null)
                        {
                            availabilityRes.Data.Name = item.Name;
                            var updateRes = availabilityManager.Update(availabilityRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangAvailability = GetByavailabilityIdAndsupprotedLangId(item.FK_Availability_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangAvailability != null && prevLangAvailability.Data != null && prevLangAvailability.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByAvailabilityId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByAvailabilityId");
            }
        }

        private ProcessResult<List<Lang_Availability>> GetByavailabilityIdAndsupprotedLangId(int availabilityId, int supprotedLangId)
        {
            List<Lang_Availability> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Availability_ID == availabilityId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Availability>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByavailabilityIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Availability>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByavailabilityIdAndsupprotedLangId");
            }
        }
    }
}
