﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_AttendanceStatesManager : Repository<Lang_AttendanceStates>, ILang_AttendanceStatesManager
    {
        IServiceProvider _serviceprovider;
        public Lang_AttendanceStatesManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }

        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IAttendanceStatesManager AttendanceStatesManager
        {
            get
            {
                return _serviceprovider.GetService<IAttendanceStatesManager>();
            }
        }

        public ProcessResult<List<Lang_AttendanceStates>> GetAllLanguagesByAttendanceStatesId(int AttendanceStatesId)
        {
            List<Lang_AttendanceStates> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_AttendanceStates_ID == AttendanceStatesId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_AttendanceStates { FK_AttendanceStates_ID = AttendanceStatesId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_AttendanceStates>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByAttendanceStatesId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_AttendanceStates>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByAttendanceStatesId");
            }
        }

        public ProcessResult<bool> UpdateByAttendanceStates(List<Lang_AttendanceStates> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var AttendanceStateRes = AttendanceStatesManager.Get(item.FK_AttendanceStates_ID);
                        if (AttendanceStateRes != null && AttendanceStateRes.Data != null)
                        {
                            AttendanceStateRes.Data.Name = item.Name;
                            var updateRes = AttendanceStatesManager.Update(AttendanceStateRes.Data);
                        }
                    }
                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangAttendanceStates = GetByattendanceStatesIdAndsupprotedLangId(item.FK_AttendanceStates_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangAttendanceStates != null && prevLangAttendanceStates.Data != null && prevLangAttendanceStates.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByAttendanceStatesId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByAttendanceStatesId");
            }
        }

        private ProcessResult<List<Lang_AttendanceStates>> GetByattendanceStatesIdAndsupprotedLangId(int attendanceStatesId, int supprotedLangId)
        {
            List<Lang_AttendanceStates> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_AttendanceStates_ID == attendanceStatesId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_AttendanceStates>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByattendanceStatesIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_AttendanceStates>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByattendanceStatesIdAndsupprotedLangId");
            }
        }
    }
}
