﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public class Lang_DivisionManager : Repository<Lang_Division>, ILang_DivisionManager
    {
        IServiceProvider _serviceprovider;
        public Lang_DivisionManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IDivisionManager divisionManager
        {
            get
            {
                return _serviceprovider.GetService<IDivisionManager>();
            }
        }

        public ProcessResult<List<Lang_Division>> GetAllLanguagesByDivisionId(int DivisionId)
        {
            List<Lang_Division> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_Division_ID == DivisionId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_Division { FK_Division_ID = DivisionId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Division>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByDivisionId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Division>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByDivisionId");
            }
        }

        public ProcessResult<bool> UpdateByDivision(List<Lang_Division> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var divisionRes = divisionManager.Get(item.FK_Division_ID);
                        if (divisionRes != null && divisionRes.Data != null)
                        {
                            divisionRes.Data.Name = item.Name;
                            var updateRes = divisionManager.Update(divisionRes.Data);
                        }
                    }
                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangDivision = GetByDivisionIdAndsupprotedLangId(item.FK_Division_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangDivision != null && prevLangDivision.Data != null && prevLangDivision.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByDivisionId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByDivisionId");
            }
        }

        private ProcessResult<List<Lang_Division>> GetByDivisionIdAndsupprotedLangId(int DivisionId, int supprotedLangId)
        {
            List<Lang_Division> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Division_ID == DivisionId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Division>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByDivisionIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Division>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDivisionIdAndsupprotedLangId");
            }
        }
    }
}
