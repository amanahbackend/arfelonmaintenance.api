﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class SupportedLanguagesManager : Repository<SupportedLanguages>, ISupportedLanguagesManager
    {
        public SupportedLanguagesManager(LookUpDbContext context)
            : base(context)
        {
        }

       public  ProcessResult<SupportedLanguages> GetLanguageByName(string name)
        {
            SupportedLanguages input = null;
            try
            {
                input = GetAll().Data.Find(x => x.Name == name);
                return ProcessResultHelper.Succeeded<SupportedLanguages>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAreasByGovId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<SupportedLanguages>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }
      
    }
}
