﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public class Lang_GovernoratesManager : Repository<Lang_Governorates>, ILang_GovernoratesManager
    {
        IServiceProvider _serviceprovider;

        public Lang_GovernoratesManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IGovernoratesManager governoratesManager
        {
            get
            {
                return _serviceprovider.GetService<IGovernoratesManager>();
            }
        }

        public ProcessResult<List<Lang_Governorates>> GetAllLanguagesByGovernorateId(int GovernoratesId)
        {
            List<Lang_Governorates> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_Governorates_ID == GovernoratesId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_Governorates { FK_Governorates_ID = GovernoratesId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Governorates>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetAllLanguagesByGovernoratesId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Governorates>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByGovernoratesId");
            }
        }

        public ProcessResult<bool> UpdateByGovernorate(List<Lang_Governorates> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var governorateRes = governoratesManager.Get(item.FK_Governorates_ID);
                        if (governorateRes != null && governorateRes.Data != null)
                        {
                            governorateRes.Data.Name = item.Name;
                            var updateRes = governoratesManager.Update(governorateRes.Data);
                        }
                    }
                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangGovernorates = GetByGovernoratesIdAndsupprotedLangId(item.FK_Governorates_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangGovernorates != null && prevLangGovernorates.Data != null && prevLangGovernorates.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByGovernorate");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succeeded<bool>(result, (string)null, ProcessResultStatusCode.Succeeded, "UpdateByGovernorate");
            }
        }

        private ProcessResult<List<Lang_Governorates>> GetByGovernoratesIdAndsupprotedLangId(int GovernoratesId, int supprotedLangId)
        {
            List<Lang_Governorates> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Governorates_ID == GovernoratesId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succeeded<List<Lang_Governorates>>(input, (string)null, ProcessResultStatusCode.Succeeded, "GetByGovernoratesIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Governorates>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByGovernoratesIdAndsupprotedLangId");
            }
        }
    }
}
