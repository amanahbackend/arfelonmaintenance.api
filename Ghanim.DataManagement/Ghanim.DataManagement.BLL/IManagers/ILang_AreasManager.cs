﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_AreasManager : IRepository<Lang_Areas>
    {
        ProcessResult<List<Lang_Areas>> GetAllLanguagesByAreaId(int AreaId);
        ProcessResult<bool> UpdateByArea(List<Lang_Areas> entities);
    }
}
