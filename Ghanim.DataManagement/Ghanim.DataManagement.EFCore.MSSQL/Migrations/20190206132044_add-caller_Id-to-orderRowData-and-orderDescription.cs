﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class addcaller_IdtoorderRowDataandorderDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Caller_ID",
                table: "OrderRowData",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrderDescription",
                table: "OrderRowData",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Caller_ID",
                table: "OrderRowData");

            migrationBuilder.DropColumn(
                name: "OrderDescription",
                table: "OrderRowData");
        }
    }
}
