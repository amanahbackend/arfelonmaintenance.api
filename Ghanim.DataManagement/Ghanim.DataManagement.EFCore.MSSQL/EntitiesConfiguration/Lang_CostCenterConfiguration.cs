﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class Lang_CostCenterConfiguration : BaseEntityTypeConfiguration<Lang_CostCenter>, IEntityTypeConfiguration<Lang_CostCenter>
    {
        public override void Configure(EntityTypeBuilder<Lang_CostCenter> Lang_CostCenterConfiguration)
        {
            base.Configure(Lang_CostCenterConfiguration);

            Lang_CostCenterConfiguration.ToTable("Lang_CostCenter");
            Lang_CostCenterConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_CostCenterConfiguration.Property(o => o.Name).IsRequired();
            Lang_CostCenterConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
            Lang_CostCenterConfiguration.Property(o => o.FK_CostCenter_ID).IsRequired();
        }
    }
}
