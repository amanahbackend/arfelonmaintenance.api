﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class Lang_BuildingTypesConfiguration : BaseEntityTypeConfiguration<Lang_BuildingTypes>, IEntityTypeConfiguration<Lang_BuildingTypes>
    {
        public override void Configure(EntityTypeBuilder<Lang_BuildingTypes> Lang_BuildingTypesConfiguration)
        {
            base.Configure(Lang_BuildingTypesConfiguration);
            Lang_BuildingTypesConfiguration.ToTable("Lang_BuildingTypes");
            Lang_BuildingTypesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_BuildingTypesConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
