﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class Lang_ShiftConfiguration : BaseEntityTypeConfiguration<Lang_Shift>, IEntityTypeConfiguration<Lang_Shift>
    {
        public override void Configure(EntityTypeBuilder<Lang_Shift> Lang_ShiftConfiguration)
        {
            base.Configure(Lang_ShiftConfiguration);

            Lang_ShiftConfiguration.ToTable("Lang_Shift");
            Lang_ShiftConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_ShiftConfiguration.Property(o => o.FromTime).IsRequired();
            Lang_ShiftConfiguration.Property(o => o.ToTime).IsRequired();
            Lang_ShiftConfiguration.Property(o => o.FK_Shift_ID).IsRequired();
            Lang_ShiftConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
        }
    }
}
