﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class CostCenterConfiguration : BaseEntityTypeConfiguration<CostCenter>, IEntityTypeConfiguration<CostCenter>
    {
        public override void Configure(EntityTypeBuilder<CostCenter> CostCenterConfiguration)
        {
            base.Configure(CostCenterConfiguration);

            CostCenterConfiguration.ToTable("CostCenter");
            CostCenterConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CostCenterConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
