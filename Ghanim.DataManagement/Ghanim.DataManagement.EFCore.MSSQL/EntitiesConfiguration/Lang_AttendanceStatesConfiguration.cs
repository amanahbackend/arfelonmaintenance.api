﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_AttendanceStatesConfiguration : BaseEntityTypeConfiguration<Lang_AttendanceStates>, IEntityTypeConfiguration<Lang_AttendanceStates>
    {
        public override void Configure(EntityTypeBuilder<Lang_AttendanceStates> Lang_AttendanceStatesConfiguration)
        {
            base.Configure(Lang_AttendanceStatesConfiguration);

            Lang_AttendanceStatesConfiguration.ToTable("Lang_AttendanceStates");
            Lang_AttendanceStatesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_AttendanceStatesConfiguration.Property(o => o.Name).IsRequired();
            Lang_AttendanceStatesConfiguration.Property(o => o.FK_AttendanceStates_ID).IsRequired();
            Lang_AttendanceStatesConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
        }
    }
}
