﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class ShiftConfiguration : BaseEntityTypeConfiguration<Shift>, IEntityTypeConfiguration<Shift>
    {
        public override void Configure(EntityTypeBuilder<Shift> ShiftConfiguration)
        {
            base.Configure(ShiftConfiguration);

            ShiftConfiguration.ToTable("Shift");
            ShiftConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            ShiftConfiguration.Property(o => o.FromTime).IsRequired();
            ShiftConfiguration.Property(o => o.ToTime).IsRequired();
        }
    }
}
