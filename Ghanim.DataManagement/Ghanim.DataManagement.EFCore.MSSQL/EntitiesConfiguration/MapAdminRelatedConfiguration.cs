﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    public class MapAdminRelatedConfiguration : BaseEntityTypeConfiguration<MapAdminRelated>, IEntityTypeConfiguration<MapAdminRelated>
    {
        public override void Configure(EntityTypeBuilder<MapAdminRelated> MapAdminRelatedConfiguration)
        {
            base.Configure(MapAdminRelatedConfiguration);

            MapAdminRelatedConfiguration.ToTable("MapAdminRelated");
        }

    }
}
