﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.DataManagement.API.ViewModels;

namespace Ghanim.DataManagement.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<Areas, AreasViewModel>();
            CreateMap<AreasViewModel, Areas>()
                .IgnoreBaseEntityProperties(); ;

            CreateMap<AttendanceStates, AttendanceStatesViewModel>();
            CreateMap<AttendanceStatesViewModel, AttendanceStates>()
                .IgnoreBaseEntityProperties();

            CreateMap<Availability, AvailabilityViewModel>();
            CreateMap<AvailabilityViewModel, Availability>()
                .IgnoreBaseEntityProperties();

            CreateMap<BuildingTypes, BuildingTypesViewModel>();
            CreateMap<BuildingTypesViewModel, BuildingTypes>()
                     .IgnoreBaseEntityProperties();

            CreateMap<CompanyCode, CompanyCodeViewModel>();
            CreateMap<CompanyCodeViewModel, CompanyCode>()
                .IgnoreBaseEntityProperties();

            CreateMap<ContractTypes, ContractTypesViewModel>();
            CreateMap<ContractTypesViewModel, ContractTypes>()
                .IgnoreBaseEntityProperties();

            CreateMap<Division, DivisionViewModel>();
            CreateMap<DivisionViewModel, Division>()
                .IgnoreBaseEntityProperties();

            CreateMap<Governorates, GovernoratesViewModel>();
            CreateMap<GovernoratesViewModel, Governorates>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_Areas, Lang_AreasViewModel>();
            CreateMap<Lang_AreasViewModel, Lang_Areas>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_AttendanceStates, Lang_AttendanceStatesViewModel>();
            CreateMap<Lang_AttendanceStatesViewModel, Lang_AttendanceStates>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_Availability, Lang_AvailabilityViewModel>();
            CreateMap<Lang_AvailabilityViewModel, Lang_Availability>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_BuildingTypes, Lang_BuildingTypesViewModel>();
            CreateMap<Lang_BuildingTypesViewModel, Lang_BuildingTypes>()
            .IgnoreBaseEntityProperties();

            CreateMap<Lang_CompanyCode, Lang_CompanyCodeViewModel>();
            CreateMap<Lang_CompanyCodeViewModel, Lang_CompanyCode>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_ContractTypes, Lang_ContractTypesViewModel>();
            CreateMap<Lang_ContractTypesViewModel, Lang_ContractTypes>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_CostCenter, Lang_CostCenterViewModel>();
            CreateMap<Lang_CostCenterViewModel, Lang_CostCenter>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Division, Lang_DivisionViewModel>();
            CreateMap<Lang_DivisionViewModel, Lang_Division>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Governorates, Lang_GovernoratesViewModel>();
            CreateMap<Lang_GovernoratesViewModel, Lang_Governorates>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Shift, Lang_ShiftViewModel>();
            CreateMap<Lang_ShiftViewModel, Lang_Shift>()
           .IgnoreBaseEntityProperties();

            CreateMap<CostCenter, CostCenterViewModel>();
            CreateMap<CostCenterViewModel, CostCenter>()
           .IgnoreBaseEntityProperties();

            CreateMap<Shift, ShiftViewModel>();
            CreateMap<ShiftViewModel, Shift>()
           .IgnoreBaseEntityProperties();

            CreateMap<SupportedLanguages, SupportedLanguagesViewModel>();
            CreateMap<SupportedLanguagesViewModel, SupportedLanguages>()
           .IgnoreBaseEntityProperties();

            

            CreateMap(typeof(PaginatedItems<>), typeof(PaginatedItemsViewModel<>));
            CreateMap(typeof(PaginatedItemsViewModel<>), typeof(PaginatedItems<>));

            CreateMap<MapAdminRelated, MapAdminRelatedViewModel>();
            CreateMap<MapAdminRelatedViewModel, MapAdminRelated>()
           .IgnoreBaseEntityProperties();

        }
    }
}
