﻿//using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System.Threading.Tasks;


namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public class UserManagementServiceSettings : DefaultHttpClientSettings
    {
        public string ServiceName { get; set; }
        public string GetDispatchersByDivision { get; set; }
        public string GetDispatcherSettings { get; set; }
        public string GetDispatcherById { get; set; }
        public string GetSupervisorByDivisionId { get; set; }
        public string GetSupervisorsByDivisionId { get; set; }
        //public string AddOrders { get; set; }
        //public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        //{
        //    return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        //}
    }
}