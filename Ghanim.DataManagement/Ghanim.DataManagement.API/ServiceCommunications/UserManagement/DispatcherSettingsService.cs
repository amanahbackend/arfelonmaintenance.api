﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.DataManagement.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public class DispatcherSettingsService : DefaultHttpClientCrud<UserManagementServiceSettings, DispatcherSettingsViewModel, DispatcherSettingsViewModel>,
       IDispatcherSettingsService
    {
        UserManagementServiceSettings _settings;

        public DispatcherSettingsService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<List<DispatcherSettingsViewModel>>> GetDispatcherSettingsByOrder(SettingsViewModel model)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetDispatcherSettings;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<SettingsViewModel, List<DispatcherSettingsViewModel>>(url, model);
            }
            return null;
        }
    }
}
