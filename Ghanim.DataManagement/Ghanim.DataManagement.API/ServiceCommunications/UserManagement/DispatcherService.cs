﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.DataManagement.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public class DispatcherService : DefaultHttpClientCrud<UserManagementServiceSettings, DispatcherViewModel, DispatcherViewModel>,
       IDispatcherService
    {
        UserManagementServiceSettings _settings;

        public DispatcherService(IOptions<UserManagementServiceSettings> obj) : base(obj.Value)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatchersByDivision(int divisonId)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetDispatchersByDivision;
                var url = $"{baseUrl}/{requestedAction}/{divisonId}";
                return await GetByUriCustomized<List<DispatcherViewModel>>(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<DispatcherViewModel>> GetDispatcherById(int id)
        {
            string baseUrl = _settings.Uri;
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetDispatcherById;
                var url = $"{baseUrl}/{requestedAction}/{id}";
                return await GetByUriCustomized<DispatcherViewModel>(url);
            }
            return null;
        }

    }
}