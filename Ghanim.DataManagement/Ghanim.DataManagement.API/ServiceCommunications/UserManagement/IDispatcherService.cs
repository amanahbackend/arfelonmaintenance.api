﻿using DispatchProduct.HttpClient;
using Ghanim.DataManagement.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public interface IDispatcherService : IDefaultHttpClientCrud<UserManagementServiceSettings, DispatcherViewModel, DispatcherViewModel>
    {
        Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatchersByDivision(int divisonId);
        Task<ProcessResultViewModel<DispatcherViewModel>> GetDispatcherById(int id);
    }
}