﻿using DispatchProduct.HttpClient;
using Ghanim.DataManagement.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public interface IDispatcherSettingsService : IDefaultHttpClientCrud<UserManagementServiceSettings, DispatcherSettingsViewModel, DispatcherSettingsViewModel>
    {
        Task<ProcessResultViewModel<List<DispatcherSettingsViewModel>>> GetDispatcherSettingsByOrder(SettingsViewModel model);
    }
}
