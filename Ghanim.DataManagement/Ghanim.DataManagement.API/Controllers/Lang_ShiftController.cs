﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_ShiftController : BaseController<ILang_ShiftManager, Lang_Shift, Lang_ShiftViewModel>
    {
        ILang_ShiftManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_ShiftController(ILang_ShiftManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        [HttpGet]
        [Route("GetAllLanguagesByShiftId/{ShiftId}")]
        public ProcessResultViewModel<List<Lang_ShiftViewModel>> GetAllLanguagesByAreaId([FromRoute]int ShiftId)
        {
            var entityResult = manager.GetAllLanguagesByShiftId(ShiftId);
            var result = processResultMapper.Map<List<Lang_Shift>, List<Lang_ShiftViewModel>>(entityResult);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Shift> lstModel)
        {
            var entityResult = manager.UpdateByShift(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
