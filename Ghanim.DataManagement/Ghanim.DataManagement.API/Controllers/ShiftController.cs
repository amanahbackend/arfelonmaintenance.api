﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class ShiftController : BaseController<IShiftManager, Shift, ShiftViewModel>
    {
        IServiceProvider _serviceprovider;
        IShiftManager manager;
        IProcessResultMapper processResultMapper;
        public ShiftController(IServiceProvider serviceprovider, IShiftManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_ShiftManager Lang_Shiftmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_ShiftManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetShiftByLanguage")]
        public ProcessResultViewModel<ShiftViewModel> GetShiftByLanguage([FromQuery]int ShiftId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_ShiftRes = Lang_Shiftmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(ShiftId);
            entityResult.Data.ToTime = Lang_ShiftRes.Data.ToTime;
            entityResult.Data.FromTime = Lang_ShiftRes.Data.FromTime;
            var result = processResultMapper.Map<Shift, ShiftViewModel>(entityResult);
            return result;
        }


        [HttpGet]
        [Route("GetShiftWithAllLanguages/{ShiftId}")]
        public ProcessResultViewModel<List<ShiftViewModel>> GetShiftWithAllLanguages([FromRoute]int ShiftId)
        {
            ProcessResult<List<Shift>> entityResult = null;
            var Lang_ShiftRes = Lang_Shiftmanager.GetAll().Data.FindAll(x => x.FK_Shift_ID == ShiftId);
            foreach (var item in Lang_ShiftRes)
            {
                var ShiftRes = manager.Get(x => x.Id == item.FK_Shift_ID);
                ShiftRes.Data.ToTime = item.ToTime;
                ShiftRes.Data.FromTime = item.FromTime;
                entityResult.Data.Add(ShiftRes.Data);
            }
            var result = processResultMapper.Map<List<Shift>, List<ShiftViewModel>>(entityResult);
            return result;
        }
    }
}
