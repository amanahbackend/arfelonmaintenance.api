﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class GovernoratesController : BaseController<IGovernoratesManager,Governorates, GovernoratesViewModel>
    {
        IServiceProvider serviceprovider;
        IGovernoratesManager manager;
        IProcessResultMapper processResultMapper;
        public GovernoratesController(IServiceProvider _serviceprovider, IGovernoratesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            serviceprovider= _serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_GovernoratesManager Lang_Governoratesmanager
        {
            get
            {
                return serviceprovider.GetService<ILang_GovernoratesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetGovernorateByLanguage")]
        public ProcessResultViewModel<GovernoratesViewModel> GetGovernorateByLanguage([FromQuery]int GovernorateId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_GovernoratesRes = Lang_Governoratesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(GovernorateId);
            entityResult.Data.Name = Lang_GovernoratesRes.Data.Name;
            var result = processResultMapper.Map<Governorates, GovernoratesViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public ProcessResultViewModel<GovernoratesViewModel> GetByName([FromRoute]string name)
        {
            var entityResult = manager.Get(x => x.Name == name);
            var result = processResultMapper.Map<Governorates, GovernoratesViewModel>(entityResult);
            return result;
        }

        //[HttpGet]
        //[Route("GetGovernorateWithAllLanguages/{GovernorateId}")]
        //public ProcessResultViewModel<List<GovernoratesViewModel>> GetGovernorateWithAllLanguages([FromRoute]int GovernorateId)
        //{
        //    ProcessResult<List<Governorates>> entityResult = null;
        //    var Lang_GovernoratesRes = Lang_Governoratesmanager.GetAll().Data.FindAll(x => x.FK_Governorates_ID == GovernorateId);
        //    foreach (var item in Lang_GovernoratesRes)
        //    {
        //        var GovernoratesRes = manager.Get(x => x.Id == item.FK_Governorates_ID);
        //        GovernoratesRes.Data.Name = item.Name;
        //        entityResult.Data.Add(GovernoratesRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<Governorates>, List<GovernoratesViewModel>>(entityResult);
        //    return result;
        //}
    }
}
