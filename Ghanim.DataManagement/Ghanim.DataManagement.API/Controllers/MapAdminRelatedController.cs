﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]

    public class MapAdminRelatedController : BaseController<IMapAdminRelatedManager, MapAdminRelated, MapAdminRelatedViewModel>
    {
        private IMapAdminRelatedManager _manger;
        private IMapper _mapper;
        private IProcessResultMapper _processResultMapper;
        private IProcessResultPaginatedMapper _processResultPaginatedMapper;

        public MapAdminRelatedController(IMapAdminRelatedManager manger, IMapper mapper, IProcessResultMapper processResultMapper, IProcessResultPaginatedMapper processResultPaginatedMapper)
            : base(manger, mapper, processResultMapper, processResultPaginatedMapper)
        {
            _manger = manger;
            _mapper = mapper;
            _processResultMapper = processResultMapper;
            _processResultPaginatedMapper = processResultPaginatedMapper;
        }



    }
}