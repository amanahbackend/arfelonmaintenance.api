﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_AreasController : BaseController<ILang_AreasManager, Lang_Areas, Lang_AreasViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_AreasManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_AreasController(IServiceProvider serviceprovider, ILang_AreasManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IAreasManager Areamanager
        {
            get
            {
                return _serviceprovider.GetService<IAreasManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }


        [HttpGet]
        [Route("GetAllLanguagesByAreaId/{AreaId}")]
        public ProcessResultViewModel<List<AreaWithAllLanguagesViewModel>> GetAllLanguagesByAreaId([FromRoute]int AreaId)
        {
            List<AreaWithAllLanguagesViewModel> areaWithAllLanguages = new List<AreaWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AreaRes = Areamanager.Get(AreaId);
            var entityResult = manager.GetAllLanguagesByAreaId(AreaId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key= SupportedLanguagesRes.Data.Code, Value=item.Name,Id=item.Id });
            }
            
            areaWithAllLanguages.Add(new AreaWithAllLanguagesViewModel { Area_No = AreaRes.Data.Area_No, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succeeded<List<AreaWithAllLanguagesViewModel>>(areaWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<AreaWithAllLanguagesViewModel>> GetAllLanguages()
        {
            List<AreaWithAllLanguagesViewModel> areaWithAllLanguages = new List<AreaWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            var AreaRes = Areamanager.GetAll();
            foreach (var Area in AreaRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByAreaId(Area.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                   
                }
                
                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name=="English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = Area.Name });

                        }
                        else {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                       
                    }
                }
                areaWithAllLanguages.Add(new AreaWithAllLanguagesViewModel { Area_No = Area.Area_No, languagesDictionaries = languagesDictionary, Id=Area.Id });
            }
            
            var result = ProcessResultViewModelHelper.Succeeded<List<AreaWithAllLanguagesViewModel>>(areaWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Areas> lstModel)
        {
            var entityResult = manager.UpdateByArea(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]AreaWithAllLanguagesViewModel Model)
        {
            List<Lang_Areas> lstModel = new List<Lang_Areas>();
            var AreasRes = Areamanager.GetAreasByArea_No(Model.Area_No);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {   
                    var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                    lstModel.Add(new Lang_Areas { FK_Area_ID = AreasRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByArea(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
