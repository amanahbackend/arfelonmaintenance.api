﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_AttendanceStatesController : BaseController<ILang_AttendanceStatesManager, Lang_AttendanceStates, Lang_AttendanceStatesViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_AttendanceStatesManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_AttendanceStatesController(IServiceProvider serviceprovider, ILang_AttendanceStatesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IAttendanceStatesManager AttendanceStatesmanager
        {
            get
            {
                return _serviceprovider.GetService<IAttendanceStatesManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByAttendanceStatesId/{AttendanceStatesId}")]
        public ProcessResultViewModel<List<LanguagesDictionaryViewModel>> GetAllLanguagesByAreaId([FromRoute]int AttendanceStatesId)
        {
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AttendanceStatesRes = AttendanceStatesmanager.Get(AttendanceStatesId);
            var entityResult = manager.GetAllLanguagesByAttendanceStatesId(AttendanceStatesId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succeeded<List<LanguagesDictionaryViewModel>>(languagesDictionary);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<NullWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<NullWithAllLanguagesViewModel> nullWithAllLanguages = new List<NullWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AttendanceStatesRes = AttendanceStatesmanager.GetAll();
            foreach (var AttendanceStates in AttendanceStatesRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByAttendanceStatesId(AttendanceStates.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = AttendanceStates.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = AttendanceStates.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                nullWithAllLanguages.Add(new NullWithAllLanguagesViewModel { languagesDictionaries = languagesDictionary,Id= AttendanceStates.Id });

            }
            var result = ProcessResultViewModelHelper.Succeeded<List<NullWithAllLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_AttendanceStates> lstModel)
        {
            var entityResult = manager.UpdateByAttendanceStates(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]NullWithAllLanguagesViewModel Model)
        {
            List<Lang_AttendanceStates> lstModel = new List<Lang_AttendanceStates>();
            var AttendanceStatesRes = AttendanceStatesmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                lstModel.Add(new Lang_AttendanceStates { FK_AttendanceStates_ID = AttendanceStatesRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByAttendanceStates(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
