﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class AvailabilityController : BaseController<IAvailabilityManager, Availability, AvailabilityViewModel>
    {
        IServiceProvider _serviceprovider;
        IAvailabilityManager manager;
        IProcessResultMapper processResultMapper;
        public AvailabilityController(IServiceProvider serviceprovider, IAvailabilityManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_AvailabilityManager Lang_Availabilitymanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_AvailabilityManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAvailabilityByLanguage")]
        public ProcessResultViewModel<AvailabilityViewModel> GetAvailabilityByLanguage([FromQuery]int AvailabilityId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_AvailabilityRes = Lang_Availabilitymanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(AvailabilityId);
            entityResult.Data.Name = Lang_AvailabilityRes.Data.Name;
            var result = processResultMapper.Map<Availability, AvailabilityViewModel>(entityResult);
            return result;
        }


        //[HttpGet]
        //[Route("GetAvailabilityWithAllLanguages/{AvailabilityId}")]
        //public ProcessResultViewModel<List<AvailabilityViewModel>> GetAvailabilityWithAllLanguages([FromRoute]int AvailabilityId)
        //{
        //    ProcessResult<List<Availability>> entityResult = null;
        //    var Lang_AvailabilityRes = Lang_Availabilitymanager.GetAll().Data.FindAll(x => x.FK_Availability_ID == AvailabilityId);
        //    foreach (var item in Lang_AvailabilityRes)
        //    {
        //        var AvailabilityRes = manager.Get(x => x.Id == item.FK_Availability_ID);
        //        AvailabilityRes.Data.Name = item.Name;
        //        entityResult.Data.Add(AvailabilityRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<Availability>, List<AvailabilityViewModel>>(entityResult);
        //    return result;
        //}
    }
}
