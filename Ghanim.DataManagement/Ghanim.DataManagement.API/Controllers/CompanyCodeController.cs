﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class CompanyCodeController : BaseController<ICompanyCodeManager, CompanyCode, CompanyCodeViewModel>
    {
        IServiceProvider _serviceprovider;
        ICompanyCodeManager manager;
        IProcessResultMapper processResultMapper;
        public CompanyCodeController(IServiceProvider serviceprovider, ICompanyCodeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_CompanyCodeManager Lang_CompanyCodemanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_CompanyCodeManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetCompanyCodeByLanguage")]
        public ProcessResultViewModel<CompanyCodeViewModel> GetCompanyCodeByLanguage([FromQuery]int CompanyCodeId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_CompanyCodeRes = Lang_CompanyCodemanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(CompanyCodeId);
            entityResult.Data.Name = Lang_CompanyCodeRes.Data.Name;
            var result = processResultMapper.Map<CompanyCode, CompanyCodeViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByCode/{code}")]
        public ProcessResultViewModel<CompanyCodeViewModel> GetByCode([FromRoute]string code)
        {
            var entityResult = manager.Get(x => x.Code == code);
            var result = processResultMapper.Map<CompanyCode, CompanyCodeViewModel>(entityResult);
            return result;
        }

        //[HttpGet]
        //[Route("GetCompanyCodeWithAllLanguages/{CompanyCodeId}")]
        //public ProcessResultViewModel<List<CompanyCodeViewModel>> GetCompanyCodeWithAllLanguages([FromRoute]int CompanyCodeId)
        //{
        //    ProcessResult<List<CompanyCode>> entityResult = null;
        //    var Lang_CompanyCodeRes = Lang_CompanyCodemanager.GetAll().Data.FindAll(x => x.FK_CompanyCode_ID == CompanyCodeId);
        //    foreach (var item in Lang_CompanyCodeRes)
        //    {
        //        var CompanyCodeRes = manager.Get(x => x.Id == item.FK_CompanyCode_ID);
        //        CompanyCodeRes.Data.Name = item.Name;
        //        entityResult.Data.Add(CompanyCodeRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<CompanyCode>, List<CompanyCodeViewModel>>(entityResult);
        //    return result;
        //}
    }
}
