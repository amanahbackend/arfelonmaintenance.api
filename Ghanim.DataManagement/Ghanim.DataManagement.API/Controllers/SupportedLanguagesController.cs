﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class SupportedLanguagesController : BaseController<ISupportedLanguagesManager, SupportedLanguages, SupportedLanguagesViewModel>
    {
        IProcessResultMapper processResultMapper;
        public SupportedLanguagesController(ISupportedLanguagesManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            processResultMapper = _processResultMapper;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public ProcessResultViewModel<SupportedLanguagesViewModel> GetByName(string name)
        {
            try
            {
                var entityResult= manger.GetLanguageByName(name);
                var result = processResultMapper.Map<SupportedLanguages,SupportedLanguagesViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
