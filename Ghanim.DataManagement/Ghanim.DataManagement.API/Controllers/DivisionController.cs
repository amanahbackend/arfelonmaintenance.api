﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class DivisionController : BaseController<IDivisionManager, Division, DivisionViewModel>
    {
        IServiceProvider _serviceprovider;
        IDivisionManager manager;
        IProcessResultMapper processResultMapper;
        public DivisionController(IServiceProvider serviceprovider, IDivisionManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_DivisionManager Lang_Divisionmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_DivisionManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetDivisionByLanguage")]
        public ProcessResultViewModel<DivisionViewModel> GetDivisionByLanguage([FromQuery]int DivisionId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_DivisionRes = Lang_Divisionmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(DivisionId);
            entityResult.Data.Name = Lang_DivisionRes.Data.Name;
            var result = processResultMapper.Map<Division, DivisionViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public ProcessResultViewModel<DivisionViewModel> GetByName([FromRoute]string name)
        {
            var entityResult = manager.Get(x => x.Name == name);
            var result = processResultMapper.Map<Division, DivisionViewModel>(entityResult);
            return result;
        }

        //[HttpGet]
        //[Route("GetDivisionWithAllLanguages/{DivisionId}")]
        //public ProcessResultViewModel<List<DivisionViewModel>> GetDivisionWithAllLanguages([FromRoute]int DivisionId)
        //{
        //    ProcessResult<List<Division>> entityResult = null;
        //    var Lang_DivisionRes = Lang_Divisionmanager.GetAll().Data.FindAll(x => x.FK_Division_ID == DivisionId);
        //    foreach (var item in Lang_DivisionRes)
        //    {
        //        var DivisionRes = manager.Get(x => x.Id == item.FK_Division_ID);
        //        DivisionRes.Data.Name = item.Name;
        //        entityResult.Data.Add(DivisionRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<Division>, List<DivisionViewModel>>(entityResult);
        //    return result;
        //}
    }
}
