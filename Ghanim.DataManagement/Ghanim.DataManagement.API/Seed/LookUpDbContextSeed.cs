﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.DataManagement.BLL.IManagers;

namespace Ghanim.DataManagement.EFCore.MSSQL.Context
{
    public class LookUpDbContextSeed : ContextSeed
    {
        //IServiceProvider _serviceprovider;
        //public LookUpDbContextSeed(IServiceProvider serviceprovider)
        //{
        //    _serviceprovider = serviceprovider;
        //}

        //private IAreasManager areasManager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<IAreasManager>();
        //    }
        //}

        public async Task SeedAsync(LookUpDbContext context, IHostingEnvironment env,
            ILogger<LookUpDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            
            try
            {
                // settings = appSettings.Value;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                var divisions = GetDefaultDivision();
                var lstAreas = GetDefaultAreas();
                var lstAttendanceStates = GetDefaultAttendanceStates();
                var lstAvailability = GetDefaultAvailability();
                var lstCompanyCode = GetDefaultCompanyCode();
                var lstContractTypes = GetDefaultContractTypes();
                var lstGovernorates = GetDefaultGovernorates();
                var lstBuildingTypes = GetDefaultBuildingTypes();
                var lstSupportedLanguages = GetDefaultSupportedLanguages();
                
                var MapAdminRelated = GetDefaultMapAdminRelated();
                using (var transaction = context.Database.BeginTransaction())
                {
                    //var result=  areasManager.Add(lstAreas);

                    await SeedEntityAsync(context, lstAreas, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Areas ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Areas OFF");

                    await SeedEntityAsync(context, lstAttendanceStates, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT AttendanceStates ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT AttendanceStates OFF");

                    

                    await SeedEntityAsync(context, lstAvailability, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Availability ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Availability OFF");

                    await SeedEntityAsync(context, lstCompanyCode, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT CompanyCode ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT CompanyCode OFF");

                    await SeedEntityAsync(context, divisions, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Division ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Division OFF");

                    await SeedEntityAsync(context, lstContractTypes, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT ContractTypes ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT ContractTypes OFF");

                    await SeedEntityAsync(context, lstGovernorates, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Governorates ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Governorates OFF");

                    await SeedEntityAsync(context, lstBuildingTypes, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT BuildingTypes ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT BuildingTypes OFF");

                    await SeedEntityAsync(context, lstSupportedLanguages, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT SupportedLanguages ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT SupportedLanguages OFF");

                    

                    await SeedEntityAsync(context, MapAdminRelated, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT MapAdminRelated ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT MapAdminRelated OFF");

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for LookUpDbContext");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
        
        private List<Division> GetDefaultDivision()
        {
            List<Division> result = new List<Division>();
            result.Add(new Division() { Id = 1, code = "30", Name = "Residential Services" });
            result.Add(new Division() { Id = 2, code = "40", Name = "Non Res. Services" });
            return result;
        }
        private List<Areas> GetDefaultAreas()
        {
            List<Areas> result = new List<Areas>();
            result.Add(new Areas() { Id = 1, Area_No = 1, Name = "Abdali" });
            result.Add(new Areas() { Id = 2, Area_No = 2, Name = "Abdulla Al Salem" });
            result.Add(new Areas() { Id = 3, Area_No = 3, Name = "Abdullah Mubarak(Wes" });
            result.Add(new Areas() { Id = 4, Area_No = 4, Name = "Abu Fatera" });
            result.Add(new Areas() { Id = 5, Area_No = 5, Name = "Abu Halifa" });
            result.Add(new Areas() { Id = 6, Area_No = 6, Name = "Abu Hasaniya" });
            result.Add(new Areas() { Id = 7, Area_No = 7, Name = "Adailiya" });
            result.Add(new Areas() { Id = 8, Area_No = 8, Name = "Adan" });
            result.Add(new Areas() { Id = 9, Area_No = 9, Name = "Agricultural Fintass" });
            result.Add(new Areas() { Id = 10,Area_No = 10, Name = "Ahmadi" });
            result.Add(new Areas() { Id = 11,Area_No = 11, Name = "Airport" });
            result.Add(new Areas() { Id = 12,Area_No = 12, Name = "Al-Dhebaiyah" });
            result.Add(new Areas() { Id = 13,Area_No = 13, Name = "Al-Hejan" });
            result.Add(new Areas() { Id = 14,Area_No = 14, Name = "Al-Nahda" });
            result.Add(new Areas() { Id = 15,Area_No = 15, Name = "Al-Subiya" });
            result.Add(new Areas() { Id = 16,Area_No = 16, Name = "Amghra" });
            result.Add(new Areas() { Id = 17,Area_No = 17, Name = "Andalous" });
            result.Add(new Areas() { Id = 18,Area_No = 18, Name = "Aqaela" });
            result.Add(new Areas() { Id = 19,Area_No = 19, Name = "Ardiya" });
            result.Add(new Areas() { Id = 20,Area_No = 20, Name = "Ardiya 5" });
            result.Add(new Areas() { Id = 21,Area_No = 21, Name = "Ardiya 6" });
            result.Add(new Areas() { Id = 22,Area_No = 22, Name = "Ardiya Herafi" });
            result.Add(new Areas() { Id = 23,Area_No = 23, Name = "Ardiya Store" });
            result.Add(new Areas() { Id = 24,Area_No = 24, Name = "Oyoun" });
            result.Add(new Areas() { Id = 25,Area_No = 25, Name = "Bayan" });
            result.Add(new Areas() { Id = 26,Area_No = 26, Name = "Bnaid Al-Qar" });
            result.Add(new Areas() { Id = 27,Area_No = 27, Name = "Coastal Strip" });
            result.Add(new Areas() { Id = 28,Area_No = 28, Name = "Daiya" });
            result.Add(new Areas() { Id = 29,Area_No = 29, Name = "Dajeej" });
            result.Add(new Areas() { Id = 30,Area_No = 30, Name = "Dasma" });
            result.Add(new Areas() { Id = 31,Area_No = 31, Name = "Dasman" });
            result.Add(new Areas() { Id = 32,Area_No = 32, Name = "Dhahar" });
            result.Add(new Areas() { Id = 33,Area_No = 33, Name = "Doha" });
            result.Add(new Areas() { Id = 34,Area_No = 34, Name = "Doha Port" });
            result.Add(new Areas() { Id = 35,Area_No = 35, Name = "East Ahmadi" });
            result.Add(new Areas() { Id = 36,Area_No = 36, Name = "Fahaheel" });
            result.Add(new Areas() { Id = 37,Area_No = 37, Name = "Fahd Al Ahmad" });
            result.Add(new Areas() { Id = 38,Area_No = 38, Name = "Faiha" });
            result.Add(new Areas() { Id = 39,Area_No = 39, Name = "Failaka" });
            result.Add(new Areas() { Id = 40,Area_No = 40, Name = "Farwaniya" });
            result.Add(new Areas() { Id = 41,Area_No = 41, Name = "Fintas" });
            result.Add(new Areas() { Id = 42,Area_No = 42, Name = "Firdous" });
            result.Add(new Areas() { Id = 43,Area_No = 43, Name = "Free Trade Zone" });
            result.Add(new Areas() { Id = 44,Area_No = 44, Name = "Funaitees" });
            result.Add(new Areas() { Id = 45,Area_No = 45, Name = "Gharnata" });
            result.Add(new Areas() { Id = 46,Area_No = 46, Name = "Hadiya" });
            result.Add(new Areas() { Id = 47,Area_No = 47, Name = "Hawalli" });
            result.Add(new Areas() { Id = 48,Area_No = 48, Name = "Hitteen" });
            result.Add(new Areas() { Id = 49,Area_No = 49, Name = "Ishbliya" });
            result.Add(new Areas() { Id = 50,Area_No = 50, Name = "Jaber Al Ahmad" });
            result.Add(new Areas() { Id = 51,Area_No = 51, Name = "Jaber Al-Ali" });
            result.Add(new Areas() { Id = 52,Area_No = 52, Name = "Jabriya" });
            result.Add(new Areas() { Id = 53,Area_No = 53, Name = "Jahra" });
            result.Add(new Areas() { Id = 54,Area_No = 54, Name = "Jahra Camp" });
            result.Add(new Areas() { Id = 55,Area_No = 55, Name = "Jahra Industrial" });
            result.Add(new Areas() { Id = 56,Area_No = 56, Name = "Jaleeb Shuwyukh" });
            result.Add(new Areas() { Id = 57,Area_No = 57, Name = "Julai'a" });
            result.Add(new Areas() { Id = 58,Area_No = 58, Name = "Kabed" });
            result.Add(new Areas() { Id = 59,Area_No = 59, Name = "Khaitan" });
            result.Add(new Areas() { Id = 60,Area_No = 60, Name = "Khaldiya" });
            result.Add(new Areas() { Id = 61,Area_No = 61, Name = "Kheiran" });
            result.Add(new Areas() { Id = 62,Area_No = 62, Name = "Kheiran Pearl" });
            result.Add(new Areas() { Id = 63,Area_No = 63, Name = "Kifan" });
            result.Add(new Areas() { Id = 64,Area_No = 64, Name = "Kuwait City" });
            result.Add(new Areas() { Id = 65,Area_No = 65, Name = "Magwa" });
            result.Add(new Areas() { Id = 66,Area_No = 66, Name = "Mahboola" });
            result.Add(new Areas() { Id = 67,Area_No = 67, Name = "Mangaf" });
            result.Add(new Areas() { Id = 68,Area_No = 68, Name = "Mansouriya" });
            result.Add(new Areas() { Id = 69,Area_No = 69, Name = "Maqbara" });
            result.Add(new Areas() { Id = 70,Area_No = 70, Name = "Maseela" });
            result.Add(new Areas() { Id = 71,Area_No = 71, Name = "Metla'a" });
            result.Add(new Areas() { Id = 72,Area_No = 72, Name = "Mina Abdullah" });
            result.Add(new Areas() { Id = 73,Area_No = 73, Name = "Mirqab" });
            result.Add(new Areas() { Id = 74,Area_No = 74, Name = "Mishrif" });
            result.Add(new Areas() { Id = 75,Area_No = 75, Name = "Mubarak Al Abdulla A" });
            result.Add(new Areas() { Id = 76,Area_No = 76, Name = "Mubarak Al-Kabeer" });
            result.Add(new Areas() { Id = 77,Area_No = 77, Name = "Mubarakiya Camps" });
            result.Add(new Areas() { Id = 78,Area_No = 78, Name = "Naeem" });
            result.Add(new Areas() { Id = 79,Area_No = 79, Name = "Naseem" });
            result.Add(new Areas() { Id = 80,Area_No = 80, Name = "North Ahmadi" });
            result.Add(new Areas() { Id = 81,Area_No = 81, Name = "North West Sulaibikh" });
            result.Add(new Areas() { Id = 82,Area_No = 82, Name = "Nuwaseeb" });
            result.Add(new Areas() { Id = 83,Area_No = 83, Name = "Nuzha" });
            result.Add(new Areas() { Id = 84,Area_No = 84, Name = "Omariya" });
            result.Add(new Areas() { Id = 85,Area_No = 85, Name = "Oum Al Namel Island" });
            result.Add(new Areas() { Id = 86,Area_No = 86, Name = "Oum Al Himan" });
            result.Add(new Areas() { Id = 87,Area_No = 87, Name = "Qadsiya" });
            result.Add(new Areas() { Id = 88,Area_No = 88, Name = "Qairawan" });
            result.Add(new Areas() { Id = 89,Area_No = 89, Name = "Qasar" });
            result.Add(new Areas() { Id = 90,Area_No = 90, Name = "Qibla" });
            result.Add(new Areas() { Id = 91,Area_No = 91, Name = "Qortuba" });
            result.Add(new Areas() { Id = 92,Area_No = 92, Name = "Qurain" });
            result.Add(new Areas() { Id = 93,Area_No = 93, Name = "Qusoor" });
            result.Add(new Areas() { Id = 94,Area_No = 94, Name = "Rabiya" });
            result.Add(new Areas() { Id = 95,Area_No = 95, Name = "Rai" });
            result.Add(new Areas() { Id = 96,Area_No = 96, Name = "Rawda" });
            result.Add(new Areas() { Id = 97,Area_No = 97, Name = "Rehab" });
            result.Add(new Areas() { Id = 98,Area_No = 98, Name = "Rigai" });
            result.Add(new Areas() { Id = 99,Area_No = 99, Name = "Riqaa" });
            result.Add(new Areas() { Id = 100, Area_No = 100, Name = "Rumaithiya" });
            result.Add(new Areas() { Id = 101, Area_No = 101, Name = "Saad Al Abdallah" });
            result.Add(new Areas() { Id = 102, Area_No = 102, Name = "Sabah Al Ahmad" });
            result.Add(new Areas() { Id = 103, Area_No = 103, Name = "Sabah Al-Naser" });
            result.Add(new Areas() { Id = 104, Area_No = 104, Name = "Sabah Al-Salem" });
            result.Add(new Areas() { Id = 105, Area_No = 105, Name = "Sabahiya" });
            result.Add(new Areas() { Id = 106, Area_No = 106, Name = "Sakrab" });
            result.Add(new Areas() { Id = 107, Area_No = 107, Name = "Salam" });
            result.Add(new Areas() { Id = 108, Area_No = 108, Name = "Salmyia" });
            result.Add(new Areas() { Id = 109, Area_No = 109, Name = "Salwa" });
            result.Add(new Areas() { Id = 110, Area_No = 110, Name = "Shaab" });
            result.Add(new Areas() { Id = 111, Area_No = 111, Name = "Shamiya" });
            result.Add(new Areas() { Id = 112, Area_No = 112, Name = "Sharq" });
            result.Add(new Areas() { Id = 113, Area_No = 113, Name = "Shuhadaa" });
            result.Add(new Areas() { Id = 114, Area_No = 114, Name = "Shuwaikh (Commercial)" });
            result.Add(new Areas() { Id = 115, Area_No = 115, Name = "Shuwaikh (Residential)" });
            result.Add(new Areas() { Id = 116, Area_No = 116, Name = "Shuwaikh Educational" });
            result.Add(new Areas() { Id = 117, Area_No = 117, Name = "Shuwaikh Health" });
            result.Add(new Areas() { Id = 118, Area_No = 118, Name = "Shuwaikh Indust. 1" });
            result.Add(new Areas() { Id = 119, Area_No = 119, Name = "Shuwaikh Indust. 2" });
            result.Add(new Areas() { Id = 120, Area_No = 120, Name = "Shuwaikh Indust. 3" });
            result.Add(new Areas() { Id = 121, Area_No = 121, Name = "Shuwaikh Port" });
            result.Add(new Areas() { Id = 122, Area_No = 122, Name = "Shuaiba" });
            result.Add(new Areas() { Id = 123, Area_No = 123, Name = "Siddiq" });
            result.Add(new Areas() { Id = 124, Area_No = 124, Name = "South Ahmadi" });
            result.Add(new Areas() { Id = 125, Area_No = 125, Name = "South Hadiya" });
            result.Add(new Areas() { Id = 126, Area_No = 126, Name = "South Jahra" });
            result.Add(new Areas() { Id = 127, Area_No = 127, Name = "South Riqaa" });
            result.Add(new Areas() { Id = 128, Area_No = 128, Name = "South Sabahiya" });
            result.Add(new Areas() { Id = 129, Area_No = 129, Name = "South Shuaiba" });
            result.Add(new Areas() { Id = 130, Area_No = 130, Name = "South Wostaa" });
            result.Add(new Areas() { Id = 131, Area_No = 131, Name = "Subhan" });
            result.Add(new Areas() { Id = 132, Area_No = 132, Name = "Sulaibikhaat" });
            result.Add(new Areas() { Id = 133, Area_No = 133, Name = "Sulaibiya" });
            result.Add(new Areas() { Id = 134, Area_No = 134, Name = "Sulaibiya Agricultur" });
            result.Add(new Areas() { Id = 135, Area_No = 135, Name = "Sulaibiya Indust. 1" });
            result.Add(new Areas() { Id = 136, Area_No = 136, Name = "Sulaibiya Indust. 2" });
            result.Add(new Areas() { Id = 137, Area_No = 137, Name = "Surra" });
            result.Add(new Areas() { Id = 138, Area_No = 138, Name = "Taima" });
            result.Add(new Areas() { Id = 139, Area_No = 139, Name = "Wafra" });
            result.Add(new Areas() { Id = 140, Area_No = 140, Name = "Waha" });
            result.Add(new Areas() { Id = 141, Area_No = 141, Name = "West Ahmadi" });
            result.Add(new Areas() { Id = 142, Area_No = 142, Name = "West Jahra" });
            result.Add(new Areas() { Id = 143, Area_No = 143, Name = "Wostaa" });
            result.Add(new Areas() { Id = 144, Area_No = 144, Name = "Yarmouk" });
            result.Add(new Areas() { Id = 145, Area_No = 145, Name = "Zahra" });
            result.Add(new Areas() { Id = 146, Area_No = 146, Name = "Hawally Square" });
            result.Add(new Areas() { Id = 147, Area_No = 147, Name = "Al Zour" });
            result.Add(new Areas() { Id = 148, Area_No = 148, Name = "Benaider" });
            result.Add(new Areas() { Id = 149, Area_No = 149, Name = "Shaab Albahry" });
            result.Add(new Areas() { Id = 150, Area_No = 150, Name = "Abbasiya" });
            result.Add(new Areas() { Id = 151, Area_No = 151, Name = "Bedaa" });
            result.Add(new Areas() { Id = 152, Area_No = 152, Name = "Masayel" });
            result.Add(new Areas() { Id = 153, Area_No = 153, Name = "West Abdullah Al Mub" });
            result.Add(new Areas() { Id = 154, Area_No = 154, Name = "S.Abdullah Al Mubark" });
            result.Add(new Areas() { Id = 155, Area_No = 155, Name = "Al Mutlaa" });
            return result;            
        }
        private List<AttendanceStates> GetDefaultAttendanceStates()
        {
            List<AttendanceStates> result = new List<AttendanceStates>();

            result.Add(new AttendanceStates() { Id = 1, Name = "Attend" });
            result.Add(new AttendanceStates() { Id = 2, Name = "Absent" });

            return result;
        }
        private List<Availability> GetDefaultAvailability()
        {
            List<Availability> result = new List<Availability>();
            result.Add(new Availability() { Id = 1, Name = "Available" });
            result.Add(new Availability() { Id = 2, Name = "Unavailable" });

            return result;
        }
        private List<CompanyCode> GetDefaultCompanyCode()
        {
            List<CompanyCode> result = new List<CompanyCode>
            {
                new CompanyCode() {Id=1, Code="EN01", Name ="Yusuf A. Alghanim & Sons w.l.l."},
                new CompanyCode() {Id=2, Code="EN02", Name ="Alamana Industries Co."},
                new CompanyCode() {Id=3, Code="EN03", Name ="Alghanim Engineering for General Trading"}
            };

            return result;
        }
        private List<ContractTypes> GetDefaultContractTypes()
        {
            List<ContractTypes> result = new List<ContractTypes>
            {
                new ContractTypes() {Id=1, Code="TYP9", Name="Pearl Contract"},
                new ContractTypes() {Id=2, Code="TYP8", Name="Bronze Contract"},
                new ContractTypes() {Id=3, Code="TYP7", Name="Platinum Contract"},
                new ContractTypes() {Id=4, Code="TYP6", Name="Silver Contract"},
                new ContractTypes() {Id=5, Code="TYP5", Name="Basic Contract"},
                new ContractTypes() {Id=6, Code="TYP4", Name="Diamond Contract"},
                new ContractTypes() {Id=7, Code="TYP3", Name="Golden Contract"},
                new ContractTypes() {Id=8, Code="TYP2", Name="Extend Warranty Contract"},
                new ContractTypes() {Id=9, Code="TYP1", Name="Warranty Contract"},
                new ContractTypes() {Id=10, Code="TY11",Name="Elevator Contracts"},
                new ContractTypes() {Id=11, Code="TY11",Name="Special Contracts"},
            };
            return result;
        }
        private List<Governorates> GetDefaultGovernorates()
        {
            List<Governorates> result = new List<Governorates>
            {
                new Governorates() {Id=1, Gov_No=1, Name ="Al Farwaniyah"},
                new Governorates() {Id=2, Gov_No=2, Name="Al Jahra"},
                new Governorates() {Id=3, Gov_No=3, Name="Al Ahmadi"},
                new Governorates() {Id=4, Gov_No=4, Name="Mubarak Al-Kabeer"},
                new Governorates() {Id=5, Gov_No=5, Name="Al Asimah"},
                new Governorates() {Id=6, Gov_No=6, Name="Hawalli"},
            };

            return result;
        }
        private List<BuildingTypes> GetDefaultBuildingTypes()
        {
            List<BuildingTypes> result = new List<BuildingTypes>
            {
                new BuildingTypes() {Id=1 ,Code="1",  Name="Res. Apartment build"},
                new BuildingTypes() {Id=10,Code="10", Name="Private School"},
                new BuildingTypes() {Id=11,Code="11", Name="Private Hospital"},
                new BuildingTypes() {Id=12,Code="12", Name="Private University"},
                new BuildingTypes() {Id=13,Code="13", Name="Private Worship Build"},
                new BuildingTypes() {Id=14,Code="14", Name="Government School"},
                new BuildingTypes() {Id=15,Code="15", Name="Government University"},
                new BuildingTypes() {Id=16,Code="16", Name="Government Worship Build"},
                new BuildingTypes() {Id=17,Code="17", Name="Government Hospital"},
                new BuildingTypes() {Id=18,Code="18", Name="Supermarket"},
                new BuildingTypes() {Id=19,Code="19", Name="Restaurant"},
                new BuildingTypes() {Id=2, Code="2" , Name="Apartment"},
                new BuildingTypes() {Id=20,Code="20", Name="Health Club"},
                new BuildingTypes() {Id=21,Code="21", Name="Store"},
                new BuildingTypes() {Id=22,Code="22", Name="Corporate Office"},
                new BuildingTypes() {Id=23,Code="23", Name="Workshop"},
                new BuildingTypes() {Id=24,Code="24", Name="Showroom"},
                new BuildingTypes() {Id=25,Code="25", Name="Factory"},
                new BuildingTypes() {Id=26,Code="26", Name="Co. Ops."},
                new BuildingTypes() {Id=27,Code="27", Name="Clinic"},
                new BuildingTypes() {Id=28,Code="28", Name="Shop"},
                new BuildingTypes() {Id=29,Code="29", Name="Office"},
                new BuildingTypes() {Id=3 , Code="3" ,Name="Com. Apartment Build"},
                new BuildingTypes() {Id=30,Code="30", Name="Commercial Tower"},
                new BuildingTypes() {Id=31,Code="31", Name="Complex"},
                new BuildingTypes() {Id=32,Code="32", Name="Govt. Admin Building"},
                new BuildingTypes() {Id=33,Code="33", Name="Banks"},
                new BuildingTypes() {Id=34,Code="34", Name="Private Mosques"},
                new BuildingTypes() {Id=35,Code="35", Name="Government Mosques"},
                new BuildingTypes() {Id=36,Code="36", Name="Private Church"},
                new BuildingTypes() {Id=37,Code="37", Name="Government Church"},
                new BuildingTypes() {Id=38,Code="38", Name="Wedding Halls"},
                new BuildingTypes() {Id=39,Code="39", Name="Beauty Saloons"},
                new BuildingTypes() {Id=4,Code="4", Name="Villa"},
                new BuildingTypes() {Id=5,Code="5", Name="Mall"},
                new BuildingTypes() {Id=6,Code="6", Name="Chalet"},
                new BuildingTypes() {Id=7,Code="7", Name="Hotel"},
               new BuildingTypes()  {Id=8,Code="8", Name="Medical center"},
               new BuildingTypes()  {Id=9,Code="9", Name="Farm"},
            };
            return result;
        }
        private List<SupportedLanguages> GetDefaultSupportedLanguages()
        {
            List<SupportedLanguages> result = new List<SupportedLanguages>
            {
                new SupportedLanguages() {Id=1, Name="العربية",  Code="Ar"},
                new SupportedLanguages() {Id=2, Name="English", Code="En"},
                new SupportedLanguages() {Id=3, Name="Tamil",   Code="Ta"},
                new SupportedLanguages() {Id=4, Name="Marati",  Code="Mar"},
                new SupportedLanguages() {Id=5, Name="Malyalam",Code="Mal"},
                new SupportedLanguages() {Id=6, Name="Hindi",   Code="Hi"},
                new SupportedLanguages() {Id=7, Name="Urdu",    Code="Ur"},
            };
            return result;
        }
        private List<MapAdminRelated> GetDefaultMapAdminRelated()
        {
            List<MapAdminRelated> result = new List<MapAdminRelated>
            {
                new MapAdminRelated (){Id=1,Mobile_Api_Key= "AIzaSyD-R37eg6NC5d3IrIX3JqhXAVLocddFXq4",Web_Api_Key= "AIzaSyD-R37eg6NC5d3IrIX3JqhXAVLocddFXq4",Refresh_Rate=2}

            };
            return result;
        }
    }
}
