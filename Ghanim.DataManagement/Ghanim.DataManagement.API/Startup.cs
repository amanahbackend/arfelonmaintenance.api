﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using AutoMapper;
//using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.BLL.Managers;
using Ghanim.DataManagement.Models.Entities;
using Ghanim.DataManagement.Models.IEntities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Autofac;
using Swashbuckle.AspNetCore.Swagger;
using Ghanim.DataManagement.EFCore.MSSQL;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.API.ServiceCommunications.UserManagement;

namespace Ghanim.DataManagement.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            //   var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
            builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void ConfigureServices(IServiceCollection services)
        {

            //services.Configure<FileUploadSettings>(Configuration.GetSection("OrderFileUpload"));

            #region intializations
            

            services.Configure<UserManagementServiceSettings>(Configuration.GetSection("ServiceCommunications:UserManagementService"));

            StaticAppSettings.ExcludedStatusIds = Configuration.GetSection("ExcludeOrderActions:ExcludedIds").Get<List<int>>();
            StaticAppSettings.ExcludedStatusNames = Configuration.GetSection("ExcludeOrderActions:ExcludedNames").Get<List<string>>();
            #endregion

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);



            //services.AddDbContext<LookUpDbContext>(options =>
            //options.UseSqlServer(Configuration["ConnectionString"],
            //sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.DataManagement.EFCore.MSSQL")));
            //services.AddOptions();


            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1",
            //        new Info()
            //        {
            //            Title = "Calling API",
            //            Description = "Calling  API"
            //        });
            //    c.AddSecurityDefinition("oauth2", new OAuth2Scheme
            //    {
            //        Type = "oauth2",
            //        Flow = "implicit",
            //        AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
            //        TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
            //        Scopes = new Dictionary<string, string>()
            //        {
            //            { "calling", "calling API" }
            //        }
            //    });
            //});

            services.AddAutoMapper(typeof(Startup));

            //services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.AddScoped<DbContext, LookUpDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IAreas), typeof(Areas));
            services.AddTransient(typeof(IAttendanceStates), typeof(AttendanceStates));
            services.AddTransient(typeof(IAvailability), typeof(Availability));
            services.AddScoped(typeof(IBuildingTypes), typeof(BuildingTypes));
            services.AddScoped(typeof(ICompanyCode), typeof(CompanyCode));
            services.AddScoped(typeof(Expression<>), typeof(Expression<>));
            services.AddScoped(typeof(Func<>), typeof(Func<>));
            services.AddScoped(typeof(IContractTypes), typeof(ContractTypes));
            services.AddTransient(typeof(IDivision), typeof(Division));
            services.AddScoped(typeof(IGovernorates), typeof(Governorates));
            services.AddScoped(typeof(ILang_Areas), typeof(Lang_Areas));
            services.AddScoped(typeof(ILang_AttendanceStates), typeof(Lang_AttendanceStates));
            services.AddScoped(typeof(ILang_Availability), typeof(Lang_Availability));
            services.AddScoped(typeof(ILang_BuildingTypes), typeof(Lang_BuildingTypes));
            services.AddScoped(typeof(ILang_CompanyCode), typeof(Lang_CompanyCode));
            services.AddScoped(typeof(ILang_ContractTypes), typeof(Lang_ContractTypes));
            services.AddScoped(typeof(ILang_Division), typeof(Lang_Division));
            services.AddScoped(typeof(ILang_Governorates), typeof(Lang_Governorates));
            services.AddScoped(typeof(ILang_Shift), typeof(Lang_Shift));
            services.AddScoped(typeof(IShift), typeof(Shift));
            services.AddScoped(typeof(ISupportedLanguages), typeof(SupportedLanguages));
            services.AddScoped(typeof(ILang_CostCenter), typeof(Lang_CostCenter));
            
           
            services.AddScoped(typeof(IMapAdminRelated), typeof(MapAdminRelated));


            services.AddScoped(typeof(IAreasManager), typeof(AreasManager));
            services.AddScoped(typeof(IAttendanceStatesManager), typeof(AttendanceStatesManager));
            services.AddScoped(typeof(IAvailabilityManager), typeof(AvailabilityManager));
            services.AddScoped(typeof(IBuildingTypesManager), typeof(BuildingTypesManager));
            services.AddScoped(typeof(ICompanyCodeManager), typeof(CompanyCodeManager));
            services.AddScoped(typeof(IContractTypesManager), typeof(ContractTypesManager));
            services.AddScoped(typeof(IDivisionManager), typeof(DivisionManager));
            services.AddScoped(typeof(IGovernoratesManager), typeof(GovernoratesManager));
            services.AddScoped(typeof(ILang_AreasManager), typeof(Lang_AreasManager));
            services.AddScoped(typeof(ILang_AttendanceStatesManager), typeof(Lang_AttendanceStatesManager));
            services.AddScoped(typeof(ILang_AvailabilityManager), typeof(Lang_AvailabilityManager));
            services.AddScoped(typeof(ILang_BuildingTypesManager), typeof(Lang_BuildingTypesManager));
            services.AddScoped(typeof(ILang_CompanyCodeManager), typeof(Lang_CompanyCodeManager));
            services.AddScoped(typeof(ILang_ContractTypesManager), typeof(Lang_ContractTypesManager));
            services.AddScoped(typeof(ILang_DivisionManager), typeof(Lang_DivisionManager));
            services.AddScoped(typeof(ILang_GovernoratesManager), typeof(Lang_GovernoratesManager));
            services.AddScoped(typeof(ILang_ShiftManager), typeof(Lang_ShiftManager));
            services.AddScoped(typeof(IShiftManager), typeof(ShiftManager));
            services.AddScoped(typeof(ISupportedLanguagesManager), typeof(SupportedLanguagesManager));
            services.AddScoped(typeof(ICostCenterManager), typeof(CostCenterManager));
            services.AddScoped(typeof(ILang_CostCenterManager), typeof(Lang_CostCenterManager));
           
            services.AddScoped(typeof(IMapAdminRelatedManager), typeof(MapAdminRelatedManager));


            services.AddScoped<IDispatcherService, DispatcherService>();
            services.AddScoped<IDispatcherSettingsService, DispatcherSettingsService>();
            services.AddScoped<ISupervisorService, SupervisorService>();
            


            //services.AddHangfire(x => x.UseSqlServerStorage(Configuration["ConnectionString"]));


            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));


            services.AddDbContext<LookUpDbContext>(options =>
                options.UseSqlServer(Configuration["ConnectionString"],
                sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.DataManagement.EFCore.MSSQL")));
            services.AddOptions();

            //////////////////// commented the part of ServiceRegisteryPort  ////////////////////////////
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            // services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            ///////////////////////////////////////////////////////////////////////////////////////

            //Mapper.AssertConfigurationIsValid();
            //var container = new ContainerBuilder();
            //container.Populate(services);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "DataManagement API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                //// Define the BearerAuth scheme that's in use
                //options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                //{
                //    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                //    Name = "Authorization",
                //    In = "header",
                //    Type = "apiKey"
                //});
                //// Assign scope requirements to operations based on AuthorizeAttribute
                //options.OperationFilter<SecurityRequirementsOperationFilter>();
            });


            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            //var serviceProvider = new AutofacServiceProvider(container.Build());
            ////GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(serviceProvider));
            //return serviceProvider;//new AutofacServiceProvider(container.Build());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseStaticFiles();
            // Autoregister using server.Features (does not work in reverse proxy mode)
            //app.UseConsulRegisterService();

            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "DataManagement API V1");
            }); // URL: /swagger
        }
    }
}
