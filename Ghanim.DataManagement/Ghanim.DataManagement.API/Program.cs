﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Options;
//using Utilities.Utilites.SerilogExtensions;
//using Ghanim.DataManagement.EFCore.MSSQL;
//using Ghanim.DataManagement.EFCore.MSSQL.Context;

//namespace Ghanim.DataManagement.API
//{
//    public class Program
//    {
//        public static void Main(string[] args)
//        {
//            BuildWebHost(args)
//               .MigrateDbContext<LookUpDbContext>((context, services) =>
//               {
//                   var configuration = services.GetService<IConfiguration>();

//                   //new ConfigurationDbContextSeed()
//                   //    .SeedAsync(context, configuration)
//                   //    .Wait();
//               }).Run();
//        }

//        public static IWebHost BuildWebHost(string[] args) =>
//             WebHost.CreateDefaultBuilder(args)
//                .UseKestrel()
//                .UseContentRoot(Directory.GetCurrentDirectory())
//                .UseIISIntegration()
//                .UseStartup<Startup>()
//                .UseSetting("detailedErrors", "true")
//                .CaptureStartupErrors(true)
//                //.UseSerilog()
//                .Build();
//    }
//}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Utilities.Utilites.SerilogExtensions;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.API;

namespace Sprintoo.DataManagement.API
{
    public class Program
    {

        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.With<CustomExceptionEnricher>()
                .MinimumLevel.Error()
                .Enrich.FromLogContext()
                .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                .CreateLogger();

            BuildWebHost(args)
                .MigrateDbContext<LookUpDbContext>((context, services) =>
                {
                    
                    var env = services.GetService<IHostingEnvironment>();
                    var logger = services.GetService<ILogger<LookUpDbContextSeed>>();
                    //var settings = services.GetService<IOptions<InventoryAppSettings>>();
                    new LookUpDbContextSeed()
                        .SeedAsync(context, env, logger)
                        .Wait();
                }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .UseSerilog()
              .Build();
    }
}
