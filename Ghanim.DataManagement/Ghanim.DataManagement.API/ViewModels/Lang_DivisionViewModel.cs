﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class Lang_DivisionViewModel : RepoistryBaseEntity
    {
        public int FK_Division_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public Division Division { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
