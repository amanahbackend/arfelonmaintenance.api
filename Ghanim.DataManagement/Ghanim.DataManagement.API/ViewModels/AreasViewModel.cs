﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.API.ViewModels
{
  public class AreasViewModel : RepoistryBaseEntity
    {
       public int Area_No { get; set; }
       public string Name { get; set; }
       public int? Fk_Governorate_Id { get; set; }
    }
}
