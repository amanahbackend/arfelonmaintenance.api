﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class LanguagesDictionaryViewModel : RepoistryBaseEntity
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
