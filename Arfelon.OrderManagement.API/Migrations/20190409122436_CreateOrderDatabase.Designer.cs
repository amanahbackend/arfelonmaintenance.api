﻿// <auto-generated />
using System;
using Arfelon.OrderManagement.BLL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Arfelon.OrderManagement.API.Migrations
{
    [DbContext(typeof(ArfelonOrderManagementDbContext))]
    [Migration("20190409122436_CreateOrderDatabase")]
    partial class CreateOrderDatabase
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.3-servicing-35854")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ApartmentNo");

                    b.Property<string>("Area")
                        .HasMaxLength(20);

                    b.Property<string>("AreaDesc")
                        .HasMaxLength(20);

                    b.Property<string>("Block")
                        .HasMaxLength(20);

                    b.Property<string>("BuildingType")
                        .HasMaxLength(20);

                    b.Property<string>("CompanyCode")
                        .HasMaxLength(20);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<int?>("CustomerId");

                    b.Property<int?>("Floor");

                    b.Property<string>("House")
                        .HasMaxLength(10);

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<string>("OrderDescription")
                        .HasMaxLength(250);

                    b.Property<int?>("OrderDivisionId");

                    b.Property<string>("OrderNo")
                        .HasMaxLength(20);

                    b.Property<string>("OrderNotes")
                        .HasMaxLength(200);

                    b.Property<int?>("OrderPriorityId");

                    b.Property<int?>("OrderStatusId");

                    b.Property<int?>("OrderTypeId");

                    b.Property<string>("PACI")
                        .HasMaxLength(10);

                    b.Property<int?>("RowStatusId");

                    b.Property<string>("Street")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.HasIndex("OrderDivisionId");

                    b.HasIndex("OrderPriorityId");

                    b.HasIndex("OrderStatusId");

                    b.HasIndex("OrderTypeId");

                    b.ToTable("Order","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderDivision", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code")
                        .HasMaxLength(20);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<int?>("RowStatusId");

                    b.HasKey("Id");

                    b.ToTable("OrderDivision","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderPriority", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code")
                        .HasMaxLength(20);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<int?>("RowStatusId");

                    b.HasKey("Id");

                    b.ToTable("OrderPriority","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderProblem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code")
                        .HasMaxLength(20);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<int?>("RowStatusId");

                    b.HasKey("Id");

                    b.ToTable("OrderProblem","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderProblems", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<int?>("OrderId");

                    b.Property<int?>("OrderProblemId");

                    b.Property<int?>("RowStatusId");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("OrderProblemId");

                    b.ToTable("OrderProblems","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code")
                        .HasMaxLength(20);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<int?>("RowStatusId");

                    b.HasKey("Id");

                    b.ToTable("OrderStatus","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code")
                        .HasMaxLength(20);

                    b.Property<string>("CreatedByUserId")
                        .HasMaxLength(36);

                    b.Property<DateTime?>("CreationDate");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<DateTime?>("ModificationDate");

                    b.Property<string>("ModifiedByUserId")
                        .HasMaxLength(36);

                    b.Property<int?>("RowStatusId");

                    b.HasKey("Id");

                    b.ToTable("OrderType","orderMgt");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.Order", b =>
                {
                    b.HasOne("Arfelon.OrderManagement.BLL.Orders.OrderDivision", "OrderDivision")
                        .WithMany()
                        .HasForeignKey("OrderDivisionId");

                    b.HasOne("Arfelon.OrderManagement.BLL.Orders.OrderPriority", "OrderPriority")
                        .WithMany()
                        .HasForeignKey("OrderPriorityId");

                    b.HasOne("Arfelon.OrderManagement.BLL.Orders.OrderType", "OrderStatus")
                        .WithMany()
                        .HasForeignKey("OrderStatusId");

                    b.HasOne("Arfelon.OrderManagement.BLL.Orders.OrderType", "OrderType")
                        .WithMany()
                        .HasForeignKey("OrderTypeId");
                });

            modelBuilder.Entity("Arfelon.OrderManagement.BLL.Orders.OrderProblems", b =>
                {
                    b.HasOne("Arfelon.OrderManagement.BLL.Orders.Order")
                        .WithMany("OrderProblems")
                        .HasForeignKey("OrderId");

                    b.HasOne("Arfelon.OrderManagement.BLL.Orders.OrderProblem", "OrderProblem")
                        .WithMany()
                        .HasForeignKey("OrderProblemId");
                });
#pragma warning restore 612, 618
        }
    }
}
