﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arfelon.OrderManagement.API.Migrations
{
    public partial class CreateOrderDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "orderMgt");

            migrationBuilder.CreateTable(
                name: "OrderDivision",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDivision", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderPriority",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPriority", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderProblem",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProblem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderStatus",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderType",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    OrderNo = table.Column<string>(maxLength: 20, nullable: true),
                    OrderDescription = table.Column<string>(maxLength: 250, nullable: true),
                    CustomerId = table.Column<int>(nullable: true),
                    OrderTypeId = table.Column<int>(nullable: true),
                    CompanyCode = table.Column<string>(maxLength: 20, nullable: true),
                    OrderStatusId = table.Column<int>(nullable: true),
                    OrderDivisionId = table.Column<int>(nullable: true),
                    OrderPriorityId = table.Column<int>(nullable: true),
                    OrderNotes = table.Column<string>(maxLength: 200, nullable: true),
                    PACI = table.Column<string>(maxLength: 10, nullable: true),
                    BuildingType = table.Column<string>(maxLength: 20, nullable: true),
                    Area = table.Column<string>(maxLength: 20, nullable: true),
                    AreaDesc = table.Column<string>(maxLength: 20, nullable: true),
                    Block = table.Column<string>(maxLength: 20, nullable: true),
                    Street = table.Column<string>(maxLength: 100, nullable: true),
                    House = table.Column<string>(maxLength: 10, nullable: true),
                    Floor = table.Column<int>(nullable: true),
                    ApartmentNo = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_OrderDivision_OrderDivisionId",
                        column: x => x.OrderDivisionId,
                        principalSchema: "orderMgt",
                        principalTable: "OrderDivision",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order_OrderPriority_OrderPriorityId",
                        column: x => x.OrderPriorityId,
                        principalSchema: "orderMgt",
                        principalTable: "OrderPriority",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order_OrderType_OrderStatusId",
                        column: x => x.OrderStatusId,
                        principalSchema: "orderMgt",
                        principalTable: "OrderType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order_OrderType_OrderTypeId",
                        column: x => x.OrderTypeId,
                        principalSchema: "orderMgt",
                        principalTable: "OrderType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderProblems",
                schema: "orderMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    OrderId = table.Column<int>(nullable: true),
                    OrderProblemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProblems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderProblems_Order_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "orderMgt",
                        principalTable: "Order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderProblems_OrderProblem_OrderProblemId",
                        column: x => x.OrderProblemId,
                        principalSchema: "orderMgt",
                        principalTable: "OrderProblem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Order_OrderDivisionId",
                schema: "orderMgt",
                table: "Order",
                column: "OrderDivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_OrderPriorityId",
                schema: "orderMgt",
                table: "Order",
                column: "OrderPriorityId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_OrderStatusId",
                schema: "orderMgt",
                table: "Order",
                column: "OrderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_OrderTypeId",
                schema: "orderMgt",
                table: "Order",
                column: "OrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProblems_OrderId",
                schema: "orderMgt",
                table: "OrderProblems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProblems_OrderProblemId",
                schema: "orderMgt",
                table: "OrderProblems",
                column: "OrderProblemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderProblems",
                schema: "orderMgt");

            migrationBuilder.DropTable(
                name: "OrderStatus",
                schema: "orderMgt");

            migrationBuilder.DropTable(
                name: "Order",
                schema: "orderMgt");

            migrationBuilder.DropTable(
                name: "OrderProblem",
                schema: "orderMgt");

            migrationBuilder.DropTable(
                name: "OrderDivision",
                schema: "orderMgt");

            migrationBuilder.DropTable(
                name: "OrderPriority",
                schema: "orderMgt");

            migrationBuilder.DropTable(
                name: "OrderType",
                schema: "orderMgt");
        }
    }
}
