﻿using Arfelon.CustomerManagement.API.Models.Customers;
using Arfelon.CustomerManagement.BLL.Customers;
using AutoMapper;

namespace Arfelon.CustomerManagement.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerViewModel>();
        }
    }
}
