using System;
using Amanah.Microservices.CommonLib;
using Arfelon.CustomerManagement.API.Models;

namespace Arfelon.CustomerManagement.API.Models
{
    public class ResponseResult<T>
    {
        public T Data { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// 1: Success; 0: Unknown; Negative values are handled.
        /// </summary>
        public int StatusCode { get; set; }

        public bool IsSucceeded => StatusCode == 1;

        public ResponseResult()
        {
            StatusCode = 1;
        }
    }
}

public static class Util
{
    public static void PrepareEntityForAdding(this BaseEntity baseEntity)
    {
        baseEntity.CreationDate = DateTime.Now;
    }

    public static void PrepareEntityForEditing(this BaseEntity baseEntity, BaseViewModel baseViewModel)
    {
        baseEntity.RowStatusId = baseViewModel.RowStatusId;
        baseEntity.ModifiedByUserId = baseViewModel.ModifiedByUserId;
        baseEntity.ModificationDate = DateTime.Now;
    }

    public static void PrepareEntityForDeleting(this BaseEntity baseEntity, string deletedByUserId)
    {
        baseEntity.RowStatusId = (int)EnumRowStatus.Deleted;
        baseEntity.ModifiedByUserId = deletedByUserId;
        baseEntity.ModificationDate = DateTime.Now;
    }
}