﻿namespace Arfelon.CustomerManagement.API.Models.Customers
{
    public class CustomerViewModel : BaseViewModel
    {
        public string Code { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string ThirdName { get; set; }

        public string FourthName { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
    }
}
