﻿using Arfelon.CustomerManagement.BLL;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Arfelon.CustomerManagement.API.Controllers
{
    public class BaseController : Controller
    {
        protected readonly UnitOfWork UnitOfWork;
        protected readonly IMapper Mapper;
        protected readonly IHostingEnvironment HostingEnvironment;

        public BaseController(UnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }

        public BaseController(UnitOfWork unitOfWork, IMapper mapper, IHostingEnvironment hostingEnvironment)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
            HostingEnvironment = hostingEnvironment;
        }
    }
}
