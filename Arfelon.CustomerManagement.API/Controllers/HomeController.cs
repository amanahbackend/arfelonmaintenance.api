﻿using Microsoft.AspNetCore.Mvc;

namespace Arfelon.CustomerManagement.API.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
