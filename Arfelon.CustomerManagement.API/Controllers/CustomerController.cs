﻿using System;
using System.Collections.Generic;
using System.Linq;
using Arfelon.CustomerManagement.API.Models;
using Arfelon.CustomerManagement.API.Models.Customers;
using Arfelon.CustomerManagement.BLL;
using Arfelon.CustomerManagement.BLL.Customers;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Arfelon.CustomerManagement.API.Controllers
{
    [Route("api/Customer")]
    [Produces("application/json")]
    public class CustomerController : BaseController
    {
        public CustomerController(UnitOfWork context, IMapper mapper) : base(context, mapper)
        {
        }


        [HttpGet, Route("Get")]
        public ResponseResult<List<CustomerViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<CustomerViewModel>>();

            try
            {
                List<Customer> lstCustomers = UnitOfWork.Customers.Find(obj => obj.RowStatusId != -1).ToList();
                var lstCustomerVm = Mapper.Map<List<CustomerViewModel>>(lstCustomers);

                responseResult.Data = lstCustomerVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [Route("Get/{id}")]
        public ResponseResult<CustomerViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<CustomerViewModel>();

            try
            {
                Customer customer = UnitOfWork.Customers.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                CustomerViewModel customerVm = Mapper.Map<CustomerViewModel>(customer);
                responseResult.Data = customerVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("SearchByWord")]
        public ResponseResult<List<CustomerViewModel>> SearchByWord(string keyword)
        {
            var responseResult = new ResponseResult<List<CustomerViewModel>>();

            try
            {
                List<Customer> lstCustomers = UnitOfWork.Customers.GetAll().ToList();
                var lstCustomersVm = Mapper.Map<List<CustomerViewModel>>(lstCustomers);

                responseResult.Data = lstCustomersVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<CustomerViewModel> Add([FromBody] CustomerViewModel model)
        {
            var responseResult = new ResponseResult<CustomerViewModel>();

            try
            {
                Customer customer = Mapper.Map<Customer>(model);
                customer.PrepareEntityForAdding();

                UnitOfWork.Customers.Add(customer);

                // Re-map the added Customer again to update Id.
                var customerVm = Mapper.Map<CustomerViewModel>(customer);
                responseResult.Data = customerVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<CustomerViewModel> Update([FromBody] CustomerViewModel model)
        {
            var responseResult = new ResponseResult<CustomerViewModel>();

            try
            {
                Customer customer = Mapper.Map<Customer>(model);
                customer.PrepareEntityForEditing(model);

                UnitOfWork.Customers.Update(customer);

                var customerVm = Mapper.Map<CustomerViewModel>(customer);
                responseResult.Data = customerVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Customer customer = UnitOfWork.Customers.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                customer.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.Customers.Update(customer);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}