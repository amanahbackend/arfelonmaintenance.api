﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arfelon.CustomerManagement.API.Migrations
{
    public partial class CreateCustomerDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "custMgt");

            migrationBuilder.CreateTable(
                name: "Customer",
                schema: "custMgt",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    FirstName = table.Column<string>(maxLength: 20, nullable: true),
                    SecondName = table.Column<string>(maxLength: 20, nullable: true),
                    ThirdName = table.Column<string>(maxLength: 20, nullable: true),
                    FourthName = table.Column<string>(maxLength: 20, nullable: true),
                    Phone1 = table.Column<string>(maxLength: 15, nullable: true),
                    Phone2 = table.Column<string>(maxLength: 15, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customer",
                schema: "custMgt");
        }
    }
}
